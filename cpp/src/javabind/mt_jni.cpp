#include "org_cytoscape_gedevo_GedevoNative.h"
#include "Threading.h"
#include "mt_jni.h"

/*
private static java.lang.Thread startThread(long, java.lang.String, long);
Signature: (Ljava/lang/String;JJ)Ljava/lang/Thread;

private static void joinThread(java.lang.Thread);
Signature: (Ljava/lang/Thread;)V

private static org.cytoscape.gedevo.GedevoNative$Waitable createWaitable();
Signature: ()Lorg/cytoscape/gedevo/GedevoNative$Waitable;

private static void lockWaitable(org.cytoscape.gedevo.GedevoNative$Waitable);
Signature: (Lorg/cytoscape/gedevo/GedevoNative$Waitable;)V

private static void unlockWaitable(org.cytoscape.gedevo.GedevoNative$Waitable);
Signature: (Lorg/cytoscape/gedevo/GedevoNative$Waitable;)V

private static void waitWaitable(org.cytoscape.gedevo.GedevoNative$Waitable);
Signature: (Lorg/cytoscape/gedevo/GedevoNative$Waitable;)V

private static void signalWaitable(org.cytoscape.gedevo.GedevoNative$Waitable);
Signature: (Lorg/cytoscape/gedevo/GedevoNative$Waitable;)V

private static void broadcastWaitable(org.cytoscape.gedevo.GedevoNative$Waitable);
Signature: (Lorg/cytoscape/gedevo/GedevoNative$Waitable;)V
*/

static jmethodID mid_startThread = 0;
static jmethodID mid_joinThread = 0;
static jmethodID mid_createWaitable = 0;
static jmethodID mid_lockWaitable = 0;
static jmethodID mid_unlockWaitable = 0;
static jmethodID mid_waitWaitable = 0;
static jmethodID mid_signalWaitable = 0;
static jmethodID mid_broadcastWaitable = 0;
static jmethodID mid_countCPUCores = 0;
static jclass s_cls = NULL;
static JavaVM *s_jvm = NULL;
static int s_jvmVer = 0;

#define JENV(var) ASSERT(s_jvm); JNIEnv *var = NULL; s_jvm->GetEnv((void**)&var, s_jvmVer); ASSERT(var);

template <typename T> jobject jobject_cast(const T& p)
{
	ASSERT(!!p);
	return reinterpret_cast<jobject>(p);
}

static void *thcreate(const char *name, void (*body)(void*), void *user)
{
	JENV(jj);
	jlong jbody = reinterpret_cast<jlong>(body);
	jlong juser = reinterpret_cast<jlong>(user);
	jstring jname = jj->NewStringUTF(name);
	jvalue args[3];
	args[0].l = jname;
	args[1].j = jbody;
	args[2].j = juser;
	jobject jth = jj->CallStaticObjectMethodA(s_cls, mid_startThread, &args[0]);
	ASSERT(!jj->ExceptionCheck());
	return jj->NewGlobalRef(jth);
}

static void thjoin(void *th)
{
	JENV(jj);
	jobject jth = jobject_cast(th);
	jj->CallStaticVoidMethod(s_cls, mid_joinThread, jth);
	jj->DeleteGlobalRef(jth);
	ASSERT(!jj->ExceptionCheck());
}

static void *wcreate()
{
	JENV(jj);
	jobject jobj = jj->CallStaticObjectMethod(s_cls, mid_createWaitable);
	ASSERT(!jj->ExceptionCheck());
	return jj->NewGlobalRef(jobj);
}

static void wdelete(void *w)
{
	JENV(jj);
	jobject jref = jobject_cast(w);
	jj->DeleteGlobalRef(jref);
	ASSERT(!jj->ExceptionCheck());
}

static void wlock(void *w)
{
	JENV(jj);
	jj->CallStaticVoidMethod(s_cls, mid_lockWaitable, w);
	ASSERT(!jj->ExceptionCheck());
}

static void wunlock(void *w)
{
	JENV(jj);
	jj->CallStaticVoidMethod(s_cls, mid_unlockWaitable, w);
	ASSERT(!jj->ExceptionCheck());
}

static void wwait(void *w)
{
	JENV(jj);
	jj->CallStaticVoidMethod(s_cls, mid_waitWaitable, w);
	ASSERT(!jj->ExceptionCheck());
}

static void wsignal(void *w)
{
	JENV(jj);
	jj->CallStaticVoidMethod(s_cls, mid_signalWaitable, w);
	ASSERT(!jj->ExceptionCheck());
}

static void wbroadcast(void *w)
{
	JENV(jj);
	jj->CallStaticVoidMethod(s_cls, mid_broadcastWaitable, w);
	ASSERT(!jj->ExceptionCheck());
}

static unsigned countCPUCores()
{
	JENV(jj);
	jint c = jj->CallStaticIntMethod(s_cls, mid_countCPUCores);
	ASSERT(!jj->ExceptionCheck());
	if(c < 1)
		c = 1;
	return (unsigned)c;
}

// external interface
bool InitMT(void *ppass)
{
	if(!ppass)
		return false;

	jni_pass *pass = static_cast<jni_pass*>(ppass);
	JNIEnv *jj = pass->jj;
	jclass cls = pass->cls;
	if(!(jj && cls))
		return false;
	
	mid_startThread = jj->GetStaticMethodID(cls, "startThread", "(Ljava/lang/String;JJ)Ljava/lang/Thread;");
	mid_joinThread = jj->GetStaticMethodID(cls, "joinThread", "(Ljava/lang/Thread;)V");

	mid_createWaitable = jj->GetStaticMethodID(cls, "createWaitable", "()Lorg/cytoscape/gedevo/GedevoNative$Waitable;");

#define W(name) mid_##name = jj->GetStaticMethodID(cls, #name, "(Lorg/cytoscape/gedevo/GedevoNative$Waitable;)V")
	W(lockWaitable);
	W(unlockWaitable);
	W(waitWaitable);
	W(signalWaitable);
	W(broadcastWaitable);
#undef W

	mid_countCPUCores = jj->GetStaticMethodID(cls, "countCPUCores", "()I");

	if(!mid_startThread || !mid_joinThread || !mid_createWaitable || !mid_lockWaitable
		|| !mid_unlockWaitable || !mid_waitWaitable || !mid_signalWaitable || !mid_broadcastWaitable)
	{
		return false;
	}

	bool good = threading::Init(thcreate, thjoin, wcreate, wdelete, wlock, wunlock, wwait, wsignal, wbroadcast, countCPUCores);

	if(good)
	{
		jj->GetJavaVM(&s_jvm);
		s_jvmVer = jj->GetVersion();
		ASSERT(s_jvmVer);
		s_cls = (jclass)jj->NewGlobalRef(cls);
		ASSERT(!jj->ExceptionCheck());
	}

	return good;
}

void QuitMT()
{
	if(!s_jvm)
		return;
	mid_startThread = 0;
	mid_joinThread = 0;
	mid_createWaitable = 0;
	mid_lockWaitable = 0;
	mid_unlockWaitable = 0;
	mid_waitWaitable = 0;
	mid_signalWaitable = 0;
	mid_broadcastWaitable = 0;
	mid_countCPUCores = 0;
	JENV(jj);
	jj->DeleteGlobalRef(s_cls);
	s_jvm = NULL;
	s_cls = NULL;
	threading::Quit();
}
