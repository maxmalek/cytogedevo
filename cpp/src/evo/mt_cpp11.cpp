#include "Threading.h"
#include "cpucount.h"
#include <thread>
#include <mutex>
#include <condition_variable>

class Cpp11Waitable
{
public:
	Cpp11Waitable()
	{
	}

	~Cpp11Waitable()
	{
	}

    void lock()
	{
		_mtx.lock();
	}

    void unlock()
	{
		_mtx.unlock();
	}

    void wait()
	{
		_cond.wait(_mtx);
	}

    void signal()
	{
		_cond.notify_one();
	}

    void broadcast()
	{
		_cond.notify_all();
	}

private:
	std::recursive_mutex _mtx;
	std::condition_variable_any _cond;
};



static void *thcreate(const char *name, void (*body)(void*), void *user)
{
    std::thread *th = new std::thread(body, user);
    return th;
}

static void thjoin(void *pth)
{
	std::thread *th = static_cast<std::thread*>(pth);
	th->join();
	delete th;
}

static void *wcreate()
{
	return new Cpp11Waitable;
}

static void wdelete(void *w)
{
	delete static_cast<Cpp11Waitable*>(w);
}

static void wlock(void *w)
{
	static_cast<Cpp11Waitable*>(w)->lock();
}

static void wunlock(void *w)
{
	static_cast<Cpp11Waitable*>(w)->unlock();
}

static void wwait(void *w)
{
	static_cast<Cpp11Waitable*>(w)->wait();
}

static void wsignal(void *w)
{
	static_cast<Cpp11Waitable*>(w)->signal();
}

static void wbroadcast(void *w)
{
	static_cast<Cpp11Waitable*>(w)->broadcast();
}


// external interface
bool InitMT(void*)
{
	return threading::Init(thcreate, thjoin, wcreate, wdelete, wlock, wunlock, wwait, wsignal, wbroadcast, countCPUCores);
}

void QuitMT()
{
	threading::Quit();
}

