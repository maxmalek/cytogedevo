#ifndef AGENT_H
#define AGENT_H

#include "common.h"
#include <vector>
#include "NodeMapping.h"
#include "GEDScore.h"
#include "Matrix.h"

class AgentMgr;
class UserSettings;
class DataHolder;


struct PairInfo
{
	PairInfo() {}
	PairInfo(score sc, uint i) : scor(sc), index(i) {}
	score scor;
	uint index;
};

class Agent
{
private:
	Agent(const Agent& other); // Should not be used (Undefined - will lead to linker error)

public:
	Agent(const DataHolder& datah, const NodeMapping& m);

	const DataHolder& GetDataHolder() const { return _dataH; }

	void copyFrom(const Agent& other);

	// Create one agent inheriting features from up to 255 parents,
	// inheriting features determined by mask.
	// Mask is used as: for(i=0; i<masksize; ++i) { copy from parents[mask[i]] }
	// EXTREME CAUTION WHEN USING THIS - IT WILL MESS UP!!
	// Things need to be fixed with _setMappingPos() afterwards, or everything will break.
	void octopusMerge(const Agent * const * parents, unsigned char *mask, uint masksize);

	inline score getScore() const // Overall score, used as "fitness". Lower is better.
	{
		//ASSERT(locked);
		if(_dirtyFinalScore)
			_CalcFinalScore();

		return _scoreFinal;
	}

	score getEdgeCorrectness() const;

	score getPairSumScore() const; // Average of all pairs
	score getGEDScore() const; // GED score of this agent's mapping - scaled by worst GED -> always in [0...1]
	score getCombinedFinalScore() const;

	// -- actual GED, unscaled --
	inline uint getGEDRawFull() const // full changes: added and removed
	{
		uint ret = GEACC_ADDED(_rawGED) + GEACC_REMOVED(_rawGED);
		ASSERT((ret & 1) == 0);
		return ret >> 1;
	}
	inline uint getGEDRawPartial() const // partial changes: flipped, direction changed
	{
		uint ret = GEACC_FLIP(_rawGED) + GEACC_D_TO_U(_rawGED) + GEACC_U_TO_D(_rawGED);
		ASSERT((ret & 1) == 0);
		return ret >> 1;
	}
	inline uint getGEDRawSubstituted() const // substituted: edges match perfectly
	{
		uint ret = GEACC_SUBST(_rawGED);
		ASSERT((ret & 1) == 0);
		return ret >> 1;
	}

	score getPairScore(uint i) const; // Score for pair i in the mapping. Lower is better.
	void getPairGED(uint i, GEDInfo& pair) const; // Partial edit distance for this pair (= # of added/removed edges).
	score getPairGEDScore(uint i) const;

	inline void incrLife() { ++_life; }
	inline uint getLife() const { return _life; }
	inline void resetLife() { _life = 0; }

	inline float getHealth() const { return _health; }
	inline void decrHealth(float by) { _health -= by; }
	inline void setHealth(float h) { _health = h; }
	inline bool isAlive() const { return _health > 0;}

	// This is true if the mapping was modified and some of the cached values are no longer valid, and need to be recalculated.
	inline bool isDirty() const { return _dirtyPairSumScore || _dirtyGED || _dirtyRawGED || _dirtyFinalScore; }

	// Flush all cached values and rebuild index vector
	void makeDirty();

	// Returns the variable vector, with a node ordering unique to this agent.
	inline const NodeMapping& getMapping() const { return _mapping; }
	inline NodeMapping& getMappingNonConst() { return _mapping; } // <- after messing with this, don't forget to makeDirty() !

	// Like above, but this is the *sorted* mapping that never changes.
	inline const NodeMapping& getReferenceMapping() const { return _refMapping;}

	// DANGEROUS - use only to fix up values left out by octopusMerge()
	void _setMappingPos(uint pos, const Node *node);


	inline void swapTemp(const size_t a, const size_t b)
	{
		ASSERT(!locked);
		ASSERT(!_mapping.node[a] || _mapping.node[a]->prematched < 0);
		ASSERT(!_mapping.node[b] || _mapping.node[b]->prematched < 0);
		_mapping.swap(a, b);
		dirtyTablePairs[a] = 1;
		dirtyTablePairs[b] = 1;
		if(const uint id = _mapping.nodeId[a])
			_indexv[id-1] = a;
		if(const uint id = _mapping.nodeId[b])
			_indexv[id-1] = b;
	}

	// Swaps two indices in the mapping, and sets the corresponding locations to dirty, so that they will be recalculated.
	void swap(const size_t a, const size_t b);

	void completePartialMapping(); // for importing. adds missing nodes to a partial mapping.
	bool sanityCheck() const; // for debugging and state loading only
	uint getDistanceTo(const Agent *other) const; // calculate amount of mismatches in mapping between 2 agents

	typedef score (Agent::* const PairwiseScoringMethod)(uint) const;

	// Fills p with infos about the n worst pairs.
	// p must be an array of length n.
	// On return, it is not sorted.
	// Does not consider prematched pairs.
	void getWorstPairs(PairInfo *p, uint n, PairwiseScoringMethod mth) const;


	bool locked; // if true, Agent is not supposed to be changed (debugging aid)

protected:

	void _UpdateMappingGED() const; // not really const, but used by some const methods
	void _CalcFinalScore() const;

	const DataHolder& _dataH;

    // If this is true, something was changed and the score needs to be updated.
    // dirtyTable further specifies which entries to update.
    // (affects _scoreGED and _scorePairSum)
    mutable bool _dirtyFinalScore : 1;
    mutable bool _dirtyGED : 1;
    mutable bool _dirtyPairSumScore : 1;
    mutable bool _dirtyRawGED : 1;

	mutable score _scoreGED; // cached value
	mutable score _scorePairSum; // cached value
	mutable score _scoreFinal; // final score
	float _health;
	uint _life; // number of rounds alive
	mutable GEDInfo _rawGED;

	NodeMapping _mapping; // this is what we're trying to swap around so it fits to _refMapping as good as possible
	const NodeMapping &_refMapping; // ptr to object stored in AgentMgr

	mutable std::vector<score> pairScoreTable; // per-pair BLAST/Graphlet score cache ("pairscore")
	mutable std::vector<char> dirtyTablePairs; // like above

	std::vector<uint> _indexv; // Index vector for faster GED calculation

	const UserSettings& _settings;
};

// Comparator for std::sort, that sorts Agent* by score (lower first).
struct AgentPtrCmp
{
	inline bool operator() (const Agent *a, const Agent *b) const
	{
		return a->getScore() < b->getScore();
	}
};

// Comparator for std::sort, that sorts Agent* by score AND hash (lower first).
// Used for fast duplicate removal.
struct AgentPtrHashCmp
{
	inline bool operator() (const Agent *a, const Agent *b) const
	{
		const score sa = a->getScore();
		const score sb = b->getScore();
		return sa < sb || (sa == sb && a->getMapping() < b->getMapping());
	}
};



#endif

