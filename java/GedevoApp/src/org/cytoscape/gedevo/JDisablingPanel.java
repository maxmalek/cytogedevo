package org.cytoscape.gedevo;

import javax.swing.*;
import java.awt.*;

// from https://stackoverflow.com/questions/2471597/disable-jpanel-with-visual-effect
public class JDisablingPanel extends JPanel
{
    @Override
    public void setEnabled(boolean enabled)
    {
        super.setEnabled(enabled);
        this.setEnabledRecursive(this, enabled);
    }

    protected void setEnabledRecursive(Component component, boolean enabled)
    {
        if (component instanceof Container)
        {
            for (Component child : ((Container) component).getComponents())
            {
                child.setEnabled(enabled);

                if (!(child instanceof JDisablingPanel))
                {
                    setEnabledRecursive(child, enabled);
                }
            }
        }
    }
}
