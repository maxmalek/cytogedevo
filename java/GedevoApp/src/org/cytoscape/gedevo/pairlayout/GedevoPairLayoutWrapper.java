package org.cytoscape.gedevo.pairlayout;

import org.cytoscape.model.CyNode;
import org.cytoscape.view.layout.AbstractLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.undo.UndoSupport;

import java.util.Set;

public class GedevoPairLayoutWrapper extends AbstractLayoutAlgorithm
{
    private CyLayoutAlgorithm algo;
    private IPairLayout.Factory pairfactory;

    public GedevoPairLayoutWrapper(CyLayoutAlgorithm algo, IPairLayout.Factory pairfactory, UndoSupport undo)
    {
        super("pair-" + algo.getName(), algo.toString(), undo);
        this.algo = algo;
        this.pairfactory = pairfactory;
    }

    public final CyLayoutAlgorithm getUnderlyingLayout()
    {
        return algo;
    }

    public final IPairLayout.Factory getPairLayoutFactory()
    {
        return pairfactory;
    }

    @Override
    public TaskIterator createTaskIterator(final CyNetworkView cyview, Object o, Set<View<CyNode>> views, String s)
    {
        final PairLayoutTask pairTask = new PairLayoutTask(cyview, algo, o, views, s, undoSupport);
        final IPairLayout pairlayout = pairfactory.create(pairTask);
        TaskIterator it = new TaskIterator();
        it.append(pairTask);
        it.append(
                new Task()
                {
                    @Override
                    public void run(TaskMonitor taskMonitor) throws Exception
                    {
                        pairlayout.apply(cyview);
                    }

                    @Override
                    public void cancel() {}
                }
        );
        it.append(new Task()
        {
            @Override
            public void run(TaskMonitor taskMonitor) throws Exception
            {
                cyview.updateView();
                cyview.fitContent();
            }

            @Override
            public void cancel() {}
        });
        return it;
    }

    @Override
    public Object createLayoutContext()
    {
        return algo.createLayoutContext();
    }

    @Override
    public Set<Class<?>> getSupportedNodeAttributeTypes()
    {
        return algo.getSupportedNodeAttributeTypes();
    }

    @Override
    public Set<Class<?>> getSupportedEdgeAttributeTypes()
    {
        return algo.getSupportedEdgeAttributeTypes();
    }

    @Override
    public boolean getSupportsSelectedOnly()
    {
        return algo.getSupportsSelectedOnly();
    }

    public boolean isReady(CyNetworkView view, Object tunableContext, Set<View<CyNode>> nodesToLayout, String attributeName)
    {
        return super.isReady(view, tunableContext, nodesToLayout, attributeName)
            && algo.isReady(view, tunableContext, nodesToLayout, attributeName)
            && PairLayoutTask.isReady(view);
    }

}
