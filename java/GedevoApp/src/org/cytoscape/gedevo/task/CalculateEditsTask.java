package org.cytoscape.gedevo.task;


import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.gedevo.GedevoFilters;
import org.cytoscape.gedevo.integration.ccs.EditType;
import org.cytoscape.gedevo.util.CyNodePair;
import org.cytoscape.model.*;
import org.cytoscape.work.TaskMonitor;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CalculateEditsTask extends AbstractAlignmentTask
{
    private boolean undirected;
    private CyNetwork cynetOverride;

    public CalculateEditsTask(AlignmentTaskData common)
    {
        super(common);
        this.undirected = common.settings.forceUndirectedEdges;
        this.cynetOverride = null;
    }

    public CalculateEditsTask(CyNetwork cynet, boolean undirected)
    {
        super(null);
        this.undirected = undirected;
        this.cynetOverride = cynet;
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        final CyNetwork cynet = (common != null && common.cynet != null) ? common.cynet : cynetOverride;
        Collection<CyNodePair> pairs = GedevoAlignmentUtil.getAlignedNodePairs(cynet, true);
        Map<CyIdentifiable, EditType> edits = new HashMap<CyIdentifiable, EditType>();
        Map<CyNode, CyNode> partners = new HashMap<CyNode, CyNode>();

        // fill partners map so that it always contains both directions if a, b  are valid,
        // and only one direction otherwise (making sure that a (= the key) is always valid)
        for(CyNodePair p : pairs)
        {
            if(p.a != null)
            {
                partners.put(p.a, p.b);
                edits.put(p.a, p.b != null ? EditType.SUBSTITUTED : EditType.DELETED);
            }
            if(p.b != null)
            {
                partners.put(p.b, p.a);
                edits.put(p.b, p.a != null ? EditType.SUBSTITUTED : EditType.ADDED);
            }
        }

        // NOTE: This does not cope well with multiple edges between nodes!
        // It works, but results can be inaccurate:
        // Any of flipped/undirected/substitured may be selected, depending on which edge is seen first.
        for(CyEdge e : GedevoFilters.getNonMappingEdges(cynet))
        {
            if(cancelled)
                return;

            CyNode a = e.getSource();
            CyNode b = e.getTarget();
            CyNode ap = partners.get(a);
            CyNode bp = partners.get(b);

            boolean hasPartnerEdge = false;

            // are partners also connected by an edge?
            if(ap != null && bp != null)
            {
                for(CyEdge eother : cynet.getConnectingEdgeList(ap, bp, CyEdge.Type.ANY))
                {
                    CyNode ao = eother.getSource();
                    CyNode bo = eother.getTarget();

                    if(ap == ao && bp == bo) // connected the same way
                    {
                        hasPartnerEdge = true;

                        if(e.isDirected() == eother.isDirected())
                            edits.put(e, EditType.SUBSTITUTED);
                        else // undirected became directed or vice versa
                            edits.put(e, EditType.DIRECTION_CHANGED);

                        break;
                    }
                    else if(ap == bo && bp == ao) // connected flipped
                    {
                        hasPartnerEdge = true;

                        if(!e.isDirected() && !eother.isDirected())
                            edits.put(e, EditType.SUBSTITUTED); // undirected anyway; flipping doesn't matter
                        else if (e.isDirected() && eother.isDirected())
                            edits.put(e, EditType.FLIPPED);
                        else
                            edits.put(e, EditType.DIRECTION_CHANGED);

                        break;
                    }
                    else
                        throw new IllegalStateException("Betrayal! CyNetwork::getConnectingEdgeList() lied to us!");
                }
            }

            if(!hasPartnerEdge)
            {
                // if it doesn't have a partner and it's in the first network, it was removed.
                // and if it appears in the 2nd network but wasn't in the first, it was added.
                int srcNetId = cynet.getRow(a).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class); // taking a or b does't matter, same network
                edits.put(e, srcNetId == 0 ? EditType.DELETED : EditType.ADDED);
            }
        }

        if(cancelled)
            return;


        // Write table data

        CyTable tab = cynet.getDefaultNodeTable();
        if (tab.getColumn(ColumnNames.EDIT_TYPE) == null)
            tab.createColumn(ColumnNames.EDIT_TYPE, Integer.class, false, -1);

        tab = cynet.getDefaultEdgeTable();
        if (tab.getColumn(ColumnNames.EDIT_TYPE) == null)
            tab.createColumn(ColumnNames.EDIT_TYPE, Integer.class, false, -1);

        // ~~ Actual data update happens here
        if(undirected)
        {
            for(Map.Entry<CyIdentifiable, EditType> ee : edits.entrySet())
            {
                EditType et = ee.getValue();
                switch (et)
                {
                    // not interested in these
                    case DIRECTION_CHANGED:
                    case FLIPPED:
                        et = EditType.SUBSTITUTED;
                        break;
                }
                cynet.getRow(ee.getKey()).set(ColumnNames.EDIT_TYPE, et.ordinal());
            }
        }
        else
        {
            for (Map.Entry<CyIdentifiable, EditType> ee : edits.entrySet())
                cynet.getRow(ee.getKey()).set(ColumnNames.EDIT_TYPE, ee.getValue().ordinal());
        }
    }


    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
