#!/bin/bash

. config.inc
needlua

if [ ! -d data2 ]; then
    mkdir data2
fi

echo "Shrink GO terms to contain only used data"

# shrink GO terms file so that only GO entries remain that are actually part of the *.nodelist files
$LUAEXE shrinkgos.lua work/uniprot_sprot.gos data2/uniprot_sprot.goshrink work/*.nodelist

# Fix up data files; replace as many DIP-xxxx as possible with their Uniprot accession number

for f in data1/*.el; do
    a="${f%.el}"
    a="${a##*/}"
    f2="data2/$a.elu"
    echo "Creating $f2"
    nf="work/${a}.nodelist"
    $LUAEXE insertuniprot.lua "$nf" "$nf" "$f" "$f2"
done

#for f in data1/*.blastlist; do
#    fbase="${f%.blastlist}"
#    fbase="${fbase##*/}"
#    a=`echo "$fbase" | sed -r 's/([^_]+)_.*/\1/'`
#    b=`echo "$fbase" | sed -r 's/[^_]+_([^_\.])/\1/'`
#    f2="data2/${a}_${b}.blastlistu"
#    echo "Creating $f2"
#    $LUAEXE insertuniprot.lua "work/${a}.nodelist" "work/${b}.nodelist" "$f" "$f2"
#done
