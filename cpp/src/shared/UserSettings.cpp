#include "UserSettings.h"
#include "ProgramState.h"
#include <sstream>
#include "tools.h"



UserSettings::UserSettings()
{
	/* double */    weightPairsum = 1;
	/* double */    weightGraphlets = 0;
	/* double */    weightGED = 0;

	/* double */    pairWeightGraphlets = score(0.5);
	/* double */    pairWeightGED = 1;

	/* float */     basicHealth = 100;
	/* float */     maxHealthDrop = 100;

	// When mapping a valid node against NULL, this is returned for scores where no better value is available.
	// This value controls how "forcefully" the two graphs are aligned, in other words,
	// how high the penalty for mapping a node to NULL is.
	// A value of 1 is the worst possible, so the algorithm will try to prevent NULL<->node pairs at all costs.
	// Lower values will cause exceptionally bad pairs to be considered worse than mapping against NULL,
	// so that such nodes will be preferably counted as inserted or deleted.
	// 0 will produce mappings where no node has a partner.
	// It depends on your goal:
	// To minimize GED, this value should be 1.
	// To maximize biological meaning, set this value lower.
	/* double */    pairNullValue = 1;

	// When loading network edge lists, automatically transform directed edges into undirected ones
	// so that the final network will not have any directed edges.
	/* uint */      forceUndirectedEdges = 0;

	// When both networks to be worked on contain proteins with the same name,
	// assume they are the same proteins and should always be matched to each other.
	/* uint */      matchSameNames = 0;

	// -- For GED calculation/scoring. Graphs are G1 [varGroup] and G2 [!varGroup]

	// Penalty if an edge exists in G1, but not G2
	/* double */    ged_eAdd = 1;

	// Penalty if an edge exists in G2, but not G1
	/* double */    ged_eRm = 1;

	// Penalty if an edge exists in both G1 and G2 in exactly the same direction
	/* double */    ged_eSub = 0;

	// Penalty if a directed edge changed it's direction
	/* double */    ged_eFlip = score(0.8);

	// Penalty if an edge exists as directed edge in G1, but as undirected edge in G2
	/* double */    ged_eD2U = score(0.2);

	// Penalty if an edge exists as undirected edge in G1, but as directed edge in G2
	/* double */    ged_eU2D = score(0.2);

	// Penalty if a node exists in G2, but not G1
	/* double */    ged_nAdd = 0;

	// Penalty if a node exists in G1, but not G2
	/* double */    ged_nRm = 0;

	// -----------

	// group ID that should be mapped to the variable group (0 or 1)
	/* uint */      varGroup = 0;

	// Rough, relative limit of Agents to spawn or keep. // TODO: clarify this
	/* uint */      maxAgents = 400;

	// Use greedy init to create the initial population?
	/* uint */      evo_greedyInitOnInit = 0;

	// Evo's greedy init will only consider scores strictly lower than this as a pair worth keeping,
	// and will otherwise map the node in question to NULL (ie. deletion or insertion).
	/* double */    evo_greedyInitScoreLimit = score(0.4);  // In [0 .. 1]

	// This fraction of agents will be initialized using the greedy method.
	// The rest will be mutated normally.
	// Warning: maxAgents * evo_greedyInitPerRound must be >= 1, otherwise this has no effect
	/* double */    evo_greedyInitPerRound = 0;  // In [0 .. 1]


	// Abort criteria - by default we don't abort
	abort_seconds = 0;
	abort_iterations = 0;
	abort_nochange = 0;

	// Misc settings
	numThreads = 0; // auto-detect unless specified otherwise
	keepWorkfiles = 0; // by default do NOT create auxiliary files such as matrix binary dumps
	autosaveSecs = 0; // do not autosave by default
	logger_iterations = 0;
	trimNames = 0;
	ignoreSelfLoops = 0;
	saveResultsAddTimeStamp = 0;
	saveSeries = 0; // instead of overwriting the autosave, suffix it with a series number

	// Internal stuff
	_finalCalcGED = true;
	_pairCalcGED = true;
	_finalCalcPairsum = true;
	_good = false;
	_pairScaler = -1;
	_finalScaler = -1;

};

template <typename T> static void dumpHelper(const char *var, const T& val, UserSettings::DumpFunc f, void *ud)
{
	std::ostringstream os;
	os << val;
	f(var, os.str(), ud);
}

template <typename T> static bool importHelper(std::string var, T& val, std::string toImportVar, const std::string& toImportVal)
{
	strToLower(toImportVar);
	strToLower(var);
	if(toImportVar != var)
		return false;

	std::stringstream os;
	os << toImportVal;
	os >> val;
	return true;
}

#define ALLSETTINGS \
	D(weightPairsum); \
	D(weightGraphlets); \
	D(weightGED); \
	D(pairWeightGraphlets); \
	D(pairWeightGED); \
	D(basicHealth); \
	D(maxHealthDrop); \
	D(pairNullValue); \
	D(forceUndirectedEdges); \
	D(matchSameNames); \
	D(ged_eAdd); \
	D(ged_eRm); \
	D(ged_eSub); \
	D(ged_eFlip); \
	D(ged_eD2U); \
	D(ged_eU2D); \
	D(ged_nAdd); \
	D(ged_nRm); \
	D(varGroup); \
	D(maxAgents); \
	D(evo_greedyInitOnInit); \
	D(evo_greedyInitScoreLimit); \
	D(evo_greedyInitPerRound); \
	D(abort_seconds); \
	D(abort_iterations); \
	D(abort_nochange); \
	D(numThreads); \
	D(keepWorkfiles); \
	D(autosaveSecs); \
	D(logger_iterations); \
	D(logger_file); \
	D(trimNames); \
	D(ignoreSelfLoops); \
	D(saveResultsFileName); \
	D(saveResultsAddTimeStamp);


void UserSettings::Dump(DumpFunc f, void *ud)
{
	#define D(v) dumpHelper(#v, v, f, ud)
	ALLSETTINGS
	#undef D
}

bool UserSettings::Import(const std::string& var, const std::string& val)
{
	#define D(v) do { if(importHelper(#v, v, var, val)) return true; } while (0)
	ALLSETTINGS
	#undef D
	return false;
}


bool UserSettings::CheckAndApply()
{
	if(evo_greedyInitPerRound > 0 && evo_greedyInitPerRound * maxAgents < 1)
		printf("Settings: WARNING: (evo_greedyInitPerRound * maxAgents) < 1 -- greedy will not be used\n");

	if(!(varGroup == 0 || varGroup == 1))
	{
		printf("Settings: ERROR: varGroup is not 0 or 1\n");
		return false;
	}

	_pairCalcGED = (pairWeightGED > 0);
	_pairScaler = pairWeightGED + pairWeightGraphlets;

	_finalCalcGED = (weightGED > 0);
	_finalCalcPairsum = (weightPairsum > 0);
	_finalCalcGraphlet = (weightGraphlets > 0);
	_finalScaler = weightPairsum + weightGED + weightGraphlets;
	for(size_t i = 0; i < customMatrix.size(); ++i)
		if(customMatrix[i].isWeight())
		{
			_pairScaler += customMatrix[i].value1; // pair score weight
			_finalScaler += customMatrix[i].value2; // final score weight
		}

	_mustCalcGraphlets = _finalCalcGraphlet || (pairWeightGraphlets > 0);

	// HACK: cheap default
	// TODO: calcuate a suitable value in DataHolder or somewhere once networks are loaded
	if(!maxAgents)
		maxAgents = 400;

	_good = true;
	return true;
}
