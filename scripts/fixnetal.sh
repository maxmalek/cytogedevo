#!/bin/sh

# Convert NETAL's alignment result file into the two-column format used by EVERYONE ELSE

awk '{print $1 "\t" $3}' < "$1" > "$2"

