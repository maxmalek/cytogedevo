package org.cytoscape.gedevo;

import org.cytoscape.gedevo.resources.Resources;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class AboutBox extends Unloadable implements ActionListener
{
    private JPanel panel1;
    private JTextArea citeText;
    private JLabel versionLabel;
    private JButton bClose;
    private JLabel logos;
    private JLabel website;
    private JPanel citationPanel;
    private JLabel citationLink;
    private JFrame frame;


    public AboutBox()
    {
        createUIComponents();

        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(450, 600);
        frame.setIconImage(Resources.getTabIcon().getImage());
        //frame.setResizable(false);
        //frame.setSize(650, 400);

        Dimension sz = frame.getSize();
        panel1.setMinimumSize(sz);
        panel1.setMaximumSize(sz);
        panel1.setPreferredSize(sz);

        Container cp = frame.getContentPane();
        cp.setLayout(new BorderLayout());
        cp.add(panel1, BorderLayout.CENTER);
        frame.pack();
        frame.setVisible(true);
    }

    public void close()
    {
        frame.dispose();
    }

    private void createUIComponents()
    {
        versionLabel.setText("<html><b>" + GedevoApp.VERSION_TEXT + "</b><br />" + GedevoNativeUtil.getLibInfoStr() + "</html>");
        citeText.setText(
            "Malek M, Ibragimov R, Albrecht M, Baumbach J (2015) CytoGEDEVO - Global alignment of biological networks with Cytoscape. Bioinformatics. 2015 (in press)."
        );
        citeText.setLineWrap(true);
        citeText.setWrapStyleWord(true);
        citeText.setEditable(false);

        logos.setText("");
        logos.setIcon(Resources.getLogos());

        bClose.addActionListener(this);

        // via https://stackoverflow.com/questions/8669350/jlabel-hyperlink-to-open-browser-at-correct-url
        website.setCursor(new Cursor(Cursor.HAND_CURSOR));
        website.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                try
                {
                    Desktop.getDesktop().browse(new URI(website.getText()));
                }
                catch (Exception ex)
                {
                    GedevoUtil.msgbox("Failed to open web browser");
                }
            }
        });
        citationLink.setCursor(new Cursor(Cursor.HAND_CURSOR));
        citationLink.addMouseListener(new MouseAdapter()
        {
            @Override
            public void mouseClicked(MouseEvent e)
            {
                try
                {
                    Desktop.getDesktop().browse(new URI("http://bioinformatics.oxfordjournals.org/content/early/2016/01/14/bioinformatics.btv732"));
                }
                catch (Exception ex)
                {
                    GedevoUtil.msgbox("Failed to open web browser");
                }
            }
        });

    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        Object src = e.getSource();
        if(src == bClose)
            close();
    }

    @Override
    public void unload()
    {
        close();
    }
}
