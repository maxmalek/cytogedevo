package org.cytoscape.gedevo;

import org.cytoscape.gedevo.integration.ccs.EnumerateCCSTask;
import org.cytoscape.gedevo.pairlayout.PairLayoutTask;
import org.cytoscape.gedevo.pairlayout.SideBySide;
import org.cytoscape.gedevo.task.GedevoTask;
import org.cytoscape.gedevo.util.NetworkDim;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.vizmap.VisualMappingFunctionFactory;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import java.awt.*;


public class DebugStuff extends Unloadable
{
    GedevoApp app;

    DebugStuff(GedevoApp a)
    {
        app = a;

        a.addViewContextMenuEntry("GEDEVO debug", "View stats", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                NetworkDim dim = new NetworkDim(cyview);

                GedevoUtil.msgbox("Network: " + cyview.getModel().getRow(cyview.getModel()).get(CyNetwork.NAME, String.class) + "\n"
                        + "Node bounds: [" + dim.minx + ", " + dim.miny + "], [" + dim.maxx + ", " + dim.maxy + "]");
            }
        });

        a.addViewContextMenuEntry("GEDEVO debug", "Perform pair layout", new ContextMenuAction()
        {
            @Override
            public void onClick(final CyNetworkView cyview)
            {
                final PairLayoutTask pairlayout = new PairLayoutTask(cyview,
                        app.cyreg.getService(CyLayoutAlgorithmManager.class).getLayout("force-directed"), null, null, null, null);
                final SideBySide sbs = new SideBySide(pairlayout);
                TaskIterator it = new TaskIterator();
                it.append(pairlayout);
                it.append(
                    new Task()
                    {
                        @Override
                        public void run(TaskMonitor taskMonitor) throws Exception
                        {
                            sbs.apply(cyview);
                            app.repaint();
                        }

                        @Override
                        public void cancel() {}
                    }
                );
                app.taskmgr.execute("Debug: Apply pair layout", it, false);
            }
        });

        a.addViewContextMenuEntry("GEDEVO debug", "Recalc CCS", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                EnumerateCCSTask ccs = new EnumerateCCSTask(cyview.getModel());
                TaskIterator it = new TaskIterator(ccs);
                app.taskmgr.execute("Debug: Recalc CCS", it, false);
            }
        });

        /*a.addViewContextMenuEntry("Debug: Create GEDEVO table", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                GedevoAlignmentUtil.createAlignmentTable(cyview.getModel());
            }
        });*/

        a.addViewContextMenuEntry("GEDEVO debug", "Task test", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                GedevoTask task = new GedevoTask()
                {
                    @Override
                    public boolean canFinishNow()
                    {
                        return true;
                    }

                    @Override
                    public void run(TaskMonitor mon) throws Exception
                    {
                        mon.setTitle("Test task");
                        mon.setProgress(0.5);
                        // the layout used to go haywire with long texts like this
                        mon.setStatusMessage("Attempting to give a damn...[Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.]");
                        while(!cancelled)
                            Thread.sleep(500);
                    }
                };
                TaskIterator it = new TaskIterator(task);
                app.taskmgr.execute("Debug: Task test", it, false);
            }
        });

        a.addViewContextMenuEntry("GEDEVO debug", "Set viz icon", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                GedevoTask task = new GedevoTask()
                {
                    @Override
                    public boolean canFinishNow()
                    {
                        return false;
                    }

                    @Override
                    public void run(TaskMonitor taskMonitor) throws Exception
                    {
                        VisualMappingFunctionFactory ffC = app.cyreg.getService(VisualMappingFunctionFactory.class, "(mapping.type=continuous)");
                        ContinuousMapping<Double, Paint> mapping =
                                (ContinuousMapping<Double, Paint>) ffC.createVisualMappingFunction("gedevoPairScore", Double.class, BasicVisualLexicon.NODE_FILL_COLOR);

                        Color darkblue = new Color(0, 0, 50);
                        Color lightblue = new Color(127, 201, 255);
                        Color lightgreen = new Color(204, 255, 45);
                        Color uglypink = new Color(255, 0, 136);
                        mapping.addPoint(0.0, new BoundaryRangeValues<Paint>(Color.WHITE, uglypink, uglypink));
                        mapping.addPoint(0.1, new BoundaryRangeValues<Paint>(lightgreen, lightgreen, lightgreen));
                        mapping.addPoint(0.5, new BoundaryRangeValues<Paint>(lightblue, lightblue, lightblue));
                        mapping.addPoint(1.0, new BoundaryRangeValues<Paint>(darkblue, darkblue, Color.BLACK));
                        ////

                        app.mainPanel.setMappingPreview(mapping, "This is a test!");
                        app.repaint();
                    }
                };
                TaskIterator it = new TaskIterator(task);
                app.taskmgr.execute("", it, false);
            }
        });
    }

    @Override
    public void unload()
    {
        app = null;
    }
}
