package org.cytoscape.gedevo.integration.customlayoutmenu;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.work.TaskManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class StaticLayoutMenu extends JMenuItem implements ActionListener
{
    private final LayoutLauncher launch;

    public StaticLayoutMenu(CyLayoutAlgorithm layout, boolean enabled, CyApplicationManager appMgr, TaskManager tm)
    {
        super(layout.toString());
        launch = new LayoutLauncher(layout, appMgr, tm);
        addActionListener(this);
        setEnabled(enabled);
    }

    public void actionPerformed(ActionEvent e)
    {
        launch.doLayout(CyLayoutAlgorithm.ALL_NODE_VIEWS, "");
    }
}
