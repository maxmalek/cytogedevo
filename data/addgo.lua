dofile("util.lua")

local pairs = pairs
local tins = table.insert
local max = math.max
local tconcat = table.concat

local args = {...}
local gofile = table.remove(args, 1)

local allGOs = setmetatable({}, { __index = function(t, k) local r = {}; t[k] = r; return r end })


do
    local f, readGOs, readAccs
    local nextAccs
    readAccs = function(a)
        nextAccs = a
        f = readGOs
    end
    
    readGOs = function(a)
        --print(" * " .. #a .. " new GOs for: " .. table.concat(nextAccs, " "))
        for _, acc in pairs(nextAccs) do
            local gos = allGOs[acc]
            for _, go in pairs(a) do
                gos[go] = true
            end
        end
        f = readAccs
    end
    
    f = readAccs
    for line in io.lines(gofile) do
        f(line:explode("\t", true))
    end
end
setmetatable(allGOs, nil)


local function countGOsPair(a, b)
    local ga = allGOs[a]
    local gb = allGOs[b]
    if not (ga and gb) then
        local notfound = 0
        if not ga and a ~= "-" then notfound = notfound + 1 end
        if not gb and b ~= "-" then notfound = notfound + 1 end
        return 0, notfound
    end
    local c = 0
    for g, _ in pairs(ga) do
        if gb[g] then
            c = c + 1
        end
    end
    return c, 0
end

local function countGOsFile(fn)
    local c = 0
    local nf = 0
    local n = 0
    local firstline = true
    local idxGo, idxGoPercScore
    local maxgo = 0
    local alldata = {}
    for line in io.lines(fn) do
        if line ~= "" and not line:startsWith("#") then
            local a = line:explode("\t", true)
            if #a == 1 then
                a = line:explode(" ", true)
                if #a ~= 2 then
                     error("split fail in " .. fn)
                end
            end
            if firstline then
                firstline = false
                if a[1] == "P1" and a[2] == "P2" then -- GEDEVO annotated format?
                    idxGo = #a + 1
                    idxGoPercScore = idxGo + 1
                    a[idxGo] = "gedevoGOSumScore"
                    a[idxGoPercScore] = "gedevoGOPercScore"
                    a.header = true
                    tins(alldata, a)
                else
                    idxGo = 3
                    idxGoPercScore = 4
                    tins(alldata, { "P1", "P2", "gedevoGOSumScore", "gedevoGOPercScore", header = true })
                    
                    local found, notfound = countGOsPair(a[1], a[2])
                    c = c + found
                    nf = nf + notfound
                    n = n + 1
                    
                    a[idxGo] = found
                    maxgo = max(maxgo, found)
                
                    tins(alldata, a)
                end
            else
                local found, notfound = countGOsPair(a[1], a[2])
                c = c + found
                nf = nf + notfound
                n = n + 1
                
                a[idxGo] = found
                maxgo = max(maxgo, found)
                
                tins(alldata, a)
            end
        else
            tins(alldata, line)
        end
    end
    
    local out = io.open(fn .. ".withgo", "w")
    for _, a in ipairs(alldata) do
        if type(a) == "table" then
            if not a.header then
                a[idxGoPercScore] = a[idxGo] / maxgo
            end
            a = tconcat(a, "\t")
        end
        out:write(a, "\n")
    end
    out:close()
    
    return c, n, nf
end


for _, fn in ipairs(args) do
    local c, n, nf = countGOsFile(fn)
    print(fn .. " [pairs: " .. n .. ", not found: " .. nf .. "] = " .. c)
end

