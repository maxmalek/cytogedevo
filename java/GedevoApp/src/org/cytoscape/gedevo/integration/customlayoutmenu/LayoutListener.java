package org.cytoscape.gedevo.integration.customlayoutmenu;

import org.cytoscape.view.layout.CyLayoutAlgorithm;

public interface LayoutListener
{
    public void addLayout(CyLayoutAlgorithm layout);
    public void removeLayout(CyLayoutAlgorithm layout);
}
