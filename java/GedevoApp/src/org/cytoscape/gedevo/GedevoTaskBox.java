package org.cytoscape.gedevo;

import org.cytoscape.gedevo.task.GedevoTask;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class GedevoTaskBox
{
    private JProgressBar progress;
    private JButton abortButton;
    private JButton finishNowButton;
    private JLabel label1;
    private JLabel label2;
    private JPanel panel;
    private GedevoTaskMonitor taskmon;
    private GedevoTask task;
    private String prefix;
    private Object removeLock;
    private boolean hasError = false;


    public GedevoTaskBox(String prefix, GedevoTask t)
    {
        task = t;
        this.prefix = prefix;
        taskmon = new GedevoTaskMonitor(this);

        createUIComponents();

        setTitle(prefix);
        setDetail("");
        setProgress(0);

        ActionListener acl = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                final Object src = e.getSource();
                if(src == abortButton)
                {
                    abortButton.setEnabled(false);
                    task.cancel();
                    removeSelf();
                }
                else if(src == finishNowButton)
                {
                    task.finishNow();
                    finishNowButton.setEnabled(false);
                    finishNowButton.setText("Finishing...");
                }
            }
        };

        abortButton.addActionListener(acl);
        finishNowButton.addActionListener(acl);
    }

    public boolean hasError()
    {
        return hasError;
    }

    public void setCanFastFinish(boolean on)
    {
        finishNowButton.setVisible(on);
    }

    public void setRemovalLock(Object lock)
    {
        removeLock = lock;
    }

    public JPanel getPanel()
    {
        return panel;
    }

    public GedevoTaskMonitor getTaskMonitor()
    {
        return taskmon;
    }

    private void createUIComponents()
    {
        finishNowButton.setVisible(false);
    }

    public void setTitle(String title)
    {
        label1.setText(prefix + ": " + title);
    }

    public void setDetail(String detail)
    {
        label2.setText(detail);
    }

    public void setProgress(double p)
    {
        progress.setValue((int) (p * 100.0));
        progress.setIndeterminate(p < 0);
    }

    public void setError(String s)
    {
        hasError = true;
        panel.setOpaque(true);
        panel.setBackground(Color.RED);
        setTitle(s);
    }

    public void setFinished()
    {
        panel.setOpaque(true);
        panel.setBackground(Color.GREEN);
        abortButton.setText("Remove");
        setProgress(1.0);
    }

    public void removeSelf()
    {
        if(removeLock != null)
            synchronized (removeLock)
            {
                _removeSelf();
            }
        else
            _removeSelf();
    }

    private void _removeSelf()
    {
        Container p = panel.getParent();
        if(p != null)
        {
            p.remove(panel);
            p.validate();
            p.repaint();
        }
    }
}
