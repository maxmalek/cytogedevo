#ifndef GRAPHLET_SCORE_H
#define GRAPHLET_SCORE_H

#include "common.h"

class Node;

score calcGraphletDistance(const Node *a, const Node *b);

#endif
