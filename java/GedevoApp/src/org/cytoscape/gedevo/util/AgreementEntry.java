package org.cytoscape.gedevo.util;

// I can't even begin to express how much java annoys me for things like this.
public final class AgreementEntry implements Comparable<AgreementEntry>
{
    public final int nodeId1;
    public final int nodeId2;
    public final double agreement;
    public final int rawCount;
    public final int group1;
    public final int group2;

    public AgreementEntry(int id1, int id2, double c, int raw, int g1, int g2)
    {
        nodeId1 = id1;
        nodeId2 = id2;
        agreement = c;
        rawCount = raw;
        group1 = g1;
        group2 = g2;
    }

    @Override
    public final int compareTo(AgreementEntry o)
    {
        return Double.compare(agreement, o.agreement);
    }
}
