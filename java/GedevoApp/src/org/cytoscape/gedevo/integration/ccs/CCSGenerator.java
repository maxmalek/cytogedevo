package org.cytoscape.gedevo.integration.ccs;

import org.cytoscape.gedevo.simplenet.Graph;
import org.cytoscape.gedevo.simplenet.Node;

import java.util.*;

public class CCSGenerator
{
    private static class SimpleNodePair
    {
        SimpleNodePair(Node p1, Node p2)
        {
            a = p1;
            b = p2;
        }
        final Node a, b;
    }

    // IntPair[number-of-CCS][number-of-entries]
    public static IntPair[][] extractCCS(Graph ga, Graph gb, int[] mapping) // mapping[node idx in ga] == partner node idx in gb
    {
        HashMap<Node, Node> partner = new HashMap<Node, Node>();

        for(int i = 0; i < mapping.length; ++i)
        {
            if(mapping[i] < 0)
                continue;

            Node a = ga.nodes[i];
            Node b = mapping[i] < 0 ? null : gb.nodes[mapping[i]];

            partner.put(a, b);
            if(b != null)
                partner.put(b, a);
        }

        LinkedList<Node> unseen = new LinkedList<Node>();
        Collections.addAll(unseen, ga.nodes);
        LinkedList<Node> q = new LinkedList<Node>();
        HashSet<Node> seen = new HashSet<Node>();
        ArrayList<ArrayList<SimpleNodePair>> cclist = new ArrayList<ArrayList<SimpleNodePair>>();

        while(!unseen.isEmpty())
        {
            Node begin = unseen.removeLast();
            if(seen.contains(begin))
                continue;
            seen.add(begin);
            q.add(begin);

            ArrayList<SimpleNodePair> cc = null;

            while(!q.isEmpty())
            {
                Node a = q.removeFirst();
                Node b = partner.get(a);
                if (b == null)
                    continue; // no partner, can't be member of CCS

                if (cc == null) // start new CCS if necessary
                    cc = new ArrayList<SimpleNodePair>();

                cc.add(new SimpleNodePair(a, b));

                // CCS enumeration does not respect edge directions -- does it make sense to do so?
                for (Node anext : a.neighbors)
                {
                    if (seen.contains(anext))
                        continue;
                    Node bnext = partner.get(anext);
                    if (bnext == null)
                        continue;
                    if (b.neighbors.contains(bnext))
                    {
                        q.add(anext);
                        seen.add(anext);
                    }
                }
            }

            if(cc != null && !cc.isEmpty())
                cclist.add(cc);
        }

        if(ga.nodes.length != seen.size())
            throw new IllegalStateException("wat");

        IntPair[][] ret = new IntPair[cclist.size()][];
        for(int i = 0; i < cclist.size(); ++i)
        {
            ArrayList<SimpleNodePair> nps = cclist.get(i);
            IntPair[] ipa = new IntPair[nps.size()];
            for(int x = 0; x < ipa.length; ++x)
            {
                SimpleNodePair np = nps.get(x);
                ipa[x] = new IntPair(np.a.id, np.b.id);
            }
            ret[i] = ipa;
        }

        return ret;
    }
}
