#include "CmdHelp.h"
#include "common.h"

namespace CmdHelp
{

// Note: if you change this, update CmdlineParser.cpp if necessary!
void Print()
{
	puts(
		"--edgelist <file>   * Read network file, EDGELIST format.\n"
		"--edgelisth <file>  * Read network file, EDGELISTH format.\n"
		"--sif <file>        * Read network file, SIF format.\n"
		"^ Always specify exactly two input networks.\n"
		"\n"
		"--pop <N>           * Number of new individuals per iteration.\n"
		"                      (default = 400)\n" // FIXME: 400?
		"--threads <N>       * Number of threads to use.\n"
		"                      (default = number of CPU cores)\n"
		"--undirected/-u     * Treat networks as undirected.\n"
		"--maxiter <N>       * Stop after N iterations.\n"
		"--maxsecs <N>       * Stop after N seconds.\n"
		"--maxsame <N>       * Stop after N iterations without\n"
		"                      any score improvement.\n"
		"--keep-workfiles    * Dump some matrices to disk for later re-use;\n"
		"                      speeds up subsequent startup on the same data.\n"
		"--prematch          * Assume that nodes with the same name\n"
		"                      (case sensitive!) in both networks should be\n"
		"                      always matched.\n"
		"--greedy-init       * May speed up initial convergence, but is very slow.\n"
		"                      It is usually not beneficial to enable this.\n"
		"--trim              * Trim node names/remove everything after | char.\n"
		"--ignore-self-loops * Do not consider self loops when loading networks.\n"
		"--logfile <file>    * Save statistics to file. (default: Off)\n"
		"--logiter <N>       * Save one log entry each N iterations.\n"
		"                      Note that the first 200 iterations are always\n"
		"                      logged regardless of this setting. (default = 10)\n"
		"--resume <file>     * Load a prev. result and continue alignment.\n"
		"--save <file>       * Specify file name for alignment result.\n"
		"                      If not given, this is derived from time+input.\n"
		"--timestamp         * Add timestamp to the file name set via --save.\n"
		"--autosave <N>      * Save results every N seconds. (Default: Off)\n"
		"--series            * If used, autosave will create a new file every save.\n"
		"--config OPT=VALUE  * Set internal config option, e.g.\n"
		"or -c OPT=VALUE       \"--config weightGraphlets=0.5\".\n"
		"                      The param=value block must not contain spaces.\n"
		"                      See README file for a list of supported options.\n"
		"\n"
		"--custom <file> <LeftNetwork> <DataModel> <ValueRange> <Value1> <Value2>\n"
		"    * Loads 'file' in pairlist format as custom data matrix.\n"
		"      LeftNetwork specifies which network (1 or 2) is on the left column;\n"
		"       can also be 0 or 'auto' to determine this automatically.\n"
		"      DataModel  can be one of: distance, similarity.\n"
		"      ValueRange can be one of: clamp, rescale, override.\n"
		"      For override: Value1 means first threshold,\n"
		"                    Value2 means second threshold.\n"
		"      For the rest: Value1 means pair score contribution factor,\n"
		"                    Value2 means final score contributioin factor\n"
		"\n"
		);
}


}; // end namespace CmdHelp

