#!/bin/bash

. config.inc
needlua

if [ ! -d data1 ]; then
    mkdir data1
fi

blastname() {
    # from: z_3_4_melanogaster_vs_sapiens.z_pairlist
    # to:   melanogaster_sapiens.blastlist
    echo "$1" | sed -r 's/z_[0-9]+_[0-9]+_([^_]+)_vs_([^_]+)\.z_pairlist/\1_\2\.blastlist/'
}

# Remove cruft from BLAST raw pairlist files
for f in work/*.z_pairlist; do
    f1=`blastname "$f"`
    f1="${f1%.blastlist}"
    
    f2="data1/${f1##*/}.blastlist"
    echo "Cleanlist $f -> $f2"
    $LUAEXE cleanlist.lua blast "$f" "$f2"
    
    ## This is for generating MI-GRAAL precursor matrices -- they still need a column swap if the 2nd network is bigger than the first
    #f3="data1/${f1##*/}.bitlistx"
    #echo "Cleanlist $f -> $f3"
    #$LUAEXE cleanlist.lua blast2 "$f" "$f3"
done

# Format *.edgelist files properly
for f in work/*.edgelist; do
    f2="${f%.edgelist}"
    f2="data1/${f2##*/}.el"
    echo "Cleanlist $f -> $f2"
    $LUAEXE cleanlist.lua net "$f" "$f2"
done

