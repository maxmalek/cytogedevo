#!/bin/bash

# Multi-process all-vs-all BLAST matrix generator


EVALUE=1000

# 1: query file name
# 2: subject string
# 3: target file name
singleblast()
{
    val=`echo "$2" | blastp -max_hsps 1 -max_target_seqs 1 -query "$1" -subject - -outfmt "6 evalue bitscore" -evalue $EVALUE`
    if [ -z "$val" ]; then
        val="$EVALUE 0"
    fi
    echo -e "$prot1\t$prot2\t$val" > "$3"
}


. config.inc

out="$1"
tmpd="$2"
n1=$3
n2=$4

SPECIES1="${SPECIESNAMES[${n1}]}"
SPECIES2="${SPECIESNAMES[${n2}]}"
PAIRFILE="$out/z_${n1}_${n2}_${SPECIES1}_vs_${SPECIES2}.z_pairlist"

echo "[$n1, $n2] -- ${SPECIES1} vs ${SPECIES2}"

[ -e "$PAIRFILE" ] && rm "$PAIRFILE"

IN1="$tmpd/x_species_${n1}_vs_${n2}.z"
OUTPREFIX="$tmpd/r_species_${n1}_vs_${n2}"
CORES=`nproc`

rm "${OUTPREFIX}".*

nruns=0
pidlist=""

# pid list
waitandwrite()
{
    for p in $pidlist; do
        wait $p
    done;
    i=0
    while [ $i -lt $nruns ]; do
        cat "${OUTPREFIX}".$i >> "$PAIRFILE"
        let i++
    done
    nruns=0
    pidlist=""
}

# pid list
waitandwritefast()
{
    for p in $pidlist; do
        wait $p
    done;
    i=0
    cat "${OUTPREFIX}".* >> "$PAIRFILE"
    nruns=0
    pidlist=""
}

RCOUNTER1=0
while read prot1; do
    read seq1
    echo  -e "$prot1\n$seq1" > "$IN1"
    while read prot2; do
        read seq2
        in2=$(echo -e "$prot2\n$seq2")
        singleblast "$IN1" "$in2" "${OUTPREFIX}.$nruns" &
        pidlist="$pidlist $!"
        let nruns++
        if [ $nruns -ge $CORES ]; then
            # show progress
            echo -ne "[$RCOUNTER1]: $prot1 -- $prot2          \r"
            waitandwritefast
        fi
    done < "$out/$SPECIES2.fasta"

    waitandwrite

    let RCOUNTER1++
done < "$out/$SPECIES1.fasta"

