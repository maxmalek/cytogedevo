package org.cytoscape.gedevo;

// Accessed from C++ via JNI
public class AlignmentInfo
{
    // misc, single agent
    public int lifeTime;
    public double edgeCorrectness;
    public long unalignedEdges;
    public long partialAlignedEdges;
    public long alignedEdges;

    // instance state
    public long iterations;
    public int iterationsWithoutScoreChange;
    public int runningTime;
    public int numAgents;
}
