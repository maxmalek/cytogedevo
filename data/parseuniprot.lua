dofile("util.lua")

local string = string
local tins = table.insert
local tconcat = table.concat


local infile, outfile = ...
--infile = "uniprot_sprot_human.dat"
--outfile = "uniprot_sprot_human.gos"

local out = io.open(outfile, "w")
out:setvbuf("full", 1024*1024*8)

--local curID
local curGOs = {}
local curACs = {}
--local allACs = {}
--local nDoubleDef = 0
local nOk = 0
local nNoData = 0

-- ID
--local function f_ID(s)
--    curID = s:explode(" ", true)[1]
--end

-- //
local function f_FINISH()
    --print(curID, #curACs, #curGOs)
    if next(curACs) and next(curGOs) then
        out:write(tconcat(curACs, "\t"), "\n", tconcat(curGOs, "\t"), "\n")
        nOk = nOk + 1
    else
        nNoData = nNoData + 1
    end
    --curID = nil
    curACs = {}
    curGOs = {}
end

-- AC   P0CG48; P02248; P02249; P02250; P62988; Q29120; Q6LBL4; Q6LDU5;
local nospace = { [" "] = "" }
local function f_AC(s)
    local n = #curACs
    local t = s:gsub(".", nospace):explode(";", true)
    local ac
    for i = 1, #t do
        n = n + 1
        ac = t[i]
        --if allACs[ac] then -- double defined accession number ?!
        --    --print("Double defined accession ID: [" .. ac .. " = " .. curID .. "] was [" .. ac .. " = " .. allACs[ac] .. "]")
        --    nDoubleDef = nDoubleDef + 1
        --end
        curACs[n] = ac
        --allACs[ac] = curID
    end
end

-- DR   GO; GO:0000122; P:negative regulation of transcription from RNA polymerase II promoter; IMP:BHF-UCL.
-- DR   DIP; DIP-56190N; -.
local function f_DR(s)
    local a = s:gsub(".", nospace):explode(";", true)
    if a[1] == "GO" then
        tins(curGOs, assert(tonumber(a[2]:sub(4))))
    elseif a[1] == "DIP" then
        tins(curACs, a[2])
    end
end

local handlers =
{
    --ID = f_ID, -- unnecessary
    ["//"] = f_FINISH,
    AC = f_AC,
    DR = f_DR,
}
    
for line in io.lines(infile) do
    local f = handlers[line:sub(1, 2)]
    if f then
        f(line:sub(6))
    end
end

print("Ok: " .. nOk)
print("No data: " .. nNoData)
--print("nDoubleDef: " .. nDoubleDef)

