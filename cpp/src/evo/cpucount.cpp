#include "cpucount.h"
#include <stdlib.h>

#if _WIN32
#   define WIN32_LEAN_AND_MEAN
#   include <windows.h>
#   include <intrin.h>
#else
#   include <dirent.h>
#   include <sys/stat.h>
#   include <sys/types.h>
#   include <unistd.h>
#   include <sys/ioctl.h>
#   include <sys/param.h>
#   include <sys/sysctl.h>
#endif

// This could be std::thread::hardware_concurrency(), but the C++11 documentation states that it is unreliable.
unsigned countCPUCores()
{
    size_t cpus = 1; // fail safe.

#if defined(_WIN32)
    SYSTEM_INFO sysinfo;
    GetSystemInfo(&sysinfo);
    cpus = sysinfo.dwNumberOfProcessors;
#elif defined(__APPLE_CC__) // FIXME: not sure if this works -- untested!
    int nm[2];
    size_t len = 4;
    uint32_t count;

    nm[0] = CTL_HW; nm[1] = HW_AVAILCPU;
    sysctl(nm, 2, &count, &len, NULL, 0);

    if(count < 1) {
        nm[1] = HW_NCPU;
        sysctl(nm, 2, &count, &len, NULL, 0);
        if(count < 1) { count = 1; }
    }
    cpus = count;
#else
    cpus = sysconf(_SC_NPROCESSORS_ONLN);
#endif

    return (unsigned)(cpus);
}
