package org.cytoscape.gedevo;

import javax.swing.text.JTextComponent;
import java.awt.*;

public final class U
{
    private U() {}

    public static double getdouble(String s, double def)
    {
        double d;
        try { d = Double.parseDouble(s); } catch(NumberFormatException ex) { d = def; }
        return d;
    }
    public static int getint(String s, int def)
    {
        int i;
        try { i = Integer.parseInt(s); } catch(NumberFormatException ex) { i = def; }
        return i;
    }
    public static float getfloat(String s, float def)
    {
        float f;
        try { f = Float.parseFloat(s); } catch(NumberFormatException ex) { f = def; }
        return f;
    }

    public static double getdouble(JTextComponent c, double def) throws NumberFormatException
    {
        String s = c.getText().trim();
        if(s.isEmpty())
            return def;

        try
        {
            double d = Double.parseDouble(s);
            resetText(c);
            return d;
        }
        catch(NumberFormatException ex)
        {
            c.setBackground(Color.RED);
            throw ex;
        }
    }
    public static int getint(JTextComponent c, int def) throws NumberFormatException
    {
        String s = c.getText().trim();
        if(s.isEmpty())
            return def;

        try
        {
            int i = Integer.parseInt(s);
            resetText(c);
            return i;
        }
        catch(NumberFormatException ex)
        {
            c.setBackground(Color.RED);
            throw ex;
        }
    }
    public static float getfloat(JTextComponent c, float def) throws NumberFormatException
    {
        String s = c.getText().trim();
        if(s.isEmpty())
            return def;

        try
        {
            float f = Float.parseFloat(s);
            resetText(c);
            return f;
        }
        catch(NumberFormatException ex)
        {
            c.setBackground(Color.RED);
            throw ex;
        }
    }

    private static void resetText(JTextComponent c)
    {
        if(GedevoApp.DEBUG)
            c.setBackground(Color.GREEN);
        else
            c.setBackground(Color.WHITE);
    }
}
