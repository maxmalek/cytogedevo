#include "common.h"
#include <iostream>
#include "tools.h"

static void tt(const char *a)
{
	std::string s = a;
	addTimestampToFileName(s, NULL);
	std::cout << a << std::endl << s << std::endl << std::endl;
}

void dotest()
{
	tt("test");
	tt("test.ext");
	tt("path/to/test");
	tt("path/to/test.ext");
	tt("path/to.dot/test");
	tt("path/to.dot/test.ext");
}

