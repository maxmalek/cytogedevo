#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H


class ProgressBar
{
public:
	void Step(bool force = false);
	void Update(bool force = false);
	void Print(void);
	ProgressBar(unsigned int total = 0);
	~ProgressBar();

	unsigned int total, done;

private:
	unsigned int _perc, _oldperc;
};

#endif
