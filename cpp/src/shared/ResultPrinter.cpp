#include "ResultPrinter.h"
#include "ProgramState.h"
#include "Agent.h"
#include "GEDScore.h"
#include <algorithm>
#include <utility>
#include <iostream>

template <typename T> struct SortableHelper
{
	SortableHelper(score s, const T& e)
		: val(s)
		, item(e)
	{
	}

	inline bool operator< (const SortableHelper& o) const
	{
		return val < o.val;
	}

	score val;
	T item;
};


typedef std::vector<SortableHelper<uint> > MappingSorter;

static void configDumper(const std::string& var, const std::string& val, void *ud)
{
	std::ostream& o = *static_cast<std::ostream*>(ud);
	o << "##   " << var << " = " << val << std::endl;
}

static void dumpCustomMatrixConfig(size_t i, const CustomMatrixData& data, std::ostream& o)
{
	o << "##  -[" << i << "]-" << std::endl;
	o << "##    File:        " << data.filename << std::endl;
	o << "##    LeftNetwork: " << (data.leftNetwork + 1);
	if(data.leftNetwork < 0)
		o << " (autodetect)";
	o << std::endl;

	o << "##    ModelType:   " << data.modelType << " (" << data.getModelTypeDesc() << ")" << std::endl;
	o << "##    ValueRange:  " << data.valueRange << " (" << data.getValueRangeDesc() << ")" << std::endl;
	o << "##    Value1:      " << data.value1 << " (" << data.getValue1Desc() << ")" << std::endl;
	o << "##    Value2:      " << data.value2 << " (" << data.getValue2Desc() << ")" << std::endl;
}


void ResultPrinter::AsList(const ProgramState& state, const Agent *dude, std::ostream& o)
{
	const NodeMapping& my = dude->getMapping();
	const NodeMapping& ref = dude->getReferenceMapping();
	const UserSettings& settings = state.settings;

	uint aligned = 0, left1 = 0, left2 = 0;

	for(size_t i = 0; i < my.size(); ++i)
	{
		const Node *a = my.node[i];
		const Node *b = ref.node[i];

		if(a && !b)
			++left2;
		if(b && !a)
			++left1;
		if(a && b)
			++aligned;
	}

	o << "## == GEDEVO full alignment report ==" << std::endl;
	o << "## Compilation date:       " << __DATE__ "  " __TIME__ << std::endl;
	o << "## --- Config dump ---" << std::endl;
	state.settings.Dump(configDumper, &o);
	if(state.settings.customMatrix.size())
	{
		o << "## --- Custom matrices ---" << std::endl;
		for(size_t i = 0; i < state.settings.customMatrix.size(); ++i)
			dumpCustomMatrixConfig(i, state.settings.customMatrix[i], o);
	}
	o << "## -------------------" << std::endl;
	o << "## Start time:             " << generateTimestampStr(&state.GetStartTime()) << std::endl;
	o << "## Network file 1:         " << state.ph.GetNetworkFileName(0) << std::endl;
	o << "## Network file 2:         " << state.ph.GetNetworkFileName(1) << std::endl;
	o << "## Network 1 nodes:        " << state.ph.GetGroup(0).size()<< std::endl;
	o << "## Network 1 edges:        " << state.dh.GetEdgeAmount(0)  << std::endl;
	o << "## Network 2 nodes:        " << state.ph.GetGroup(1).size()<< std::endl;
	o << "## Network 2 edges:        " << state.dh.GetEdgeAmount(1)  << std::endl;
	o << "## Aligned nodes:          " << aligned                    << std::endl;
	o << "## Unaligned in 1:         " << left1                      << std::endl;
	o << "## Unaligned in 2:         " << left2                      << std::endl;
	o << "##" << std::endl;
	o << "## Edge correctness:       " << (dude->getEdgeCorrectness() * 100) << "%" << std::endl;
	o << "## Iterations:             " << state.GetIterationCount()  << std::endl;
	o << "## GED score:              " << dude->getGEDScore()        << std::endl;
	o << "## Avg. pair score:        " << dude->getPairSumScore()    << std::endl;
	o << "##" << std::endl;
	o << "## Unaligned interactions: " << dude->getGEDRawFull()      << std::endl;
	o << "## Partially aligned:      " << dude->getGEDRawPartial()   << std::endl;
	o << "## Fully aligned:          " << dude->getGEDRawSubstituted()<< std::endl;
	o << "##" << std::endl;
	o << "## (Lower pair score value is better)" << std::endl;
	o << "## FA = fully aligned" << std::endl;
	o << "## PA = partially aligned (undirected->directed or vice versa)" << std::endl;
	o << "## Add = added edge" << std::endl;
	o << "## Rm = removed edge" << std::endl;
	o << "## Flip = flipped edge direction" << std::endl;
	o << "## Info = Extra comment" << std::endl;
	o << "## Various other scores may follow, depending on what was used for the calculations." << std::endl;
	o << std::endl;
	o << "P1\tP2\tPairScore\tGEDScore\tFA\tPA\tAdd\tRm\tFlip\tInfo"; // TODO: is there a way NOT to calculate GED?
	if(settings._mustCalcGraphlets)
		o << "\tGrletSigScore";
	if(settings.customMatrix.size())
	{
		for(size_t i = 0; i < settings.customMatrix.size(); ++i)
			o << "\tCustomScore" << (i+1); // HMM: name?
	}

	o << std::endl;
	o << std::endl;

	MappingSorter v;

	for(size_t i = 0; i < my.size(); ++i)
	{
		const Node *a = my.node[i];
		const Node *b = ref.node[i];

		if(!(a || b))
			continue;

		v.push_back(SortableHelper<uint>(dude->getPairScore(i), i));
	}

	std::sort(v.begin(), v.end());

	for(size_t i = 0; i < v.size(); ++i)
	{
		const uint pairIdx = v[i].item;
		const Node *a = my.node[pairIdx];
		const Node *b = ref.node[pairIdx];

		o <<         (a ? a->name() : "-");
		o << '\t' << (b ? b->name() : "-");
		o << '\t' << v[i].val;

		o << '\t' << dude->getPairGEDScore(pairIdx);

		GEDInfo pairged;
		dude->getPairGED(pairIdx, pairged);
		o << '\t' << GEACC_SUBST(pairged);
		o << '\t' << (GEACC_D_TO_U(pairged) + GEACC_U_TO_D(pairged));
		o << '\t' << GEACC_ADDED(pairged);
		o << '\t' << GEACC_REMOVED(pairged);
		o << '\t' << GEACC_FLIP(pairged);

		o << '\t';
		if(a && a->prematched >= 0)
			o << "prematched";
		else
			o << '-';

		const uint aidx = my.nodeId[pairIdx];
		const uint bidx = ref.nodeId[pairIdx];

		if(settings._mustCalcGraphlets)
			o << '\t' << state.dh.GetGraphletScore_unsafe(aidx, bidx);

		for(size_t j = 0; j < state.dh.GetNumCustomMatrices(); ++j)
			o << '\t' << state.dh.GetCustomScoreRaw(j, aidx, bidx);

		o << std::endl;
	}
}

