#!/bin/bash

# Slow (single-process) version of pairblast.sh
# Unused -- kept for reference

. config.inc

EVALUE=1000

out="$1"
tmpd="$2"
n1=$3
n2=$4

SPECIES1="${SPECIESNAMES[${n1}]}"
SPECIES2="${SPECIESNAMES[${n2}]}"
PAIRFILE="$out/z_${n1}_${n2}_${SPECIES1}_vs_${SPECIES2}.z_pairlist"

echo "[$n1, $n2] -- ${SPECIES1} vs ${SPECIES2}"

[ -e "$PAIRFILE" ] && rm "$PAIRFILE"

IN1="$tmpd/x_species_${n1}_vs_${n2}.z"
IN2="$tmpd/y_species_${n1}_vs_${n2}.z"

RCOUNTER1=0
while read prot1; do
    read seq1
    echo  -e "$prot1\n$seq1" > "$IN1"
    while read prot2; do
        read seq2
        echo  -e "$prot2\n$seq2" > "$IN2"
        
        # show progress
        echo -ne "[$RCOUNTER1]: $prot1 -- $prot2          \r"
        
        val=$(\
        blastp \
             -max_hsps 1        \
             -max_target_seqs 1 \
             -query $IN1 -subject $IN2 \
             -outfmt "6 evalue bitscore" \
             -evalue $EVALUE\
             )
             
        if [ -z "$val" ]; then
            val="$EVALUE 0"
        fi
        echo -e "$prot1\t$prot2\t$val" >> "$PAIRFILE"
    done < "$out/$SPECIES2.fasta"

    let RCOUNTER1++
done < "$out/$SPECIES1.fasta"

