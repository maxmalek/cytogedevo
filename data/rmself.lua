-- little utility to remove self loops from edge list files

dofile("util.lua")

for _, fn in ipairs({...}) do
    local out = io.open(fn .. "q", "w")
    local c = 0
    for line in io.lines(fn) do
        local a = line:explode("\t", true)
        if a[1] ~= a[2] then
            out:write(line, "\n")
        else
            c = c + 1
        end
    end
    print(fn .. ": " .. c .. " self loops removed")
    out:close()
end
