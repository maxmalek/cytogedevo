//-----------------------------------------------------------------------------
// MurmurHash3 was written by Austin Appleby, and is placed in the public
// domain. The author hereby disclaims copyright to this source code.

#ifndef _MURMURHASH3_H_
#define _MURMURHASH3_H_

void MurmurHash3_x86_32  ( const void * key, int len, unsigned seed, void * out );

void MurmurHash3_x86_128 ( const void * key, int len, unsigned seed, void * out );

void MurmurHash3_x64_128 ( const void * key, int len, unsigned seed, void * out );

//-----------------------------------------------------------------------------

#endif // _MURMURHASH3_H_
