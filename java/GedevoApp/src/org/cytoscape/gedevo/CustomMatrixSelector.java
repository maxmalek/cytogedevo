package org.cytoscape.gedevo;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

// Do *NOT* change the order of the following enums!
// Their integer values are also defined in C++
enum DataModel
{
    DISTANCE("Distance"),
    SIMILARITY("Similarity");


    DataModel(String name) { this.name = name; }
    private String name;
    public String toString() { return name; }
}

enum ValueRange
{
    CLAMP("Clamp"),
    RESCALE("Rescale"),
    OVERRIDE("Override clamped (BLAST)");

    ValueRange(String name) { this.name = name; }
    private String name;
    public String toString() { return name; }
}


public class CustomMatrixSelector
{
    private JPanel panel1;
    private JTextField textValue1;
    private JButton xButton;
    private FileSelectorBox fileSelector;
    private JTextField textValue2;
    private JComboBox comboBox1;
    private JComboBox/*<DataModel>*/ comboBoxDataModel;
    private JComboBox/*<ValueRange>*/ comboBoxValueRange;
    private JPanel panelValue1;
    private JPanel panelValue2;
    private JButton bUp;
    private JButton bDown;

    private Runnable _onUnregister;
    private String[] _sourceNames = new String[2];

    public CustomMatrixSelector()
    {
        createUIComponents();
    }

    public void setOnUnregister(Runnable r)
    {
        _onUnregister = r;
    }

    public void setMoveListeners(ActionListener up, ActionListener down)
    {
        bUp.addActionListener(up);
        bDown.addActionListener(down);
    }

    public JPanel getPanel()
    {
        return panel1;
    }

    public void setSourceName(int index, String s)
    {
        _sourceNames[index] = s;
        comboBox1.removeAllItems();
        comboBox1.addItem("Autodetect");
        comboBox1.addItem("Network #1 (" + _sourceNames[0] + ")");
        comboBox1.addItem("Network #2 (" + _sourceNames[1] + ")");
    }

    private void createUIComponents()
    {
        xButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                if(_onUnregister != null)
                    _onUnregister.run();
            }
        });

        comboBoxDataModel.addItem(DataModel.DISTANCE);
        comboBoxDataModel.addItem(DataModel.SIMILARITY);

        comboBoxValueRange.addItem(ValueRange.CLAMP);
        comboBoxValueRange.addItem(ValueRange.RESCALE);
        comboBoxValueRange.addItem(ValueRange.OVERRIDE);

        comboBoxValueRange.addItemListener(new ItemListener()
        {
            private Border etched = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
            @Override
            public void itemStateChanged(ItemEvent e)
            {
                JComboBox cb = (JComboBox)e.getSource();
                if(cb.getSelectedItem().equals(ValueRange.OVERRIDE))
                {
                    panelValue1.setBorder(BorderFactory.createTitledBorder(etched, "Threshold 1"));
                    panelValue2.setBorder(BorderFactory.createTitledBorder(etched, "Threshold 2"));
                }
                else
                {
                    panelValue1.setBorder(BorderFactory.createTitledBorder(etched, "Pair score weight"));
                    panelValue2.setBorder(BorderFactory.createTitledBorder(etched, "Final score weight"));
                }
            }
        });

    }

    public UserSettings.CustomMatrixData getData()
    {
        UserSettings.CustomMatrixData d = new UserSettings.CustomMatrixData();
        d.filename = fileSelector.getFileName();
        d.value1 = U.getdouble(textValue1, 0.0);
        d.value2 = U.getdouble(textValue2, 0.0);

        d.modelType = ((DataModel)comboBoxDataModel.getSelectedItem()).ordinal();
        d.valueRange = ((ValueRange)comboBoxValueRange.getSelectedItem()).ordinal();

        d.leftNetwork = comboBox1.getSelectedIndex() - 1;

        return d;
    }
}
