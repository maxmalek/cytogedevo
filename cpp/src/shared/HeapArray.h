#ifndef HEAPARRAY_H
#define HEAPARRAY_H

// Simplistic min-heap implementation that keeps the "biggest" value in _array[0],
// i.e. the root of the heap.
// Used to extract the N largest elements from a stream or very large list,
// where sorting is inappropriate.

#include <functional>

// (T must implement operator<() )
template <typename T, typename CMP = std::less<T> > class HeapArray
{
private:

	T *_array; 
	unsigned int _size;
	CMP _less;

public:

	HeapArray(T *array, unsigned int size)
		: _array(array), _size(size)
	{
	}

	inline void Add(const T& val)
	{
		// do not insert values that are too small
		if(_less(_array[0], val))
			return;
		_array[0] = val;
		trickleDown(0);
	}

private:

	inline static int parent(unsigned int index)
	{
		return (index - 1) >> 1;
	}
	inline static int leftOf(unsigned int index)
	{
		return (index << 1) + 1;
	}

	void bubbleUp(unsigned int index)
	{
		T item = _array[index];
		int p = parent(index);
		while (index > 0 && _less(_array[p],_array[index]))
		{
			// Parent is smaller - move it down
			_array[index] = _array[p];
			index = p;
			p = parent(index);
		}
		//	Write the item once in its correct place
		_array[index] = item;
	}

	// Push low priority items from the top down
	void trickleDown(unsigned int index)
	{
		T item = _array[index];
		unsigned int child = leftOf(index);
		while (child < _size)
		{
			// be sure we stay inside of the array, if so, choose the smaller element
			if (child + 1 < _size)
			{
				if (_less(_array[child], _array[child + 1]))
					child++;
			}
			// _array[child] points at smaller sibling - choose that one
			_array[index] = _array[child];
			index = child;
			child = leftOf(index);
		}
		// Put the former root in its correct place
		_array[index] = item;
		bubbleUp(index);
	}
};

#endif
