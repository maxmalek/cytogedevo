package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.GedevoNative;
import org.cytoscape.work.TaskMonitor;

public class ImportGraphsTask extends AbstractAlignmentTask
{
    public ImportGraphsTask(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor mon) throws Exception
    {
        mon.setTitle("Preparing Gedevo alignment...");
        mon.setProgress(0);
        double done = 0;
        double step = 1.0 / (common.graphs.length * 2);

        for(int i = 0; i < common.graphs.length && !cancelled; ++i)
        {
            final String name = common.graphs[i].getName();
            mon.setStatusMessage("Importing network " + (i + 1) + ": [" + name + "]");
            final GedevoNative.Network n = GedevoNative.Network.convertToNative(common.graphs[i]);
            mon.setProgress(done += step);
            if (!common.instance.importNetwork(n, i))
                throw new RuntimeException("Failed to import native net " + name);
            mon.setProgress(done += step);
        }

        mon.setProgress(1);
        mon.setStatusMessage("Done");
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
