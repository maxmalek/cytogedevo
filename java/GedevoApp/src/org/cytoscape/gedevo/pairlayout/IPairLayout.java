package org.cytoscape.gedevo.pairlayout;

import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

public abstract class IPairLayout
{
    public abstract void apply(CyNetworkView view);

    public TaskIterator makeTask(final CyNetworkView view)
    {
        return new TaskIterator(new AbstractTask()
        {
            @Override
            public void run(TaskMonitor taskMonitor) throws Exception
            {
                apply(view);
            }
        });
    }

    public static interface Factory
    {
        public IPairLayout create(PairLayoutTask pl);
        public String getName();
    }
}
