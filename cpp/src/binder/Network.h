#ifndef NETWORK_H
#define NETWORK_H

#include "common.h"
#include <vector>

class SimpleNetwork
{
public:
	typedef std::vector<std::string> NodeList;
	typedef std::vector<std::pair<uint, uint> > EdgeList;

	SimpleNetwork() {}
	SimpleNetwork(NodeList& n, EdgeList& e) : _nodes(n), _edges(e) {}


	void addNode(const char *name);
	void addEdge(uint a, uint b);
	bool prepare() const;

	inline const NodeList& getNodeList() const
	{
		return _nodes;
	}

	inline const EdgeList& getEdgeList() const
	{
		return _edges;
	}


protected:
	NodeList _nodes;
	EdgeList _edges;
};


#endif
