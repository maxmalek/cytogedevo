package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.gedevo.simplenet.Node;
import org.cytoscape.gedevo.util.CyNodePair;
import org.cytoscape.model.CyNode;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskMonitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplyMappingTaskFactory extends AbstractAlignmentTask
{
    private ArrayList<Integer> alist, blist, apre, bpre;

    public ApplyMappingTaskFactory(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        List<CyNodePair> pairlist = GedevoAlignmentUtil.getAlignedNodePairs(common.cynet, false); // TODO: how to support pre-matching against NULL?
        Map<CyNode, Integer> node2id = new HashMap<CyNode, Integer>();
        for(int gid = 0; gid < common.graphs.length; ++gid)
            for (Node n : common.graphs[gid].nodes)
                node2id.put(n.cynode, n.id);

        alist = new ArrayList<Integer>();
        blist = new ArrayList<Integer>();
        apre = new ArrayList<Integer>();
        bpre = new ArrayList<Integer>();

        for(CyNodePair p : pairlist)
        {
            assert(p.a != null && p.b != null);

            if(p.fixed)
            {
                apre.add(node2id.get(p.a));
                bpre.add(node2id.get(p.b));
            }
            alist.add(node2id.get(p.a));
            blist.add(node2id.get(p.b));
        }
    }

    public Task makePrematchTask()
    {
        return new AbstractTask()
        {
            @Override
            public void run(TaskMonitor taskMonitor) throws Exception
            {
                if(apre.size() > 0)
                    if(!common.instance.applyPrematch(toarray(apre), toarray(bpre)))
                        throw new RuntimeException("Prematch apply failed");
            }
        };
    }

    public Task makeConstructAgentTask()
    {
        return new AbstractTask()
        {
            @Override
            public void run(TaskMonitor taskMonitor) throws Exception
            {
                if(!common.instance.constructAgent(toarray(alist), toarray(blist)))
                    throw new RuntimeException("Initial agent construct failed");
            }
        };
    }

    private static int[] toarray(List<Integer> c)
    {
        final int sz = c.size();
        int[] a = new int[c.size()];
        for(int i = 0; i < sz; ++i)
            a[i] = c.get(i);
        return a;
    }


    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
