#ifndef WRAP_MT_JNI_H
#define WRAP_MT_JNI_H

#include <jni.h>

struct jni_pass
{
	JNIEnv *jj;
	jclass cls;
};

bool InitMT(void*); // takes jni_pass
void QuitMT();

#endif
