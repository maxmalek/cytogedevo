import org.cytoscape.app.swing.AbstractCySwingApp;
import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.swing.CySwingApplication;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class GedevoStubApp extends AbstractCySwingApp
{
    private CySwingApplication cyapp;
    private CySwingAppAdapter cyadapter;
    private Class<?> appclass = null;
    private Object realapp = null;
    private GedevoStubDynClassLoader loader;
    private GedevoLogWindow logwin;
    private GedevoOutputInterceptor interceptor;

	public GedevoStubApp(CySwingAppAdapter adapter)
	{
		super(adapter);

        cyadapter = adapter;
        cyapp = adapter.getCySwingApplication();
        GedevoStubMenuAction ac;
        final List<GedevoStubMenuAction> menulist = new ArrayList<GedevoStubMenuAction>();

        cyapp.addAction(ac = new GedevoStubMenuAction(this, "(Re-)load Gedevo app")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                app.reload();
            }
        });
        menulist.add(ac);

        cyapp.addAction(ac = new GedevoStubMenuAction(this, "Unload Gedevo app")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                teardownInstance();
            }
        });
        menulist.add(ac);

        cyapp.addAction(ac = new GedevoStubMenuAction(this, "Show debug log")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                logwin.setVisible(true);
            }
        });
        menulist.add(ac);

        cyapp.addAction(ac = new GedevoStubMenuAction(this, "Unload Gedevo + stub loader")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                for(GedevoStubMenuAction a : menulist)
                    cyapp.removeAction(a);
                menulist.clear();
                interceptor.unlink();
                interceptor = null;
                logwin.unload();
                logwin = null;
                cyapp = null;
                cyadapter = null;

                teardownInstance();
            }
        });
        menulist.add(ac);

        logwin = new GedevoLogWindow(cyapp.getJFrame());
        interceptor = new GedevoOutputInterceptor(logwin);
	}

    public CySwingAppAdapter getCyAdapter()
    {
        return cyadapter;
    }

    public static String getStackTrace(Throwable t)
    {
        Writer w = new StringWriter();
        t.printStackTrace(new PrintWriter(w));
        return w.toString();
    }

    public static void msgbox(String s)
    {
        JOptionPane.showMessageDialog(null,s,"Error", JOptionPane.WARNING_MESSAGE);
    }

    public boolean reloadClasses()
    {
        try
        {
            String path = System.getenv("GEDEVO_CLASS_PATH");
            loader = new GedevoStubDynClassLoader(path, getClass().getClassLoader());
            appclass = loader.loadClass("org.cytoscape.gedevo.GedevoApp");
        }
        catch(ClassNotFoundException ex)
        {
            msgbox("Class not found: org.cytoscape.gedevo.GedevoApp");
            return false;
        }

        return true;
    }

    public boolean reload()
    {
        boolean ok = false;
        try
        {
            teardownInstance();
            if(reloadClasses() && createInstance())
            {
                //msgbox("Reload OK");
                ok = true;
            }
            else
                msgbox("Fail.");
        }
        catch(Exception ex)
        {
            msgbox("Exception in reload: " + getStackTrace(ex));
        }
        return ok;
    }

    private boolean createInstance()
    {
        //msgbox("createInstance...");

        if(appclass == null)
            return false;

        Constructor<?> ctor;
        try
        {
            ctor = appclass.getConstructor(CySwingAppAdapter.class);
            ctor.setAccessible(true);
        }
        catch (NoSuchMethodException e)
        {
            msgbox("GedevoApp is missing ctor");
            return false;
        }

        try
        {
            realapp = ctor.newInstance(cyadapter);
        }
        catch(Exception ex)
        {
            msgbox("GedevoApp ctor: Invocation exception: " + getStackTrace(ex));
            return false;
        }

        try
        {
            Method init = realapp.getClass().getDeclaredMethod("initApp");
            init.invoke(realapp);
        }
        catch(Exception ex)
        {
            msgbox("GedevoApp init: Invocation exception: " + getStackTrace(ex));
            // Was loaded, can continue
        }

        return true;
    }

    private void teardownInstance()
    {
        appclass = null;

        if(realapp != null)
        {
            try
            {
                Class<?> cls = realapp.getClass();
                Method teardown = cls.getDeclaredMethod("teardown");
                teardown.invoke(realapp);
            }
            catch(Exception ex)
            {
                msgbox("Instance teardown exception: " + getStackTrace(ex));
            }
        }
        realapp = null;

        System.gc();
        System.runFinalization();

        if(loader != null)
            loader.teardown();
        loader = null;

        System.gc();
        System.runFinalization();
    }

}
