#ifndef PROGRAMSTATE_H
#define PROGRAMSTATE_H

#include <list>

#include "ProteinHolder.h"
#include "DataHolder.h"
#include "AgentMgr.h"
#include "AlgoBase.h"
#include "UserSettings.h"
#include "Logger.h"

class ThreadPool;

class ProgramState
{
public:
	ProgramState(UserSettings& settings, ThreadPool *thpool); // can be NULL
	~ProgramState();

	bool HandleCommandlineCmd(const char *str);

	bool SaveResults(std::string fn);
	void HookSignals();
	void SaveData();

	bool Init1(AlgoBase& wobj);
	bool Init2();

	bool Resume(const std::string& fn);

	// ProteinHolder proxy methods
	bool ReadNetworkSIF(const std::string& fn, uint groupIdx); // for easy cytoscape import
	bool ReadNetworkEDGELIST(const std::string& fn, uint groupIdx, bool hasHeader);

	// Checks abort criterions
	bool IsDone();

	void NextIteration(); // to be called once per iteration
	uint GetRunningTime() const; // in seconds
	uint GetTotalRunningTime() const { return GetRunningTime() + _totalRunningTime; }
	uint GetIterationCount() const { return _iterationCount; }
	uint GetIterationsWithoutScoreChange() const { return _iterationsWithoutScoreChange; }

	inline const AlgoBase *GetWorkObject() const { return workObject; }

	void Shutdown(); // Must be called before a clean exit

	ProteinHolder ph;
	DataHolder dh;
	UserSettings &settings;

	void SetQuit(bool quit = true) { _mustQuit = quit; }
	bool MustQuit() { return _mustQuit; }
	bool IsSaving() { return _isSaving; }

	const time_t& GetStartTime() const { return _startTime; }

	uint networksLoaded; // used by CmdlineParser.cpp

protected:

	bool PrepareLight();
	bool PrepareHeavy();
	bool _Resume();
	bool _ResumeFromFile(const std::string& fn);
	bool _ImportMapping(const std::vector<std::string>& va, const std::vector<std::string>& vb);
	bool _ImportMapping(const std::vector<const Node*>& mynodes, const std::vector<const Node*>& refnodes);

	bool _mustQuit;
	bool _isSaving;
	bool _saveOnQuit;

	uint _iterationCount;
	time_t _startTime;
	size_t _iterationsWithoutScoreChange;
	score _lastScore;
	uint _startIterationCount;
	uint _totalRunningTime; // accumulated from all prev states, but not current
	uint _lastSaveTime;
	uint _seriesCounter;
	AlgoBase *workObject;

	std::list<std::string> _resumeList; // file names with result files to resume

	Logger _logger;
	ThreadPool *thpool;
};

#endif
