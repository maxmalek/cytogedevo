package org.cytoscape.gedevo;

import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Set;

public class GedevoFilters
{
    public static class Filter
    {
        protected final String netname;
        protected final Collection<CyNode> filteredNodes;
        protected final Collection<CyEdge> filteredEdges;
        protected final CyNetwork cynet;

        public Filter(CyNetwork cynet, INodeFilter nf, IEdgeFilter ef)
        {
            this.cynet = cynet;
            netname = cynet.getRow(cynet).get(CyNetwork.NAME, String.class);
            filteredNodes = nf != null ? nf.filterNodes(cynet.getNodeList()) : null;
            filteredEdges = ef != null ? ef.filterEdges(cynet.getEdgeList()) : null;
        }

        public final Collection<CyNode> getNodes() { return filteredNodes; }
        public final Collection<CyEdge> getEdges() { return filteredEdges; }
        public final String getNetworkName() { return netname; }
        public final String getNodeName(CyNode node) { return cynet.getRow(node).get(CyNetwork.NAME, String.class); }
        public final CyNetwork getCyNetwork() { return cynet; }
    }

    public static interface INodeFilter
    {
        public Collection<CyNode> filterNodes(Collection<CyNode> nodes);
    }

    public static interface IEdgeFilter
    {
        public Collection<CyEdge> filterEdges(Collection<CyEdge> edges);
    }

    public static class NodeFilterChain implements INodeFilter
    {
        private ArrayList<INodeFilter> filters = new ArrayList<INodeFilter>();
        public void add(INodeFilter f)
        {
            filters.add(f);
        }
        public Collection<CyNode> filterNodes(Collection<CyNode> nodes)
        {
            for(INodeFilter f : filters)
                nodes = f.filterNodes(nodes);
            return nodes;
        }
    }

    public static class EdgeFilterChain implements IEdgeFilter
    {
        private ArrayList<IEdgeFilter> filters = new ArrayList<IEdgeFilter>();
        public void add(IEdgeFilter f)
        {
            filters.add(f);
        }
        public Collection<CyEdge> filterEdges(Collection<CyEdge> edges)
        {
            for(IEdgeFilter f : filters)
                edges = f.filterEdges(edges);
            return edges;
        }
    }


    public static class AllNodesFilter implements INodeFilter
    {
        @Override
        public Collection<CyNode> filterNodes(Collection<CyNode> nodes)
        {
            return nodes;
        }
    }

    public static class SelectedNodesFilter implements INodeFilter
    {
        private CyNetwork cynet;
        public SelectedNodesFilter(CyNetwork cynet)
        {
            this.cynet = cynet;
        }

        @Override
        public Collection<CyNode> filterNodes(Collection<CyNode> nodes)
        {
            ArrayList<CyNode> ret = new ArrayList<CyNode>();
            for(CyNode n : nodes)
                if(cynet.getRow(n).get(CyNetwork.SELECTED, Boolean.class))
                    ret.add(n);
            return ret;
        }
    }

    public static class AllNodesWithValueFilter<T> implements INodeFilter
    {
        private String field;
        private T value;
        private CyNetwork cynet;

        public AllNodesWithValueFilter(CyNetwork cynet, String field, T value)
        {
            this.field = field;
            this.value = value;
            this.cynet = cynet;
        }

        @Override
        public Collection<CyNode> filterNodes(Collection<CyNode> nodes)
        {
            final Class<? extends T> cls = (Class<? extends T>)value.getClass();
            ArrayList<CyNode> ret = new ArrayList<CyNode>();
            for(CyNode node : nodes)
            {
                CyRow row = cynet.getRow(node);
                if(row != null && value.equals(row.get(field, cls)))
                    ret.add(node);
            }
            return ret;
        }
    }

    public static class AllNonGroupNodesFilter implements INodeFilter
    {
        private CyNetwork cynet;
        public AllNonGroupNodesFilter(CyNetwork cynet)
        {
            this.cynet = cynet;
        }

        @Override
        public Collection<CyNode> filterNodes(Collection<CyNode> nodes)
        {
            // If any grouped nodes exit, this field exists. So if it's not there, no node is grouped and there's nothing to filter out.
            if(cynet.getDefaultNodeTable().getColumn("NumChildren") == null)
                return nodes;

            ArrayList<CyNode> ret = new ArrayList<CyNode>();
            for(CyNode node : nodes)
            {
                CyRow row = cynet.getRow(node);
                if(row != null)
                {
                    Integer ch = row.get("NumChildren", Integer.class);
                    if (ch == null || ch.equals(0))
                        ret.add(node);
                }
            }
            return ret;
        }
    }

    public static class AllEdgesFilter implements IEdgeFilter
    {
        @Override
        public Collection<CyEdge> filterEdges(Collection<CyEdge> edges)
        {
            return edges;
        }
    }

    public static class SelectedEdgesFilter implements IEdgeFilter
    {
        private CyNetwork cynet;
        SelectedEdgesFilter(CyNetwork cynet)
        {
            this.cynet = cynet;
        }

        @Override
        public Collection<CyEdge> filterEdges(Collection<CyEdge> edges)
        {
            ArrayList<CyEdge> ret = new ArrayList<CyEdge>();
            for(CyEdge e : edges)
                if(cynet.getRow(e).get(CyNetwork.SELECTED, Boolean.class))
                    ret.add(e);
            return ret;
        }
    }

    public static class AllEdgesWithValueFilter<T> implements IEdgeFilter
    {
        private String field;
        private T value;
        private CyNetwork cynet;

        public AllEdgesWithValueFilter(CyNetwork cynet, String field, T value)
        {
            this.field = field;
            this.value = value;
            this.cynet = cynet;
        }

        @Override
        public Collection<CyEdge> filterEdges(Collection<CyEdge> edges)
        {
            final Class<? extends T> cls = (Class<? extends T>)value.getClass();
            ArrayList<CyEdge> ret = new ArrayList<CyEdge>();
            for(CyEdge e : edges)
            {
                CyRow row = cynet.getRow(e);
                if(row != null && value.equals(row.get(field, cls)))
                    ret.add(e);
            }
            return ret;
        }
    }

    public static class IgnoreSelfLoopsEdgeFilter implements IEdgeFilter
    {
        private CyNetwork cynet;
        public IgnoreSelfLoopsEdgeFilter(CyNetwork cynet)
        {
            this.cynet = cynet;
        }

        @Override
        public Collection<CyEdge> filterEdges(Collection<CyEdge> edges)
        {
            ArrayList<CyEdge> ret = new ArrayList<CyEdge>();
            for(CyEdge e : edges)
                if(e.getSource() != e.getTarget())
                    ret.add(e);
            return ret;
        }
    }

    public static class AllEdgesFullyInNodeSet implements IEdgeFilter
    {
        private Set<CyNode> nodes;

        public AllEdgesFullyInNodeSet(Set<CyNode> nodes)
        {
            this.nodes = nodes;
        }

        @Override
        public Collection<CyEdge> filterEdges(Collection<CyEdge> edges)
        {
            ArrayList<CyEdge> ret = new ArrayList<CyEdge>();
            for (CyEdge e : edges)
                if (nodes.contains(e.getSource()) && nodes.contains(e.getTarget()))
                    ret.add(e);
            return ret;
        }
    }

    // keep an instance of each around to avoid allocating all the time
    public static final AllNodesFilter allnodes = new AllNodesFilter();
    public static final AllEdgesFilter alledges = new AllEdgesFilter();

    public static class Everything extends Filter
    {
        public Everything(CyNetwork n) { super(n, allnodes, alledges); }
    }

    public static class SelectedNodesAllEdges extends Filter
    {
        public SelectedNodesAllEdges(CyNetwork n) { super(n, new SelectedNodesFilter(n), alledges); }
    }

    public static class AllNodesSelectedEdges extends Filter
    {
        public AllNodesSelectedEdges(CyNetwork n) { super(n, allnodes, new SelectedEdgesFilter(n)); }
    }

    public static class SelectedNodesSelectedEdges extends Filter
    {
        public SelectedNodesSelectedEdges(CyNetwork n) { super(n,  new SelectedNodesFilter(n), new SelectedEdgesFilter(n)); }
    }


    // ---- SIMPLE FUNCTIONS FOR COMMON TASKS ----

    public static Collection<CyEdge> getEdgesWithFixation(CyNetwork cynet, Collection<CyEdge> edges, boolean fixed)
    {
        return (new AllEdgesWithValueFilter<Boolean>(cynet, ColumnNames.IS_FIXED_EDGE, fixed)).filterEdges(edges);
    }
    public static Collection<CyEdge> getEdgesWithFixation(CyNetwork cynet, boolean fixed)
    {
        return getEdgesWithFixation(cynet, cynet.getEdgeList(), fixed);
    }

    public static Collection<CyEdge> getMappingEdges(CyNetwork cynet, Collection<CyEdge> edges)
    {
        return (new AllEdgesWithValueFilter<Boolean>(cynet, ColumnNames.IS_MAPPING_EDGE, true)).filterEdges(edges);
    }
    public static Collection<CyEdge> getMappingEdges(CyNetwork cynet)
    {
        return getMappingEdges(cynet, cynet.getEdgeList());
    }

    public static Collection<CyEdge> getNonMappingEdges(CyNetwork cynet, Collection<CyEdge> edges)
    {
        return (new AllEdgesWithValueFilter<Boolean>(cynet, ColumnNames.IS_MAPPING_EDGE, false)).filterEdges(edges);
    }
    public static Collection<CyEdge> getNonMappingEdges(CyNetwork cynet)
    {
        return getNonMappingEdges(cynet, cynet.getEdgeList());
    }

    public static Collection<CyNode> getNonGroupedNodes(CyNetwork cynet, Collection<CyNode> nodes)
    {
        return (new AllNonGroupNodesFilter(cynet)).filterNodes(nodes);
    }

}
