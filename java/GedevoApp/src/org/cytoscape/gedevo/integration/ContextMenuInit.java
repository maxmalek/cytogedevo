package org.cytoscape.gedevo.integration;

import org.cytoscape.gedevo.ContextMenuAction;
import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.view.model.CyNetworkView;

public class ContextMenuInit
{
    public static void create(final GedevoApp app)
    {
        app.addViewContextMenuEntry("GEDEVO", "Include mapped partner nodes to selection", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                UIShortcuts.includePartnerNodesToSelection(cyview);
            }
        });
        app.addViewContextMenuEntry("GEDEVO", "Generate mapping edges", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                UIShortcuts.reconstructAlignmentEdges(cyview);
            }
        });
        app.addViewContextMenuEntry("GEDEVO", "Remove non-fixed mapping edges", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                UIShortcuts.removeNonFixedMappingEdges(cyview);
            }
        });
        app.addViewContextMenuEntry("GEDEVO", "Fixate selected mapping edges (and nodes)", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                UIShortcuts.fixateSelectedEdgesAndNodes(cyview);
            }
        });

        app.addViewContextMenuEntry("GEDEVO", "Construct combined network from current mapping", new ContextMenuAction()
        {
            @Override
            public void onClick(final CyNetworkView cyview)
            {
                UIShortcuts.createCombinedNetwork(cyview);
            }
        });

        app.addViewContextMenuEntry("GEDEVO import/export", "Export (minimal)", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                UIShortcuts.exportAlignment(cyview, false, false);
            }
        });

        app.addViewContextMenuEntry("GEDEVO import/export", "Export (annotated)", new ContextMenuAction()
        {
            @Override
            public void onClick(CyNetworkView cyview)
            {
                UIShortcuts.exportAlignment(cyview, true, false);
            }
        });

        app.addViewContextMenuEntry("GEDEVO import/export", "Import", new ContextMenuAction()
        {
            @Override
            public void onClick(final CyNetworkView cyview)
            {
                UIShortcuts.importAlignment(cyview);
            }
        });
    }
}


