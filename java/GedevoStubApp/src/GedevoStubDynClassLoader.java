import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.TreeMap;
import java.util.Vector;

public final class GedevoStubDynClassLoader extends ClassLoader
{
    private String prefix;
    private TreeMap<String, Class> classmap = new TreeMap<String, Class>();

    public GedevoStubDynClassLoader(String pre, ClassLoader parent)
    {
        super(parent);
        prefix = pre;
    }

    private static byte[] _loadFile(String file) throws IOException
    {
        URL myUrl = new URL(file);
        URLConnection connection = myUrl.openConnection();
        InputStream strm = connection.getInputStream();

        ByteArrayOutputStream buffer = new ByteArrayOutputStream();

        while(true)
        {
            int data = strm.read();
            if(data < 0)
                break;
            buffer.write(data);
        }
        strm.close();

        byte[] ret =  buffer.toByteArray();
        buffer.close();
        return ret;
    }

    protected URL findResource(String name)
    {
        URL url = null;
        try
        {
            int lastdot = name.lastIndexOf('.');
            String front = name.substring(0, lastdot);
            String ext = name.substring(lastdot);
            String fixed = "file:" + prefix + "/" + front.replace('.', '/') + ext;
            url = new URL(fixed);
        } catch (MalformedURLException e)
        {
        }
        return url;
    }

    @Override
    public Class loadClass(String name) throws ClassNotFoundException
    {
        Class cls = classmap.get(name);
        String err = null;

        if(cls == null)
        {
            //System.out.println("Load class [" + name + "]...");
            try
            {
                byte[] array = _loadFile("file:" + prefix + "/" + name.replace('.', '/') + ".class");
                cls = defineClass(name, array, 0, array.length);
                if(cls != null)
                {
                    resolveClass(cls);
                    classmap.put(name, cls);
                }
            }
            catch (Exception ex)
            {
                err = GedevoStubApp.getStackTrace(ex);
            }
        }

        if(cls == null)
        {
            try
            {
                cls = super.loadClass(name);
            }
            catch(Exception ex) {}
        }

        if(cls == null && err != null)
        {
            GedevoStubApp.msgbox("GedevoStubDynClassLoader [" + name + "] error:\n" + err);
        }

        return cls;
    }

    // HACK: Very unsafe, but good enough for debugging
    private void unloadNativeLibs()
    {
        try
        {
            Field field = ClassLoader.class.getDeclaredField("nativeLibraries");
            field.setAccessible(true);
            Vector libs = (Vector) field.get(this);
            for (Object o : libs)
            {
                System.out.println("Unload native lib: " + o +  " ...");
                Method finalize = o.getClass().getDeclaredMethod("finalize");
                finalize.setAccessible(true);
                finalize.invoke(o);
                System.out.println("Unload native lib: " + o + " done.");
            }
            libs.clear();
        }
        catch(Exception ex)
        {
            GedevoStubApp.msgbox(GedevoStubApp.getStackTrace(ex));
        }
    }

    public void teardown()
    {
        classmap.clear();
        unloadNativeLibs();
    }

}
