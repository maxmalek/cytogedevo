package org.cytoscape.gedevo.integration.customlayoutmenu;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.work.TaskManager;

import java.util.Set;

public class LayoutLauncher
{
    public final CyLayoutAlgorithm layout;
    public final TaskManager tm;
    public final CyApplicationManager appMgr;

    public LayoutLauncher(CyLayoutAlgorithm layout, CyApplicationManager appMgr, TaskManager tm)
    {
        this.layout = layout;
        this.appMgr = appMgr;
        this.tm = tm;
    }

    public void doLayout( Set<View<CyNode>> views, String attrib)
    {
        for (CyNetworkView view : this.appMgr.getSelectedNetworkViews())
        {
            this.tm.execute(this.layout.createTaskIterator(view, this.layout.getDefaultLayoutContext(), views, attrib));
            view.updateView();
        }
    }
}
