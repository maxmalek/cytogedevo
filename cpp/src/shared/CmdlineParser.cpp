#include "CmdlineParser.h"
#include "ProgramState.h"
#include <float.h>

typedef int (*CfgFunc)(ProgramState&, const char * const *);

struct CmdlineEntry
{
	const char *opt;
	CfgFunc func;
	int params;
	int pass;
};

// returns how many params have been used, -1 on error
static int cmdKeepWorkfiles(ProgramState& state, const char * const * args);
static int cmdMaxiter(ProgramState& state, const char * const * args);
static int cmdMaxsecs(ProgramState& state, const char * const * args);
static int cmdMaxsame(ProgramState& state, const char * const * args);
static int cmdLogfile(ProgramState& state, const char * const * args);
static int cmdLogiter(ProgramState& state, const char * const * args);
static int cmdThreads(ProgramState& state, const char * const * args);
static int cmdConfig(ProgramState& state, const char * const * args);
static int cmdAutosave(ProgramState& state, const char * const * args);
static int cmdSave(ProgramState& state, const char * const * args);
static int cmdResume(ProgramState& state, const char * const * args);
static int cmdTimestamp(ProgramState& state, const char * const * args);
static int cmdDensity(ProgramState& state, const char * const * args);
static int cmdSeries(ProgramState& state, const char * const * args);

static int cmdSif(ProgramState& state, const char * const * args);
static int cmdEdgeList(ProgramState& state, const char * const * args);
static int cmdEdgeListHeader(ProgramState& state, const char * const * args);

static int cmdPop(ProgramState& state, const char * const * args);
static int cmdUndirected(ProgramState& state, const char * const * args);
static int cmdPrematch(ProgramState& state, const char * const * args);
static int cmdIgnoreSelfLoops(ProgramState& state, const char * const * args);
static int cmdTrim(ProgramState& state, const char * const * args);
static int cmdGreedyInit(ProgramState& state, const char * const * args);

static int cmdCustom(ProgramState& state, const char * const * args);


// Note: if you change this, update CmdHelp.cpp if necessary!
static CmdlineEntry cmdtab[] =
{
	{ "maxiter",        cmdMaxiter,       1,    0 },  // integer
	{ "maxsecs",        cmdMaxsecs,       1,    0 },  // integer
	{ "maxsame",        cmdMaxsame,       1,    0 },  // integer
	{ "logfile",        cmdLogfile,       1,    0 },  // filename
	{ "logiter",        cmdLogiter,       1,    0 },  // integer
	{ "threads",        cmdThreads,       1,    0 },  // numThreads
	{ "config",         cmdConfig,        1,    0 },
	{ "c",              cmdConfig,        1,    0 },
	{ "autosave",       cmdAutosave,      1,    0 },  // autosaveSecs
	{ "save",           cmdSave,          1,    0 },  // resultfile.ext
	{ "density",        cmdDensity,       1,    0 },  // float
	{ "series",         cmdSeries,        0,    0 },

	{ "sif",            cmdSif,           1,    1 },  // file.sif
	{ "edgelist",       cmdEdgeList,      1,    1 },  // file.edgelist
	{ "edgelisth",      cmdEdgeListHeader,1,    1 },  // file.edgelisth
	{ "custom",         cmdCustom,        6,    1 },  // file.pairlist leftNetwork DataModel ValueRange PairScore FinalScore

	{ "resume",         cmdResume,        1,    2 },  // file.txt

	// shortcuts
	{ "pop",            cmdPop,           1,    0 },  // integer
	{ "trim",           cmdTrim,          0,    0 },
	{ "undirected",     cmdUndirected,    0,    0 },
	{ "u",              cmdUndirected,    0,    0 },
	{ "prematch",       cmdPrematch,      0,    0 },
	{ "keep-workfiles", cmdKeepWorkfiles, 0,    0 },
	{ "ignore-self-loops", cmdIgnoreSelfLoops, 0,0},
	{ "greedy-init",    cmdGreedyInit,    0,    0 },
	{ "timestamp",      cmdTimestamp,     0,    0 },


	{ NULL, NULL }
};

const int MAX_PASS = 2; // max pass value in table above

CmdlineParser::CmdlineParser(int argc, char **argv)
	: argc(argc - 1)
	, argv(argv + 1)
{
}

bool CmdlineParser::Apply(ProgramState& state)
{
	for(int i = 0; i <= MAX_PASS; ++i)
		if(!_ApplyPass(state, i))
			return false;

	// check that nothing is missing
	if(state.networksLoaded != 2)
	{
		// too many networks is already checked in the option handlers
		printf("Error: Not enough networks loaded. You need to load exactly 2 networks.\n");
		return false;
	}

	return true;
}

bool CmdlineParser::_ApplyPass(ProgramState& state, int pass)
{
	int idx = 0;
	while(idx < argc)
	{
		int result = _HandleEntry(idx, state, pass);
		if(result < 0)
		{
			printf("There were command line errors, bailing out...\n");
			return false;
		}
		else
			idx += result + 1;
	}
	return true;
}

int CmdlineParser::_HandleEntry(int idx, ProgramState& state, int pass)
{
	const char *e = argv[idx];

	if(!e)
		return -1;

	int paramsRemain = argc - idx - 1;
	if(paramsRemain < 0)
		return -1;

	if(*e != '-')
	{
		printf("Error: No context for \"%s\"\n", e);
		return -1;
	}

	// strip "-", "--", ... from option
	while(*e && *e == '-')
		++e;
	if(!*e)
	{
		printf("Error: Malformed command line: empty option\n");
		return -1;
	}

	// scan forward until first -param is found
	int paramsAvail = paramsRemain;
	for(int i = 1; i < paramsRemain; ++i)
		if(*argv[idx + i] == '-')
		{
			paramsAvail = i - 1;
			break;
		}

	// Look for handler in table
	for(uint i = 0; cmdtab[i].func; ++i)
	{
		if(!strcmp(e, cmdtab[i].opt))
		{
			if(paramsAvail >= cmdtab[i].params)
			{
				if(cmdtab[i].pass != pass)
					return cmdtab[i].params;

				printf("Option: %s\n", e);
				int res = cmdtab[i].func(state, &argv[idx+1]);
				if(res < 0)
					return res;

				return cmdtab[i].params;
			}
			else
			{
				printf("Error: Option \"%s\": Need %u params, got %u\n", e, cmdtab[i].params, paramsAvail);
				return -1;
			}
		}
	}

	printf("Error: Unknown option: \"%s\"\n", e);
	return -1;
}


static int cmdKeepWorkfiles(ProgramState& state, const char * const * /*args*/)
{
	state.settings.keepWorkfiles = 1;
	return 0;
}

static int cmdMaxiter(ProgramState& state, const char * const * args)
{
	state.settings.abort_iterations = atoi(args[0]);
	return 1;
}

static int cmdMaxsecs(ProgramState& state, const char * const * args)
{
	state.settings.abort_seconds = atoi(args[0]);
	return 1;
}

static int cmdMaxsame(ProgramState& state, const char * const * args)
{
	state.settings.abort_nochange = atoi(args[0]);
	return 1;
}

static int cmdLogfile(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	state.settings.logger_file = args[0];
	return 1;
}

static int cmdLogiter(ProgramState& state, const char * const * args)
{
	state.settings.logger_iterations = atoi(args[0]);
	return 1;
}

static int cmdThreads(ProgramState& state, const char * const * args)
{
	state.settings.numThreads = atoi(args[0]);
	return 1;
}

static int cmdTimestamp(ProgramState& state, const char * const * args)
{
	state.settings.saveResultsAddTimeStamp = 1;
	return 0;
}

static int cmdDensity(ProgramState& state, const char * const * args)
{
	state.settings.pairNullValue = (score)strtod(args[0], NULL);
	if(state.settings.pairNullValue <= 0)
	{
		printf("Error: Option `--density`: Invalid input, must be a number and > 0.\n");
		return -1;
	}

	return 1;
}

static int cmdConfig(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	if(!state.HandleCommandlineCmd(args[0]))
	{
		printf("No such setting: '%s'\n", args[0]);
		return -1;
	}

	return 1;
}

static int cmdSave(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	state.settings.saveResultsFileName = args[0];
	return 1;
}

static int cmdAutosave(ProgramState& state, const char * const * args)
{
	if(!*args[0])
		return -1;
	state.settings.autosaveSecs = atoi(args[0]);
	return 1;
}

static int cmdSeries(ProgramState& state, const char * const * /*args*/)
{
	state.settings.saveSeries = 1;
	return 0;
}

// common network loading preparation, returns 
static int _loadNetworkGetIdx(ProgramState& state, const char * name, const char *cmd)
{
	if(!name[0])
		return -1;

	const uint group = state.networksLoaded++;
	if(group > 1)
	{
		printf("Error: option '%s': Attempt to load too many networks!", cmd);
		return -1;
	}
	state.ph.SetNetworkFileName(group, name);
	return group;
}

static int cmdSif(ProgramState& state, const char * const * args)
{
	int group = _loadNetworkGetIdx(state, args[0], "sif");
	if(group < 0 && !state.ReadNetworkSIF(args[0], group))
		return -1;

	return 1;
}

static int _cmdEdgeList_Ex(ProgramState& state, const char * const * args, bool header, const char *cmd)
{
	int group = _loadNetworkGetIdx(state, args[0], cmd);
	if(group < 0 || !state.ReadNetworkEDGELIST(args[0], group, header))
		return -1;

	return 1;
}

static int cmdEdgeList(ProgramState& state, const char * const * args)
{
	return _cmdEdgeList_Ex(state, args, false, "edgelist");
}

static int cmdEdgeListHeader(ProgramState& state, const char * const * args)
{
	return _cmdEdgeList_Ex(state, args, true, "edgelisth");
}

static int cmdPop(ProgramState& state, const char * const * args)
{
	state.settings.maxAgents = atoi(args[0]);
	return state.settings.maxAgents ? 1 : -1;
}

static int cmdUndirected(ProgramState& state, const char * const * /*args*/)
{
	state.settings.forceUndirectedEdges = 1;
	return 0;
}

static int cmdPrematch(ProgramState& state, const char * const * /*args*/)
{
	state.settings.matchSameNames = 1;
	return 0;
}

static int cmdIgnoreSelfLoops(ProgramState& state, const char * const * /*args*/)
{
	state.settings.ignoreSelfLoops = 1;
	return 0;
}

static int cmdTrim(ProgramState& state, const char * const * /*args*/)
{
	state.settings.trimNames = 1;
	return 0;
}

static int cmdGreedyInit(ProgramState& state, const char * const * /*args*/)
{
	state.settings.evo_greedyInitOnInit = 1;
	return 0;
}

// file.pairlist leftNetwork DataModel ValueRange PairScore FinalScore
static int cmdCustom(ProgramState& state, const char * const * args)
{
	CustomMatrixData data;
	data.filename = args[0];
	data.leftNetwork = atoi(args[1]) - 1; // this will end up being -1 for 0 or invalid input, which is exactly what we need
	if(data.leftNetwork > 1)
	{
		printf("Error: Network ID out of range (use 1 or 2 to specify, or 0 or \"auto\" to autodetect.\n");
		return -1;
	}

	std::string t;
	data.valueRange = CustomMatrixData::INVALID_VALUE_RANGE;
	data.modelType  = CustomMatrixData::INVALID_MODEL_TYPE;
	for(uint i = 2; i <= 3; ++i)
	{
		t = args[i];
		strToLower(t);
		if(t == "d" || t == "dist" || t == "distance")
			data.modelType = CustomMatrixData::DISTANCE;
		else if(t == "s" || t == "sim" || t == "similarity")
			data.modelType = CustomMatrixData::SIMILARITY;
		else if(t == "c" || t == "clamp")
			data.valueRange = CustomMatrixData::CLAMP;
		else if(t == "r" || t == "rescale")
			data.valueRange = CustomMatrixData::RESCALE;
		else if(t == "o" || t == "override")
			data.valueRange = CustomMatrixData::OVERRIDE;
	}
	if(data.valueRange == CustomMatrixData::INVALID_VALUE_RANGE)
	{
		printf("Error: Value range is not set. Can be one of 'clamp', 'rescale', 'override'.\n");
		return -1;
	}
	if(data.modelType == CustomMatrixData::INVALID_MODEL_TYPE)
	{
		printf("Error: Model type is not set. Can be 'distance' or 'similarity'.\n");
		return -1;
	}

	for(uint i = 4; i <= 5; ++i)
	{
		if(!(isdigit(args[i][0]) || args[i][0] == '.')) // accept 0.1 and also .1
		{
			printf("Error: Value%u is not a number. Forgot a param?\n"
			       "Expecting:\n"
			       "  --custom file LeftNetwork DataModel ValueRange Value1 Value2",
			       i-3
			);
			return -1;
		}
	}

	data.value1 = (score)strtod(args[4], NULL);
	data.value2 = (score)strtod(args[5], NULL);

	state.settings.customMatrix.push_back(data);

	return 6;
}

static int cmdResume(ProgramState& state, const char * const * args)
{
	return state.Resume(args[0]) ? 1 : -1;
}

