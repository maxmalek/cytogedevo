package org.cytoscape.gedevo.task;


import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.GedevoFilters;
import org.cytoscape.model.*;
import org.cytoscape.work.TaskMonitor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConstructCombinedNetworkTask extends AbstractAlignmentTask
{
    private GedevoFilters.Filter[] sources;


    public ConstructCombinedNetworkTask(AlignmentTaskData common, GedevoFilters.Filter... sources)
    {
        super(common);
        if(sources.length != 2)
            throw new IllegalArgumentException("need 2 nets");
        this.sources = sources;
    }


    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        taskMonitor.setStatusMessage("Constructing combined network...");
        common.cynet = constructCombinedNetwork();
    }

    private CyNetwork constructCombinedNetwork()
    {
        CyNetworkFactory nf = GedevoApp.appInstance.cyreg.getService(CyNetworkFactory.class);
        CyNetwork net = nf.createNetwork();

        String newname = common.settings.mergedNetworkName;
        if(newname.isEmpty())
        {
            StringBuilder sb = new StringBuilder("GEDEVO_Aligned_[");
            for (int arg = 0; arg < sources.length; ++arg)
                sb.append(sources[arg].getNetworkName()).append(arg + 1 == sources.length ? "]" : "]_[");
            newname = sb.toString();
        }

        net.getRow(net).set(CyNetwork.NAME, newname);

        CyTable netTable = net.getDefaultNetworkTable();
        netTable.createListColumn(ColumnNames.SOURCE_NETWORKS_LIST, String.class, false);

        CyTable nodeTable = net.getDefaultNodeTable();
        nodeTable.createColumn(ColumnNames.SOURCE_NETWORK_ID, Integer.class, false, -1);
        nodeTable.createColumn(ColumnNames.NODE_PARTNER_UID, Long.class, true, -1L);
        nodeTable.createColumn(ColumnNames.NODE_PARTNER_NAME, String.class, false, "");

        CyTable edgeTable = net.getDefaultEdgeTable();
        edgeTable.createColumn(ColumnNames.IS_MAPPING_EDGE, Boolean.class, false, Boolean.FALSE);
        edgeTable.createColumn(ColumnNames.IS_FIXED_EDGE, Boolean.class, false, false);
        edgeTable.createColumn(ColumnNames.SOURCE_NETWORK_ID, Integer.class, false, -1);

        List<String> srcNameList = new ArrayList<String>();

        Map<CyNode, CyNode> old2new = new HashMap<CyNode, CyNode>();
        int netID = 0;
        for(GedevoFilters.Filter src : sources)
        {
            Map<String, Class> nodeCols = getColumnsToCopy(src.getCyNetwork().getDefaultNodeTable(), net.getDefaultNodeTable());
            Map<String, Class> edgeCols = getColumnsToCopy(src.getCyNetwork().getDefaultEdgeTable(), net.getDefaultEdgeTable());

            final String nnname = src.getNetworkName();
            srcNameList.add(nnname);

            for(CyNode srcnode : src.getNodes())
            {
                CyNode newnode = net.addNode();
                old2new.put(srcnode, newnode);
                CyRow newrow = net.getRow(newnode);
                newrow.set(ColumnNames.SOURCE_NETWORK_ID, netID);
                CyRow oldrow = src.getCyNetwork().getRow(srcnode);
                for(Map.Entry<String, Class> e : nodeCols.entrySet())
                    newrow.set(e.getKey(), oldrow.get(e.getKey(), e.getValue()));
            }
            for(CyEdge edge : src.getEdges())
            {
                CyEdge newedge = net.addEdge(old2new.get(edge.getSource()), old2new.get(edge.getTarget()), edge.isDirected());
                CyRow newrow = net.getRow(newedge);
                newrow.set(ColumnNames.SOURCE_NETWORK_ID, netID);
                CyRow oldrow = src.getCyNetwork().getRow(edge);
                for(Map.Entry<String, Class> e : edgeCols.entrySet())
                    newrow.set(e.getKey(), oldrow.get(e.getKey(), e.getValue()));
            }
            netID++;
        }

        netTable.getRow(net.getSUID()).set(ColumnNames.SOURCE_NETWORKS_LIST, srcNameList);

        GedevoAlignmentUtil.distributeUIDs(net);

        return net;
    }

    private static Map<String, Class> getColumnsToCopy(CyTable src, CyTable dst)
    {
        Map<String, Class> colsToCopy = new HashMap<String, Class>();
        for(CyColumn srccol : src.getColumns())
        {
            CyColumn dstcol = dst.getColumn(srccol.getName());
            if (dstcol != null && !(dstcol.getType().equals(srccol.getType())))
            {
                dst.deleteColumn(dstcol.getName());
                dstcol = null;
            }
            // FIXME: handle list columns?
            if (srccol.getListElementType() != null)
                GedevoApp.log("skipping list column: " + srccol.getName());

            VirtualColumnInfo srcvirt = srccol.getVirtualColumnInfo();

            final boolean isvirtcol = srcvirt != null && srcvirt.isVirtual();

            if (dstcol == null && !srccol.isPrimaryKey())
            {
                if (!isvirtcol)
                    dst.createColumn(srccol.getName(), srccol.getType(), srccol.isImmutable(), srccol.getDefaultValue());
                else
                    dst.addVirtualColumn(srccol.getName(), srcvirt.getSourceColumn(), srcvirt.getSourceTable(), srcvirt.getTargetJoinKey(), srccol.isImmutable());
            }

            if(!isvirtcol)
                colsToCopy.put(srccol.getName(), srccol.getType());
            else
                GedevoApp.log("Virtual col: " + srccol.getName());
        }
        return colsToCopy;
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
