package org.cytoscape.gedevo;

import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyTable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ColumnNames
{
    private ColumnNames() {}

    // edge table
    public static final String IS_MAPPING_EDGE = "gedevoIsMappingEdge";
    public static final String IS_FIXED_EDGE = "gedevoFixedMapping";

    // node table
    public static final String NODE_UID = "gedevoUID";
    public static final String NODE_PARTNER_UID = "gedevoPartnerUID";
    public static final String NODE_PARTNER_NAME = "gedevoPartnerName";
    public static final String AGREEMENT = "gedevoAgreement";

    // both
    public static final String SOURCE_NETWORK_ID = "sourceNetworkID";
    public static final String EDIT_TYPE = "gedevoEditType";
    public static final String CCS = "CCS";
    public static final String CCS_SIZE = "CCS_size";


    // network table
    public static final String SOURCE_NETWORKS_LIST = "gedevoSourceNetworks";


    public static class NameAndType
    {
        NameAndType(String name, Class type)
        {
            this.name = name;
            this.type = type;
        }
        public final String name;
        public final Class type;
    }

    private static NameAndType[] mirrored = new NameAndType[]
    {
        new NameAndType(CCS, Integer.class),
        new NameAndType(CCS_SIZE, Integer.class),
        new NameAndType(AGREEMENT, Double.class),
        new NameAndType(EDIT_TYPE, Integer.class),
    };

    public static String buildColumnNameFromScoreName(String scorename)
    {
        String colname = scorename;
        String check = colname.toLowerCase();
        if(!check.startsWith("gedevo"))
            colname = "gedevo" + colname;
        if(!check.contains("score"))
            colname = colname + "Score";
        return colname;
    }

    public static String buildScoreNameFromColumnName(String colname)
    {
        String scorename = colname;
        if(scorename.startsWith("gedevo"))
            scorename = scorename.substring(6); // length of "gedevo"
        return scorename;
    }

    public static List<String> getScoreColumnNames(CyTable tab)
    {
        ArrayList<String> ret = new ArrayList<String>();
        for(CyColumn col : tab.getColumns())
        {
            if (Double.class.equals(col.getType()))
            {
                String name = col.getName();
                String check = name.toLowerCase();
                if (check.startsWith("gedevo") && check.contains("score"))
                    ret.add(name);
            }
        }
        return ret;
    }


    public static ArrayList<NameAndType> getMirroredColumns(CyTable tab)
    {
        ArrayList<NameAndType> a = new ArrayList<NameAndType>(6);
        Collections.addAll(a, mirrored);
        for(String colname : getScoreColumnNames(tab))
            a.add(new NameAndType(colname, tab.getColumn(colname).getType()));
        return a;
    }
}
