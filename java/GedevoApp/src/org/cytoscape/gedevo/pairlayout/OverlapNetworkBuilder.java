package org.cytoscape.gedevo.pairlayout;


import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.gedevo.GedevoFilters;
import org.cytoscape.gedevo.util.CyNodePair;
import org.cytoscape.model.*;

import java.util.*;

public final class OverlapNetworkBuilder
{
    private OverlapNetworkBuilder() {}

    // backmap<node in overlapNet, corresponds to which other nodes in cynet>
    public static Map<CyNode, CyNode> constructOverlapNetwork(final CyNetwork cynet, final CyNetwork overlapNet, final Map<CyNode, CyNode[]> backmap)
    {
        HashMap<CyNode, CyNode> mapping = new HashMap<CyNode, CyNode>();
        HashSet<CyNode> alone = new HashSet<CyNode>();
        HashMap<CyNode, CyNode> old2new = new HashMap<CyNode, CyNode>();

        CyTable nodeTable = overlapNet.getDefaultNodeTable();
        nodeTable.createColumn(ColumnNames.SOURCE_NETWORK_ID, Integer.class, false, -1);
        List<ColumnNames.NameAndType> mirroredNodeCols = initMirrorColumns(nodeTable, cynet.getDefaultNodeTable());

        CyTable edgeTable = overlapNet.getDefaultEdgeTable();
        List<ColumnNames.NameAndType> mirroredEdgeCols = initMirrorColumns(edgeTable, cynet.getDefaultEdgeTable());

        // just put one side in the set - want only one node in the overlap graph that represents two mapped nodes
        fillMapping(cynet, mapping, alone);

        for(CyNode n : alone)
        {
            CyNode other = overlapNet.addNode();
            CyRow otherRow = overlapNet.getRow(other);
            CyRow nRow = cynet.getRow(n);
            int srcNetId = nRow.get(ColumnNames.SOURCE_NETWORK_ID, Integer.class, -1);
            if(srcNetId < 0)
                continue;
            otherRow.set(ColumnNames.SOURCE_NETWORK_ID, srcNetId);
            otherRow.set(CyNetwork.NAME, nRow.get(CyNetwork.NAME, String.class));

            for(ColumnNames.NameAndType nt : mirroredNodeCols)
                otherRow.set(nt.name, nRow.get(nt.name, nt.type));

            if(backmap != null)
                backmap.put(other, new CyNode[]{n});
            old2new.put(n, other);
        }

        for(Map.Entry<CyNode, CyNode> entry : mapping.entrySet())
        {
            CyNode n = entry.getKey();
            CyNode partner = entry.getValue();
            CyNode other = overlapNet.addNode();
            if(backmap != null)
                backmap.put(other, new CyNode[]{n, partner});
            old2new.put(n, other);
            old2new.put(partner, other);

            CyRow nrow = cynet.getRow(n);
            CyRow prow = cynet.getRow(partner);

            CyRow otherRow = overlapNet.getRow(other);
            otherRow.set(CyNetwork.NAME,
                    nrow.get(CyNetwork.NAME, String.class) + "|" +
                    prow.get(CyNetwork.NAME, String.class)
            );
            otherRow.set(ColumnNames.SOURCE_NETWORK_ID, -2); // -2 means both, in this case (-1 is already used for invalid/unassigned)

            for(ColumnNames.NameAndType nt : mirroredEdgeCols)
                otherRow.set(nt.name, nrow.get(nt.name, nt.type));
        }

        final boolean hasEditType = overlapNet.getDefaultEdgeTable().getColumn(ColumnNames.EDIT_TYPE) != null;

        // distribute edges
        for(CyEdge e : getNonMappingEdges(cynet))
        {
            CyNode oldsrc = e.getSource();
            CyNode olddst = e.getTarget();
            CyNode newsrc = old2new.get(oldsrc);
            CyNode newdst = old2new.get(olddst);

            // for substituted edges, there's two - if nodes were already connected, don't add another edge
            // Note: this is crappy and slow and does not respect multiple edges
            if(!overlapNet.getConnectingEdgeList(newsrc, newdst, CyEdge.Type.ANY).isEmpty())
                continue;

            CyEdge otherE = overlapNet.addEdge(newsrc, newdst, e.isDirected());

            if(hasEditType)
            {
                CyRow otherRow = overlapNet.getRow(otherE);
                CyRow nRow = cynet.getRow(e);
                Integer editType = nRow.get(ColumnNames.EDIT_TYPE, Integer.class);
                if(editType != null) // likely that this column was just added and is not initialized
                    otherRow.set(ColumnNames.EDIT_TYPE, editType);
            }
        }

        return old2new;
    }

    private static List<ColumnNames.NameAndType> initMirrorColumns(CyTable tab, CyTable template)
    {
        List<ColumnNames.NameAndType> list = ColumnNames.getMirroredColumns(template);
        for(ColumnNames.NameAndType nt : list)
        {
            CyColumn col = tab.getColumn(nt.name);
            if(col != null && !nt.type.equals(col.getType()))
            {
                tab.deleteColumn(nt.name);
                col = null;
            }
            if(col == null)
                tab.createColumn(nt.name, nt.type, false);
        }
        return list;
    }

    private static void fillMapping(CyNetwork cynet, Map<CyNode, CyNode> mapping, HashSet<CyNode> alone)
    {
        Set<CyNode> seen = new HashSet<CyNode>();

        for(CyNodePair pair : GedevoAlignmentUtil.getAlignedNodePairs(cynet, false))
        {
            CyNode src = pair.a;
            CyNode dst = pair.b;
            if(!seen.contains(src) && !seen.contains(dst))
            {
                seen.add(src);
                seen.add(dst);
                mapping.put(src, dst);
            }
        }

        for(CyNode n : cynet.getNodeList())
            if (!seen.contains(n))
                alone.add(n);
    }

    private static Collection<CyEdge> getNonMappingEdges(CyNetwork cynet)
    {
        return GedevoFilters.getNonMappingEdges(cynet);
    }
}
