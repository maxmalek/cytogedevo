Complete data download, conversion, and cleanup pipeline.

Be sure you have blastp and a Lua interpreter installed.
Any of luajit, lua, lua51, lua52, lua53 is fine.
Luajit is preferred, as it's the fastest of all of them.

To get started, run ./makeall.sh and wait for completion.
!!! Warning: This might take a few days to finish. !!!

Optionally, change settings in config.inc, especially
which data you want to extract, and for which BLAST E-value
matrices should be generated.
If you do NOT want GO data analysis, comment out the last
two steps in makeall.sh.

Also make sure that there is enough disk space free,
recommended 12 GB+, more if more BLAST matrices are to be generated.



After the script finishes, the generated data can be found in "data1".
These can be directly used as input files for (Cyto)GEDEVO.


If you did NOT disable GO data generation:

An addtional data set is created in data2,
that has all possible "DIP-xxxx" replaced with
their accession numbers in Uniprot.
data2/uniprot_sprot.goshrink contains GO terms
for all accession numbers present in the species that
were extracted.

Use ./addgo.sh <list-of-mapping-result-files> to add some
GO based statistics to a mapping, which can then
be further analyzed (e.g. imported in Cytoscape)

Note: Use data2 as input for GEDEVO for a later GO analysis!
Otherwise, it is always possible to convert a mapping file that
was generated with data1 to its data2 counterpart by using

$LUAEXE insertuniprot.lua <nodelist for first col> <nodelist for 2nd col> <input file> <output file>
