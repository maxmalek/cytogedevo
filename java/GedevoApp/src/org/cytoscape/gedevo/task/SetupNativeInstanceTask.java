package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.GedevoNative;
import org.cytoscape.work.TaskMonitor;

public class SetupNativeInstanceTask extends AbstractAlignmentTask
{
    public SetupNativeInstanceTask(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        GedevoNative.Instance ins = common.instance;

        if(ins == null)
            ins = GedevoNative.Instance.create(common.settings);
        else if(!ins.applySettings(common.settings))
            throw new IllegalStateException("Instance existed, but failed to reconfigure -- User settings not accepted");

        if(ins == null)
            throw new RuntimeException("Failed to create native instance");

        ins.setQuit(false);

        common.instance = ins;
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
