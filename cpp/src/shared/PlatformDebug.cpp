#include "PlatformDebug.h"
#include <signal.h>

#if _WIN32
#   define WIN32_LEAN_AND_MEAN
#   include <intrin.h>
#endif

void TriggerBreakpoint()
{
#ifdef _MSC_VER
	__debugbreak();
#elif defined(__GNUC__) && ((__i386__) || (__x86_64__))
	__asm__ __volatile__ ( "int $3\n\t" );
#else
	raise(SIGTRAP);
#endif
}

