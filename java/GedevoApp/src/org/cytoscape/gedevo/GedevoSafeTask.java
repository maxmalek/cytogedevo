package org.cytoscape.gedevo;

import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

public abstract class GedevoSafeTask extends AbstractTask
{
    @Override
    public void run(TaskMonitor mon) throws Exception
    {
        try
        {
            runSafe(mon);
        }
        catch(Exception ex)
        {
            GedevoUtil.msgboxAsync("TASK FAILED!!\n" + GedevoUtil.getStackTrace(ex));
            throw ex;
        }
    }

    protected abstract void runSafe(TaskMonitor mon) throws Exception;
}
