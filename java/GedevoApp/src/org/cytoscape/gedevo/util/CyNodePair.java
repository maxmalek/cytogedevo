package org.cytoscape.gedevo.util;

import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;

public class CyNodePair
{
    public CyNodePair() {}
    public CyNodePair(CyNode a, CyNode b)
    {
        this.a = a;
        this.b = b;
        this.fixed = false;
    }
    public CyNodePair(CyNode a, CyNode b, boolean fixed)
    {
        this.a = a;
        this.b = b;
        this.fixed = fixed;
    }
    public static CyNodePair sorted(CyNode a, CyNode b, boolean fixed, CyNetwork cynet)
    {
        if(a != null && b != null)
        {
            // make a net 0, b net 1
            if (cynet.getRow(b).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class) < cynet.getRow(a).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class))
            {
                CyNode tmp = a;
                a = b;
                b = tmp;
            }

            return new CyNodePair(a, b, fixed);
        }
        else if(a != null)
            return CyNodePair.single(a, fixed, cynet);
        else if(b != null)
            return CyNodePair.single(b, fixed, cynet);

        throw new IllegalArgumentException("null pair");
    }

    public static CyNodePair single(CyNode a, boolean fixed, CyNetwork cynet)
    {
        Integer isrcID = cynet.getRow(a).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class);
        if(isrcID == null)
            return null;

        return isrcID.intValue() == 0
             ? new CyNodePair(a, null, fixed)
             : new CyNodePair(null, a, fixed);
    }


    public CyNode a;
    public CyNode b;
    public boolean fixed;
}
