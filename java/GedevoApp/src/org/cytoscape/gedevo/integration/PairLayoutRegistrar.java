package org.cytoscape.gedevo.integration;

import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.GedevoUtil;
import org.cytoscape.gedevo.Unloadable;
import org.cytoscape.gedevo.integration.customlayoutmenu.LayoutListener;
import org.cytoscape.gedevo.integration.customlayoutmenu.LayoutMenu;
import org.cytoscape.gedevo.pairlayout.GedevoPairLayoutWrapper;
import org.cytoscape.gedevo.pairlayout.IPairLayout;
import org.cytoscape.gedevo.pairlayout.PairLayoutTask;
import org.cytoscape.gedevo.pairlayout.PairLayouts;
import org.cytoscape.util.swing.GravityTracker;
import org.cytoscape.util.swing.JMenuTracker;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.lang.reflect.Method;
import java.util.*;

public class PairLayoutRegistrar extends Unloadable implements LayoutListener
{
    private GedevoApp app;
    private Set<GedevoPairLayoutWrapper> algoWrappers = new HashSet<GedevoPairLayoutWrapper>();
    private JMenu layoutMenu;
    private JMenu mymenu;
    private Map<IPairLayout.Factory, LayoutMenu> submenus = new HashMap<IPairLayout.Factory, LayoutMenu>();
    private GravityTracker layoutMenuTracker;
    private MenuListener layoutMenuListener;
    private ArrayList<JMenuItem> addedMenuItems = new ArrayList<JMenuItem>();
    private double gravity = 13.37;

    PairLayoutRegistrar(GedevoApp a)
    {
        a.log("PairLayoutRegistrar ctor");
        app = a;

        layoutMenu = app.cyapp.getJMenu("Layout");

        // Not a problem if this fails
        obtainMenuHack();

        layoutMenu.addMenuListener(layoutMenuListener = new MenuListener()
        {
            @Override
            public void menuSelected(MenuEvent e)
            {
                mymenu.setEnabled(isPairLayoutApplicable());
            }

            @Override
            public void menuDeselected(MenuEvent e) {}

            @Override
            public void menuCanceled(MenuEvent e) {}
        });

        mymenu = new JMenu("GEDEVO pair layouts");
        populateSubMenu(mymenu);

        _addLayoutMenuItem(mymenu);

        // This needs to be done after setting up everything else
        app.listeners.layoutListener.addLayoutListener(this);
    }

    private boolean isPairLayoutApplicable()
    {
        List<CyNetworkView> selviews = app.cyappmgr.getSelectedNetworkViews();
        if(selviews != null && !selviews.isEmpty())
            for(CyNetworkView view : selviews)
                if(PairLayoutTask.isReady(view))
                    return true;
        return false;
    }

    private void populateSubMenu(JMenu mymenu)
    {
        for(IPairLayout.Factory f : PairLayouts.supported)
        {
            LayoutMenu m = new LayoutMenu(f.getName(), app);
            mymenu.add(m);
            submenus.put(f, m);
        }
    }

    private void _addLayoutMenuItem(JMenuItem item)
    {
        addedMenuItems.add(item);
        if(layoutMenuTracker != null)
        {
            layoutMenuTracker.addMenuItem(item, gravity += 0.001);
            if(!GedevoApp.DEBUG) // impossible to remove the separator again once added
                layoutMenuTracker.addMenuSeparator(gravity += 0.001);
        }
        else
            layoutMenu.add(item);
    }

    private void obtainMenuHack()
    {
        // The following was obtained by examining:
        // path/to/Cytoscape3/framework/system/org/cytoscape/swing-application-impl/.../*.jar:
        // - org.cytoscape.internal.layout.ui.LayoutMenuPopulator
        // - org.cytoscape.internal.view.CytoscapeMenuBar
        JMenuBar bar = app.cyapp.getJMenuBar();
        try
        {
            app.log("Menu bar class: " + bar.getClass().getName());
            Method getMenuTracker = bar.getClass().getMethod("getMenuTracker");
            Object o = getMenuTracker.invoke(bar);
            app.log("getMenuTracker() result: " + o.getClass().getName());
            JMenuTracker tracker = (JMenuTracker)o;
            GravityTracker gt = tracker.getGravityTracker("Layout");

            layoutMenu = (JMenu)gt.getMenu();
            layoutMenuTracker = gt;
            app.log("SUCCESS: PairLayoutRegistrar menu hack");
        }
        catch(Exception ex)
        {
            app.log("PairLayoutRegistrar: Menu hack failed: " + GedevoUtil.getStackTrace(ex));
        }
    }

    private void registerVariant(CyLayoutAlgorithm algo, IPairLayout.Factory pairfactory)
    {
        GedevoPairLayoutWrapper wrap = new GedevoPairLayoutWrapper(algo, pairfactory, app.undo);

        // Intentionally NOT registered as a service (clutters the layout menu and there is nothing to do against it)
        // FIXME: As soon as cytoscape allows adding subcategories in the layout menu, remove the menu hack and use subcategories instead.
        /*
        Properties p = new Properties();
        p.setProperty("preferredMenu", "Apps.Layout.GEDEVO pair layouts");
        p.setProperty("title", algo.toString());
        //p.setProperty("menuGravity", ??); // sadly there doesn't seem to be a way to retrieve this at this point

        app.cyreg.registerService(wrap, CyLayoutAlgorithm.class, p);
        */

        algoWrappers.add(wrap);
        submenus.get(pairfactory).addLayoutWrapper(wrap);

        app.log("Wrapped layout algo: " + algo.toString());
    }

    private void unregisterVariant(CyLayoutAlgorithm algo)
    {
        Iterator<GedevoPairLayoutWrapper> it = algoWrappers.iterator();
        while(it.hasNext())
        {
            GedevoPairLayoutWrapper wrap = it.next();
            if(wrap.getUnderlyingLayout() == algo)
            {
                submenus.get(wrap.getPairLayoutFactory()).removeLayoutWrapper(wrap);
                it.remove();
            }
        }
    }

    @Override
    public void unload()
    {
        for (JMenuItem item : addedMenuItems)
            layoutMenu.remove(item);

        if(layoutMenuListener != null)
            layoutMenu.removeMenuListener(layoutMenuListener);

        layoutMenuListener = null;
        algoWrappers = null;
        app = null;
        layoutMenu = null;
        submenus = null;
        mymenu = null;
    }

    @Override
    public synchronized void addLayout(CyLayoutAlgorithm layout)
    {
        if(!(layout instanceof GedevoPairLayoutWrapper))
            for(IPairLayout.Factory f : submenus.keySet())
                registerVariant(layout, f);
    }

    @Override
    public synchronized void removeLayout(CyLayoutAlgorithm layout)
    {
        if(!(layout instanceof GedevoPairLayoutWrapper))
            unregisterVariant(layout);
    }
}
