#ifndef NODE_H
#define NODE_H

#include <set>
#include <vector>
#include <map>
#include "common.h"

#define GRAPHLET_SIGS 73

class Node;

typedef std::vector<Node*> NodeArray;
typedef std::set<Node*> NodeSet;
typedef std::map<std::string, Node*> NodeMap;

struct NodeAndIDArray
{
	// always same size
	std::vector<uint> nodeId;
	std::vector<const Node*> node;

	inline size_t size() const
	{
		ASSERT(node.size() == nodeId.size());
		return nodeId.size();
	}
	inline void swap(size_t a, size_t b)
	{
		std::swap(nodeId[a], nodeId[b]);
		std::swap(node[a], node[b]);
	}
	inline void reserve(size_t sz)
	{
		nodeId.reserve(sz);
		node.reserve(sz);
	}
	inline void clear()
	{
		nodeId.clear();
		node.clear();
	}
	inline void resize(size_t sz)
	{
		nodeId.resize(sz, 0);
		node.resize(sz, NULL);
	}
	inline void push_back(const Node *p); // defined below
	inline void set(size_t idx, const Node *p);
};

// temporary data that is only required during init time.
// factored out of the main Node class to save a bit of memory later on
struct NodePrototype
{
	NodePrototype() {}

	uint graphlets[GRAPHLET_SIGS];
	score graphletLogs[GRAPHLET_SIGS];

	score degreeLogIn;
	score degreeLogOut;
};

class Node
{
private:
	// forbid copying
	Node(const Node&);

public:
	Node(uint group, uint id, const std::string& name);

	~Node();

	void dropProto()
	{
		delete proto;
		proto = NULL;
	}

	// somehow connected
	inline bool isConnTo(const Node *n) const
	{
		return (nb.find(const_cast<Node*>(n)) != nb.end() );// || ((this == n) && (hasSelfLoop));
	}

	inline bool hasEdgeTo(const Node *n) const
	{
		return (outgoing.find(const_cast<Node*>(n)) != outgoing.end());// || ((this == n) && (hasSelfLoop));
	}

	inline bool hasEdgeFrom(const Node *n) const
	{
		return (incoming.find(const_cast<Node*>(n)) != incoming.end());// || ((this == n) && (hasSelfLoop));
	}

	inline size_t getDegree() const
	{
		return nb.size() + hasSelfLoop;
	}

	inline size_t getDegreeIncoming() const
	{
		return incoming.size() + hasSelfLoop;
	}

	inline size_t getDegreeOutgoing() const
	{
		return outgoing.size() + hasSelfLoop;
	}

	uint id; // internal ID (per group). For any actual node, this is never 0.
	const uint group; // internal group ID (0 or 1) - used as array index!

	// -- fast accessors --
	// These vectors differ from those below! Here, outgoing/incoming does NOT contain undirected edges!
	// (sorted by name (same as the originating sets), used for fast GED lookup)

	NodeAndIDArray outgoingv; // this ---> other, strictly
	NodeAndIDArray incomingv; // this <--- other, strictly
	NodeAndIDArray undirectedv; // this <--> other
	NodeAndIDArray allv; //  all neighbors 

	int hasSelfLoop;

	inline const std::string& name() const { return _name; }


	// -- slow accessors --

	NodeSet outgoing; // this ---> other, and undirected
	NodeSet incoming; // this <--- other, and undirected
	NodeSet nb; // all neighbors


	// -- misc --
	int prematched; // > 0 if there is a predetermined partner for this node. -1 if not. Used by ProteinHolder only.
	int tag; // custom user data (used for java bindings)

	NodePrototype *proto; // deleted after precalc

protected:

	std::string _name;
};

inline void NodeAndIDArray::push_back(const Node *p)
{
	nodeId.push_back(p ? p->id : 0);
	node.push_back(p);
}

inline void NodeAndIDArray::set(size_t idx, const Node *p)
{
	nodeId[idx] = (p ? p->id : 0);
	node[idx] = p;
}



#endif
