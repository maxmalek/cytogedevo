package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.*;
import org.cytoscape.gedevo.util.CyNodePair;
import org.cytoscape.model.*;
import org.cytoscape.work.TaskManager;
import org.cytoscape.work.TaskMonitor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.regex.Pattern;

public class ImportAlignmentFileTask extends GedevoTask
{
    private static Pattern splitRegex = Pattern.compile("\\s+");

    private File infile;
    private boolean isSimple = false;
    private CyNetwork cynet;
    private int linesRead = 0;
    private String errStr = "";

    public static ImportAlignmentFileTask askForLoad(CyNetwork cynet)
    {
        File which = FileSelectorBox.ask(false);
        return which != null ? new ImportAlignmentFileTask(cynet, which) : null;
    }

    public ImportAlignmentFileTask(CyNetwork cynet, File f)
    {
        this.cynet = cynet;
        this.infile = f;
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }

    private boolean autodetectFormat(String line)
    {
        String[] parts = splitRegex.split(line);
        isSimple = parts.length == 2;
        return parts.length >= 2;
    }

    private String getNextLine(BufferedReader br) throws IOException
    {
        String line;
        while ((line = br.readLine()) != null)
        {
            ++linesRead;
            if (!line.startsWith("#") && !line.trim().isEmpty())
                return line;
        }
        return null;
    }

    @Override
    public void run(TaskMonitor mon) throws Exception
    {
        mon.setTitle("Importing alignment file");
        BufferedReader br = new BufferedReader(new FileReader(infile));

        try
        {
            runUnsafe(br, mon);
        }
        finally
        {
            br.close();
        }
    }

    public void runUnsafe(BufferedReader br, TaskMonitor mon) throws Exception
    {
        String line = getNextLine(br);
        if (!autodetectFormat(line))
            throw new Exception("Malformed input / Unable to deal with file type (while reading header)");

        if (isSimple)
        {
            ArrayList<String> p1 = new ArrayList<String>();
            ArrayList<String> p2 = new ArrayList<String>();
            // use the line still in the buffer first
            do
            {
                String[] parts = splitRegex.split(line);
                switch (parts.length)
                {
                    case 0:
                    case 1:
                        throw new Exception("Malformed input / Unable to deal with file type");

                    default:
                        throw new Exception("Trailing data on line " + linesRead);

                    case 2:
                        p1.add(parts[0]);
                        p2.add(parts[1]);
                        break;
                }
            }
            while ((line = getNextLine(br)) != null);

            ArrayList<CyNodePair> pairs = generatePairList(p1, p2, null);
            importAlignmentResult(pairs, null);
        }
        else
        {
            readRich(br, line, mon);
        }

        if(!errStr.isEmpty())
        {
            mon.showMessage(TaskMonitor.Level.WARN, errStr);
            // ... and because warnings are not prominently displayed:
            GedevoUtil.msgboxNonBlock(errStr);
        }
    }

    private void readRich(BufferedReader br, String def, TaskMonitor mon) throws IOException, Exception
    {
        HashMap<String, ArrayList<String>> colsByName = new HashMap<String, ArrayList<String>>();
        ArrayList<String> p1, p2;
        {
            // first line (= the line passed) contains the column names
            String[] colhdr = splitRegex.split(def);
            final int numCols = colhdr.length;

            ArrayList<String>[] colsIndexed = new ArrayList[numCols];
            for (int i = 0; i < numCols; ++i)
            {
                ArrayList<String> a = new ArrayList<String>();
                colsByName.put(colhdr[i], a);
                colsIndexed[i] = a;
            }

            p1 = colsByName.get("P1");
            p2 = colsByName.get("P2");
            if (p1 == null || p2 == null)
                throw new IllegalStateException("Not a valid alignment file: Columns are either not annotated, or P1 & P2 columns are missing.");

            for (String line; (line = getNextLine(br)) != null; )
            {
                String[] parts = splitRegex.split(line);
                if (parts.length < numCols)
                    throw new Exception("Malformed input / Unable to deal with file type (truncated line)");

                if (parts.length > numCols)
                    throw new Exception("Trailing data on line " + linesRead);

                for (int i = 0; i < numCols; ++i)
                    colsIndexed[i].add(parts[i]);
            }
        }

        ArrayList<String> info = colsByName.get("Info");
        ArrayList<CyNodePair> pairs = generatePairList(p1, p2, info);

        // parse strings and pass over scores
        HashMap<String, double[]> scores = new HashMap<String, double[]>();

        for(Map.Entry<String, ArrayList<String>> e : colsByName.entrySet())
        {
            if(e.getKey().toLowerCase().contains("score"))
            {
                ArrayList<String> a = e.getValue();
                double[] vals = new double[a.size()];
                for(int i = 0; i < vals.length; ++i)
                    vals[i] = Double.parseDouble(a.get(i));

                scores.put(e.getKey(), vals);
            }
        }

        importAlignmentResult(pairs, scores);

        // add fixed prematch edges
        ArrayList<CyNodePair> fixedPairs = new ArrayList<CyNodePair>();
        for(CyNodePair p : pairs)
            if(p.fixed)
                fixedPairs.add(p);
        // this overwrites any conflicting fixed pairs
        GedevoAlignmentUtil.mapPairs(cynet, fixedPairs, true);
    }

    void importAlignmentResult(ArrayList<CyNodePair> pairs, HashMap<String, double[]> scores)
    {
        double[][] saa = null;
        String[] scoreNames = null;
        int wpos = 0;

        if (scores != null)
        {
            saa = new double[scores.size()][];
            scoreNames = new String[scores.size()];
            for (Map.Entry<String, double[]> e : scores.entrySet())
            {
                saa[wpos] = e.getValue();
                scoreNames[wpos] = e.getKey();
                ++wpos;
            }
        }

        CyNode[][] naa = new CyNode[2][];
        naa[0] = new CyNode[pairs.size()];
        naa[1] = new CyNode[pairs.size()];
        wpos = 0;
        for(CyNodePair p : pairs)
        {
            naa[0][wpos] = p.a;
            naa[1][wpos] = p.b;
            ++wpos;
        }

        // keep the fixed pairs around if any
        GedevoAlignmentUtil.deleteAllNonFixedMappingEdges(cynet);

        fixupTable(cynet.getDefaultNodeTable());
        fixupTable(cynet.getDefaultEdgeTable());

        // this always adds pairs as non-fixed (if they are, fixed edges are created after this function returns)
        GedevoAlignmentUtil.linkPartnerNodes(cynet, naa, saa, scoreNames);
    }

    static void fixupTable(CyTable tab)
    {
        // if the alignment file didn't have all the nodes, and this is an existing alignment, there might be leftover table data
        // that would not get updated.
        // -> clear all, everything will be set in linkPartnerNodes()
        clearColumn(tab, ColumnNames.NODE_PARTNER_NAME, "");
        clearColumn(tab, ColumnNames.NODE_PARTNER_UID, -1L);
    }

    static void clearColumn(CyTable tab, String colname, Object def)
    {
        CyColumn col = tab.getColumn(colname);
        if(col == null)
            return;

        for(CyRow row : tab.getAllRows())
            row.set(colname, def);
    }

    private int overlap(Collection<String> tofind, Set<String> haystack)
    {
        int c = 0;
        for(String s : tofind)
            if(haystack.contains(s))
                ++c;
        return c;
    }

    private ArrayList<CyNodePair> generatePairList(ArrayList<String> p1, ArrayList<String> p2, ArrayList<String> info)
    {
        // try to figure out which column is which group.
        // the arrangement that gets more hits wins and is taken.
        HashMap<String, CyNode> h1 = getNodesAndNamesByGroup(0);
        HashMap<String, CyNode> h2 = getNodesAndNamesByGroup(1);

        int thisway = overlap(p1, h1.keySet()) + overlap(p2, h2.keySet());
        int otherway = overlap(p2, h1.keySet()) + overlap(p1, h2.keySet());

        if(thisway >= otherway)
            return _generatePairList(h1, h2, p1, p2, info);

        GedevoApp.appInstance.log("ImportAlignmentFileTask: swapping node name columns");
        return _generatePairList(h1, h2, p2, p1, info); // swap
    }

    private ArrayList<CyNodePair> _generatePairList(HashMap<String, CyNode> h1, HashMap<String, CyNode> h2, ArrayList<String> p1, ArrayList<String> p2, ArrayList<String> info)
    {
        ArrayList<CyNodePair> ret = new ArrayList<CyNodePair>(p1.size());
        Set<String> fails = new HashSet<String>();
        int realpairs = 0;
        for(int i = 0; i < p1.size(); ++i)
        {
            boolean fixed = info != null && info.get(i).contains("prematched");
            CyNode a = h1.get(p1.get(i));
            CyNode b = h2.get(p2.get(i));
            if(a == null && !h1.containsKey(p1.get(i)))
                fails.add(p1.get(i));
            if(b == null && !h2.containsKey(p2.get(i)))
                fails.add(p2.get(i));
            if(a != null || b != null)
                ret.add(CyNodePair.sorted(a, b, fixed, cynet));
            if(a != null && b != null)
                ++realpairs;
        }
        if(!fails.isEmpty())
        {
            errStr += "Warning: " + fails.size() + " unique nodes specified in the alignment could not be found in the network.\n"
                    + "Check that input networks and alignment are compatible and from the same data set.\n"
                    + ret.size() + " mappings were imported, of which " + realpairs + " were actual node pairs.";
        }
        return ret;
    }

    private HashMap<String, CyNode> getNodesAndNamesByGroup(int group)
    {
        HashMap<String, CyNode> ret = new HashMap<String, CyNode>();

        Collection<CyNode> nodes =
            (new GedevoFilters.AllNodesWithValueFilter<Integer>(cynet, ColumnNames.SOURCE_NETWORK_ID, group))
                    .filterNodes(cynet.getNodeList());

        for(CyNode n : nodes)
        {
            String name = cynet.getRow(n).get(CyNetwork.NAME, String.class);
            ret.put(name, n);
        }

        // "-" is used for the non-existing node aka mapping against NULL,
        // add this explicitly in case of user stupidity
        ret.put("-", null);

        return ret;
    }
}
