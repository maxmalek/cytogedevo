package org.cytoscape.gedevo;

import org.cytoscape.gedevo.simplenet.Graph;
import org.cytoscape.gedevo.simplenet.Node;
import org.cytoscape.gedevo.util.CyNodePair;
import org.cytoscape.model.*;
import org.cytoscape.model.subnetwork.CyRootNetwork;

import java.util.*;

public final class GedevoAlignmentUtil
{
    private GedevoAlignmentUtil() {}

    public static boolean isGEDEVONetwork(CyNetwork cynet)
    {
        if(cynet == null)
            throw new NullPointerException("isGEDEVONetwork: null (should not happen!)");
        return cynet.getDefaultNodeTable().getColumn(ColumnNames.NODE_PARTNER_UID) != null;
    }

    public static boolean checkIsGEDEVONetwork(CyNetwork cynet)
    {
        boolean ok = isGEDEVONetwork(cynet);
        if (!ok)
            throw new IllegalStateException("Network is not a GEDEVO alignment!");
        return ok;
    }

    // DOES NOT WORK
    /*public static void createAlignmentTable(CyNetwork cynet)
    {
        CyTable tab = cynet.getTable(CyNetwork.class, "GEDEVO");
        if(tab != null)
            return;

        CyTableManager tmgr = GedevoApp.appInstance.cyreg.getService(CyTableManager.class);
        CyTableFactory tf = GedevoApp.appInstance.cyreg.getService(CyTableFactory.class);
        MapTableToNetworkTablesTaskFactory mapnet = GedevoApp.appInstance.cyreg.getService(MapTableToNetworkTablesTaskFactory.class);

        tab = tf.createTable("GEDEVO", "id", Integer.class, true, true); // public, mutable
        tab.createColumn("Node1.SUID", Long.class, false);
        tab.createColumn("Node2.SUID", Long.class, false);

        List<CyNetwork> netlist = new ArrayList<CyNetwork>();
        netlist.add(cynet);
        GedevoApp.appInstance.cytaskmgr.execute(mapnet.createTaskIterator(tab, false, netlist, CyNetwork.class));
    }*/


    public static void distributeUIDs(CyNetwork cynet)
    {
        GedevoApp.log("distribute UIDs...");
        CyTable nodeTable = cynet.getDefaultNodeTable();
        CyColumn col = nodeTable.getColumn(ColumnNames.NODE_UID);
        if(col == null)
            nodeTable.createColumn(ColumnNames.NODE_UID, Long.class, true, -1L);

        ArrayList<CyNode> todo = new ArrayList<CyNode>();
        long maxuid = -1;
        for(CyNode n : GedevoFilters.getNonGroupedNodes(cynet, cynet.getNodeList()))
        {
            CyRow row = cynet.getRow(n);
            long uid = row.get(ColumnNames.NODE_UID, Long.class, -1L);
            if(uid < 0)
                todo.add(n);
            else
                maxuid = Math.max(uid, maxuid);
        }
        GedevoApp.log("distribute UIDs: maxuid = " + maxuid + ", todo: " + todo.size());

        for(CyNode n : todo)
        {
            CyRow row = cynet.getRow(n);
            row.set(ColumnNames.NODE_UID, ++maxuid);
        }
    }

    public static CyNode[] getCyNodesFromNodes(Node[] a)
    {
        CyNode[] cy = new CyNode[a.length];
        for(int i = 0; i < a.length; ++i)
            cy[i] = a[i].cynode;
        return cy;
    }

    public static void linkPartnerNodes(CyNetwork cynet, AlignmentResult res, Graph[] gs)
    {
        Node[][] nodes = new Node[gs.length][];
        for(int i = 0; i < gs.length; ++i)
            nodes[i] = gs[i].nodes;

        linkPartnerNodes(cynet, res, nodes);
    }

    public static void linkPartnerNodes(CyNetwork cynet, AlignmentResult res, Node[][] nodes)
    {
        CyNode[][] cynodes = new CyNode[nodes.length][];
        for(int k = 0; k < nodes.length; ++k)
            cynodes[k] = getCyNodesFromNodes(nodes[k]);
        linkPartnerNodes(cynet, res, cynodes);
    }

    private static void assignScoresToRow(double[][] scores, List<String> colnames, CyRow row, int i)
    {
        for(int si = 0; si < scores.length; ++si)
            row.set(colnames.get(si), scores[si][i]);
    }

    private static List<String> createScoreColumns(CyTable tab, String[] scoreNames)
    {
        List<String> used = new ArrayList<String>();
        if(scoreNames != null)
        {
            for (String name : scoreNames)
            {
                String colname = ColumnNames.buildColumnNameFromScoreName(name);
                used.add(colname);
                if (tab.getColumn(colname) == null)
                    tab.createColumn(colname, Double.class, false, -1.0);
            }
        }
        return used;
    }

    private static void removeUnusedScoreColumns(CyTable tab, Collection<String> used)
    {
        for(String name : ColumnNames.getScoreColumnNames(tab))
            if(!used.contains(name))
                tab.deleteColumn(name);
    }


    public static void linkPartnerNodes(CyNetwork cynet, AlignmentResult res, CyNode[][] cynodes)
    {
        // reorder nodes, so that cynodes[0][i] is aligned to cynodes[1][i] for every i.
        // this allows to get rid of all those integer indices.
        assert(res.nodes[0].length == res.nodes[1].length);
        int numNodes = res.nodes[0].length;
        CyNode[][] naa = new CyNode[res.nodes.length][];
        for(int g = 0; g < naa.length; ++g)
        {
            CyNode[] na = new CyNode[numNodes];
            naa[g] = na;
            int[] idxs = res.nodes[g];
            CyNode[] cyg = cynodes[g];
            for (int i = 0; i < idxs.length; ++i)
            {
                int where = idxs[i];
                na[i] = where < 0 ? null : cyg[where];
            }
        }

        linkPartnerNodes(cynet, naa, res.scores, res.scoreNames);
    }

    // Update table data from alignment result, cynodes sorted in a way that cynodes[0][i] is aligned to cynodes[1][i] for every i.
    // scores, scoreNames can be null.
    public static void linkPartnerNodes(CyNetwork cynet, CyNode[][] cynodes, double[][] scores, String[] scoreNames)
    {
        distributeUIDs(cynet);

        // -------------- NODE TABLE -----------------------
        CyTable nodeTable = cynet.getDefaultNodeTable();

        // Same order/index as in res.scoreNames
        List<String> usedScoreColumnNames = createScoreColumns(nodeTable, scoreNames);

        // Better to drop usued columns than to risk stale data from old alignment runs
        removeUnusedScoreColumns(nodeTable, usedScoreColumnNames);

        // ------------------------------------------------


        for (int i = 0; i < cynodes[0].length; ++i)
        {
            for(int g = 0; g < 2; ++g)
            {
                CyNode node = cynodes[g][i];
                if(node == null)
                    continue;

                CyRow row = cynet.getRow(node);

                if(scores != null)
                    assignScoresToRow(scores, usedScoreColumnNames, row, i);

                CyNode p = cynodes[1 - g][i]; // from other network

                if (p == null)
                {
                    row.set(ColumnNames.NODE_PARTNER_NAME, "");
                    row.set(ColumnNames.NODE_PARTNER_UID, -1L);
                }
                else
                {
                    CyRow prow = cynet.getRow(p);
                    String pname = prow.get(CyNetwork.NAME, String.class);
                    long puid = prow.get(ColumnNames.NODE_UID, Long.class);

                    row.set(ColumnNames.NODE_PARTNER_NAME, pname);
                    row.set(ColumnNames.NODE_PARTNER_UID, puid);

                    if(scores != null)
                        assignScoresToRow(scores, usedScoreColumnNames, prow, i);
                }
            }
        }
    }

    private static void _collectPairsByMappingEdges(CyNetwork cynet, List<CyNodePair> pairs, Set<CyNode> used, Collection<CyEdge> mappingEdges, boolean fixed)
    {
        for(CyEdge e : mappingEdges)
            if(cynet.getRow(e).get(ColumnNames.IS_FIXED_EDGE, Boolean.class) == fixed)
                if(!used.contains(e.getSource()) && !used.contains(e.getTarget()))
                {
                    used.add(e.getSource());
                    used.add(e.getTarget());
                    pairs.add(CyNodePair.sorted(e.getSource(), e.getTarget(), fixed, cynet));
                }
    }

    // probably super inefficient
    public static Map<CyEdge, CyEdge> getAlignedEdges(CyNetwork cynet, Collection<CyEdge> in)
    {
        in = GedevoFilters.getNonMappingEdges(cynet, in);

        Map<CyEdge, CyEdge> res = new HashMap<CyEdge, CyEdge>();
        Map<CyNode, CyNode> mapp = new HashMap<CyNode, CyNode>();
        List<CyNodePair> partners = getAlignedNodePairs(cynet, false);
        for(CyNodePair pair : partners)
        {
            mapp.put(pair.a, pair.b);
            mapp.put(pair.b, pair.a);
        }

        for(CyEdge e : in)
        {
            CyNode src = e.getSource();
            CyNode dst = e.getTarget();
            CyNode psrc = mapp.get(src);
            CyNode pdst = mapp.get(dst);
            if(psrc != null && pdst != null)
            {
                List<CyEdge> conn = cynet.getConnectingEdgeList(psrc, pdst, CyEdge.Type.ANY);
                if (!conn.isEmpty())
                    res.put(e, conn.get(0)); // assume no more than 1 edge between nodes
            }
        }

        return res;
    }

    public static List<CyNodePair> getAlignedNodePairs(CyNetwork cynet, boolean includeUnmapped)
    {
        return getAlignedNodePairsEx(cynet, true, true, true, includeUnmapped);
    }

    // In this order:
    // 1) fixed mapping edges, 2) normal mapping edges, 3) table data
    public static List<CyNodePair> getAlignedNodePairsEx(CyNetwork cynet, boolean useFixedEdges, boolean useMappingEdges, boolean useTableData, boolean includeUnmapped)
    {
        if(!checkIsGEDEVONetwork(cynet))
            return null;

        ArrayList<CyNodePair> pairs = new ArrayList<CyNodePair>();
        HashSet<CyNode> used = new HashSet<CyNode>();
        Collection<CyEdge> mappingEdges = GedevoFilters.getMappingEdges(cynet);

        // 1) Fixed mapping eges
        if(useFixedEdges)
            _collectPairsByMappingEdges(cynet, pairs, used, mappingEdges, true);

        // 2) Normal mapping edges
        if(useMappingEdges)
            _collectPairsByMappingEdges(cynet, pairs, used, mappingEdges, false);

        // 3) Table data
        if(useTableData)
        {
            Map<Long, CyNode> uid2node = getUIDNodeMap(cynet);
            for (Map.Entry<Long, CyNode> entry : uid2node.entrySet())
            {
                CyNode a = entry.getValue();
                if (used.contains(a))
                    continue;

                CyRow arow = cynet.getRow(a);
                long puid = arow.get(ColumnNames.NODE_PARTNER_UID, Long.class, -1L); // MUST be distributed at this point
                if (puid >= 0)
                {
                    CyNode b = uid2node.get(puid);
                    if (b != null && !used.contains(b))
                    {
                        used.add(a);
                        used.add(b);
                        pairs.add(CyNodePair.sorted(a, b, false, cynet));
                    }
                }
            }
        }

        if(includeUnmapped)
            for(CyNode n : GedevoFilters.getNonGroupedNodes(cynet, cynet.getNodeList()))
                if(!used.contains(n))
                {
                    CyNodePair pair = CyNodePair.single(n, false, cynet);
                    if(pair != null)
                        pairs.add(pair);
                }


        return pairs;
    }

    public static void deleteAllNonFixedMappingEdges(CyNetwork cynet)
    {
        removeAllMappingEdgesInvolvingNodes(cynet, null, false, null);
    }

    // assumes linked partner nodes
    public static void generateMappingEdges(CyNetwork cynet, Set<CyNode> included)
    {
        List<CyNodePair> pairs = getAlignedNodePairs(cynet, false);
        if(pairs == null)
            return;

        // keep fixed edges
        removeAllMappingEdgesInvolvingNodes(cynet, included, false, null);

        // add mapping edges, but ignore fixed
        for(CyNodePair pair : pairs)
            if(!pair.fixed && (included == null || included.contains(pair.a) || included.contains(pair.b)))
                createMappingEdgeSorted(cynet, pair.a, pair.b, false);
    }

    public static Map<Long, CyNode> getUIDNodeMap(CyNetwork cynet, Collection<CyNode> which)
    {
        GedevoAlignmentUtil.distributeUIDs(cynet);
        which = GedevoFilters.getNonGroupedNodes(cynet, which);
        HashMap<Long, CyNode> uid2node = new HashMap<Long, CyNode>();
        for (CyNode n : which)
        {
            CyRow row = cynet.getRow(n);
            long uid = row.get(ColumnNames.NODE_UID, Long.class, -1L);
            if (uid >= 0)
                uid2node.put(uid, n);
        }
        return uid2node;
    }

    public static Map<Long, CyNode> getUIDNodeMap(CyNetwork cynet)
    {
        return getUIDNodeMap(cynet, cynet.getNodeList());
    }

    public static CyNode getNodeByUID(CyNetwork cynet, long findUID)
    {
        for (CyNode n : cynet.getNodeList())
        {
            CyRow row = cynet.getRow(n);
            long uid = row.get(ColumnNames.NODE_UID, Long.class);
            if (uid == findUID)
                return n;
        }
        return null;
    }

    // slow
    /*public static CyNode getPartner(CyNode n)
    {
        CyNetwork cynet = n.getNetworkPointer();
        final Long uid = cynet.getRow(n).get(ColumnNames.NODE_PARTNER_UID, Long.class);
        for(CyNode p : cynet.getNodeList())
            if(uid.equals(cynet.getRow(p).get(ColumnNames.NODE_UID, Long.class)))
                return p;
        return null;
    }*/

    // for some reason cytoscape does NOT delete table data together with edges/nodes automatically...
    // See https://groups.google.com/forum/#!topic/cytoscape-helpdesk/1Rt971XFxnE
    // Since this seems to be especially hard and stupid, try really hard
    public static void removeEdgesAndTableRows(final CyNetwork cynet, Collection<CyEdge> rm)
    {
        final List<Long> rmid = new ArrayList<Long>();
        for(CyEdge e : rm)
                rmid.add(e.getSUID());

        CyRootNetwork root = GedevoApp.appInstance.cyrootmgr.getRootNetwork(cynet);
        root.getSharedEdgeTable().deleteRows(rmid);
        root.getDefaultEdgeTable().deleteRows(rmid);
        cynet.getDefaultEdgeTable().deleteRows(rmid);
        cynet.removeEdges(rm);
        root.removeEdges(rm);
        GedevoApp.log("Removed " + rm.size() + " edges + table data");
    }

    public static void removeNodesAndTableRows(final CyNetwork cynet, Collection<CyNode> rm)
    {
        final List<Long> rmid = new ArrayList<Long>();
        for(CyNode e : rm)
            rmid.add(e.getSUID());

        CyRootNetwork root = GedevoApp.appInstance.cyrootmgr.getRootNetwork(cynet);
        root.getSharedNodeTable().deleteRows(rmid);
        root.getDefaultNodeTable().deleteRows(rmid);
        cynet.getDefaultNodeTable().deleteRows(rmid);
        cynet.removeNodes(rm);
        root.removeNodes(rm);
        GedevoApp.log("Removed " + rm.size() + " nodes + table data");
    }

    public static void removeAllMappingEdgesInvolvingNodes(final CyNetwork cynet, Set<CyNode> nodes, boolean fixedToo, CyEdge except)
    {
        Set<CyEdge> edges = new HashSet<CyEdge>(GedevoFilters.getMappingEdges(cynet));
        removeMappingEdgesInvolvingNodes(cynet, edges, nodes, fixedToo, except);
    }

    private static void removeMappingEdgesInvolvingNodes(final CyNetwork cynet, Set<CyEdge> edges, Set<CyNode> nodes, boolean fixedToo, CyEdge except)
    {
        final List<CyEdge> rm = new ArrayList<CyEdge>();

        for(CyEdge e : edges)
            if(e != except
              && (nodes == null || nodes.contains(e.getSource()) || nodes.contains(e.getTarget()))
              && (fixedToo || !cynet.getRow(e).get(ColumnNames.IS_FIXED_EDGE, Boolean.class)) )
                rm.add(e);

        removeEdgesAndTableRows(cynet, rm);
    }

    public static void mapPairs(CyNetwork cynet, List<CyNodePair> pairs, boolean forceFixed)
    {
        if(pairs.isEmpty())
            return;

        Set<CyNode> rm = new HashSet<CyNode>();
        for(CyNodePair pair : pairs)
        {
            rm.add(pair.a);
            rm.add(pair.b);
        }
        removeAllMappingEdgesInvolvingNodes(cynet, rm, true, null);

        for(CyNodePair pair : pairs)
            createMappingEdge(cynet, pair.a, pair.b, forceFixed || pair.fixed);
    }

    public static CyEdge mapSingleNodes(CyNetwork cynet, CyNode a, CyNode b, boolean fixed)
    {
        if(a == null || b == null)
            return null;

        HashSet<CyNode> rm = new HashSet<CyNode>();
        rm.add(a);
        rm.add(b);
        CyEdge added = createMappingEdge(cynet, a, b, fixed);
        if(added != null)
            removeAllMappingEdgesInvolvingNodes(cynet, rm, true, added);
        return added;
    }

    public static CyEdge createMappingEdge(CyNetwork cynet, CyNode a, CyNode b, boolean fixed)
    {
        if(a == null || b == null)
            return null;
        int aNetID = cynet.getRow(a).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class, -1);
        int bNetID = cynet.getRow(b).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class, -1);
        if(aNetID == bNetID || aNetID < 0 || bNetID < 0)
            return null;
        if(bNetID < aNetID)
        {
            CyNode tmp = a;
            a = b;
            b = tmp;
        }

        return createMappingEdgeSorted(cynet, a, b, fixed);
    }

    public static CyEdge createMappingEdgeSorted(CyNetwork cynet, CyNode a, CyNode b, boolean fixed)
    {
        CyEdge e = cynet.addEdge(a, b, false);
        CyRow erow = cynet.getRow(e);
        erow.set(ColumnNames.IS_MAPPING_EDGE, true);
        erow.set(ColumnNames.IS_FIXED_EDGE, fixed);

        //CyRow nrow = cynet.getRow(a);
        //erow.set(ColumnNames.PAIRSCORE, nrow.get(ColumnNames.PAIRSCORE, Double.class));

        return e;
    }


    public static CyNetwork getNetworkForTable(CyTable tab)
    {
        // Unreliable:
        //for(CyNetwork cynet : GedevoApp.appInstance.cynetmgr.getNetworkSet())
        //    if(cynet.getDefaultNodeTable() == tab || cynet.getDefaultEdgeTable() == tab || cynet.getDefaultNetworkTable() == tab)
        //        return cynet;

        // Nope
        //long suid = tab.getSUID();
        //return GedevoApp.appInstance.cynetmgr.getNetwork(suid);

        CyNetworkTableManager tm = GedevoApp.appInstance.cyreg.getService(CyNetworkTableManager.class);
        return tm.getNetworkForTable(tab);
    }

    public static void selectNodesAndEdgesFromSameCCS(CyNetwork net, Collection<CyIdentifiable> sels)
    {
        if(net.getDefaultNodeTable().getColumn(ColumnNames.CCS) == null)
        {
            GedevoUtil.msgbox("\"" + ColumnNames.CCS + "\" column does not exist in the node table - this is required");
            return;
        }
        if(net.getDefaultNodeTable().getColumn(ColumnNames.SOURCE_NETWORK_ID) == null)
        {
            GedevoUtil.msgbox("\"" + ColumnNames.SOURCE_NETWORK_ID + "\" column does not exist in the node table - this is required");
            return;
        }

        // Select nodes from same source network in same CCS - so when selecting from multiple source IDs, take care to include exactly the right CCSs
        Set<Integer>[] ccsids = new HashSet[2];
        ccsids[0] = new HashSet<Integer>();
        ccsids[1] = new HashSet<Integer>();

        for(CyIdentifiable s : sels)
        {
            CyRow row = net.getRow(s);
            int srcid = row.get(ColumnNames.SOURCE_NETWORK_ID, Integer.class, -1);
            if(srcid >= 0 && srcid < ccsids.length)
                ccsids[srcid].add(net.getRow(s).get(ColumnNames.CCS, Integer.class));
        }

        for(CyNode n : net.getNodeList())
            _selectIdentifiableFromSameCCS(net, n, ccsids);
        for(CyEdge e : net.getEdgeList())
            _selectIdentifiableFromSameCCS(net, e, ccsids);
    }

    private static void _selectIdentifiableFromSameCCS(CyNetwork net, CyIdentifiable ident, Set<Integer>[] ccsids)
    {
        CyRow row = net.getRow(ident);
        int srcid = row.get(ColumnNames.SOURCE_NETWORK_ID, Integer.class);
        if(srcid >= 0 && srcid < ccsids.length && ccsids[srcid].contains(row.get(ColumnNames.CCS, Integer.class)))
            net.getRow(ident).set(CyNetwork.SELECTED, true);
    }

    public static List<CyNodePair> getSelectedAlignedNodePairs(CyNetwork cynet)
    {
        List<CyNodePair> pairs = new ArrayList<CyNodePair>();
        Set<CyNode> used = new HashSet<CyNode>();

        List<CyEdge> selectedEdges = CyTableUtil.getEdgesInState(cynet, CyNetwork.SELECTED, true);
        for (CyEdge e : GedevoFilters.getMappingEdges(cynet, selectedEdges))
        {
            pairs.add(new CyNodePair(e.getSource(), e.getTarget()));
            used.add(e.getSource());
            used.add(e.getTarget());
        }

        List<CyNode> selectedNodes = CyTableUtil.getNodesInState(cynet, CyNetwork.SELECTED, true);
        if (!selectedNodes.isEmpty())
        {
            Map<Long, CyNode> uid2node = GedevoAlignmentUtil.getUIDNodeMap(cynet);

            for (CyNode n : selectedNodes)
            {
                Long puid = cynet.getRow(n).get(ColumnNames.NODE_PARTNER_UID, Long.class);
                CyNode p = uid2node.get(puid);
                if(p != null && !used.contains(n) && !used.contains(p))
                {
                    used.add(n);
                    used.add(p);
                    pairs.add(new CyNodePair(n, p));
                }
            }
        }
        return pairs;
    }

    private static void setSelectedNodesAndEdgesFixation(CyNetwork cynet, boolean fixed)
    {
        List<CyNodePair> pairs = getSelectedAlignedNodePairs(cynet);
        for(CyNodePair p : pairs)
            p.fixed = fixed;

        GedevoAlignmentUtil.mapPairs(cynet, pairs, fixed);
    }


    public static void fixateSelectedNodesAndEdges(CyNetwork cynet)
    {
        setSelectedNodesAndEdgesFixation(cynet, true);
    }

    public static void unfixateSelectedNodesAndEdges(CyNetwork cynet)
    {
        setSelectedNodesAndEdgesFixation(cynet, false);
    }
}
