package org.cytoscape.gedevo.util;

import org.cytoscape.model.CyNetwork;

// for sorting and stuff
public final class NetworkWrapper implements Comparable
{
    public NetworkWrapper(CyNetwork cynet)
    {
        this.cynet = cynet;
    }

    public String getName()
    {
        String ret = "<Failed to get network name>";
        try
        {
            String n = cynet.getRow(cynet).get(CyNetwork.NAME, String.class);
            if(n != null)
                ret = n;
        }
        catch(Exception ex)
        {
        }
        return ret;
    }


    public final CyNetwork cynet;

    @Override
    public int compareTo(Object o)
    {
        return getName().compareTo(o.toString());
    }

    @Override public String toString()
    {
        return getName();
    }
}
