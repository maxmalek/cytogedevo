import java.io.PrintStream;

class GedevoOutputInterceptor extends PrintStream
{
    private GedevoLogWindow logwin;
    private PrintStream origOut;
    public GedevoOutputInterceptor(GedevoLogWindow win)
    {
        super(System.out, true);
        logwin = win;
        origOut = System.out;
        System.setOut(this);
    }
    @Override public void print(String s)
    {
        logwin.log(s);
        super.print(s);
    }
    public void unlink()
    {
        logwin = null;
        System.setOut(origOut);
    }
}
