#ifndef RESULTPRINTER_H
#define RESULTPRINTER_H

#include "common.h"
#include <iosfwd>

class ProgramState;
class Agent;

namespace ResultPrinter {

void AsList(const ProgramState& state, const Agent *dude, std::ostream& o);


};

#endif
