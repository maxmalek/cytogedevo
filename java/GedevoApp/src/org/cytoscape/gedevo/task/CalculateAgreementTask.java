package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.AlignmentResult;
import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.work.TaskMonitor;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

// TODO: TEST & FIX THIS

public class CalculateAgreementTask extends AbstractAlignmentTask
{
    public CalculateAgreementTask(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor mon) throws Exception
    {
        mon.setStatusMessage("Agreement...");
        int howMany = (int)((float)common.info.numAgents * common.settings.agreementFraction);
        if(howMany < 4 || howMany > common.info.numAgents)
            howMany = common.info.numAgents;

        CyTable nodeTable = common.cynet.getDefaultNodeTable();
        CyColumn col = nodeTable.getColumn(ColumnNames.AGREEMENT);
        if(col == null)
            nodeTable.createColumn(ColumnNames.AGREEMENT, Double.class, true);

        AlignmentResult bestState = common.instance.getAlignmentResult(0);

        storeAgreement(howMany, 0, 1, mon, bestState);
        storeAgreement(howMany, 1, 0, mon, bestState);
    }

    private void storeAgreement(final long howMany, final int g1, final int g2, TaskMonitor mon, AlignmentResult bestState)
    {
        mon.setStatusMessage("Calculating agreement sum (" + g1 + ") ...");

        @SuppressWarnings("unchecked")
        Map<Integer, Integer>[] agreementMaps = new HashMap[bestState.nodes[g1].length]; // All nodes arrays have the same size, so just get any
        for(int i = 0; i < agreementMaps.length; ++i)
            agreementMaps[i] = new HashMap<Integer, Integer>();

        final double howManyD = (double)howMany;
        for(long id = 0; id < howMany; ++id)
        {
            mon.setProgress(id / howManyD);
            // Check out each agent...
            AlignmentResult astate = common.instance.getAlignmentResult(id);
            // Get nodes1[i] is mapped to nodes2[i] according to this agent...
            final int[] nodes1 = astate.nodes[g1];
            final int[] nodes2 = astate.nodes[g2];

            // For each node, record & count which other node was how often mapped to it, in total
            for(int i = 0; i < nodes1.length; ++i)
            {
                final int node1 = nodes1[i];
                if(node1 < 0)
                    continue;
                final Map<Integer, Integer> m = agreementMaps[node1];
                final Integer prevI = m.get(nodes2[i]);
                final int prev = prevI != null ? prevI : 0;
                m.put(nodes2[i], prev + 1);
            }
        }

        int[] partnerIdx = new int[bestState.nodes[g1].length];
        Arrays.fill(partnerIdx, -1);
        {
            final int[] nodes2 = bestState.nodes[g1];
            for(int i = 0; i < partnerIdx.length; ++i)
            {
                int nodeId = nodes2[i];
                if (nodeId >= 0)
                    partnerIdx[nodeId] = i;
            }
        }

        mon.setStatusMessage("Calculating agreement table data (" + g1 + ") ...");

        CyNode[] nodesG1 = GedevoAlignmentUtil.getCyNodesFromNodes(common.graphs[g1].nodes);
        int[] mappedNodeIDs = bestState.nodes[g2];

        final double lenD = (double)agreementMaps.length;

        for(int i = 0; i < agreementMaps.length; ++i)
        {
            Map<Integer, Integer> m = agreementMaps[i];
            if(m.isEmpty())
                continue;
            mon.setProgress(i / lenD);
            int total = 0;
            for(Integer count : m.values())
                total += count;
            final double totalF = (double)total;
            CyNode cynode1 = nodesG1[i];
            int mappedNode2 = mappedNodeIDs[partnerIdx[i]];
            double agreement = -1.0;
            for(Integer node2 : m.keySet())
            {
                int rawCount = m.get(node2);
                final double a = rawCount / totalF;
                if(mappedNode2 == node2)
                    agreement = a;
            }
            if(agreement < 0)
                throw new IllegalStateException("holy sh*t this can't go wrong - something internal just went hurrdurr");

            CyRow row = common.cynet.getRow(cynode1);
            row.set(ColumnNames.AGREEMENT, agreement);
        }
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
