package org.cytoscape.gedevo.simplenet;

import org.cytoscape.gedevo.GedevoFilters;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNode;

import java.util.Collection;
import java.util.HashMap;


public final class Graph
{
    public Node[] nodes;
    public int[] eSrc;
    public int[] eDst;
    public Edge[] edges;
    private String name;
    //public List<NodePair> mapping; // null if no mapping

    public Graph(String name)
    {
        this.name = name;
    }


    public final String getName() { return name; }

    public static Graph importNetworkFilter(GedevoFilters.Filter filt)
    {
        Graph g = new Graph(filt.getNetworkName());
        HashMap<CyNode, Node> nmap = new HashMap<CyNode, Node>();

        // Create all nodes first
        {
            Collection<CyNode> nodes = filt.getNodes();
            g.nodes = new Node[nodes.size()];
            int nodeId = 0;
            for (CyNode cyn : nodes)
            {
                Node n = new Node(nodeId, filt.getNodeName(cyn));
                n.cynode = cyn;
                nmap.put(cyn, n);
                g.nodes[nodeId] = n;
                ++nodeId;
            }
        }

        Collection<CyEdge> edges = filt.getEdges();

        g.eSrc = new int[edges.size()];
        g.eDst = new int[edges.size()];
        g.edges = new Edge[edges.size()];
        int idx = 0;
        for(CyEdge e : edges)
        {
            CyNode cysrc = e.getSource();
            CyNode cydst = e.getTarget();
            Node src = nmap.get(cysrc);
            Node dst = nmap.get(cydst);

            g.eSrc[idx] = src.id;
            g.eDst[idx] = dst.id;
            g.edges[idx] = new Edge(src.id, dst.id, e.isDirected());

            src.outgoing.add(dst);
            dst.incoming.add(src);

            if(!e.isDirected())
            {
                src.incoming.add(dst);
                dst.outgoing.add(src);
            }

            // any direction
            src.neighbors.add(dst);
            dst.neighbors.add(src);

            ++idx;
        }

        return g;
    }

}

