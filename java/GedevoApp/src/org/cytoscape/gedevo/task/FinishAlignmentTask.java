package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.pairlayout.PairLayoutTask;
import org.cytoscape.gedevo.pairlayout.SideBySide;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import java.util.Collection;

public class FinishAlignmentTask extends AbstractAlignmentTask
{
    public FinishAlignmentTask(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        GedevoAlignmentUtil.deleteAllNonFixedMappingEdges(common.cynet);

        final GedevoApp app = GedevoApp.appInstance;
        CyNetworkManager networkManager = app.cyreg.getService(CyNetworkManager.class);
        networkManager.addNetwork(common.cynet);

        CyNetworkView view = null;
        boolean add = false;
        final CyNetworkViewManager vm = app.cyviewmgr;
        if(common.settings.openViewWhenDone)
        {
            CyNetworkViewFactory vf = app.cyviewfac;
            Collection<CyNetworkView> views = vm.getNetworkViews(common.cynet);
            if(views != null && !views.isEmpty())
            {
                view = views.iterator().next(); // HACK: This isn't exactly right, what about > 1 views?
                if(views.size() > 1)
                    app.log("FinishAlignmentTask: Warning: > 1 views exist, took first");
            }

            if(view == null)
            {
                view = vf.createNetworkView(common.cynet);
                add = view != null;
            }
        }

        TaskIterator extra = new TaskIterator();

        if(view != null)
        {
            // Always perform at least grid layout, otherwise all nodes are at (0, 0)
            CyLayoutAlgorithmManager algomgr = app.cyreg.getService(CyLayoutAlgorithmManager.class);
            CyLayoutAlgorithm algo = algomgr.getLayout(common.settings.performInitialLayout ? "force-directed" : "grid");
            if(algo == null)
            {
                app.log("Warning: force-directed is null, using default");
                algo = algomgr.getDefaultLayout();
            }
            final PairLayoutTask pairlayout = new PairLayoutTask(view, algo, null, null, null, null);
            final SideBySide sbs = new SideBySide(pairlayout);
            extra.append(new AbstractTask()
            {
                @Override
                public void run(TaskMonitor taskMonitor) throws Exception
                {
                    app.cyeventmgr.flushPayloadEvents(); // make sure the network is created
                }
            });
            extra.append(pairlayout);
            extra.append(sbs.makeTask(view));
        }

        if(view != null)
        {
            final CyNetworkView finalview = view;
            final boolean finaladd = add;
            extra.append(new AbstractTask()
            {
                @Override
                public void run(TaskMonitor taskMonitor) throws Exception
                {
                    // flush to add emphasis that it REALLY does the thing
                    if(finaladd)
                    {
                        vm.addNetworkView(finalview);
                        app.cyeventmgr.flushPayloadEvents();
                }
                    finalview.fitContent();
                    app.cyeventmgr.flushPayloadEvents();
                    app.vmm.getVisualStyle(finalview).apply(finalview);
                    app.cyeventmgr.flushPayloadEvents();
                    finalview.updateView();
                }
            });
        }

        super.insertTasksAfterCurrentTask(extra);
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
