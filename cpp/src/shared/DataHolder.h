#ifndef DATA_HOLDER_H
#define DATA_HOLDER_H

// -- DataHolder --
// Precalculates data used by AgentMgr & co,
// or brings data (matrices) into a shape that accessing them is a lot faster.
// This, after initialization, is a pure read-only class.

#include "common.h"
#include "Matrix.h"
#include "NodeMapping.h"
#include "UserSettings.h"
#include "GEDScore.h"
#include <float.h>

class ProteinHolder;
class ThreadPool;

// IMPORTANT: variable is always on X axis! (and static group on Y axis)

class DataHolder
{
public:
	DataHolder(UserSettings& settings);
	~DataHolder();

	void SetProteinHolder(ProteinHolder *ph) { _ph = ph; }
	void SetThreadPool(ThreadPool *th) { _thpool = th; }

	inline const UserSettings& GetUserSettings() const { return _settings; }

	bool PrecalcLight();
	bool PrecalcHeavy();


    inline score GetGraphletScore_unsafe(uint a, uint b) const
	{
		return _lookupGraphletScore(a, b);
	}

	inline bool HasCombinedPairScore() const { return !_lookupCombinedPairScore.empty(); }
	inline score GetCombinedPairScore_unsafe(uint a, uint b) const
	{
		return _lookupCombinedPairScore(a, b);
	}
	inline score GetCombinedPairScore(uint a, uint b) const
	{
		return _lookupCombinedPairScore.empty() ? score(0) : GetCombinedPairScore_unsafe(a, b);
	}

	inline bool HasCombinedFinalScore() const { return !_lookupCombinedFinalScore.empty(); }
	inline score GetCombinedFinalScore(uint a, uint b) const
	{
		return _lookupCombinedFinalScore.empty() ? score(0) : GetCombinedFinalScore_unsafe(a, b);
	}
	inline score GetCombinedFinalScore_unsafe(uint a, uint b) const
	{
		return _lookupCombinedFinalScore(a, b);
	}

	/*inline char IsConnected(uint group, uint a, uint b) const
	{
		return _lookupConnRow[group](a, b);
	}*/

	// for sequential access
	inline const uchar *GetConnectionsRow(uint group, uint a) const
	{
		return _lookupConnRow[group].getRow(a);
	}
	inline const uchar *GetConnectionsCol(uint group, uint a) const
	{
		return _lookupConnCol[group].getColumn(a);
	}

	inline uint GetMaxVariableNodeID() const  { return _nodeMaxID[_settings.varGroup]; }
	inline uint GetMaxReferenceNodeID() const { return _nodeMaxID[!_settings.varGroup]; }

	inline uint GetMaxTotalNodeDegree() const { return _nodeMaxDegreeTotal; }
	inline uint GetMaxVariableNodeDegree() const  { return _nodeMaxDegree[_settings.varGroup]; }
	inline uint GetMaxReferenceNodeDegree() const { return _nodeMaxDegree[!_settings.varGroup]; }

	inline uint GetVariableEdgeAmount() const { return _totalEdgeAmount[_settings.varGroup]; }
	inline uint GetReferenceEdgeAmount() const { return _totalEdgeAmount[!_settings.varGroup]; }
	inline uint GetEdgeAmount(uint g) const { return _totalEdgeAmount[g]; }

	inline const NodeMapping& GetRefMapping() const { return _refMapping; }
	inline const NodeMapping& GetDefaultMapping() const { return _defaultMapping; }
	inline const unsigned *GetRefMappingIndex() const { return &_refIndex[0]; }

	// Do NOT call this with 0 !
	inline const Node *GetNodeByID(uint id) const
	{
		ASSERT(id && id < _defaultMapping.size());
		const Node *node = _defaultMapping.node[_defaultIndex[id - 1]];
		ASSERT(node->id == id);
		return node;
	}

	// equal to total edge amount in both graphs
	inline uint getWorstGEDRaw() const { return _worstGEDRaw; }
	inline score getWorstGEDScore() const { return _worstGEDScore; }

	bool HasDirectedEdges() const { return _hasDirectedEdges; }

	inline const score *getGEDScoreArray() const { return &_gedScores[0]; }

	// This is also the amount of prematched nodes that exist.
	inline uint getFirstNonPrematchedIndex() const { return _firstNonPrematchedIndex; }

	inline bool IsInitialized() const { return _wasInit; }

	inline const std::pair<score, score> GetOverrideInterval(uint a, uint b) const
	{
		// if score is double, FLT_MAX is also not a problem. as long as both values are strictly > 1 in the fallback case.
		return !_customOverrideMtx.empty() ? _customOverrideMtx(a, b) : std::make_pair(score(FLT_MAX), score(FLT_MAX));
	}

	inline size_t GetNumCustomMatrices() const { return _customMatricesRaw.size(); }

	inline score GetCustomScoreRaw(uint i, uint a, uint b) const
	{
		return _customMatricesRaw[i](a, b);
	}

protected:
	template <typename T, template <typename> class M> bool _InitScoreMatrix(M<T>& m, const NodeArray& gxv, const NodeArray& gyv, bool fill); // returns true if matrix already contains data
	void _SetupGraphletMatrix();
	void _PrecalcGraphletScores(Matrix<score>& m);
	void _FillNeighborVector(uint group);
	void _TransferConnectionMatrix(uint group);
	void _GenerateDefaultMappings();
	void _CalcMisc(); // This figures out max node IDs and degree.
	void _CalcMisc2();
	void _CalcCombinedPairScores();
	void _CalcCombinedFinalScores();
	void _FillGEDScoreArray();
	bool _CheckPrematch(uint g);
	bool _LoadCustomMatrices();
	void _SetupCustomOverrideMatrices();

	// -------- Hot data -----------

	// row-wise
	FastMatrix<uchar> _lookupConnRow[2];

	// same data, but column-wise memory layout
	FastMatrixColumnWise<uchar> _lookupConnCol[2];

	bool _hasDirectedEdges;
	uint _firstNonPrematchedIndex;

	FastMatrix<score> _lookupCombinedPairScore;
	FastMatrix<score> _lookupCombinedFinalScore;
	Matrix<std::pair<score, score> > _customOverrideMtx; // two matrices in one, values are pre-mangled and always needed in pairs


	// -------- Cold data -------------

	Matrix<score> _lookupGraphletScore;

	uint _nodeMaxID[2];
	uint _nodeMaxDegree[2];
	uint _totalEdgeAmount[2];

	uint _nodeMaxDegreeTotal;

	score _gedScores[GEDI_MAX];


	ProteinHolder *_ph;

	// Both initially filled, padded to same length, and then left untouched. NULLs only at end of array.
	NodeMapping _refMapping;
	NodeMapping _defaultMapping;
	std::vector<unsigned> _refIndex;
	std::vector<unsigned> _defaultIndex;

	// Precalculated once, later used to scale GED score into a [0...1] range
	uint _worstGEDRaw;
	score _worstGEDScore;

	UserSettings& _settings;

	ThreadPool *_thpool; // optional
	bool _wasInit;

	UserSettings _currentSettings;
	std::vector<Matrix<score> > _customMatricesRaw;
};

template <typename T, template <typename> class M>
bool DataHolder::_InitScoreMatrix(M<T>& m, const NodeArray& gxv, const NodeArray& gyv, bool fill)
{
	const bool wasEmpty = !m.empty();
	size_t xmax = gxv.size() + 1;
	size_t ymax = gyv.size() + 1;

	m.resize(xmax, ymax);

	if(fill)
		m.fill(_settings.pairNullValue);
	else
	{
		// null entries
		m(0, 0) = 0; // NULL vs NULL, can't be better

		// -- Reference group
		for(size_t iy = 0; iy < gyv.size(); ++iy)
		{
			const Node *py = gyv[iy];
			ASSERT(py->group != _settings.varGroup);
			uint y = py->id;
			ASSERT(y);
			m(0, y) = _settings.pairNullValue;
		}

		// -- Variable group
		for(size_t ix = 0; ix < gxv.size(); ++ix)
		{
			const Node *px = gxv[ix];
			ASSERT(px->group == _settings.varGroup);
			uint x = px->id;
			ASSERT(x);
			m(x, 0) = _settings.pairNullValue;
		}
	}

	return wasEmpty;
}

#endif
