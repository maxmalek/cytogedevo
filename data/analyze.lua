-- some functions for generating network statistics for alignment files
-- required headered format
-- outputs tab-separated list on stdout

dofile("util.lua")

-- edge listfile
local function loadGraph(fn)
    local g = { nodes = {} } -- array of nodes + nodes indexed by name
    local gn = g.nodes
    -- node: { name = ..., incoming = { [name] = node, ... }, outgoing = { [name] = node, ...}, neighbors = ... }
    local edges = 0
    local selfloops = 0
    for line in io.lines(fn) do
        local a = line:splitws()
        assert(#a == 2)
        local name1 = a[1]
        local name2 = a[2]
        local node1 = gn[name1]
        local node2 = gn[name2]
        if not node1 then
            node1 = { name = name1, to = {}, neighbors = {} }
            gn[name1] = node1
        end
        if not node2 then
            node2 = { name = name2, to = {}, neighbors = {} }
            gn[name2] = node2
        end
        
        if node1 == node2 and not node1.to[node2] and not node2.to[node1] then
            selfloops = selfloops + 1
        end
        if not node1.neighbors[node2] then
            edges = edges + 1
        end
        
        node1.to[node2] = true
        
        
        if node1 == node2 then
            node1.selfloop = true
        else
            node1.neighbors[node2] = true
            node2.neighbors[node1] = true
        end
    end
    
    g.numEdges = edges + selfloops
    
    --print(fn, table.count(gn), edges, "self:", selfloops)
    
    return g
end

local function countNames(g, names)
    local c = 0
    for _, n in pairs(names) do
        c = c + ((g[n] and 1) or 0)
    end
    return c
end

local function loadAlignment(fn, g1, g2)
    local firstline = true
    local header
    local aln = {}
    for line in io.lines(fn) do
        if not (line == "" or line:sub(1, 1) == "#") then
            local a = line:splitws()
            if firstline then
                firstline = false
                assert(#a > 2)
                header = a
                --print(line)
                for i, x in ipairs(a) do
                    aln[x] = {}
                end
            else
                for i, x in pairs(a) do
                    table.insert(aln[header[i]], x)
                end
            end
        end
    end
    
    assert(aln.P1, "P1 missing")
    assert(aln.P2, "P2 missing")
    
    local thisway = countNames(g1, aln.P1) + countNames(g2, aln.P2)
    local otherway = countNames(g2, aln.P1) + countNames(g1, aln.P2)
    
    if otherway > thisway then
        aln.P1, aln.P2 = aln.P2, aln.P1
    end
    
    local P1 = aln.P1
    local P2 = aln.P2
    aln.M12 = {}
    aln.M21 = {}
    
    local u1, u2 = {}, {}
    for i = 1, #P1 do
        local name1 = P1[i]
        local name2 = P2[i]
        if name1 ~= "-" and name2 ~= "-" then
            local node1 = assert(g1[name1])
            local node2 = assert(g2[name2])
            --if node1 and node2 then
                aln.M12[node1] = node2
                aln.M21[node2] = node1
            --end
        else
            u1[name1] = true
            u2[name2] = true
        end
    end
    
    --[[for _, node in pairs(g1) do
        assert(aln.M12[node] or u1[node.name], node.name)
    end
    for _, node in pairs(g2) do
        assert(aln.M21[node] or u2[node.name], node.name)
    end]]
    
    return aln
end

local function sortTableSize(t1, t2)
    return #t1 > #t2
end

-- pretty much translated from the java code
local function getCCSList(aln, g1, g2)
    local unseen, seen, q, ccslist = {}, {}, {}, {}
    for _, node in pairs(g1) do
        table.insert(unseen, node)
    end
    
    while next(unseen) do
        local begin = table.remove(unseen)
        if not seen[begin] then
            seen[begin] = true
            table.insert(q, begin)
            local cc = {}
            --print("--next CCS-- [nb: " .. table.count(begin.neighbors) .. "]")
            while next(q) do
                local a = table.remove(q)
                local b = aln.M12[a]
                --print(a, b, table.count(a.neighbors))
                if b then
                    table.insert(cc, {a, b})
                    for anext, _ in pairs(a.neighbors) do
                        if not seen[anext] then
                            local bnext = aln.M12[anext]
                            if bnext and b.neighbors[bnext] then
                                table.insert(q, anext)
                                seen[anext] = true
                            end
                        end
                    end
                end
            end
            if #cc > 1 then
                table.insert(ccslist, cc)
            end
        end
    end
    
    table.sort(ccslist, sortTableSize)
    return ccslist
end

local function calcAlignedEdges(aln, g1, g2)
    local c = 0
    
    for a, b in pairs(aln.M12) do
        if a.selfloop and b.selfloop then
            c = c + 2 -- will see a self loop only once
        end
        for an, _ in pairs(a.neighbors) do
            local bn = aln.M12[an]
            if bn and (b.neighbors[bn]) then
                c = c + 1 -- will see each edge twice
            end
        end
    end
    
    --[[for a, b in pairs(aln.M12) do
        for bn, _ in pairs(b.to) do
            local an = aln.M21[bn]
            if an and a.to[an] then
                c = c + 1
            end
        end
    end]]
    
    assert((c % 2) == 0)
    return c / 2
end

local function calcEC(aln, g1, g2)
    local c = calcAlignedEdges(aln, g1.nodes, g2.nodes)
    return (c / math.min(g1.numEdges, g2.numEdges)), c
end

local function sumGO(aln)
    local c = 0
    for _, go in pairs(aln.GOSum or aln.gedevoGOSum) do
        c = c + go
    end
    return c
end


local function analyze(alnfn, g1, g2)
    local aln = loadAlignment(alnfn, g1.nodes, g2.nodes)
    local ccslist = getCCSList(aln, g1.nodes, g2.nodes)
    local gosum = sumGO(aln)
    local ec, ae = calcEC(aln, g1, g2)
    --print(alnfn, #ccslist[1], ec, ae, gosum)
    local a = alnfn:explode("/", true)
    local name = a[#a]
    local biop, grp, gedp = 0, 0, 0
    if name:sub(1,1) == "(" then -- netal?
        local _, _, b = name:find("-b(%d?%.?%d+)")
        biop = assert(b, name)
    else -- migraal or gedevo
        local _, _, b = name:find("_b(%d?%.?%d*)")
        local _, _, gr = name:find("_gr(%d?%.?%d*)")
        local _, _, ge = name:find("_ged(%d?%.?%d*)")
        biop = assert(b or 0, name) -- HACK FIXME
        grp = gr or 0
        gedp = ge or 0
    end
    biop = tonumber(biop)
    grp = tonumber(grp)
    gedp = tonumber(gedp)
    local lccs = (next(ccslist) and #ccslist[1]) or 0
    print(biop, gedp, grp, ec, lccs, gosum, name)
end



local args = {...}
if #args == 0 then
    args = {
        "data2/cerevisiae.elu",
        --"data2/melanogaster.elu",
        "data2/sapiens.elu",
        "../../gedevo_tex/R/results/cerevisiae_gedevo1/gedevo_cerevisiae_sapiens_ty1_b0_ged1.0_gr0.txt.u.withgo",
        "../../gedevo_tex/R/results/cerevisiae_netal/(sapiens.tab-cerevisiae.tab)-a0.0001-b0-c0.5-i2.alignment.aln.u.withgo",
        "../../gedevo_tex/R/results/cerevisiae_migraal/sapiens_vs_cerevisiae_b0_topo1.aln.u.withgo",
    }
end

print("bio", "ged", "gr", "ec", "lccs", "gosum", "name")
local g1 = loadGraph(table.remove(args, 1))
local g2 = loadGraph(table.remove(args, 1))

--print("name", "bio", "ec", "gosum", "lccs")
for _, fn in ipairs(args) do
    analyze(fn, g1, g2)
end
