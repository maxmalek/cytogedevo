package org.cytoscape.gedevo.integration;

import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.SimpleMenuAction;
import org.cytoscape.gedevo.Unloadable;

import java.awt.event.ActionEvent;

public class SelectMenuIntegrator extends Unloadable
{
    GedevoApp app;
    public SelectMenuIntegrator(GedevoApp a)
    {
        app = a;

        register();
    }

    private void register()
    {
        app.addMenuAction(new SimpleMenuAction(app, "GEDEVO: Include mapped partner nodes to selection", "Select")
        {
            @Override
            public void actionPerformed(ActionEvent ev)
            {
                UIShortcuts.includePartnerNodesToSelection();
            }
        });

        app.addMenuAction(new SimpleMenuAction(app, "GEDEVO: Include nodes/edges from same CCS as selected into selection", "Select")
        {
            @Override
            public void actionPerformed(ActionEvent ev)
            {
                UIShortcuts.selectNodesAndEdgesFromSameCCS();
            }
        });
    }


    @Override
    public void unload()
    {
        app = null;
    }
}
