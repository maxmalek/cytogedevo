#ifndef PROTEIN_HOLDER_H
#define PROTEIN_HOLDER_H

#include "common.h"
#include "Matrix.h"
#include <vector>
#include <map>
#include <set>
#include "NodeMapping.h"

class UserSettings;
class ThreadPool;
class MTRand;

class ProteinHolder
{
public:
	ProteinHolder(const UserSettings& settings);
	~ProteinHolder();

	void SetNetworkFileName(uint group, const std::string& name);
	void SetThreadPool(ThreadPool *pool) { _thpool = pool; }

	bool ReadNetworkSIF(const std::string& fn, uint groupIdx); // for easy cytoscape import
	bool ReadNetworkEDGELIST(const std::string& fn, uint groupIdx, bool hasHeader); // for easy cytoscape import

	void Add(Node *p);
	Node *CreateAndAdd(uint group, const std::string& name);
	
	// returns pointer to Node by its name 
	Node *GetByName(uint group, const std::string& name, bool create = false);
	
	inline Node *GetByIdx(uint group, uint idx) { return _grpV[group][idx]; }

	// a is always my group, b is always ref group
	bool PrematchByIdx(uint a, uint b);

	inline const NodeArray& GetGroup(uint g) const { return _grpV[g]; }
	inline const char *GetNetworkFileName(uint g) const { return _networkFileNames[g].c_str(); }

	bool PrepareLight();
	bool PrepareHeavy();

	inline const Matrix<uchar>& GetConnMatrix(uint group)
	{
		return _conn[group];
	}

	// Drops some internal matrices no longer needed after setting up DataHolder. Called by DataHolder.
	void Cleanup();

	// draw edges between two nodes
	void AddPairByIdx(uint groupIdx, uint i, uint j);
	void AddPair(Node *p1, Node *p2);
protected:
	void _AddPairByName(uint groupIdx, const std::string& name1, const std::string& name2); // adds one edge name1 -> name2

	void _SetupConnectivityMatrix(uint g);
	bool _ReadConnectivityMatrixBin(uint g);
	bool _WriteConnectivityMatrixBin(uint g);
	void _PrecalcGraphlets(uint g);
	void _PrecalcDegreeLogs();
	void _SetupNodeIDs();

	Matrix<uchar> _conn[2];    // connectivity (uses Node::id) - filled in Prepare(). (symmetric if all edges are undirected)

	NodeMap _grpN[2];   // maps name -> node (always first node with a given name if the same name appears multiple times)
	NodeArray _grpV[2]; // contains nodes sorted by their ID. Primary storage for nodes
	std::string _networkFileNames[2];

	std::vector<std::pair<uint, uint> > prematchIdx;

public:
	const UserSettings& _settings;
protected:
	ThreadPool *_thpool;

	bool _lightPrepared;
	bool _heavyPrepared; // once set, stays set
	bool _nodeIDsDistributed;

	enum PrepareStep
	{
		PREP_DEGREE_LOGS,
		PREP_CONN_MTX_AND_GRAPHLETS,

		PREP_MAX
	};

	void _DoPrepareStep(PrepareStep prep, int param = 0);
	static void _th_DoPrepareStep(MTRand& rng, void *x = NULL, void *y = NULL, void *z = NULL);
};



#endif
