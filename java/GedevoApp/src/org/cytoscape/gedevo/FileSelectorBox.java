package org.cytoscape.gedevo;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

public class FileSelectorBox extends JPanel implements ActionListener
{
    private static File s_lastFile;
    private JButton button;
    private JTextField text;
    private JFileChooser fileChooser;
    private boolean save = false;

    @Override
    public void setEnabled(boolean on)
    {
        super.setEnabled(on);
        button.setEnabled(on);
        text.setEnabled(on);
    }

    public FileSelectorBox()
    {
        super(new BorderLayout());
        fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setMultiSelectionEnabled(false);

        button = new JButton("...");
        button.addActionListener(this);

        text = new JTextField();
        text.setMinimumSize(new Dimension(50, -1));
        text.setPreferredSize(new Dimension(130, -1));

        add(button, BorderLayout.EAST);
        add(text, BorderLayout.CENTER);
    }

    public void makeSave()
    {
        save = true;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource() == button)
        {
            File cur = getFile();
            if(cur == null)
                cur = s_lastFile;
            fileChooser.setCurrentDirectory(cur);

            int result = save ? fileChooser.showSaveDialog(this)
                              : fileChooser.showOpenDialog(this);
            if(result == JFileChooser.APPROVE_OPTION)
            {
                File selFile = fileChooser.getSelectedFile();
                text.setText(selFile.getAbsolutePath());
                s_lastFile = selFile;
            }
        }
    }

    public String getFileName()
    {
        return text.getText().trim();
    }

    public File getFile()
    {
        String fn = getFileName();
        return fn.equals("") ? null : new File(fn);
    }

    public static File ask(boolean save)
    {
        JFileChooser c = new JFileChooser();
        c.setFileSelectionMode(JFileChooser.FILES_ONLY);
        c.setMultiSelectionEnabled(false);
        if(s_lastFile != null)
            c.setCurrentDirectory(s_lastFile);
        int result = save ? c.showSaveDialog(null)
                          : c.showOpenDialog(null);
        if(result == JFileChooser.APPROVE_OPTION)
        {
            File selFile = c.getSelectedFile();
            s_lastFile = selFile;
            return selFile;
        }
        return null;
    }
}
