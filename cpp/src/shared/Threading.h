#ifndef THREADING_H
#define THREADING_H

#include <list>
#include <vector>
#include "MersenneTwister.h"
#include "UserSettings.h"

// These are all functions necessary to provide multithreading support.
// In order to keep the core clean and not pull in dependencies, these function pointers
// are set externally when required.
// The core concept are threads and waitables.
// A waitable is simply a lock/mutex that can be waited upon, and one or all waiters can be notified.
namespace threading
{
	typedef void *(*ThreadStartFunc)(const char *name, void (*body)(void*), void *user);
	typedef void (*ThreadJoinFunc)(void *th);

	typedef void *(*WaitableCreateFunc)();
	typedef void (*WaitableFunc)(void*);

	typedef unsigned (*GetNumCPUsFunc)();

	bool Init(
		ThreadStartFunc thstart,
		ThreadJoinFunc thjoin,
		WaitableCreateFunc wcreate,
		WaitableFunc wdelete,
		WaitableFunc wlock,
		WaitableFunc wunlock,
		WaitableFunc wwait,
		WaitableFunc wsignal,
		WaitableFunc wbroadcast,
		GetNumCPUsFunc ncpu);

	bool WasInit();

	void Quit();
};

class Waitable
{
public:
	Waitable();
	virtual ~Waitable();

	void lock();
	void unlock();

	void wait(); // releases the associated lock while waiting
	void signal(); // signal a single waiting thread
	void broadcast(); // signal all waiting threads

	inline void *isValid() const { return _w; }

protected:
	void *_w;
};

class MTGuard
{
public:
	MTGuard(Waitable& x) : _obj(NULL) { _init(&x); }
	MTGuard(Waitable* x) : _obj(NULL) { _init(x); }
	~MTGuard() { if(_obj) _obj->unlock(); }
private:
	void _init(Waitable *x)
	{
		if(x && x->isValid())
		{
			_obj = x;
			x->lock();
		}
	}
	Waitable *_obj;
};



typedef void (*threadFunc)(MTRand&, void *, void*, void*);

#define DEF_THREADFUNC(func) static void func(MTRand& rng, void *x = NULL, void *y = NULL, void *z = NULL)


struct JobEntry
{
	threadFunc func;
	void *param;
	void *param2;
	void *param3;
};

typedef std::list<JobEntry> JobList;

class WorkerThread;

class ThreadPool
{
public:

	ThreadPool();
	~ThreadPool();

	void start(size_t workers = 0);

	void quit(bool wait = true);
	bool addJob(threadFunc f, void *param = NULL, void *param2 = NULL, void *param3 = NULL);
	void waitUntilEmpty();

	inline bool quitting() const { return _quit; }
	bool isIdle();
	void assertIdle();
	inline size_t getThreadCount() const { return _th.size(); }

	// callbacks used by worker threads
	inline void _onJobEnd();
	inline void _fetchJob(JobEntry& e);

	inline void lock() { _lock.lock(); }
	inline void unlock() { _lock.unlock(); }
	bool addJobUnsafe(threadFunc f, void *param = NULL, void *param2 = NULL, void *param3 = NULL);
	//inline void broadcast() { _lock.broadcast(); } // tell worker threads to get going

	void (*onThreadStart)(void*);
	void (*onThreadExit)(void*);
	void *threadCallbackUserParam;

protected:

	void _spawnWorkers(size_t count);

	JobList _jobs; // used for internal synch
	Waitable _lock;
	volatile bool _quit;
	volatile int _busyThreads; // need lock
	std::vector<WorkerThread*> _th;
};



#endif
