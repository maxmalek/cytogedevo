package org.cytoscape.gedevo.integration.ccs;

public class IntPair
{
    IntPair(int p1, int p2)
    {
        a = p1;
        b = p2;
    }
    final public int a, b;
}
