package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.integration.ccs.EnumerateCCSTask;
import org.cytoscape.work.TaskMonitor;

public class WrapEnumerateCCSTask extends AbstractAlignmentTask
{
    public WrapEnumerateCCSTask(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        super.insertTasksAfterCurrentTask(new EnumerateCCSTask(common.cynet, common.graphs[0], common.graphs[1]));
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
