package org.cytoscape.gedevo.integration.visual;

import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.GedevoUtil;
import org.cytoscape.gedevo.integration.ccs.EditType;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.VisualLexicon;
import org.cytoscape.view.model.VisualProperty;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.view.vizmap.*;
import org.cytoscape.view.vizmap.mappings.BoundaryRangeValues;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.view.vizmap.mappings.DiscreteMapping;

import java.awt.*;
import java.util.*;

public class VisualStyler
{
    private GedevoApp app;
    private VisualMappingManager vmm;
    private VisualStyleFactory vsf;
    private VisualMappingFunctionFactory ffC, ffD, ffP;
    private VisualStyleRegistrar registrar;
    private Map<What, IStyleFactory> which;

    public enum What
    {
        CCS,
        AGREEMENT,
        EDITS,
        TRANSPARENT_OVERLAP,
    }

    public enum ScoreType
    {
        DISTANCE,
        SIMILARITY,
        EVALUE,
    }

    private void fillMap()
    {
        which.put(What.CCS, new VisualCCS());
        which.put(What.AGREEMENT, new VisualAgreement());
        which.put(What.EDITS, new VisualEdits());
        which.put(What.TRANSPARENT_OVERLAP, new VisualTransparentOverlap());
    }

    VisualStyler(GedevoApp a, VisualStyleRegistrar r)
    {
        app = a;
        registrar = r;

        vmm = a.cyreg.getService(VisualMappingManager.class);
        vsf = a.cyreg.getService(VisualStyleFactory.class);
        ffC = a.cyreg.getService(VisualMappingFunctionFactory.class, "(mapping.type=continuous)");
        ffD = a.cyreg.getService(VisualMappingFunctionFactory.class, "(mapping.type=discrete)");
        ffP = a.cyreg.getService(VisualMappingFunctionFactory.class, "(mapping.type=passthrough)");

        which = new IdentityHashMap<What, IStyleFactory>();
        fillMap();
    }

    public void apply(CyNetworkView cyview, What what, boolean colorblind)
    {
        apply(cyview, which.get(what), colorblind);
    }

    public void applyScore(CyNetworkView cyview, String what, ScoreType how, boolean colorblind)
    {
        VisualScore sf = new VisualScore(what, how);
        apply(cyview, sf, colorblind);
    }

    private void apply(CyNetworkView cyview, IStyleFactory sf, boolean colorblind)
    {
        VisualStyle vs = sf.makeStyle(cyview, colorblind);

        if(vs != null)
        {
            vmm.addVisualStyle(vs);

            VisualMappingFunction<?, Paint> f = vs.getVisualMappingFunction(sf.getPreviewProperty());
            if(!(f instanceof ContinuousMapping))
                f = null;
            app.mainPanel.setMappingPreview((ContinuousMapping<?, Paint>) f, sf.getDescription());

            vmm.setCurrentVisualStyle(vs);
            vmm.setVisualStyle(vs, cyview);

            // Doing this as well is prone to cause ConcurrentModificationException,
            // so I assume the above call already pokes a background thread to do it...
            //vs.apply(cyview);

            app.cyeventmgr.flushPayloadEvents();
            cyview.updateView();
            app.mainPanel.refresh();
            app.cyeventmgr.flushPayloadEvents();
        }
        else
        {
            GedevoUtil.errorbox("Failed to apply visual style");
        }
    }

    private interface IStyleFactory
    {
        public VisualStyle makeStyle(CyNetworkView cyview, boolean colorblind);

        // for use in preview image (none or both can return null to ignore preview)
        public String getDescription();
        public VisualProperty<Paint> getPreviewProperty();
    }

    private VisualStyle cloneCurrentStyle(String title)
    {
        return cloneStyle(vmm.getCurrentVisualStyle(), title);
    }

    private VisualStyle cloneStyle(VisualStyle cur, String title)
    {
        removeStyle(title);

        VisualStyle vs = vsf.createVisualStyle(title);

        Set<VisualProperty> allprops = new HashSet<VisualProperty>();
        for (VisualLexicon vl : vmm.getAllVisualLexicon())
            allprops.addAll(vl.getAllVisualProperties());

        for (VisualProperty prop : allprops)
        {
            Object val = cur.getDefaultValue(prop);
            if (val != null)
                vs.setDefaultValue(prop, val);
        }

        for (VisualMappingFunction vf : cur.getAllVisualMappingFunctions())
            vs.addVisualMappingFunction(vf);

        return vs;
    }

    private boolean removeStyle(String title)
    {
        return registrar.removeStyle(title);
    }


    private class VisualCCS implements IStyleFactory
    {
        @Override
        public String getDescription()
        {
            return "Top 10 largest CCSs are colored differently";
        }

        @Override
        public VisualProperty<Paint> getPreviewProperty()
        {
            return null;
        }

        @Override
        public VisualStyle makeStyle(CyNetworkView cyview, boolean colorblind)
        {
            VisualStyle vs = cloneCurrentStyle("[GEDEVO] Top-10 CCS");

            vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_FILL_COLOR);
            vs.removeVisualMappingFunction(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
            vs.removeVisualMappingFunction(BasicVisualLexicon.EDGE_UNSELECTED_PAINT);

            // HACK: for some reason Cytoscape doesn't apply the -1 that is *explicitly* set below...
            vs.setDefaultValue(BasicVisualLexicon.NODE_FILL_COLOR, Color.GRAY);

            DiscreteMapping<Integer, Paint> nodeColor =
                    (DiscreteMapping<Integer, Paint>) ffD.createVisualMappingFunction(ColumnNames.CCS, Integer.class, BasicVisualLexicon.NODE_FILL_COLOR);
            DiscreteMapping<Integer, Paint> edgeColor =
                    (DiscreteMapping<Integer, Paint>) ffD.createVisualMappingFunction(ColumnNames.CCS, Integer.class, BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
            DiscreteMapping<Integer, Paint> edge2Color =
                    (DiscreteMapping<Integer, Paint>) ffD.createVisualMappingFunction(ColumnNames.CCS, Integer.class, BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);
            nodeColor.putMapValue(-1, Color.GRAY); // default value - not part of any significant CCS
            edgeColor.putMapValue(-1, Color.GRAY);
            edge2Color.putMapValue(-1, Color.GRAY);

            java.util.List<Integer> ccsvals = cyview.getModel().getDefaultNodeTable().getColumn(ColumnNames.CCS).getValues(Integer.class);
            if (ccsvals.isEmpty())
                return null;

            int maxval = Collections.max(ccsvals);
            // min val is known to be -1 (= not part of any CCS)

            int numcol = Math.min(maxval, 10); // use up to ~10 distinct colors. more than that looks too cluttered and can be hard to distinguish

            if(colorblind)
            {
                boolean skipped = false;
                for (int i = 0; i <= numcol; ++i)
                {
                    float a = i / (float)(numcol+1);
                    float blue = 1.0f - a;
                    if(!skipped && Math.abs(a - blue) < 0.001) // skip completely gray, which is indistinguishable from nodes not in any CCS
                    {
                        skipped = true;
                        continue;
                    }
                    Color cn = new Color(a, a, blue);
                    Color ce = cn.darker();
                    nodeColor.putMapValue(i, cn);
                    edgeColor.putMapValue(i, ce);
                    edge2Color.putMapValue(i, ce);
                }
            }
            else
            {
                for (int i = 0; i <= numcol; ++i)
                {
                    // don't go fully around the color circle
                    float h = 0.75f * (i / (float) numcol);
                    int cn = Color.HSBtoRGB(h, 1.0f, 1.0f);
                    int ce = Color.HSBtoRGB(h, 1.0f, 0.66f);
                    nodeColor.putMapValue(i, new Color(cn));
                    Color cec = new Color(ce);
                    edgeColor.putMapValue(i, cec);
                    edge2Color.putMapValue(i, cec);
                }
            }

            for (int i = numcol + 1; i <= maxval; ++i)
            {
                nodeColor.putMapValue(i, Color.LIGHT_GRAY);
                edgeColor.putMapValue(i, Color.LIGHT_GRAY);
                edge2Color.putMapValue(i, Color.LIGHT_GRAY);
            }

            vs.addVisualMappingFunction(nodeColor);
            vs.addVisualMappingFunction(edgeColor);
            vs.addVisualMappingFunction(edge2Color);
            return vs;
        }
    }

    private class VisualScore implements IStyleFactory
    {
        private String column;
        private double[]  points;
        String descOrder;
        String scoreType;
        boolean backwards;

        public VisualScore(String col, ScoreType how)
        {
            column = col;

            switch(how)
            {
                case DISTANCE:
                    scoreType = "distance";
                    points = new double[]{0.0, 0.1, 0.5, 1.0};
                    descOrder = "best .... worst";
                    backwards = false;
                    break;
                case SIMILARITY:
                    scoreType = "similarity";
                    points = new double[]{1.0, 0.9, 0.5, 0.0};
                    descOrder = "worst .... best";
                    backwards = true;
                    break;
                case EVALUE:
                    scoreType = "E-value";
                    points = new double[]{0.0, 0.01, 0.02, 0.05};
                    descOrder = "best .... worst";
                    backwards = false;
                    break;
            }
        }


        private final Color darkblue = new Color(0, 0, 50);

        // normal colors
        private final Color lightblue = new Color(127, 201, 255);
        private final Color lightgreen = new Color(204, 255, 45);
        private final Color uglypink = new Color(255, 0, 136);

        // color blind
        private final Color cb_lightblue2 = new Color(178, 181, 255);
        private final Color cb_darkyellow = new Color(122, 122, 0);
        private final Color cb_lightyellow = new Color(255, 255, 0);

        @Override
        public VisualStyle makeStyle(CyNetworkView cyview, boolean colorblind)
        {
            VisualStyle vs = cloneCurrentStyle("[GEDEVO] Score (" + column + ")");
            vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_FILL_COLOR);

            ContinuousMapping<Double, Paint> mapping =
                    (ContinuousMapping<Double, Paint>) ffC.createVisualMappingFunction(column, Double.class, BasicVisualLexicon.NODE_FILL_COLOR);

            @SuppressWarnings("unchecked")
            BoundaryRangeValues<Paint> cv[] =
                colorblind
                ? new BoundaryRangeValues[]
                {
                    new BoundaryRangeValues<Paint>(cb_lightyellow, cb_lightyellow, cb_lightyellow),
                    new BoundaryRangeValues<Paint>(cb_darkyellow, cb_darkyellow, cb_darkyellow),
                    new BoundaryRangeValues<Paint>(cb_lightblue2, cb_lightblue2, cb_lightblue2),
                    new BoundaryRangeValues<Paint>(darkblue, darkblue, darkblue)
                }
                : new BoundaryRangeValues[]
                {
                    new BoundaryRangeValues<Paint>(uglypink, uglypink, uglypink),
                    new BoundaryRangeValues<Paint>(lightgreen, lightgreen, lightgreen),
                    new BoundaryRangeValues<Paint>(lightblue, lightblue, lightblue),
                    new BoundaryRangeValues<Paint>(darkblue, darkblue, darkblue)
                };

            // BUG WARNING:
            // must add colors IN ASCENDING ORDER OF KEYS, else they won't show up properly
            // grrr, cytoscape, you and me...
            int i = backwards ? 3 : 0;
            int d = backwards ? -1 : 1;
            for(int _ = 0; _ < 4; ++_, i += d)
                mapping.addPoint(points[i], cv[i]);

            vs.addVisualMappingFunction(mapping);
            return vs;
        }

        @Override
        public String getDescription()
        {
            return column + ": " + descOrder;
        }

        @Override
        public VisualProperty<Paint> getPreviewProperty()
        {
            return BasicVisualLexicon.NODE_FILL_COLOR;
        }
    }

    private class VisualAgreement implements IStyleFactory
    {
        @Override
        public VisualStyle makeStyle(CyNetworkView cyview, boolean colorblind)
        {
            VisualStyle vs = cloneCurrentStyle("[GEDEVO] Agreement");
            vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_FILL_COLOR);

            Color c1 = new Color(81, 0, 107);
            Color c2 = Color.WHITE;
            Color oob = colorblind ? Color.YELLOW : Color.RED;

            ContinuousMapping<Double, Paint> mapping =
                    (ContinuousMapping<Double, Paint>)ffC.createVisualMappingFunction(ColumnNames.AGREEMENT, Double.class, BasicVisualLexicon.NODE_FILL_COLOR);

            mapping.addPoint(0.9, new BoundaryRangeValues<Paint>(oob, c1, c1));
            mapping.addPoint(1.0, new BoundaryRangeValues<Paint>(c2, c2, c2));

            vs.addVisualMappingFunction(mapping);
            return vs;
        }

        @Override
        public String getDescription()
        {
            return "Agreement: lowest .... highest";
        }

        @Override
        public VisualProperty<Paint> getPreviewProperty()
        {
            return BasicVisualLexicon.NODE_FILL_COLOR;
        }
    }

    private class VisualEdits implements IStyleFactory
    {
        @Override
        public VisualStyle makeStyle(CyNetworkView cyview, boolean colorblind)
        {
            VisualStyle vs = cloneCurrentStyle("[GEDEVO] Graph edits");
            vs.removeVisualMappingFunction(BasicVisualLexicon.NODE_FILL_COLOR);
            vs.removeVisualMappingFunction(BasicVisualLexicon.EDGE_UNSELECTED_PAINT);
            vs.removeVisualMappingFunction(BasicVisualLexicon.EDGE_WIDTH);
            vs.removeVisualMappingFunction(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);

            Map<EditType, Color> colmap = new HashMap<EditType, Color>();
            // these colors should be colorblind-safe
            colmap.put(EditType.SUBSTITUTED, new Color(139, 139, 139));
            colmap.put(EditType.ADDED, new Color(82, 237, 102));
            colmap.put(EditType.DIRECTION_CHANGED, new Color(8, 36, 70));
            colmap.put(EditType.FLIPPED, new Color(142, 54, 246));
            colmap.put(EditType.DELETED, new Color(162, 75, 0));

            DiscreteMapping<Integer, Paint> nm =
                    (DiscreteMapping<Integer, Paint>)ffD.createVisualMappingFunction(ColumnNames.EDIT_TYPE, Integer.class, BasicVisualLexicon.NODE_FILL_COLOR);

            DiscreteMapping<Integer, Paint> em =
                    (DiscreteMapping<Integer, Paint>)ffD.createVisualMappingFunction(ColumnNames.EDIT_TYPE, Integer.class, BasicVisualLexicon.EDGE_UNSELECTED_PAINT);

            DiscreteMapping<Integer, Paint> em2 =
                    (DiscreteMapping<Integer, Paint>)ffD.createVisualMappingFunction(ColumnNames.EDIT_TYPE, Integer.class, BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT);

            for(Map.Entry<EditType, Color> ee : colmap.entrySet())
                nm.putMapValue(ee.getKey().ordinal(), ee.getValue());

            for(Map.Entry<EditType, Color> ee : colmap.entrySet())
                em.putMapValue(ee.getKey().ordinal(), ee.getValue());

            for(Map.Entry<EditType, Color> ee : colmap.entrySet())
                em2.putMapValue(ee.getKey().ordinal(), ee.getValue());


            nm.putMapValue(-1, vs.getDefaultValue(BasicVisualLexicon.NODE_FILL_COLOR));
            em.putMapValue(-1, vs.getDefaultValue(BasicVisualLexicon.EDGE_PAINT));
            em2.putMapValue(-1, vs.getDefaultValue(BasicVisualLexicon.EDGE_STROKE_UNSELECTED_PAINT));

            DiscreteMapping<Integer, Double> ew =
                    (DiscreteMapping<Integer, Double>)ffD.createVisualMappingFunction(ColumnNames.EDIT_TYPE, Integer.class, BasicVisualLexicon.EDGE_WIDTH);

            double ewidth = vs.getDefaultValue(BasicVisualLexicon.EDGE_WIDTH);
            ew.putMapValue(-1, ewidth);
            ew.putMapValue(EditType.SUBSTITUTED.ordinal(), ewidth*2);
            ew.putMapValue(EditType.ADDED.ordinal(), ewidth);
            ew.putMapValue(EditType.DIRECTION_CHANGED.ordinal(), ewidth);
            ew.putMapValue(EditType.FLIPPED.ordinal(), ewidth);

            vs.addVisualMappingFunction(nm);
            vs.addVisualMappingFunction(em);
            vs.addVisualMappingFunction(ew);
            vs.addVisualMappingFunction(em2);
            return vs;
        }

        @Override
        public String getDescription()
        {
            return "<html>grey=substituted, light green=added, brown=removed<br />Edges only: dark blue=dir. changed, purple=flipped</html>";
        }

        @Override
        public VisualProperty<Paint> getPreviewProperty()
        {
            return null;
        }
    }

    private class VisualTransparentOverlap implements IStyleFactory
    {
        @Override
        public VisualStyle makeStyle(CyNetworkView cyview, boolean colorblind)
        {
            VisualStyle vs = cloneCurrentStyle("[GEDEVO] Overlap transparency");

            vs.setDefaultValue(BasicVisualLexicon.NODE_TRANSPARENCY, 100);
            vs.setDefaultValue(BasicVisualLexicon.EDGE_TRANSPARENCY, 150);

            return vs;
        }

        @Override
        public String getDescription()
        {
            return null;
        }

        @Override
        public VisualProperty<Paint> getPreviewProperty()
        {
            return null;
        }
    }
}
