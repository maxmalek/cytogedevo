package org.cytoscape.gedevo.task;

import org.cytoscape.work.AbstractTask;


public abstract class GedevoTask extends AbstractTask
{
    protected boolean mustFinish = false;

    public void finishNow()
    {
        mustFinish = true;
    }

    public abstract boolean canFinishNow();
}
