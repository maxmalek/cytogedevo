package org.cytoscape.gedevo;

// ACCESSED FROM C++ CODE - DO NOT MOVE

// State of one Agent plus node mapping
public class AlignmentResult
{
    // mapping, single agent
    public int[][] nodes; // int[group][nodeId1] = nodeId2 or -1
    public double[][] scores; // double[whichScore][pairId] array of scores
    public String[] scoreNames;
}

