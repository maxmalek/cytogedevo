package org.cytoscape.gedevo;

import org.cytoscape.view.model.CyNetworkView;

public abstract class ContextMenuAction
{
    public abstract void onClick(CyNetworkView cyview);
}
