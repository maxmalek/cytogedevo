package org.cytoscape.gedevo;

import org.cytoscape.gedevo.integration.GedevoServices;
import org.cytoscape.gedevo.integration.UIShortcuts;
import org.cytoscape.gedevo.integration.ccs.EnumerateCCSTask;
import org.cytoscape.gedevo.integration.visual.VisualStyler;
import org.cytoscape.gedevo.task.CalculateEditsTask;
import org.cytoscape.gedevo.util.NetworkWrapper;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.vizmap.gui.editor.ContinuousMappingEditor;
import org.cytoscape.view.vizmap.gui.editor.EditorManager;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyEditor;
import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class GedevoMainPanelComponent extends Unloadable
{
    public JTabbedPane tabbedPane1;
    public JComboBox comboBox1;
    public JComboBox comboBox2;
    public JPanel panel1;
    private JTextField textFieldMaxAgents;
    private JTextField textFieldNumThreads;
    private JCheckBox checkBoxPrematch;
    private JTextField textFieldPairNullValue;
    private JButton startButton;
    private JComboBox comboBoxFilter1;
    private JComboBox comboBoxFilter2;
    private JPanel tasksPanel;
    private JScrollPane taskListScrollPane;
    private JCheckBox openNetworkViewWhenCheckBox;
    private JCheckBox treatAllEdgesAsCheckBox;
    private JCheckBox initialLayoutCheckBox;
    private JCheckBox keepDataCheckBox;
    private JTextField ged_EFLIP;
    private JTextField ged_D2U;
    private JTextField ged_U2D;
    private JTextField ged_NA;
    private JTextField ged_NRM;
    private JCheckBox trimNodeNamesCheckBox;
    private JTextField abort_seconds;
    private JTextField abort_iterations;
    private JTextField abort_iterationsWithoutImprovement;
    private JTextField ged_EA;
    private JTextField ged_ERM;
    private JTextField ged_ESUB;
    private JTextField basicHealth;
    private JTextField maxHealthDrop;
    private JTextField ps_graphlet;
    private JTextField ps_ged;
    private JTextField Gs_ps;
    private JTextField Gs_graphlet;
    private JTextField Gs_ged;
    private JScrollPane basicScrollPane;
    private JScrollPane advancedScrollPane;
    private JTextField agreementFraction;
    private JCheckBox ignoreSelfLoops;
    private JPanel matrixPanel;
    private JButton addMatrixButton;
    private JCheckBox appendTimeStampCheckBox;
    private FileSelectorBox saveResultsBox;
    private JCheckBox autosaveCheckBox;
    private JTextField logger_iterations;
    private JButton bMerge;
    private FileSelectorBox fileToImportBox;
    private JPanel importPanel;
    private JButton clearCacheButton;
    private JButton exportAlignmentButton;
    private JButton importAlignmentButton;
    private JButton CCSButton;
    private JButton graphEditsButton;
    private JButton generateMappingEdgesButton;
    private JButton fixateSelectedMappingEdgesButton;
    private JButton removeNonFixedButton;
    private JButton constructCombinedNetworkButton;
    private JButton agreementButton;
    private JButton scoreDistanceButton;
    private JPanel vizPanel;
    private JCheckBox greedyCheckBox;
    private JToggleButton toggleMappingMode;
    private JButton unfixateSelectedMappingEdges;
    private JButton selectSameCCSButton;
    private JButton selectPartnersButton;
    private JLabel vizIconLabel;
    private JLabel vizIconDesc;
    private JScrollPane commonTasksScrollPane;
    private JScrollPane customDataScrollPane;
    private JPanel commonTasksPanel;
    private JPanel commonTasksInnerPanel;
    private JButton scoreSimilarityButton;
    private JButton scoreEValueButton;
    private JCheckBox avoidRedGreenCheckBox;
    public JTextField mergedNetworkNameText;
    private JButton checkMatrixOrderButton;
    private JButton showMatrixHelpButton;
    private JLabel matrixHelpText;
    private JCheckBox exportAnnotatedCheckBox;
    private JCheckBox exportSelectedOnlyCheckBox;

    private GedevoApp app;
    private NetworkWrapper selNetworks[] = new NetworkWrapper[2];

    private ArrayList<CustomMatrixSelector> customMatrices = new ArrayList<CustomMatrixSelector>();

    private volatile int numTasks = 0;
    private int spinner = 0;
    private static final char[] spinnerValues = new char[] {'|', '/', '-', '\\' };

    static final private UserSettings defaultSettings = new UserSettings();

    GedevoMainPanelComponent(GedevoApp a)
    {
        app = a;
        createUIComponents();
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override public void run() { refresh(); }
        });
        (new UpdateThread()).start();
        clearCacheButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                GedevoAlignmentCache.clear();
                System.gc();
                System.runFinalization();
            }
        });
    }

    @Override
    public void unload()
    {
        app = null;
    }

    class ChooseNetworkListener implements ItemListener
    {
        JComponent dep;
        int index;
        public ChooseNetworkListener(JComponent d, int idx)
        {
            dep = d;
            index = idx;
        }

        @Override
        public void itemStateChanged(ItemEvent ev)
        {
            if(ev.getStateChange() == ItemEvent.SELECTED)
            {
                JComboBox combo = (JComboBox)ev.getSource();
                updateNetworkCombo(combo, dep, index);
            }
        }
    }

    class ButtonListener implements ActionListener
    {
        @Override
        public void actionPerformed(ActionEvent ev)
        {
            if(ev.getSource() == startButton)
                goPressed();
            else if(ev.getSource() == bMerge)
                mergePressed();
        }
    }

    private void updateMatrixSelectors(int index, Object obj)
    {
        String s = null;
        if(obj instanceof NetworkWrapper)
        {
            CyNetwork cynet = ((NetworkWrapper) obj).cynet;
            if(GedevoAlignmentUtil.isGEDEVONetwork(cynet))
            {
                try
                {
                    List<String> namelist = cynet.getDefaultNetworkTable().getRow(cynet.getSUID()).getList(ColumnNames.SOURCE_NETWORKS_LIST, String.class);
                    updateMatrixSelectors(0, namelist.get(0));
                    updateMatrixSelectors(1, namelist.get(1));
                    return;
                }
                catch (Exception ex)
                {
                    app.log(ex.toString());
                }
            }
            else
            {
                s = cynet.getRow(cynet).get(CyNetwork.NAME, String.class);
            }
        }

        if(s == null)
            s = String.valueOf(obj);

        for(CustomMatrixSelector box : customMatrices)
        {
            box.setSourceName(index, s);
        }
    }

    private void updateNetworkCombo(JComboBox combo, JComponent dep, int index)
    {
        Object sel = combo.getSelectedItem();

        comboBox1.setEnabled(true);
        comboBoxFilter1.setEnabled(true);
        comboBox2.setEnabled(true);
        comboBoxFilter2.setEnabled(true);


        if(sel instanceof NetworkWrapper)
        {
            NetworkWrapper net = (NetworkWrapper)sel;
            selNetworks[index] = net;
            boolean noView = app.cyviewmgr.getNetworkViews(net.cynet).isEmpty();
            dep.setEnabled(!noView);
        }
        else
        {
            dep.setEnabled(false);
            selNetworks[index] = null;

            if(sel == null || sel instanceof String)
            {
            }
            else if(sel instanceof GedevoUtil.Closure<?>)
            {
                ((GedevoUtil.Closure<?>)sel).run();
            }
            else
                GedevoUtil.msgbox("updateNetworkCombo: what now?");
        }

        if (selNetworks[0] != null && GedevoAlignmentUtil.isGEDEVONetwork(selNetworks[0].cynet))
        {
            startButton.setText("Continue");
            startButton.setEnabled(true);

            comboBox2.setEnabled(false);
            comboBoxFilter2.setEnabled(false);

            importPanel.setEnabled(false);
            for(Component c : importPanel.getComponents())
                c.setEnabled(false);
            bMerge.setText("Already merged!");
        }
        else if(selNetworks[0] != null && selNetworks[1] != null)
        {
            startButton.setText("Start");
            startButton.setEnabled(true);

            importPanel.setEnabled(true);
            for(Component c : importPanel.getComponents())
                c.setEnabled(true);
            bMerge.setText("Merge sources");
        }
        else
        {
            startButton.setText("Start");
            startButton.setEnabled(false);

            importPanel.setEnabled(false);
            for(Component c : importPanel.getComponents())
                c.setEnabled(false);
            bMerge.setText("Merge sources");
        }

        if(!app.hasNativeLibs())
        {
            startButton.setText("-Disabled-");
            startButton.setEnabled(false);
        }

        updateMatrixSelectors(index, sel);
    }

    private static <C extends GedevoFilters.Filter> GedevoUtil.Closure<GedevoFilters.Filter> makeFilter(final String name, final Class<C> cls)
    {
        // Complicated bullshit because java
        try
        {
            final Constructor<C> ctor = cls.getDeclaredConstructor(CyNetwork.class);
            return new GedevoUtil.NamedClosure<GedevoFilters.Filter>(name)
            {
                @Override public GedevoFilters.Filter run(Object... args2)
                {
                    GedevoFilters.Filter f = null;
                    try { f = ctor.newInstance((CyNetwork)args2[0]); } catch(Exception ex)
                    {
                        GedevoUtil.msgbox("GedevoMainPanelComponent.makeFilter(): Hard failure instantiating class: " + cls);
                    }
                    return f;
                }
            };
        } catch (Exception e)
        {
            GedevoUtil.msgbox("GedevoMainPanelComponent.makeFilter(): Hard failure instantiating factory closure: " + cls);
        }
        return null;
    }

    private void addFilters(JComboBox... combos)
    {
        ArrayList<GedevoUtil.Closure<?>> generators = new ArrayList<GedevoUtil.Closure<?>>();
        generators.add(makeFilter("All nodes, all edges", GedevoFilters.Everything.class));
        generators.add(makeFilter("Selected nodes, all edges", GedevoFilters.SelectedNodesAllEdges.class));
        generators.add(makeFilter("All nodes, selected edges", GedevoFilters.AllNodesSelectedEdges.class));
        generators.add(makeFilter("Selected nodes, Selected edges", GedevoFilters.SelectedNodesSelectedEdges.class));

        for(JComboBox b : combos)
            for(GedevoUtil.Closure<?> g : generators)
                if(g != null)
                    b.addItem(g);
    }

    public void refresh()
    {
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override
            public void run()
            {
                _refresh();
            }
        });
    }

    private void _refresh()
    {
        updateNetworkCombo(comboBox1, comboBoxFilter1, 0);
        updateNetworkCombo(comboBox2, comboBoxFilter2, 1);
        panel1.validate();
        panel1.repaint();
    }

    public void setMappingPreview(ContinuousMapping<?, Paint> mapping, String desc)
    {
        if(mapping != null)
        {
            try
            {
                // HACK: There's no real API to generate an image from a mapping, so we'll use this kludge
                EditorManager em = app.cyreg.getService(EditorManager.class);
                PropertyEditor ed = em.getContinuousEditor(mapping.getVisualProperty());
                ContinuousMappingEditor ced = (ContinuousMappingEditor)ed;
                Method m = ced.getClass().getMethod("setValue", Object.class);
                m.invoke(ced, mapping);
                ImageIcon ico = ced.drawIcon(vizPanel.getWidth()-20, 40, false); // HACK: vizIconLabel.getMaximumSize().height (== 40)doesn't work...
                GedevoApp.log("Set viz icon: " + ico);
                vizIconLabel.setText(null);
                vizIconLabel.setIcon(ico);
            }
            catch(Exception ex)
            {
                vizIconLabel.setIcon(null);
                vizIconLabel.setText("Error rendering image :(");
            }
        }
        else
            vizIconLabel.setIcon(null);

        if(desc == null)
            desc = "Nothing to preview";

        vizIconDesc.setText(desc);
    }

    public boolean isColorBlindSelected()
    {
        return avoidRedGreenCheckBox.isSelected();
    }


    private void createUIComponents()
    {
        String maxwidthstr = "abcdefghijklmnopqrstuvwxyzabcdefg";
        comboBox1.setPrototypeDisplayValue(maxwidthstr);
        comboBox2.setPrototypeDisplayValue(maxwidthstr);

        // fix super slow mouse wheel scrolling (java? wtf? why?)
        basicScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        customDataScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        advancedScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        taskListScrollPane.getVerticalScrollBar().setUnitIncrement(16);
        commonTasksScrollPane.getVerticalScrollBar().setUnitIncrement(16);

        comboBox1.addItemListener(new ChooseNetworkListener(comboBoxFilter1, 0));
        comboBox2.addItemListener(new ChooseNetworkListener(comboBoxFilter2, 1));
        ButtonListener blisten = new ButtonListener();
        startButton.addActionListener(blisten);
        bMerge.addActionListener(blisten);

        addFilters(comboBoxFilter1, comboBoxFilter2);

        // ------------

        treatAllEdgesAsCheckBox.setSelected(defaultSettings.forceUndirectedEdges);
        ignoreSelfLoops.setSelected(defaultSettings.ignoreSelfLoops);

        autosaveCheckBox.setSelected(defaultSettings.autosaveSecs != 0);

        textFieldMaxAgents.setText(Integer.toString(defaultSettings.maxAgents));
        textFieldNumThreads.setText(Integer.toString(defaultSettings.numThreads));
        textFieldPairNullValue.setText(Double.toString(defaultSettings.pairNullValue));

        checkBoxPrematch.setSelected(defaultSettings.matchSameNames);
        trimNodeNamesCheckBox.setSelected(defaultSettings.trimNames);
        greedyCheckBox.setSelected(defaultSettings.evo_greedyInitOnInit);
        abort_seconds.setText(Integer.toString(defaultSettings.abort_seconds));
        abort_iterations.setText(Integer.toString(defaultSettings.abort_iterations));
        abort_iterationsWithoutImprovement.setText(Integer.toString(defaultSettings.abort_nochange));

        ged_EA.setText(Double.toString(defaultSettings.ged_eAdd));
        ged_ERM.setText(Double.toString(defaultSettings.ged_eRm));
        ged_ESUB.setText(Double.toString(defaultSettings.ged_eSub));
        ged_EFLIP.setText(Double.toString(defaultSettings.ged_eFlip));
        ged_D2U.setText(Double.toString(defaultSettings.ged_eD2U));
        ged_U2D.setText(Double.toString(defaultSettings.ged_eU2D));
        ged_NA.setText(Double.toString(defaultSettings.ged_nAdd));
        ged_NRM.setText(Double.toString(defaultSettings.ged_nRm));

        basicHealth.setText(Float.toString(defaultSettings.basicHealth));
        maxHealthDrop.setText(Float.toString(defaultSettings.maxHealthDrop));

        ps_ged.setText(Double.toString(defaultSettings.pairWeightGED));
        ps_graphlet.setText(Double.toString(defaultSettings.pairWeightGraphlets));

        Gs_ged.setText(Double.toString(defaultSettings.weightGED));
        Gs_graphlet.setText(Double.toString(defaultSettings.weightGraphlets));
        Gs_ps.setText(Double.toString(defaultSettings.weightPairsum));

        agreementFraction.setText(Float.toString(defaultSettings.agreementFraction));

        logger_iterations.setText(Integer.toString(defaultSettings.logger_iterations));


        // can only layout when creating view (or view is already there, in which case there is no need to create a view first.
        // handled in FinishAlignmentTask.
        openNetworkViewWhenCheckBox.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
               initialLayoutCheckBox.setEnabled(openNetworkViewWhenCheckBox.isSelected());
            }
        });


        // Make sure all task panels are on top -- this one JPanel autoexpands and pushes the others to the top
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.weighty = 1;
        tasksPanel.add(new JPanel(), gbc);
        matrixPanel.add(new JPanel(), gbc);

        // Add the listener AFTERWARDS
        tasksPanel.addContainerListener(new ContainerListener()
        {
            @Override
            public void componentAdded(ContainerEvent e)
            {
                addNumTasksAndUpdateList(1);
            }

            @Override
            public void componentRemoved(ContainerEvent e)
            {
                addNumTasksAndUpdateList(-1);
            }
        });

        addMatrixButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                final CustomMatrixSelector box = new CustomMatrixSelector();
                box.setOnUnregister(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        customMatrices.remove(box);
                        matrixPanel.remove(box.getPanel());
                    }
                });

                final ActionListener mvUp = new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        moveDataBox(box, -1);
                    }
                };
                final ActionListener mvDown = new ActionListener()
                {
                    @Override
                    public void actionPerformed(ActionEvent e)
                    {
                        moveDataBox(box, 1);
                    }
                };

                box.setMoveListeners(mvUp, mvDown);
                addMatrixBox(box);
            }
        });

        showMatrixHelpButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                final boolean vis = !matrixHelpText.isVisible();
                matrixHelpText.setVisible(vis);
                showMatrixHelpButton.setText(vis ? "Hide" : "Show");
            }
        });
        matrixHelpText.setVisible(false);

        // TODO: display number in tab
        matrixPanel.addContainerListener(new ContainerListener()
        {
            @Override
            public void componentAdded(ContainerEvent e)
            {
                refresh();
            }

            @Override
            public void componentRemoved(ContainerEvent e)
            {
                refresh();
            }
        });

        saveResultsBox.makeSave();


        ActionListener buttonL = new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                Object src = e.getSource();
                if(src == importAlignmentButton)
                    UIShortcuts.importAlignment();
                else if(src == exportAlignmentButton)
                    UIShortcuts.exportAlignment(exportAnnotatedCheckBox.isSelected(), exportSelectedOnlyCheckBox.isSelected());
                else if(src == graphEditsButton)
                    recalcAndColorEdits();
                else if(src == CCSButton)
                    recalcAndColorCCS();
                else if(src == agreementButton)
                    UIShortcuts.applyVisualStyle(VisualStyler.What.AGREEMENT, isColorBlindSelected());
                else if(src == scoreDistanceButton)
                    showScorePopup((Component) src, VisualStyler.ScoreType.DISTANCE);
                else if(src == scoreSimilarityButton)
                    showScorePopup((Component) src, VisualStyler.ScoreType.SIMILARITY);
                else if(src == scoreEValueButton)
                    showScorePopup((Component) src, VisualStyler.ScoreType.EVALUE);
                else if(src == generateMappingEdgesButton)
                    UIShortcuts.generateMappingEdges();
                else if(src == fixateSelectedMappingEdgesButton)
                    UIShortcuts.fixateSelectedEdgesAndNodes();
                else if(src == removeNonFixedButton)
                    UIShortcuts.removeNonFixedMappingEdges();
                else if(src == constructCombinedNetworkButton)
                    UIShortcuts.createCombinedNetwork();
                else if(src == unfixateSelectedMappingEdges)
                    UIShortcuts.unfixateSelectedEdgesAndNodes();
                else if(src == selectPartnersButton)
                    UIShortcuts.includePartnerNodesToSelection();
                else if(src == selectSameCCSButton)
                    UIShortcuts.selectNodesAndEdgesFromSameCCS();
                else if(src == checkMatrixOrderButton)
                    checkMatrixOrder();
                else
                    GedevoUtil.msgbox("unhandled");
            }
        };

        importAlignmentButton.addActionListener(buttonL);
        exportAlignmentButton.addActionListener(buttonL);
        graphEditsButton.addActionListener(buttonL);
        CCSButton.addActionListener(buttonL);
        agreementButton.addActionListener(buttonL);
        scoreDistanceButton.addActionListener(buttonL);
        scoreSimilarityButton.addActionListener(buttonL);
        scoreEValueButton.addActionListener(buttonL);
        generateMappingEdgesButton.addActionListener(buttonL);
        fixateSelectedMappingEdgesButton.addActionListener(buttonL);
        removeNonFixedButton.addActionListener(buttonL);
        constructCombinedNetworkButton.addActionListener(buttonL);
        unfixateSelectedMappingEdges.addActionListener(buttonL);
        selectPartnersButton.addActionListener(buttonL);
        selectSameCCSButton.addActionListener(buttonL);
        checkMatrixOrderButton.addActionListener(buttonL);
        if(!GedevoApp.DEBUG)
            checkMatrixOrderButton.setVisible(false);

        toggleMappingMode.addChangeListener(new ChangeListener()
        {
            @Override
            public void stateChanged(ChangeEvent e)
            {
                boolean on = toggleMappingMode.isSelected();
                if (on) {
                    GedevoServices.linker.enableSingleLinkOnSelect();
                    toggleMappingMode.setText("Toggle mapping mode [active]");
                }
                else {
                    GedevoServices.linker.disableSingleLinkOnSelect();
                    toggleMappingMode.setText("Toggle mapping mode");
                }
            }
        });

    }

    private void moveDataBox(CustomMatrixSelector box, int dir)
    {
        ArrayList<CustomMatrixSelector> cp = new ArrayList<CustomMatrixSelector>(customMatrices);
        int idx = cp.indexOf(box);
        if(idx < 0)
            return;
        int newidx = idx + dir;
        if(newidx < 0 || newidx >= cp.size() || idx == newidx)
            return;
        cp.set(idx, cp.get(newidx));
        cp.set(newidx, box);

        matrixPanel.removeAll();
        customMatrices.clear();

        for(CustomMatrixSelector e : cp)
            addMatrixBox(e);
    }

    private void checkMatrixOrder()
    {
        String s;
        if(customMatrices.isEmpty())
            s = "No extra matrices specified";
        else
        {
            StringBuilder sb = new StringBuilder();
            for(CustomMatrixSelector box : customMatrices)
            {
                UserSettings.CustomMatrixData data = box.getData();
                sb.append(data.filename);
                sb.append("\n");
            }
            s = sb.toString();
        }
        GedevoUtil.msgbox(s);
    }

    private void showScorePopup(Component c, final VisualStyler.ScoreType ty)
    {
        GedevoUtil.Closure<Void> f = new GedevoUtil.Closure<Void>()
        {
            @Override
            public Void run(Object... args2)
            {
                UIShortcuts.applyVisualStyleForScore((String)args2[0], ty, isColorBlindSelected());
                return null;
            }
        };
        JPopupMenu menu = UIShortcuts.createScorePopupMenu(f);

        menu.show(c, 0, c.getHeight());
    }

    public void updateCommonTasksPanel(CyNetworkView currentView)
    {
        currentView = currentView == null ? UIShortcuts.getSelectedNetworkView() : currentView;
        commonTasksInnerPanel.setEnabled(currentView != null && GedevoAlignmentUtil.isGEDEVONetwork(currentView.getModel())); // goes down recursively
    }

    private void recalcAndColorCCS()
    {
        TaskIterator ti = new TaskIterator();
        for(final CyNetworkView v : app.cyappmgr.getSelectedNetworkViews())
        {
            EnumerateCCSTask ccs = new EnumerateCCSTask(v.getModel());
            ti.append(ccs);
            ti.append(new Task()
            {
                @Override
                public void run(TaskMonitor tm) throws Exception
                {
                    tm.setTitle("Coloring CCSs");
                    tm.setStatusMessage("Applying visual style...");
                    UIShortcuts.applyVisualStyle(v, VisualStyler.What.CCS, isColorBlindSelected());
                }

                @Override
                public void cancel() {}
            });
        }
        app.cytaskmgr.execute(ti);
    }

    private void recalcAndColorEdits()
    {
        TaskIterator ti = new TaskIterator();
        for(final CyNetworkView v : app.cyappmgr.getSelectedNetworkViews())
        {
            CalculateEditsTask edits = new CalculateEditsTask(v.getModel(), isUndirected());
            ti.append(edits);
            ti.append(new Task()
            {
                @Override
                public void run(TaskMonitor tm) throws Exception
                {
                    tm.setTitle("Coloring edits");
                    tm.setStatusMessage("Applying visual style...");
                    UIShortcuts.applyVisualStyle(v, VisualStyler.What.EDITS, isColorBlindSelected());
                }

                @Override
                public void cancel() {}
            });
        }
        app.cytaskmgr.execute(ti);
    }

    private GedevoFilters.Filter[] prepareFiltersForNetworkSelection()
    {
        CyNetwork cynet1 = selNetworks[0] != null ? selNetworks[0].cynet : null;
        if(cynet1 == null)
        {
            GedevoUtil.errorbox("At least 1 network must be selected");
            return null;
        }

        CyNetwork cynet2 = selNetworks[1] != null ? selNetworks[1].cynet : null;
        Object box1obj = comboBoxFilter1.isEnabled() ? comboBoxFilter1.getSelectedItem() : null;
        Object box2obj = comboBoxFilter2.isEnabled() ? comboBoxFilter2.getSelectedItem() : null;

        GedevoFilters.Filter f1 = (box1obj != null
                ? ((GedevoUtil.Closure<GedevoFilters.Filter>)box1obj).run(cynet1)
                : new GedevoFilters.Everything(cynet1));
        GedevoFilters.Filter f2 = cynet2 == null || GedevoAlignmentUtil.isGEDEVONetwork(cynet1) ? null :
                (box2obj != null
                        ? ((GedevoUtil.Closure<GedevoFilters.Filter>)box2obj).run(cynet2)
                        : new GedevoFilters.Everything(cynet2));

        ArrayList<GedevoFilters.Filter> flist = new ArrayList<GedevoFilters.Filter>();
        if(f1 != null)
            flist.add(f1);
        if(f2 != null)
            flist.add(f2);

        return flist.toArray(new GedevoFilters.Filter[flist.size()]);
    }

    private void mergePressed()
    {
        GedevoFilters.Filter[] flist = prepareFiltersForNetworkSelection();
        if(flist == null)
            return;

        app.dispatch.doMerge(flist, fileToImportBox.getFile());
    }

    private void goPressed()
    {
        GedevoFilters.Filter[] flist = prepareFiltersForNetworkSelection();
        if(flist == null)
            return;

        UserSettings settings = new UserSettings();
        if(!app.gatherSettings(settings))
        {
            GedevoUtil.errorbox("At least one setting has caused an error.\n"
                    + "This is most likely because a number failed to be parsed.\n"
                    + "Note that you can always leave a field empty to use a default value.\n"
                    + "The offending field should be marked in red; go and fix it and try again.\n");
            return;
        }

        List<String> nonexist = getNonexistInputFiles(settings);
        if(nonexist != null)
        {
            String msg = "The following files were specified as input but do not exist:\n";
            for(String s : nonexist)
                msg += s + "\n";
            GedevoUtil.errorbox(msg);
            return;
        }

        if(!settings.saveResultsFileName.isEmpty() && !settings.saveResultsAddTimeStamp && (new File(settings.saveResultsFileName).exists()))
        {
            if(!GedevoUtil.ask("Alignment result file '" + settings.saveResultsFileName + "' already exists, overwrite?", "Warning"))
                return;
        }

        app.dispatch.doAlignment(settings, flist);
    }

    private List<String> getNonexistInputFiles(UserSettings settings)
    {
        List<String> nonexist = null;
        if(settings.customMatrixData != null)
            for(UserSettings.CustomMatrixData cmdata : settings.customMatrixData)
                if(!(new File(cmdata.filename).exists()))
                    (nonexist != null ? nonexist : (nonexist = new ArrayList<String>())).add(cmdata.filename);
        return nonexist;
    }

    public boolean gatherSettings(UserSettings settings)
    {
        try
        {
            _gatherSettings(settings);
            return true;
        }
        catch(NumberFormatException ex)
        {
            return false;
        }
    }

    public boolean isUndirected()
    {
        return treatAllEdgesAsCheckBox.isSelected();
    }

    private void _gatherSettings(UserSettings settings) throws NumberFormatException
    {
        settings.mergedNetworkName = mergedNetworkNameText.getText();

        // basic settings
        settings.forceUndirectedEdges = isUndirected();
        settings.ignoreSelfLoops = ignoreSelfLoops.isSelected();

        // visualization
        settings.openViewWhenDone = openNetworkViewWhenCheckBox.isSelected();
        settings.performInitialLayout = initialLayoutCheckBox.isSelected();

        // output
        settings.autosaveSecs = autosaveCheckBox.isSelected() ? UserSettings.DEFAULT_AUTOSAVE_TIME : 0;
        settings.saveResultsFileName = saveResultsBox.getFileName();
        settings.saveResultsAddTimeStamp = appendTimeStampCheckBox.isSelected();

        // run
        settings.keepNativeInstance = keepDataCheckBox.isSelected();

        // advanced
        settings.matchSameNames = checkBoxPrematch.isSelected();
        settings.numThreads = U.getint(textFieldNumThreads, defaultSettings.numThreads);
        settings.pairNullValue = U.getdouble(textFieldPairNullValue, defaultSettings.pairNullValue);
        settings.trimNames = trimNodeNamesCheckBox.isSelected();
        settings.evo_greedyInitOnInit = greedyCheckBox.isSelected();

        // advanced - abort
        settings.abort_seconds = U.getint(abort_seconds, defaultSettings.abort_seconds);
        settings.abort_iterations = U.getint(abort_iterations, defaultSettings.abort_iterations);
        settings.abort_nochange = U.getint(abort_iterationsWithoutImprovement, defaultSettings.abort_nochange);

        // advanced - population
        settings.maxAgents = U.getint(textFieldMaxAgents, defaultSettings.maxAgents);
        settings.basicHealth = U.getfloat(basicHealth, defaultSettings.basicHealth);
        settings.maxHealthDrop = U.getfloat(maxHealthDrop, defaultSettings.maxHealthDrop);

        // advanced - pair weights
        settings.pairWeightGED = U.getdouble(ps_ged, defaultSettings.pairWeightGED);
        settings.pairWeightGraphlets = U.getdouble(ps_graphlet, defaultSettings.pairWeightGraphlets);

        // advanced - global weights
        settings.weightGED = U.getdouble(Gs_ged, defaultSettings.weightGED);
        settings.weightGraphlets = U.getdouble(Gs_graphlet, defaultSettings.weightGraphlets);
        settings.weightPairsum = U.getdouble(Gs_ps, defaultSettings.weightPairsum);

        // advanced - GED
        settings.ged_eAdd = U.getdouble(ged_EA, defaultSettings.ged_eAdd);
        settings.ged_eRm = U.getdouble(ged_ERM, defaultSettings.ged_eRm);
        settings.ged_eSub = U.getdouble(ged_ESUB, defaultSettings.ged_eSub);
        settings.ged_eFlip = U.getdouble(ged_EFLIP, defaultSettings.ged_eFlip);
        settings.ged_eD2U = U.getdouble(ged_D2U, defaultSettings.ged_eD2U);
        settings.ged_eU2D = U.getdouble(ged_U2D, defaultSettings.ged_eU2D);
        settings.ged_nAdd = U.getdouble(ged_NA, defaultSettings.ged_nAdd);
        settings.ged_nRm = U.getdouble(ged_NRM, defaultSettings.ged_nRm);

        // advanced - visualization related
        settings.agreementFraction = U.getfloat(agreementFraction, defaultSettings.agreementFraction);

        // advanced - debugging stuff
        settings.logger_iterations = U.getint(logger_iterations, defaultSettings.logger_iterations);
        if(settings.logger_iterations > 0 && !settings.saveResultsFileName.isEmpty())
            settings.logger_file = settings.saveResultsFileName + ".log";

        assignCustomMatrixData(settings);
    }

    private void assignCustomMatrixData(UserSettings settings)
    {
        if(customMatrices.isEmpty())
        {
            settings.customMatrixData = null;
            return;
        }

        ArrayList<UserSettings.CustomMatrixData> datalist = new ArrayList<UserSettings.CustomMatrixData>();

        for(CustomMatrixSelector sel : customMatrices)
        {
            UserSettings.CustomMatrixData data = sel.getData();
            if (!data.filename.isEmpty())
                datalist.add(data);
        }

        settings.customMatrixData = datalist.toArray(new UserSettings.CustomMatrixData[datalist.size()]);
    }

    public void addTaskBox(GedevoTaskBox box)
    {
        // via https://stackoverflow.com/questions/14615888/list-of-jpanels-that-eventually-uses-a-scrollbar
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        box.setRemovalLock(this);
        tasksPanel.add(box.getPanel(), gbc, 0);
    }

    private void addMatrixBox(CustomMatrixSelector box)
    {
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        matrixPanel.add(box.getPanel(), gbc, -1);

        customMatrices.add(box);

        updateMatrixSelectors(1, comboBox2.getSelectedItem());
        updateMatrixSelectors(0, comboBox1.getSelectedItem());
    }

    private synchronized void addNumTasksAndUpdateList(int add)
    {
        numTasks += add;
        updateTaskList();
    }

    // call this only with lock held
    private void updateTaskList()
    {
        _updateSpinner();

        panel1.validate();
        panel1.repaint();
    }

    // call this only with lock held
    private void _updateSpinner()
    {
        int tabId = tabbedPane1.indexOfComponent(taskListScrollPane);
        if (tabId >= 0)
        {
            StringBuilder sb = new StringBuilder();
            if (numTasks != 0 && spinner >= 0)
                sb.append("[").append(spinnerValues[spinner]).append("] ");
            sb.append("Job list");
            if (numTasks != 0)
                sb.append(" (").append(numTasks).append(")");
            tabbedPane1.setTitleAt(tabId, sb.toString());
        }
    }

    private synchronized void tickSpinner()
    {
        if(spinner >= 0 && numTasks != 0)
        {
            spinner = (spinner + 1) % spinnerValues.length;
            _updateSpinner();
        }
    }


    private class UpdateThread extends Thread implements IUnloadable
    {
        private volatile boolean exit = false;
        private GedevoApp app = GedevoMainPanelComponent.this.app;

        UpdateThread()
        {
            unloadLater(this);
        }

        public void run()
        {
            run2();
            app.log("GedevoMainPanelComponent::UpdateThread exited");
        }

        private void run2()
        {
            while(true)
            {
                synchronized (this)
                {
                    if (exit)
                        return;
                }
                tickSpinner();
                try { sleep(100); } catch (InterruptedException e) { return; }
            }
        }

        @Override
        public synchronized void unload()
        {
            exit = true;
            interrupt();
        }
    }

}
