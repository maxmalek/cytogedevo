#include "Network.h"
#include <algorithm>

void SimpleNetwork::addNode(const char *name)
{
	_nodes.push_back(name);
}

void SimpleNetwork::addEdge(uint a, uint b)
{
	_edges.push_back(std::make_pair(a, b));
}

bool SimpleNetwork::prepare() const
{
	// Check for out-of-range edge indices
	{
		const size_t sz = _nodes.size();
		for(EdgeList::const_iterator it = _edges.begin(); it != _edges.end(); ++it)
			if(it->first >= sz || it->second >= sz)
				return false;
	}

	return true;
}

