package org.cytoscape.gedevo.integration.customlayoutmenu;

import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.pairlayout.GedevoPairLayoutWrapper;
import org.cytoscape.util.swing.GravityTracker;
import org.cytoscape.util.swing.MenuGravityTracker;
import org.cytoscape.work.swing.DialogTaskManager;

import javax.swing.*;

public class LayoutMenu extends JMenu
{
    private LayoutMenuPopulator pop;
    private GravityTracker gtracker;

    public LayoutMenu(String label, GedevoApp app)
    {
        super(label);

        gtracker = new MenuGravityTracker(this);
        // From the cookbook:
        // "We use the synchronous task manager otherwise the visual style and updateView() may occur before the view is relayed out"
        // This works, while the DialogTaskManager does not cause the view update to reflect the changes.
        pop = new LayoutMenuPopulator(gtracker, app.cyappmgr, app.cyreg.getService(DialogTaskManager.class));
    }

    public void addLayoutWrapper(GedevoPairLayoutWrapper wrap)
    {
        pop.addLayout(wrap);
    }

    public void removeLayoutWrapper(GedevoPairLayoutWrapper wrap)
    {
        pop.removeLayout(wrap);
    }

}
