#ifndef AGENT_MGR_H
#define AGENT_MGR_H

// -- AgentMgr --
// Base class to manage a set of Agents.
// Contains code that is of use for both Bees & Evo,
// and does the common initialization.

#include "common.h"
#include "MersenneTwister.h"
#include <vector>
#include "Agent.h"

class Node;
class ProteinHolder;
class DataHolder;

typedef std::vector<Agent*> AgentArray;

class AgentMgr
{
public:
	AgentMgr(const DataHolder& dh);
	virtual ~AgentMgr();

	void Init(const ProteinHolder& pholder); // must be called before Agent creation

	//drops duplicate agents in linear running time
	void DropDuplicates();
	void Sort();
	void SortForDuplicateRemoval();

	inline uint GetDeadAgentsCount() const { return (uint)_deadAgents.size(); }

	// Removes NULL entries from agents vector
	void Compact();

	const Agent *GetRandomAgent(MTRand& rng) const;

	// select one in agents[0 ... min(limit, size-1)]
	const Agent *GetRandomAgent(MTRand& rng, uint limit) const;



	//remember: array sorted by scores!
	Agent *getBestAgent() const
	{
		ASSERT(agents.size());
		return agents[0];
	}
	Agent *getWorstAgent() const
	{
		ASSERT(agents.size());
		return agents[agents.size()-1];
	}


	// Efficient replacement for continuous new/delete.
	Agent *newAgent();
	void deleteAgent(Agent *a);

	void Clear();

	// For debugging
	void AssertCompacted();


	AgentArray agents;

	const DataHolder& _dataH;

private:

	AgentArray _deadAgents; // used for recycling, to avoid excessive memory juggling
};

#endif
