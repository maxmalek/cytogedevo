package org.cytoscape.gedevo;

import org.cytoscape.gedevo.simplenet.Edge;
import org.cytoscape.gedevo.simplenet.Graph;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public final class GedevoNative
{
    private GedevoNative() {} // no ctor

    public static boolean init()
    {
        return initN();
    }
    public static void shutdown()
    {
        shutdownN();
    }

    public static boolean initThreading()
    {
        return initThreadingN();
    }

    private static native boolean initN();
    private static native boolean initThreadingN();
    private static native void shutdownN();


    //----------------------------------------------------------------------------------------------
    // Threading functions used by the C++ TheadPool class in Java mode.

    // void (*func)(void*), void *user
    private native static void callThreadLaunchFuncPtr(long func, long user);

    // launch a C++ function pointer in a new Java thread
    private static Thread startThread(final String name, final long func, final long user)
    {
        Thread th = new Thread(name)
        {
            @Override
            public void run()
            {
                // Call back into C++, interpret the long as a function pointer, and jump into it
                // This thread will continue to live until the ThreadPool dies.
                callThreadLaunchFuncPtr(func, user);
            }
        };
        try
        {
            // This is supposed to be a background worker thread. According to the docs this may fail; no problem if it does.
            th.setPriority(Thread.MIN_PRIORITY);
        }
        catch(Exception ex)
        {
        }

        th.start();
        return th;
    }

    private static void joinThread(final Thread th)
    {
        try
        {
            th.join();
        }
        catch (InterruptedException e)
        {
        }
    }

    private final static class Waitable
    {
        private Lock lock;
        private Condition cond;

        public Waitable()
        {
            lock = new ReentrantLock();
            cond = lock.newCondition();
        }
        public void lock()
        {
            lock.lock();
        }
        public void unlock()
        {
            lock.unlock();
        }
        public void condWait()
        {
            cond.awaitUninterruptibly();
        }
        public void condSignal()
        {
            cond.signal();
        }
        public void condBroadcast()
        {
            cond.signalAll();
        }
    }

    private static Waitable createWaitable()
    {
        return new Waitable();
    }
    private static void lockWaitable(Waitable w)
    {
        w.lock();
    }
    private static void unlockWaitable(Waitable w)
    {
        w.unlock();
    }
    private static void waitWaitable(Waitable w)
    {
        w.condWait();
    }
    private static void signalWaitable(Waitable w)
    {
        w.condSignal();
    }
    private static void broadcastWaitable(Waitable w)
    {
        w.condBroadcast();
    }
    private static int countCPUCores() { return Runtime.getRuntime().availableProcessors(); }



    //-----------------------------------------------------------------------------------------

    public final static class Network
    {
        private long handle;
        public long getHandle()
        {
            return handle;
        }

        public String getName() { return graph.getName(); }

        private Graph graph;
        public Graph getGraph() { return graph; }
        public Edge[] getEdges() { return graph.edges; }

        private Network()
        {
            handle = constructN();
        }

        private Network(long h)
        {
            if(h == 0)
                throw new IllegalArgumentException("0 handle");
            handle = h;
        }


        @Override
        protected void finalize() throws Throwable
        {
            destructN(handle);
            handle = 0;
            super.finalize();
        }

        public static Network createFromArrays(String[] names, int[] a, int[] b)
        {
            return new Network(createFromArraysN(names, a, b));
        }

        native private static long constructN();
        native private static void destructN(long h);
        native private static long createFromArraysN(String[] names, int[] a, int[] b);

        @Override
        public String toString()
        {
            return "GedevoNative.Network[0x" + Long.toHexString(handle) + "]";
        }

        public static Network convertToNative(Graph g)
        {
            String[] nodeNames = new String[g.nodes.length];
            for(int i = 0; i < nodeNames.length; ++i)
                nodeNames[i] = g.nodes[i].name;

            Network net = createFromArrays(nodeNames, g.eSrc, g.eDst);
            net.graph = g;
            return net;
        }
    }

    public final static class Instance
    {
        private long handle;

        private Network[] networks;

        @Override
        public String toString()
        {
            return "GedevoNative.Instance[0x" + Long.toHexString(handle) + "]";
        }


        public enum Algo
        {
            ALGO_EVO,
            ALGO_BEES,

            ALGO_MAX,
        }

        public static Instance create(UserSettings settings)
        {
            Instance ins = new Instance(settings);
            if(ins.handle == 0)
                return null;
            if(!ins.preInit(settings.usedAlgo.ordinal(), settings))
                return null;
            return ins;
        }

        private Instance(UserSettings settings)
        {
            handle = constructN(settings.numThreads != 1);
        }


        @Override
        protected void finalize() throws Throwable
        {
            destructN(handle);
            handle = 0;
            super.finalize();
        }

        public AlignmentResult getAlignmentResult(long idx)
        {
            AlignmentResult state = new AlignmentResult();
            return writeOutResultN(handle, state, idx) ? state : null;
        }
        public boolean fillAlignmentInfo(long idx, AlignmentInfo info)
        {
            return writeOutInfoN(handle, info, idx);
        }

        // forwarders
        public boolean applyPrematch(int[] a, int[] b) { return applyPrematchN(handle, a, b); }
        public boolean constructAgent(int[] a, int[] b) { return constructAgentN(handle, a, b); }
        public boolean applySettings(UserSettings settings)
        {
            return applySettingsN(handle, settings);
        }
        public boolean preInit(int algoid, UserSettings settings) { return preInitN(handle, algoid, settings); }
        public boolean init1() { return init1N(handle); }
        public boolean init2() { return init2N(handle); }
        public synchronized void update(int iterations) { updateN(handle, iterations); }
        public void shutdown() { shutdownN(handle); }
        public synchronized boolean isDone() { return isDoneN(handle); }
        public long getNumAgents() { return getNumAgentsN(handle); }
        public boolean importNetwork(Network net, int group)
        {
            if(net == null || net.getHandle() == 0)
                throw new IllegalArgumentException("huh?");

            if(importNetworkN(handle, net.getHandle(), group, net.getName()))
            {
                if(networks == null || networks.length < group + 1)
                {
                    Network[] nn = new Network[group + 1];
                    if(networks != null)
                        System.arraycopy(networks, 0, nn, 0, networks.length);
                    networks = nn;
                }
                networks[group] = net;
                return true;
            }
            return false;
        }
        public synchronized void setQuit(boolean quit) { setQuitN(handle, quit); }


        native private static long constructN(boolean useThreadPool);
        native private static void destructN(long h);
        native private static boolean importNetworkN(long h, long neth, int group, String name);
        native private static boolean isDoneN(long h);
        native private static boolean preInitN(long h, int algoid, UserSettings settings);
        native private static boolean init1N(long h);
        native private static boolean init2N(long h);
        native private static void updateN(long h, int iterations);
        native private static void shutdownN(long h);
        native private static boolean applyPrematchN(long h, int[] a, int[] b);
        native private static boolean constructAgentN(long h, int[] a, int[] b);
        native private static boolean applySettingsN(long h, UserSettings settings);
        native private static void setQuitN(long h, boolean quit);
        native private static boolean writeOutResultN(long h, AlignmentResult state, long idx);
        native private static boolean writeOutInfoN(long h, AlignmentInfo state, long idx);
        native private static long getNumAgentsN(long h);
    }
}
