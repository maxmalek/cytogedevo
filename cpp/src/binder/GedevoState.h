#ifndef PROGRAM_INSTANCE_H
#define PROGRAM_INSTANCE_H

#include "UserSettings.h"

class AlgoBase;
class ProgramState;
class ProteinHolder;
class DataHolder;
class AgentMgr;
class ThreadPool;

class ProgramInstance
{
public:
	ProgramInstance(bool useThreadPool = true);
	~ProgramInstance();

	bool preInit(AlgoID id);
	bool init1();
	bool init2();
	void update();
	void shutdown();

	AlgoBase *createAlgoByID(int id); // of enum AlgoID
	void setQuit(bool quit = true);
	
	inline bool isDone() const { return _done; }
	inline AgentMgr *getAgentMgr() const { return _agentMgr; }
	inline const ProgramState *getProgramState() const { return _progstate; }
	inline AlgoBase *getAlgo() { return _algo; }
	ProteinHolder& getProteinHolder();
	DataHolder& getDataHolder();
	inline ThreadPool *getThreadPool() { return _thpool; } // NULL if single-threaded

protected:
	AlgoBase *_algo;
	ThreadPool *_thpool;
	ProgramState *_progstate;
	AgentMgr *_agentMgr;

public:
	UserSettings settings;

protected:
	volatile bool _done;
};


#endif
