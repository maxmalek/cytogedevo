package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.GedevoAlignmentCache;
import org.cytoscape.work.TaskMonitor;

public class CacheAlignmentDataTask extends AbstractAlignmentTask
{

    public CacheAlignmentDataTask(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        GedevoAlignmentCache.cache(common);
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
