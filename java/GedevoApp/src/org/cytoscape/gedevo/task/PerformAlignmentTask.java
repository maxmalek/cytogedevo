package org.cytoscape.gedevo.task;


import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.work.TaskMonitor;

public class PerformAlignmentTask extends AbstractAlignmentTask
{
    public PerformAlignmentTask(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor mon) throws Exception
    {
        mon.setTitle("Performing Gedevo alignment...");
        mon.setProgress(0);

        final int maxsteps = common.settings.abort_iterations;
        final int nochange = common.settings.abort_nochange;
        final int maxsec = common.settings.abort_seconds;

        if(maxsteps <= 0)
            mon.setProgress(-1);
        for(int i = 0; (i < maxsteps || maxsteps <= 0) && !common.instance.isDone() && !cancelled; ++i)
        {
            common.instance.fillAlignmentInfo(0, common.info);

            mon.setStatusMessage(
                      "Iteration " + common.info.iterations
                    + ", EC: " + (float)common.info.edgeCorrectness // less precision print
                    + ", Life: " + common.info.lifeTime
                    + ", Pop: " + common.info.numAgents
            );
            double progress = -1.0;
            if(maxsteps > 0)
                progress = i / (double)maxsteps;
            if(nochange > 0)
                progress = Math.max(progress, common.info.iterationsWithoutScoreChange / (double)nochange);
            if(maxsec > 0)
                progress = Math.max(progress, common.info.runningTime / (double)maxsec);

            mon.setProgress(progress);
            common.instance.update(1);
        }

        if(cancelled)
            return;

        mon.setProgress(1);
        mon.setStatusMessage("Shutdown instance & writing report...");
        common.instance.shutdown();

        mon.setStatusMessage("Linking mapped nodes...");
        GedevoAlignmentUtil.linkPartnerNodes(common.cynet, common.instance.getAlignmentResult(0), common.graphs);
    }


    @Override
    public void cancel()
    {
        super.cancel();
        common.instance.setQuit(true);
    }

    @Override
    public void finishNow()
    {
        common.instance.setQuit(true);
    }

    @Override
    public boolean canFinishNow()
    {
        return true;
    }
}
