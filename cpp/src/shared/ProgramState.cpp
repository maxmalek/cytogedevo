#include "ProgramState.h"
#include <ctime>
#include <float.h>
#include <sstream>
#include <fstream>
#include <signal.h>
#include <algorithm>
#include "tools.h"
#include "ResultPrinter.h"
#include "Threading.h"

// MSVC silliness
#ifdef _MSC_VER
#pragma warning(disable: 4355)
#endif

ProgramState::ProgramState(UserSettings& user, ThreadPool *thpool)
 : ph(user)
 , dh(user)
 , settings(user)
 , networksLoaded(0)
 , _mustQuit(false)
 , _isSaving(false)
 , _iterationCount(0)
 , _startTime(0)
 , _iterationsWithoutScoreChange(0)
 , _lastScore(FLT_MAX)
 , _startIterationCount(0)
 , _totalRunningTime(0)
 , _lastSaveTime(0)
 , _seriesCounter(0)
 , workObject(NULL)
 , _logger(*this)
 , thpool(thpool)
{
	// Crosslink internal structures
	dh.SetThreadPool(thpool);
	dh.SetProteinHolder(&ph);
	ph.SetThreadPool(thpool);
}

ProgramState::~ProgramState()
{
}

void ProgramState::Shutdown()
{
	printf("ProgramState: Shutdown...\n");
	_logger.close();
	SaveData();
	printf("ProgramState: Shutdown complete.\n");
}

bool ProgramState::SaveResults(std::string fn)
{
	if(fn.empty())
		return true;

	if(settings.saveResultsAddTimeStamp)
		addTimestampToFileName(fn, &_startTime);

	if(settings.saveSeries)
	{
		char buf[32];
		sprintf(buf, "_%05u", _seriesCounter);
		++_seriesCounter;
		addToFileName(fn, buf);
	}

	std::ofstream outf(fn.c_str());
	if(outf)
	{
		printf("Writing results to '%s' ...\n", fn.c_str());
		ResultPrinter::AsList(*this, this->GetWorkObject()->getBestAgent(), outf);
		outf.close();
		return true;
	}

	printf("Failed to open '%s' for writing!\n", fn.c_str());
	return false;
}

void ProgramState::SaveData()
{
	// generates default name if empty string passed
	SaveResults(settings.saveResultsFileName);
}

bool ProgramState::Init1(AlgoBase& wobj)
{
	workObject = &wobj;
	workObject->Clear();

	if(thpool)
		thpool->start(settings.numThreads);

	if(!PrepareLight())
	{
		printf("ERROR: ProgramState::PrepareLight() failed!\n");
		return false;
	}

	if(!_Resume())
		return false;

	return true;
}

bool ProgramState::Init2()
{
	if(!PrepareHeavy())
	{
		printf("ERROR: ProgramState::PrepareHeavy() failed!\n");
		return false;
	}

	if(_startTime)
		_totalRunningTime += GetRunningTime();

	_startTime = time(NULL);
	_startIterationCount = _iterationCount;
	_iterationsWithoutScoreChange = 0;
	_lastScore = FLT_MAX;

	if(settings.logger_iterations && settings.logger_file.length())
	{
		std::string fn = settings.logger_file;
		if(settings.saveResultsAddTimeStamp)
			addTimestampToFileName(fn, &_startTime);

		if(_logger.open(fn.c_str(), &_startTime))
			printf("Opened log file '%s', logging every %u iterations.\n", fn.c_str(), settings.logger_iterations);
		else
			printf("WARNING: Failed to open log file '%s'\n", fn.c_str());
	}
	else
	{
		printf("Logging disabled.\n");
	}

	workObject->Init();
	return true;
}

void ProgramState::NextIteration()
{
	_logger.log();

	++_iterationCount;

	const score sc = workObject->getBestAgent()->getScore();
	if(sc < _lastScore)
	{
		_iterationsWithoutScoreChange = 0;
		_lastScore = sc;
	}
	else
		++_iterationsWithoutScoreChange;

	if(settings.autosaveSecs)
	{
		const uint runtime = GetRunningTime();
		const uint runtimeWithoutSave = runtime - _lastSaveTime;
		if(runtimeWithoutSave >= settings.autosaveSecs)
		{
			SaveData();
			_lastSaveTime = runtime;
		}
	}
}

uint ProgramState::GetRunningTime() const
{
	return uint(time(NULL) - _startTime);
}

bool ProgramState::IsDone()
{
	if(settings.abort_seconds)
	{
		if(GetRunningTime() > settings.abort_seconds)
			return true;
	}

	if(settings.abort_iterations && ((_iterationCount - _startIterationCount) >= settings.abort_iterations))
		return true;

	if(settings.abort_nochange && workObject)
	{
		if(_iterationsWithoutScoreChange >= settings.abort_nochange)
			return true;
	}

	return false;
}

bool ProgramState::PrepareLight()
{
	if(!settings.CheckAndApply())
	{
		printf("!! Settings check failed, settings are not sane and will cause problems.\n");
		return false;
	}

	// Set everything up, precalc data, etc.
	if(!ph.PrepareLight())
		return false;

	if(!dh.PrecalcLight())
		return false;

	return true;
}

bool ProgramState::PrepareHeavy()
{
	if(!ph.PrepareHeavy())
		return false;

	if(!dh.PrecalcHeavy())
		return false;

	return true;
}

bool ProgramState::HandleCommandlineCmd(const char *str)
{
	if(!str || !*str)
		return false;

	const std::string line = str;
	size_t eq = line.find('=');
	if(eq == std::string::npos)
		return false;

	// do not allow spaces here
	size_t sp = line.find(' ');
	if(sp != std::string::npos)
		return false;

	std::string cmd = line.substr(0, eq);
	std::string param = line.substr(eq + 1);

	return settings.Import(cmd, param);
}

bool ProgramState::ReadNetworkSIF(const std::string& fn, uint groupIdx)
{
	return ph.ReadNetworkSIF(fn, groupIdx);
}

bool ProgramState::ReadNetworkEDGELIST(const std::string& fn, uint groupIdx, bool hasHeader)
{
	return ph.ReadNetworkEDGELIST(fn, groupIdx, hasHeader);
}

bool ProgramState::_Resume()
{
	for(std::list<std::string>::iterator it = _resumeList.begin(); it != _resumeList.end(); ++it)
		if(!_ResumeFromFile(*it))
			return false;
	_resumeList.clear();
	return true;
}

bool ProgramState::_ResumeFromFile(const std::string& fn)
{
	std::ifstream f(fn.c_str());
	if(!f)
		return false;

	std::string line, t;
	std::vector<std::string> v, c1, c2;
	std::stringstream ss;
	while(std::getline(f, line))
	{
		chompNewline(line);
		if(line.empty() || line[0] == '#')
			continue;

		uint cols = 0;
		for( ; ss >> t; ++cols)
			v.push_back(t);

		switch(cols)
		{
			case 0:
			case 1:
				printf("ProgramState::_ResumeFromFile(): Not enough columns");
				return false;

			// 2 columns, assume simple export
			case 2:
				c1.push_back(v[0]);
				c2.push_back(v[1]);
				v.clear();
				goto readmore;

			// >2 columns, assume there was a header, and throw it away.
			// this is not as flexible as the java code for importing an alignment result,
			// because this assumes the first two columns to be P1, P2
			// but then again, we don't need any of the other things here.
			// FIXME: carry over prematched state too (and allow arbitrary column order)
			default:
				if(v[0] != "P1" || v[1] != "P2")
				{
					printf("Header mismatch (Expected P1 P2)\n");
					return false;
				}

				readmore:
				while(std::getline(f, line))
				{
					chompNewline(line);
					if(line.empty() || line[0] == '#')
						continue;
					ss.str(line);
					ss >> t;
					c1.push_back(t);
					ss >> t;
					c2.push_back(t);
				}
				goto done;
		}
	}

done:

	if(c1.empty())
		return false;

	return _ImportMapping(c1, c2);
}

bool ProgramState::_ImportMapping(const std::vector<std::string>& va, const std::vector<std::string>& vb)
{
	ASSERT(va.size() == vb.size());
	std::vector<const Node*> an(va.size(), NULL), bn(vb.size(), NULL);

	uint ahits = 0, bhits = 0;
	const uint varGroup = settings.varGroup;
	for(size_t i = 0; i < va.size(); ++i)
	{
		// Returns NULL if name is "-" -- if not, user stupidity ensued (one does not simply name a node "-"!)
		const Node *anode = ph.GetByName(varGroup, va[i]);
		const Node *bnode = ph.GetByName(varGroup, vb[i]);
		an[i] = anode;
		bn[i] = bnode;
		ahits += !!anode;
		bhits += !!bnode;
	}

	// Figure out which column corresponds to which network
	return bhits > ahits
		? _ImportMapping(bn, an)
		: _ImportMapping(an, bn);
}


bool ProgramState::_ImportMapping(const std::vector<const Node*>& mynodes, const std::vector<const Node*>& refnodes)
{
	AgentMgr& amgr = workObject->GetAgentMgr();
	Agent *a = amgr.newAgent();
	NodeMapping& my = a->getMappingNonConst();
	const NodeMapping& ref = dh.GetRefMapping();

	// where in ref array is node #i?
	std::vector<uint> whereIsRefNode(ref.size(), -1); // -1 to catch errors
	GEDPopulateIndex(ref, &whereIsRefNode[0]);

	for(size_t i = 0; i < mynodes.size(); ++i)
	{
		// Pairs passed from java are always valid (i.e. no pairs with a NULL on either side)
		const Node *mynode = mynodes[i];
		if(mynode)
		{
			const Node *refnode = refnodes[i];
			uint indexInRef = whereIsRefNode[refnode->id - 1];
			ASSERT(ref.node[indexInRef] == refnode);
			ASSERT(!my.node[indexInRef]);
			my.set(indexInRef, mynode);
		}
		// else keep unmapped, will be fixed up below
	}

	a->completePartialMapping();
	workObject->enqueueAgent(a);
	return true;
}

bool ProgramState::Resume(const std::string& fn)
{
	std::ifstream in(fn.c_str());
	if(!in)
	{
		printf("Resume: Failed to open file: '%s'", fn.c_str());
		return false;
	}
	in.close();
	_resumeList.push_back(fn);
	return true; // not reaing in the file right now; networks must be prematched first so that prematching state can be set properly
}
