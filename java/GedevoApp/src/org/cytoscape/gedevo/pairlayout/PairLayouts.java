package org.cytoscape.gedevo.pairlayout;

import java.util.ArrayList;
import java.util.List;


public final class PairLayouts
{
    private PairLayouts() {}

    public static final List<IPairLayout.Factory> supported = new ArrayList<IPairLayout.Factory>();

    static
    {
        supported.add(SideBySide.getFactory());
        supported.add(Overlapped.getFactory());
    }
}
