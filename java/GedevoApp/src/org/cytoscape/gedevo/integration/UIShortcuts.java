package org.cytoscape.gedevo.integration;

import org.cytoscape.gedevo.*;
import org.cytoscape.gedevo.integration.visual.VisualStyler;
import org.cytoscape.gedevo.pairlayout.PairLayoutTask;
import org.cytoscape.gedevo.task.ExportAlignmentTask;
import org.cytoscape.gedevo.task.ImportAlignmentFileTask;
import org.cytoscape.gedevo.util.CyNodePair;
import org.cytoscape.model.*;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;


// Commonly used UI functionality put into one place to avoid code duplication
// This is used by context menu, buttons, and varous other services

public final class UIShortcuts
{
    private static GedevoApp getApp()
    {
        return GedevoApp.appInstance;
    }

    public static List<CyNetworkView> getSelectedNetworkViews()
    {
        return getApp().cyappmgr.getSelectedNetworkViews();
    }

    public static CyNetworkView getSelectedNetworkView()
    {
        List<CyNetworkView> sel =  getApp().cyappmgr.getSelectedNetworkViews();
        return sel.isEmpty() ? null : sel.get(0);
    }

    private static boolean checkView(CyNetworkView cyview)
    {
        return cyview != null && GedevoAlignmentUtil.checkIsGEDEVONetwork(cyview.getModel());
    }

    public static void importAlignment(final CyNetworkView cyview)
    {
        if(!checkView(cyview))
            return;

        Task importer = ImportAlignmentFileTask.askForLoad(cyview.getModel());
        if (importer != null)
        {
            getApp().cytaskmgr.execute(new TaskIterator(
                    importer,
                    new Task()
                    {
                        @Override
                        public void run(TaskMonitor taskMonitor) throws Exception
                        {
                            getApp().cyeventmgr.flushPayloadEvents();
                            cyview.updateView();
                        }

                        @Override
                        public void cancel()
                        {
                        }
                    }
            ));
        }
    }

    public static void importAlignment()
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            importAlignment(view);
    }

    // -------------------------------

    public static void exportAlignment(final CyNetworkView cyview, boolean annotated, boolean selectedOnly)
    {
        if(!checkView(cyview))
            return;

        Task t = ExportAlignmentTask.askForSave(cyview.getModel(), annotated, selectedOnly);
        if(t != null)
            getApp().cytaskmgr.execute(new TaskIterator(t));
    }

    public static void exportAlignment(boolean annotated, boolean selectedOnly)
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            exportAlignment(view, annotated, selectedOnly);
    }

    // ---------------------------------

    public static void createCombinedNetwork(final CyNetworkView cyview)
    {
        if(!checkView(cyview))
            return;

        final GedevoApp app = getApp();
        CyLayoutAlgorithm layout = app.cylayoutmgr.getLayout("grid");
        if (layout == null)
            layout = app.cylayoutmgr.getDefaultLayout();

        final PairLayoutTask pairlayout = new PairLayoutTask(cyview, layout, null, null, null, null);

        TaskIterator it = new TaskIterator();
        it.append(pairlayout);
        it.append(new Task()
        {
            @Override
            public void run(TaskMonitor taskMonitor) throws Exception
            {
                CyNetwork cynet = cyview.getModel();
                CyNetwork combined = pairlayout.getResult();
                String name = cynet.getRow(cynet).get(CyNetwork.NAME, String.class);
                name = "COMBINED-" + name;
                combined.getRow(combined).set(CyNetwork.NAME, name);
                app.cynetmgr.addNetwork(combined);
                CyNetworkView cyview = pairlayout.getResultView();
                app.cyviewmgr.addNetworkView(cyview);
                app.cyeventmgr.flushPayloadEvents();
                app.styler.apply(cyview, VisualStyler.What.EDITS, app.mainPanel.isColorBlindSelected());
                app.cyeventmgr.flushPayloadEvents();
                cyview.updateView();
            }

            @Override
            public void cancel()
            {
            }
        });
        app.taskmgr.execute("Construct combined layout network", it, false);
    }

    public static void createCombinedNetwork()
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            createCombinedNetwork(view);
    }

    // ---------------------------------

    public static void removeNonFixedMappingEdges(final CyNetworkView cyview)
    {
        if(!checkView(cyview))
            return;

        // TODO: undo support
        CyNetwork cynet = cyview.getModel();
        Collection<CyNode> selectedNodes = (new GedevoFilters.SelectedNodesFilter(cynet)).filterNodes(cynet.getNodeList());
        HashSet<CyNode> include = selectedNodes.isEmpty() ? null : new HashSet<CyNode>(selectedNodes); // if none selected, take all
        // remove edges connecting selected nodes
        GedevoAlignmentUtil.removeAllMappingEdgesInvolvingNodes(cynet, include, false, null);
        // remove selected unfixed mapping edges
        GedevoAlignmentUtil.removeEdgesAndTableRows(cynet,
            GedevoFilters.getEdgesWithFixation(cynet,
                GedevoFilters.getMappingEdges(cynet,
                    CyTableUtil.getEdgesInState(cynet, CyNetwork.SELECTED, true)
                )
            , false) // unfixed
        );
        cyview.updateView();
    }

    public static void removeNonFixedMappingEdges()
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            removeNonFixedMappingEdges(view);
    }

    // -------------------------------

    public static void reconstructAlignmentEdges(final CyNetworkView cyview)
    {
        if(!checkView(cyview))
            return;

        // TODO: undo support
        CyNetwork cynet = cyview.getModel();
        Collection<CyNode> selected = (new GedevoFilters.SelectedNodesFilter(cynet)).filterNodes(cynet.getNodeList());
        HashSet<CyNode> include = selected.isEmpty() ? null : new HashSet<CyNode>(selected); // if none selected, take all
        GedevoAlignmentUtil.generateMappingEdges(cynet, include);
        cyview.updateView();
    }

    public static void generateMappingEdges()
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            reconstructAlignmentEdges(view);
    }

    // ----------------------------

    public static void includePartnerNodesToSelection(CyNetworkView cyview)
    {
        if(!checkView(cyview))
            return;

        CyNetwork cynet = cyview.getModel();
        Set<CyNode> nodes = new HashSet<CyNode>(CyTableUtil.getNodesInState(cynet, CyNetwork.SELECTED, true));
        List<CyNodePair> pairs = GedevoAlignmentUtil.getAlignedNodePairs(cynet, false);

        for (CyNodePair pair : pairs)
        {
            CyNode src = pair.a;
            CyNode dst = pair.b;
            if (nodes.contains(src) || nodes.contains(dst))
            {
                cynet.getRow(src).set(CyNetwork.SELECTED, true);
                cynet.getRow(dst).set(CyNetwork.SELECTED, true);
            }
        }

        Map<CyEdge, CyEdge> edges = GedevoAlignmentUtil.getAlignedEdges(cynet, CyTableUtil.getEdgesInState(cynet, CyNetwork.SELECTED, true));
        for(CyEdge e : edges.values())
            cynet.getRow(e).set(CyNetwork.SELECTED, true);
        cyview.updateView();
    }

    public static void includePartnerNodesToSelection()
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            includePartnerNodesToSelection(view);
    }

    // --------------------------------

    public static void applyVisualStyle(CyNetworkView cyview, VisualStyler.What what, boolean colorblind)
    {
        getApp().styler.apply(cyview, what, colorblind);
    }

    public static void applyVisualStyle(VisualStyler.What what, boolean colorblind)
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            applyVisualStyle(view, what, colorblind);
    }

    // --------------------------------

    public static void applyVisualStyleForScore(CyNetworkView cyview, String colname, VisualStyler.ScoreType ty, boolean colorblind)
    {
        getApp().styler.applyScore(cyview, colname, ty, colorblind);
    }

    public static void applyVisualStyleForScore(String colname, VisualStyler.ScoreType ty, boolean colorblind)
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            applyVisualStyleForScore(view, colname, ty, colorblind);
    }

    // --------------------------------

    public static void fixateSelectedEdgesAndNodes(CyNetworkView cyview)
    {
        if(!checkView(cyview))
            return;

        CyNetwork cynet = cyview.getModel();
        GedevoAlignmentUtil.fixateSelectedNodesAndEdges(cynet);
        getApp().cyeventmgr.flushPayloadEvents();
        cyview.updateView();
    }

    public static void fixateSelectedEdgesAndNodes()
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            fixateSelectedEdgesAndNodes(view);
    }

    // ---------------------------------

    public static void unfixateSelectedEdgesAndNodes(CyNetworkView cyview)
    {
        if(!checkView(cyview))
            return;

        CyNetwork cynet = cyview.getModel();
        GedevoAlignmentUtil.unfixateSelectedNodesAndEdges(cynet);
        getApp().cyeventmgr.flushPayloadEvents();
        cyview.updateView();
    }

    public static void unfixateSelectedEdgesAndNodes()
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            unfixateSelectedEdgesAndNodes(view);
    }

    // ---------------------------------

    public static JPopupMenu createScorePopupMenu(CyNetworkView cyview, final GedevoUtil.Closure<?> callback)
    {
        CyNetwork cynet = cyview.getModel();
        List<String> scores = ColumnNames.getScoreColumnNames(cynet.getDefaultNodeTable());

        if(scores.isEmpty())
        {
            JPopupMenu dummy = new JPopupMenu("No scores!");
            JMenuItem dis = new JMenuItem("No usable scores found (gedevo*Score table entries)");
            dis.setEnabled(false);
            dummy.add(dis);
            return dummy;
        }

        JPopupMenu menu = new JPopupMenu("Select score");
        for(final String name : scores)
        {
            JMenuItem itm = new JMenuItem(name);
            menu.add(itm);
            itm.addActionListener(new ActionListener()
            {
                @Override
                public void actionPerformed(ActionEvent e)
                {
                    callback.run(name);
                }
            });
        }

        return menu;
    }

    public static JPopupMenu createScorePopupMenu(GedevoUtil.Closure<?> callback)
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            return createScorePopupMenu(view, callback);
        return null;
    }

    public static void selectNodesAndEdgesFromSameCCS(CyNetworkView view)
    {
        CyNetwork cynet = view.getModel();
        List<CyNode> selnodes = CyTableUtil.getNodesInState(cynet, CyNetwork.SELECTED, true);
        List<CyEdge> seledges = CyTableUtil.getEdgesInState(cynet, CyNetwork.SELECTED, true);
        // Stupid java doesn't let us cast the container type without warning, so we do it the hard way...
        List<CyIdentifiable> sel = new ArrayList<CyIdentifiable>(selnodes.size() + seledges.size());
        sel.addAll(selnodes);
        sel.addAll(seledges);
        GedevoAlignmentUtil.selectNodesAndEdgesFromSameCCS(cynet, sel);
        view.updateView();
    }

    public static void selectNodesAndEdgesFromSameCCS()
    {
        for (CyNetworkView view : getSelectedNetworkViews())
            selectNodesAndEdgesFromSameCCS(view);
    }

}
