dofile("util.lua")

local ty, infile, outfile = ...
--ty = "net"
if not (ty == "blast" or ty == "net" or ty == "blast2") then
    error("blast or el or blast2")
end

local match = string.match
local explode = string.explode
local sub = string.sub


local out = assert(io.open(outfile, "w"), outfile)
out:setvbuf("full", 8*1024*1024)
local w = out.write

if ty == "blast" then
    for line in io.lines(infile) do
        if line ~= "" then
            local dip1, dip2, score = unpack(line:splitws())
            dip1 = match(explode(dip1, "|")[1], "dip:(.*)")
            dip2 = match(explode(dip2, "|")[1], "dip:(.*)")
            --local dip1, dip2, score = match(line, ".*dip:(.-)[|%s].*%s+.*dip:(.-)|?.*%s+(.*)")
            if not (dip1 and dip2 and score) then
                print("match error: " .. line)
            end
            w(out, dip1, "\t", dip2, "\t", score, "\n")
            --print(dip1, dip2, score)
        end
    end
elseif ty == "net" then
    for line in io.lines(infile) do
        if line ~= "" then
            local dip1, dip2 = unpack(line:splitws())
            dip1 = explode(dip1, "|")[1]
            dip2 = explode(dip2, "|")[1]
            if not (dip1 and dip2) then
                print("match error: " .. line)
            end
            w(out, dip1, "\t", dip2, "\n")
            --print(dip1, dip2)
        end
    end
elseif ty == "blast2" then
    for line in io.lines(infile) do
        if line ~= "" then
            local dip1, dip2, _, score = unpack(line:splitws())
            dip1 = match(explode(dip1, "|")[1], "dip:(.*)")
            dip2 = match(explode(dip2, "|")[1], "dip:(.*)")
            if not (dip1 and dip2 and score) then
                print("match error: " .. line)
            end
            w(out, dip1, "\t", dip2, "\t", score, "\n")
        end
    end
end

out:flush()
out:close()

