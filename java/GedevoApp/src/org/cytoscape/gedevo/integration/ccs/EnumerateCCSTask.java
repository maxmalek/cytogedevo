package org.cytoscape.gedevo.integration.ccs;

import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.gedevo.GedevoFilters;
import org.cytoscape.gedevo.simplenet.Graph;
import org.cytoscape.gedevo.simplenet.Node;
import org.cytoscape.gedevo.util.CyNodePair;
import org.cytoscape.model.*;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

import java.util.*;

public class EnumerateCCSTask extends AbstractTask
{
    private final CyNetwork cynet;
    private Graph _ga, _gb;

    private static Comparator<IntPair[]> pairCmp = new Comparator<IntPair[]>()
    {
        @Override
        public int compare(IntPair[] a, IntPair[] b)
        {
            return (a.length == b.length) ? 0 : (a.length > b.length ? -1 : 1); // largest first
        }
    };

    public EnumerateCCSTask(CyNetwork cynet)
    {
        if(cynet == null)
            throw new NullPointerException("EnumerateCCSTask: null (should not happen!)");
        this.cynet = cynet;
    }

    public EnumerateCCSTask(CyNetwork cynet, Graph ga, Graph gb)
    {
        this.cynet = cynet;
        this._ga = ga;
        this._gb = gb;
    }

    private Graph createGraph(int networkID)
    {
        GedevoFilters.AllNodesWithValueFilter nf = new GedevoFilters.AllNodesWithValueFilter<Integer>(cynet, ColumnNames.SOURCE_NETWORK_ID, networkID);
        Collection<CyNode> allnodes = GedevoFilters.getNonGroupedNodes(cynet, cynet.getNodeList());
        Set<CyNode> nodes = new HashSet<CyNode>(nf.filterNodes(allnodes));

        // this excludes mapping edges (they have one end not in the set)
        return Graph.importNetworkFilter(new GedevoFilters.Filter(cynet, nf, new GedevoFilters.AllEdgesFullyInNodeSet(nodes)));
    }

    private int[] constructSingleMapping(Graph ga, Graph gb)
    {
        HashMap<CyNode, Integer> idxInB = new HashMap<CyNode, Integer>();
        for(Node n : gb.nodes)
            idxInB.put(n.cynode, n.id);

        int[] mapping = new int[ga.nodes.length];

        Collection<CyNodePair> pairs = GedevoAlignmentUtil.getAlignedNodePairs(cynet, true);
        HashMap<CyNode, CyNode> partners = new HashMap<CyNode, CyNode>();
        for(CyNodePair pair : pairs)
        {
            if(pair.a != null)
                partners.put(pair.a, pair.b);
            if(pair.b != null)
                partners.put(pair.b, pair.a);
        }

        for(int i = 0; i < ga.nodes.length; ++i)
        {
            Node na = ga.nodes[i];
            CyNode partnerN = partners.get(na.cynode);
            if(partnerN != null)
            {
                int partnerIdx = idxInB.get(partnerN);
                mapping[i] = partnerIdx;
            }
            else
                mapping[i] = -1;
        }
        return mapping;
    }

    private void initTableColumns(CyTable tab)
    {
        if (tab.getColumn(ColumnNames.CCS) == null)
            tab.createColumn(ColumnNames.CCS, Integer.class, false, -1);
        if (tab.getColumn(ColumnNames.CCS_SIZE) == null)
            tab.createColumn(ColumnNames.CCS_SIZE, Integer.class, false, 0);

        for (CyRow r : tab.getAllRows())
        {
            r.set(ColumnNames.CCS, -1);
            r.set(ColumnNames.CCS_SIZE, 0);
        }
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        taskMonitor.setTitle("Calculating common connected subgraphs...");
        taskMonitor.setProgress(0);

        Graph ga = _ga != null ? _ga : createGraph(0);
        Graph gb = _gb != null ? _gb : createGraph(1);
        int[] mapping = constructSingleMapping(ga, gb);

        taskMonitor.setProgress(0.1);

        IntPair[][] ccslist = CCSGenerator.extractCCS(ga, gb, mapping);

        taskMonitor.setProgress(0.6);

        Arrays.sort(ccslist, pairCmp);

        taskMonitor.setProgress(0.7);

        // make sure columns exist and clear out old data
        initTableColumns(cynet.getDefaultNodeTable());
        taskMonitor.setProgress(0.8);
        initTableColumns(cynet.getDefaultEdgeTable());

        taskMonitor.setProgress(0.9);

        // write table data
        for (int i = 0; i < ccslist.length; ++i)
        {
            final int ccssize = ccslist[i].length;
            if (ccssize > 1) // Exclude CCSs that only contain a single node
            {
                Set<CyNode> ccsnodes = new HashSet<CyNode>();
                for (IntPair entry : ccslist[i])
                {
                    CyNode na = ga.nodes[entry.a].cynode;
                    CyNode nb = gb.nodes[entry.b].cynode;
                    CyRow ra = cynet.getRow(na);
                    CyRow rb = cynet.getRow(nb);
                    ra.set(ColumnNames.CCS, i);
                    ra.set(ColumnNames.CCS_SIZE, ccssize);
                    rb.set(ColumnNames.CCS, i);
                    rb.set(ColumnNames.CCS_SIZE, ccssize);
                    ccsnodes.add(na);
                    ccsnodes.add(nb);
                }
                Collection<CyEdge> edges = (new GedevoFilters.AllEdgesFullyInNodeSet(ccsnodes)).filterEdges(cynet.getEdgeList());
                for (CyEdge e : edges)
                {
                    CyRow row = cynet.getRow(e);
                    row.set(ColumnNames.CCS, i);
                    row.set(ColumnNames.CCS_SIZE, ccssize);
                }
            }
        }

        taskMonitor.setProgress(1);

    }
}
