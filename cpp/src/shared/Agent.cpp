#include "Agent.h"
#include "AgentMgr.h"
#include <algorithm>
#include <iostream>
#include <sstream>
#include "MersenneTwister.h"
#include "DataHolder.h"
#include "ProgramState.h"
#include "HeapArray.h"
#include "UserSettings.h"


Agent::Agent(const DataHolder& datah, const NodeMapping& m)
 : locked(false)
 , _dataH(datah)
 , _dirtyFinalScore(true)
 , _dirtyGED(true)
 , _dirtyPairSumScore(true)
 , _dirtyRawGED(true)
 , _scoreGED(-1)
 , _scorePairSum(-1)
 , _scoreFinal(-1)
 , _health(datah.GetUserSettings().basicHealth)
 , _life(0)
 , _mapping(m)
 , _refMapping(datah.GetRefMapping())
 , _settings(datah.GetUserSettings())
{
	_rawGED.reset();

	dirtyTablePairs.resize(m.size(), 1); // all dirty by default
	pairScoreTable.resize(m.size());

	_indexv.resize(_dataH.GetMaxVariableNodeID());
	GEDPopulateIndex(_mapping, &_indexv[0]);
}

void Agent::copyFrom(const Agent& other)
{
	// Agent is new, refill health and rest life
	_life = 0;
	_health = other._settings.basicHealth;

	_mapping = other._mapping;
	_indexv = other._indexv;
	pairScoreTable = other.pairScoreTable;
	dirtyTablePairs = other.dirtyTablePairs;

	_scoreGED = other._scoreGED;
	_scorePairSum = other._scorePairSum;

	_dirtyFinalScore = other._dirtyFinalScore;
	_dirtyGED = other._dirtyGED;
	_dirtyPairSumScore = other._dirtyPairSumScore;
	_dirtyRawGED = other._dirtyRawGED;

	_rawGED = other._rawGED;
}

void Agent::octopusMerge(const Agent * const * parents, unsigned char *mask, uint masksize)
{
	_scoreGED = -999;
	_scorePairSum = -999;
	_dirtyPairSumScore = true; // Always recalculate score after crossover
	_dirtyGED = true;
	_dirtyRawGED = true;
	_dirtyFinalScore = true;

	_health = parents[0]->_settings.basicHealth;

	for(uint i = 0; i < masksize; ++i)
	{
		if(mask[i] == 0xff)
		{
			_mapping.node[i] = NULL;
			_mapping.nodeId[i] = -1; // to catch errors! MUST be overwritten by setMappingPos() (only checked in debug mode)
			pairScoreTable[i]    = -1; //invalid value
			dirtyTablePairs[i]   = 1;
		}
		else
		{
			const Agent *p = parents[mask[i]];
			_mapping.nodeId[i]        = p->_mapping.nodeId[i];
			_mapping.node[i]          = p->_mapping.node[i];
			pairScoreTable[i]    = p->pairScoreTable[i];
			dirtyTablePairs[i]   = p->dirtyTablePairs[i];

			const uint id = _mapping.nodeId[i];
			if(id)
				_indexv[id-1] = i;
		}
	}
}


// Swaps two indices in the mapping, and sets the corresponding locations to dirty, so that they will be recalculated.
// TODO: optimize this - for some cases, resetting here isn't necessary
void Agent::swap(const size_t a, const size_t b)
{
	ASSERT(a < _mapping.size());
	ASSERT(b < _mapping.size());
	// make sure that no prematched node is ever swapped
	ASSERT(a >= _dataH.getFirstNonPrematchedIndex());
	ASSERT(b >= _dataH.getFirstNonPrematchedIndex());
	swapTemp(a, b);
	_dirtyGED = true;
	_dirtyPairSumScore = true;
	_dirtyRawGED = true;
	_dirtyFinalScore = true;
}

void Agent::_CalcFinalScore() const
{
	score s = getCombinedFinalScore();

	if(_settings._finalCalcPairsum)
		s += getPairSumScore() * _settings.weightPairsum;

	if(_settings._finalCalcGED)
		s += getGEDScore() * _settings.weightGED;

	_scoreFinal = s / _settings.getFinalWeightSum();
	_dirtyFinalScore = false;
	ASSERT(_scoreFinal >= 0 && _scoreFinal <= 1);
}

score Agent::getGEDScore() const
{
	if(_dirtyGED)
	{
		_UpdateMappingGED();
		const score worst = _dataH.getWorstGEDScore();
		
		// pretend that self loops are seen two times, i.e. similar to the other edges
		const score eval = 
			(_rawGED.evaluate(_dataH.getGEDScoreArray()) + _rawGED.getContributionOfSelfLoops(_dataH.getGEDScoreArray()) ) / 2;

		ASSERT(eval <= worst);

		_scoreGED = eval / worst; // This is between 0 and 1.
		ASSERT(_scoreGED >= 0 && _scoreGED <= 1);
		_dirtyGED = false;
	}
	
	return _scoreGED;
}

score Agent::getCombinedFinalScore() const
{
	if(!_dataH.HasCombinedFinalScore())
		return 0;

	const size_t sz = _mapping.size();
	score s = 0;
	for (size_t i = 0; i < sz; ++i)
	{
		const uint a = _mapping.nodeId[i];
		const uint b = _refMapping.nodeId[i];
		s += _dataH.GetCombinedFinalScore_unsafe(a, b);
	}
	s /= score(sz);
	ASSERT(s >= 0 && s <= 1);
	return s;
}

score Agent::getPairSumScore() const
{
	if(_dirtyPairSumScore)
	{
		size_t sz = _mapping.size();
		score sum = 0;
		for (size_t i = 0; i < sz; ++i)
			sum += getPairScore(i);
		_scorePairSum = sum / sz;
		_dirtyPairSumScore = false;
		ASSERT(_scorePairSum >= 0 && _scorePairSum <= 1);
	}
	return _scorePairSum;
}

void Agent::getPairGED(uint i, GEDInfo& pair) const
{
	calcPairGEDIndexed(_dataH, _mapping, _refMapping, i, pair, &_indexv[0]);
}

void Agent::_UpdateMappingGED() const
{
	if(_dirtyRawGED)
	{
		ASSERT(sanityCheck());

		_rawGED.reset();
		calcWholeGEDIndexed(_dataH, _mapping, _refMapping, _rawGED, &_indexv[0]);
		_dirtyRawGED = false;
	}
}

score Agent::getPairGEDScore(uint i) const
{
	uint totalDegree = 0;
	const Node *an = _mapping.node[i];
	const Node *bn = _refMapping.node[i];
	if(an)
		totalDegree += (uint)an->getDegree();
	if(bn)
		totalDegree += (uint)bn->getDegree();

	// Prevent division by zero for nodes without any edges
	if(totalDegree)
	{
		GEDInfo pg;
		getPairGED(i, pg);

		const score eval = pg.evaluate(_dataH.getGEDScoreArray());
		const score s = eval / score((int)totalDegree);
		ASSERT(s >= 0 && s <= 1); // Not sure if this will break with node add/remove score > 0. Even if it does, it's not really a problem...
		return s;
	}

	// FIXME: this is not 100% correct. We're not taking node added/removed scores into account here.
	return (!an == !bn) ? score(0) : _settings.pairNullValue;
}

// return score between 0 (perfect match) and > 0 (more diverging)
score Agent::getPairScore(uint i) const
{
	if(dirtyTablePairs[i])
	{
		const uint a = _mapping.nodeId[i];
		const uint b = _refMapping.nodeId[i];
		score s;
		if(a && b)
		{
			const std::pair<score, score> overrideInterval = _dataH.GetOverrideInterval(a, b);
			s = overrideInterval.first;
			
			// First, see if we can skip calculations.
			// If the prepared override value is below the set threshold (<= 1), use it immediately.
			if(s > score(1))
			{
				// Worse than first threshold, pull in other scores and do GED calculations
				s = _dataH.GetCombinedPairScore(a, b);

				if(_settings._pairCalcGED)
					s += getPairGEDScore(i) * _settings.pairWeightGED;

				s /= _settings.getPairWeightSum();
				ASSERT(s >= 0 && s <= 1);

				// Now check if the backup score (which will be <= 1 if valid) is better than what we have calculated.
				// If so, use it, and drop the topological results.
				s = std::min(s, overrideInterval.second);
			}
		}
		else if(!a && !b)
		{
			s = score(0);
		}
		else
		{
			s = _settings.pairNullValue;
		}
		
		pairScoreTable[i] = s;
		dirtyTablePairs[i] = 0;
		return s;
	}
	else
	{
		VERIFY_BLOCK
		{
			dirtyTablePairs[i] = 1;
			ASSERT(pairScoreTable[i] == getPairScore(i));
		}
		return pairScoreTable[i];
	}
}

// debug break if debugger attached, return false in release mode
#define BAIL(err)  {fprintf(stderr, "SANITY CHECK FAILED: \"%s\", File: %s:%u\n", err, __FILE__, __LINE__); TriggerBreakpoint(); return false;}
#define BAIL_FMT(err, ...) { char errbuf[256]; sprintf(errbuf, err, __VA_ARGS__); BAIL(errbuf); }
#define ASSUME(x) {if(!(x)) BAIL(#x)}

bool Agent::sanityCheck() const
{
	size_t sz = _mapping.size();
	ASSUME(sz == _refMapping.size());

	// first node has id 1 ... means node N has id N
	uint maxId = _dataH.GetMaxVariableNodeID();
	ASSERT(maxId <= sz);
	unsigned char *seen = (unsigned char *)malloc(sz); // be sure the array is large enough for all following operations
	memset(seen, 0, maxId);
	bool wasPrematched = true;
	// index 0 will be used for node 1, etc.
	for(size_t i = 0; i < sz; ++i)
	{
		const Node * const node = _mapping.node[i];
		const uint nodeId = _mapping.nodeId[i];
		if(node)
		{
			ASSUME(nodeId > 0);
			ASSUME(node->id == nodeId);

			uint k = nodeId - 1;
			if(seen[k])
			{
				BAIL_FMT("Node %u exists twice (k = %u)!\n", nodeId, k);
			}
			seen[k] = 1;

			// make sure prematched nodes appear only at beginning of mapping
			ASSUME(!(node->prematched >= 0 && !wasPrematched));

			if(node->prematched < 0)
				wasPrematched = false;

			//ASSERT(!_refMapping[i].node || !e.node->prematched); // this would prevent pre-matching against NULL, which is rather a feature than a problem.
			ASSUME(!_refMapping.node[i] || (_refMapping.node[i]->prematched == node->prematched));
		}
		else
			ASSUME(nodeId == 0);
	}

	for(uint i = 0; i < maxId; ++i)
	{
		if(seen[i] != 1)
		{
			BAIL_FMT("Node %u does not exist!\n", i+1);
		}
	}

	// Check index vector
	memset(seen, 0, sz);

	for(size_t i = 0; i < _indexv.size(); ++i)
	{
		uint idx = _indexv[i];
		if(idx >= sz)
		{
			BAIL_FMT("Index vector out of range: Is %u but should be < %u\n", idx, (uint)sz);
		}
		if(!_mapping.node[idx])
		{
			BAIL_FMT("Index vector points to NULL-node (%u at %u)\n", idx, (uint)i);
		}
		if(_mapping.node[idx] && _mapping.nodeId[idx] != i + 1)
		{
			BAIL_FMT("Index vector has wrong entry (%u at %u, should be %u)\n", idx, (uint)i, _mapping.nodeId[idx] - 1);
		}
		if(seen[idx])
		{
			printf("Index vector duplicate (%u at index %u)!\n", idx, (uint)i);
			ASSERT(false);
		}
		seen[idx] = 1;
	}
	for(size_t i = 0; i < _indexv.size(); ++i)
	{
		if(!seen[_indexv[i]])
		{
			BAIL_FMT("Index vector missing entry (%u at index %u)\n", _indexv[i], (uint)i);
		}
	}
	free(seen);

	// Create index vector on the fly and make sure it matches the current one
	ASSERT(_indexv.size()); // help static code analysis
	uint *indexv = (uint*)alloca(_indexv.size() * sizeof(uint));
	GEDPopulateIndex(_mapping, indexv);
	for(size_t i = 0; i < _indexv.size(); ++i)
		ASSUME(_indexv[i] == indexv[i]);

	return true;
}
#undef BAIL
#undef BAIL_FMT
#undef ASSUME

void Agent::makeDirty()
{
	ASSERT(!locked);

	_dirtyFinalScore = true;
	_dirtyGED = true;
	_dirtyPairSumScore = true;
	_dirtyRawGED = true;

	memset(&dirtyTablePairs[0], 1, dirtyTablePairs.size());

	GEDPopulateIndex(_mapping, &_indexv[0]);
}

void Agent::_setMappingPos(uint pos, const Node *node)
{
	ASSERT(!locked);
	ASSERT(_mapping.node[pos] == NULL);
	ASSERT(_mapping.nodeId[pos] == uint(-1)); // set by ctor for this special case
	_mapping.node[pos] = node;
	const uint id = node ? node->id : 0;
	_mapping.nodeId[pos] = id;

	// Fix index vector
	if(id)
		_indexv[id-1] = pos;

	// set by ctor
	//dirtyTablePairs[pos] = 1;
	//dirtyTableGED[pos] = 1;
}

uint Agent::getDistanceTo(const Agent *other) const
{
	uint d = 0;
	size_t sz = _mapping.size();
	const NodeMapping& om = other->_mapping;
	for(size_t i = 0; i < sz; ++i)
		if(_mapping.nodeId[i] != om.nodeId[i])
			++d;
	return d;
}

// aka. bigger score first
struct _WorsePairFirst
{
	inline bool operator() (const PairInfo& a, const PairInfo& b) const
	{
		return a.scor > b.scor;
	}
};

void Agent::getWorstPairs(PairInfo *p, uint n, PairwiseScoringMethod mth) const
{
	ASSERT(n);


	// If we just need to get all pairs, there is no special need for optimization
	if(n == _mapping.size())
	{
		for(uint i = _dataH.getFirstNonPrematchedIndex(); i < n; ++i)
		{
			p[i] = PairInfo((this->*mth)(i), i);
		}
	}
	else
	{
		// ... Otherwise, use a heap instead of getting all values and then sorting
		for(uint i = 0; i < n; ++i)
		{
			p[i].index = -1;
			p[i].scor = -1;
		}

		HeapArray<PairInfo, _WorsePairFirst> heap(p, n);

		size_t sz = _mapping.size();
		for(size_t i = _dataH.getFirstNonPrematchedIndex(); i < sz; ++i)
		{
			heap.Add(PairInfo((this->*mth)(i), i));
		}
	}
}

score Agent::getEdgeCorrectness() const
{
	_UpdateMappingGED();

	const uint minEdges = std::min(_dataH.GetVariableEdgeAmount(), _dataH.GetReferenceEdgeAmount());

	// FIXME: this only counts 100% correct edges.
	// For undirected graphs this is simply the amount of substituted edges,
	// but for directed graphs, this is currently only the number of edges which were aligned to have
	// the same direction in both graphs.
	// -> Flipped or direction-changed edges are NOT counted here!
	uint sameEdges = GEACC_SUBST(_rawGED);

	ASSERT(!(sameEdges & 1)); // we've seen each edge in question twice
	sameEdges >>= 1;

	ASSERT(sameEdges <= minEdges);

	return score(sameEdges) / score(minEdges);
}

// used[i] corresponds to node with ID i+1
void Agent::completePartialMapping()
{
	std::vector<uchar> used(_dataH.GetMaxVariableNodeID(), 0);
	const std::vector<uint>& myId = _mapping.nodeId;
	for(size_t i = 0; i < myId.size(); ++i)
		if(uint id = myId[i])
		{
			ASSERT(!used[id - 1]);
			used[id - 1] = 1;
		}

	// distribute remaining nodes - put them at the end of _mapping so that they are mapped against NULL
	size_t widx = _mapping.size() - 1;
	for(size_t i = 0; i < used.size(); ++i)
		if(!used[i])
		{
			while(myId[widx]) // make sure that there is no node in the way; if there is, skip over it
				--widx;
			_mapping.set(widx--, _dataH.GetNodeByID(i + 1));
		}
}
