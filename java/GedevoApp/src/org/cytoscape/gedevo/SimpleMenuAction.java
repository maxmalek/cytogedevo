package org.cytoscape.gedevo;

import org.cytoscape.application.swing.AbstractCyAction;

import java.awt.event.ActionEvent;


public abstract class SimpleMenuAction extends AbstractCyAction implements IUnloadable
{
    private GedevoApp app;
    public SimpleMenuAction(GedevoApp a, String label, String preferredMenu)
    {
        super(
            label,
            a.cyappmgr,
            "",
            a.cyviewmgr
        );
        app = a;
        setPreferredMenu(preferredMenu);
        Unloadable.unloadLater(this);
    }

    @Override
    public abstract void actionPerformed(ActionEvent e);

    @Override
    public void unload()
    {
        app.cyapp.removeAction(this);
        app = null;
    }
}
