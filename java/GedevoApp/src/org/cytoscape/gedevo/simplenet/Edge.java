package org.cytoscape.gedevo.simplenet;

public final class Edge
{
    public Edge(int a, int b, boolean d)
    {
        node1 = a;
        node2 = b;
        directed = d;
    }
    public final int node1;
    public final int node2;
    public final boolean directed;
}

