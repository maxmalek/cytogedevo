#ifdef USE_SDL2
#include <SDL.h>
#else
#include <SDL/SDL.h>
#include "cpucount.h"
#endif

#include "Threading.h"

class SDLWaitable
{
public:
	SDLWaitable()
	{
		_mtx = SDL_CreateMutex();
		_cond = SDL_CreateCond();
	}

	~SDLWaitable()
	{
		SDL_DestroyMutex(_mtx);
		SDL_DestroyCond(_cond);
	}

    void lock()
	{
		SDL_LockMutex(_mtx);
	}

    void unlock()
	{
		SDL_UnlockMutex(_mtx);
	}

    void wait()
	{
		SDL_CondWait(_cond, _mtx);
	}

    void signal()
	{
		SDL_CondSignal(_cond);
	}

    void broadcast()
	{
		SDL_CondBroadcast(_cond);
	}

private:
	SDL_mutex *_mtx;
	SDL_cond *_cond;
};

struct trampoline_pass
{
	void (*f)(void*);
	void *u;
};

static int th_trampoline(void *ppass)
{
	trampoline_pass *pass = static_cast<trampoline_pass*>(ppass);
	void (*body)(void*) = pass->f;
	void *user = pass->u;
	delete pass;
#if SDL_VERSION_ATLEAST(2, 0, 0)
	SDL_SetThreadPriority(SDL_THREAD_PRIORITY_LOW);
#endif
	body(user);
	return 0;
}

static void *thcreate(const char *name, void (*body)(void*), void *user)
{
	trampoline_pass *pass = new trampoline_pass;
	pass->f = body;
	pass->u = user;
#if SDL_VERSION_ATLEAST(2, 0, 0)
	return SDL_CreateThread(th_trampoline, name, pass);
#else
	return SDL_CreateThread(th_trampoline, pass); // can't pass name, so ignore it :(
#endif
}

static void thjoin(void *pth)
{
	SDL_Thread *th = static_cast<SDL_Thread*>(pth);
	SDL_WaitThread(th, NULL);
}

static void *wcreate()
{
	return new SDLWaitable;
}

static void wdelete(void *w)
{
	delete static_cast<SDLWaitable*>(w);
}

static void wlock(void *w)
{
	static_cast<SDLWaitable*>(w)->lock();
}

static void wunlock(void *w)
{
	static_cast<SDLWaitable*>(w)->unlock();
}

static void wwait(void *w)
{
	static_cast<SDLWaitable*>(w)->wait();
}

static void wsignal(void *w)
{
	static_cast<SDLWaitable*>(w)->signal();
}

static void wbroadcast(void *w)
{
	static_cast<SDLWaitable*>(w)->broadcast();
}

static unsigned getCPUCount()
{
    int c =
#if SDL_VERSION_ATLEAST(2, 0, 0)
	    SDL_GetCPUCount();
#else
        countCPUCores();
#endif
	if(c < 1)
		c = 1;
	return (unsigned)c;
}


// external interface
bool InitMT(void*)
{
	SDL_Init(SDL_INIT_NOPARACHUTE);
	return threading::Init(thcreate, thjoin, wcreate, wdelete, wlock, wunlock, wwait, wsignal, wbroadcast, getCPUCount);
}

void QuitMT()
{
	SDL_Quit();
	threading::Quit();
}

