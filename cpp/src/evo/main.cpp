#include "common.h"
#include <algorithm>

#include "Evolution.h"
#include "ProgramState.h"
#include "ResultPrinter.h"


#include "Threading.h"
#include "CmdlineParser.h"
#include "CmdHelp.h"

#include <signal.h>
#include <fstream>

#include <stdio.h>

extern bool InitMT(void*);
extern void QuitMT();

static ProgramState *progstate = NULL;

void onSignal(int s)
{
	switch(s)
	{
		case SIGINT:
			if(progstate->MustQuit() && !progstate->IsSaving()) // no abort while saving, to prevent data loss
			{
				printf("Exit forced, results not saved, bye!\n");
				raise(SIGTERM);
				exit(2);
			}
			progstate->SetQuit();
			printf("Caught exit signal... going to save results.\n");
			break;

		default:
			fprintf(stderr, "Caught signal %d\n", s);
			exit(s);
	}
}

static void usage()
{
	puts("GEDEVO v1.0.3 -- Builtin help:");
	CmdHelp::Print();
	puts(
		"Simple example:\n"
		"$  ./gedevo --sif N1.sif --sif N2.sif -u --save test.txt --maxsame 50\n"
		"More complicated example:\n"
		"$  ./gedevo --edgelist yeast.el --edgelist human.el\n"
		"    --custom blast_human_yeast.pairs auto distance override 0.0001 0.05\n"
		"    --undirected --prematch --ignore-self-loops --density 0.4\n"
		"    --save human_yeast.txt --timestamp --maxsame 100\n"
		"\n"
		"See the README for additional information and input file format descriptions.\n"
		"Feel free to contact malek@tugraz.at for questions and comments."
		);
}

static void applyDefaultSettings(ProgramState& state)
{
	UserSettings& settings = state.settings;

	// choose a suitable default file name for resulting alignment
	std::string& fn = settings.saveResultsFileName;
	if(fn.empty())
	{
		const ProteinHolder& ph = state.ph;
		fn += "gedevo_aln_";
		fn += ph.GetNetworkFileName(0);
		fn += "_vs_";
		fn += ph.GetNetworkFileName(1);
		fn += ".txt";
		settings.saveResultsAddTimeStamp = true;
	}
}

//void dotest();

int main(int argc, char **argv)
{
	//dotest(); return 0;

	if(argc <= 1 || !strcmp(argv[1], "--help"))
	{
		usage();
		return 0;
	}

	bool hasMT = InitMT(NULL);
	if(!hasMT)
		puts("Multithreading init FAILED!");

	signal(SIGFPE, onSignal);
	signal(SIGINT, onSignal);

	bool good = false;
	{
		UserSettings settings;
		ThreadPool thpool;
		ProgramState state(settings, hasMT ? &thpool : NULL);
		progstate = &state;

		CmdlineParser cmdline(argc, argv);
		if(!cmdline.Apply(state))
		{
			puts("Run the program with no parameters for a quick help.\n");
			return 2;
		}

		applyDefaultSettings(state);

		Evo evo(state.dh, &thpool);
		AgentMgr amgr(state.dh);
		evo.Link(amgr);

		good = state.Init1(evo) && state.Init2(); // console program has nothing to do in between
		if(good)
		{
			printf("Doing some evolution...\n");

			const AgentArray& arr = evo.GetAgents();
			bool stop = false;

			while(!(stop || state.MustQuit()))
			{
				state.NextIteration();
				evo.Update();

				printf("Done round #%u, %u alive. Last improvement: %u iters ago.\n", state.GetIterationCount(), (uint)arr.size(), state.GetIterationsWithoutScoreChange());
				
#ifdef _DEBUG
				Agent *top = arr[0];
				const uint limit = std::min<uint>(5, arr.size());
				for(uint j = 0; j < limit; ++j)
				{
					Agent *a = arr[j];
					printf("%2u  F: %f, GED: %u, EC: %f, PS: %f, L: %u, D: %u\n",
						j, a->getScore(), a->getGEDRawFull(), a->getEdgeCorrectness(), a->getPairSumScore(),
						a->getLife(), a->getDistanceTo(top));
				}
#endif

				stop = state.IsDone();
			}

			if(stop)
			{
				printf("Done, abort criterion reached after %u iterations.\n", state.GetIterationCount());
			}

			state.Shutdown();
		}
		else
		{
			printf("Something isn't right, exiting. Re-check your settings.\n");
		}

		evo.Unlink();
	}

	QuitMT();

	return good ? 0 : 1;
}
