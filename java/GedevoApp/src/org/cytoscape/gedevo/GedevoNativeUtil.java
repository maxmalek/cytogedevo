package org.cytoscape.gedevo;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public final class GedevoNativeUtil
{
    private static final String ARCH, OS, PATHSEP, LIBPREFIX, LIBPREFIX_NOARCH, TMPDIR;
    public static final boolean IS_SANE;
    public static boolean LIB_LOADED;
    public static String LIB_NAME = "";

    public static String getLibInfoStr()
    {
        return LIB_LOADED
            ? ("[Using " + LIB_NAME + "]")
            : "[gedevonative library not loaded]";
    }

    static
    {
        //System.getProperties().list(System.out);

        boolean sane = false;
        String os = "FAIL", sep = "/", arch = "x86", tmpdir = "/tmp";
        try
        {
            arch = System.getProperty("os.arch");

            String osraw = System.getProperty("os.name").toLowerCase();
            if(osraw.contains("os x"))
                os = "macosx";
            else
            {
                String[] osparts = osraw.split(" ");
                os = osparts[0];
            }

            sep = System.getProperty("file.separator");
            if (sep == null)
                sep = "/";

            tmpdir = System.getProperty("java.io.tmpdir");
            if(tmpdir != null && !tmpdir.endsWith(sep))
                tmpdir += sep;

            sane = os != null && arch != null && tmpdir != null;
        }
        catch(Exception ex)
        {
            GedevoUtil.msgbox("GedevoNativeUtil: Exception during platform init. Please get rid of your current operating system.");
        }
        IS_SANE = sane;
        ARCH = sanitizeArch(arch);
        PATHSEP = sep;
        OS = os;
        TMPDIR = tmpdir;

        LIBPREFIX_NOARCH = "/native/" + OS + "/"; // This is for inside of a jar file, don't use PATHSEP
        LIBPREFIX = LIBPREFIX_NOARCH + ARCH + "/";
    }

    // Sanitize ARCH names, see http://www.java-gaming.org/index.php?topic=14110.0
    private static String sanitizeArch(String arch)
    {
        arch = arch.toLowerCase();
        if(arch.equals("i386"))
            arch = "x86";
        else if(arch.equals("amd64"))
            arch = "x86_64";
        else if(arch.equals("x64"))
            arch = "x86_64";

        return arch;
    }

    // /native/windows/x86/gedevonative.dll
    // /native/linux/x86_64/libgedevonative.so
    // etc...
    private static String buildLibPath(String name)
    {
        return LIBPREFIX + System.mapLibraryName(name);
    }
    private static String buildLibPathNoArch(String name)
    {
        return LIBPREFIX_NOARCH + System.mapLibraryName(name);
    }

    public static boolean jarHasFile(String path)
    {
        InputStream is = GedevoNativeUtil.class.getResourceAsStream(path);
        if(is == null)
            return false;
        try
        {
            is.close();
            return true;
        } catch (IOException e)
        {
            return false;
        }
    }

    // Adapted from http://frommyplayground.com/how-to-load-native-jni-library-from-jar
    // Returns name of loadable lib file ON DISK or throws
    // Tries to only write the file to disk if really necessary
    public static String unpackLibraryFromJar(String path) throws IOException
    {
        if (!path.startsWith("/"))
            throw new IllegalArgumentException("The path has to be absolute (start with '/').");

        // Obtain filename from path
        String[] parts = path.split("/");
        if(parts.length <= 1)
            throw new IllegalArgumentException("File name too short");

        String filename = parts[parts.length - 1];

        // Split filename to prefix and suffix (extension)
        parts = filename.split("\\.", 2);
        String prefix = parts[0];
        String suffix = (parts.length > 1) ? "."+parts[parts.length - 1] : null;

        // Check if the filename is okay
        if (prefix.length() < 3)
            throw new IllegalArgumentException("The filename has to be at least 3 characters long.");

        GedevoApp.log("In-JAR path: " + path);
        // Open and check input stream
        byte[] jarFileContent;
        byte[] buffer = new byte[1024*16];
        {
            InputStream is = GedevoNativeUtil.class.getResourceAsStream(path);
            if (is == null)
                throw new FileNotFoundException("File " + path + " was not found inside JAR.");

            ByteArrayOutputStream jarFileBuf = new ByteArrayOutputStream(128 * 1024);

            try
            {
                int readBytes;
                while ((readBytes = is.read(buffer)) != -1)
                    jarFileBuf.write(buffer, 0, readBytes);
                jarFileContent = jarFileBuf.toByteArray();
            }
            finally
            {
                jarFileBuf.close();
                is.close();
            }
        }

        String outfn = TMPDIR + "gedevo" + path.replace("/", PATHSEP);
        File out = new File(outfn);

        if(out.exists())
        {
            // File already exists on disk, let's see if it's equal to the one in the jar

            byte[] diskFileContent;
            ByteArrayOutputStream diskFileBuf = new ByteArrayOutputStream(128 * 1024);
            FileInputStream in = new FileInputStream(out);
            try
            {
                int readBytes;
                while ((readBytes = in.read(buffer)) != -1)
                    diskFileBuf.write(buffer, 0, readBytes);
                diskFileContent = diskFileBuf.toByteArray();

                if(Arrays.equals(diskFileContent, jarFileContent))
                {
                    GedevoApp.log("Up-to-date file is already present: " + out.getAbsolutePath());
                    return out.getAbsolutePath();
                }
            }
            catch(IOException ex)
            {
                GedevoApp.log("File exists, can't verify equality: " + ex.toString());
            }
            finally
            {
                diskFileBuf.close();
                in.close();
            }
        }

        GedevoApp.log("File not found or not up-to-date, need to replace.");
        boolean written = false;

        try
        {
            dumpAsFile(out, jarFileContent);
            written = true;
        }
        catch(IOException ex)
        {
            GedevoApp.log("Exception when trying to write file: " + ex.toString());
        }

        // If the above failed, create temp file and write there.
        // This probably leaves some garbage on the system but nothing that could be done about it
        if(!written)
        {
            GedevoApp.log("Trying to write to tempfile...");
            // Prepare temporary file
            out = File.createTempFile(outfn, suffix);
            out.deleteOnExit();

            if (!out.exists())
                throw new FileNotFoundException("Unable to create temp file: " + out.getAbsolutePath());

            dumpAsFile(out, jarFileContent);
        }

        return out.getAbsolutePath();
    }

    private static void dumpAsFile(File out, byte[] data) throws IOException
    {
        GedevoApp.log("Writing in-JAR file to: " + out.getAbsolutePath());
        out.getParentFile().mkdirs();
        out.createNewFile();
        OutputStream os = new FileOutputStream(out);
        try
        {
            os.write(data);
        }
        finally
        {
            // If read/write fails, close streams safely before throwing an exception
            os.close();
        }
    }

    public static String[] loadLibrary(String fn)
    {
        GedevoApp.log("Attemping to load native library '" + fn + "'...");

        ArrayList<String> errors = new ArrayList<String>();
        int ignoreErrors = 0;

        // Only allow this in debug mode -- this opens a gaping security hole on the target system
        // (Can't just blindly load some code in some path... that's easy to abuse)
        if(GedevoApp.DEBUG)
        {
            String libpath = System.getenv("GEDEVO_LIB_PATH");
            if (libpath != null)
            {
                if(!libpath.endsWith(PATHSEP))
                    libpath += PATHSEP;
                String libname = libpath + System.mapLibraryName(fn);
                try
                {
                    LIB_NAME = libname;
                    System.load(libname);
                    GedevoApp.log("Loaded: " + libname);
                    return null; // Loaded! Done here
                }
                catch (Throwable ex)
                {
                    errors.add("While loading '" + libname + "': " + ex.toString());
                }
            }
        }

        String locallib = buildLibPathNoArch(fn); // multiarch/OSX
        if(!jarHasFile(locallib))
        {
            errors.add("Tried multiarch lib but not in JAR: '" + locallib + "'");
            locallib = buildLibPath(fn); // single arch
            if(jarHasFile(locallib))
                ++ignoreErrors;
            // and if not, the error message below will show clearly what is wrong (no need to log this here)
        }

        try
        {
            String libname = unpackLibraryFromJar(locallib);
            try
            {
                System.load(libname);
                GedevoApp.log("Loaded: " + libname);
                LIB_NAME = locallib;
                return null; // Loaded! Done here
            }
            catch(Throwable ex)
            {
                errors.add("While loading '" + libname + "': " + ex.toString());
        }
        }
        catch(Throwable ex)
        {
            errors.add("While extracting '" + locallib + "': " + ex.toString());
        }

        return errors.size() <= ignoreErrors ? null : errors.toArray(new String[errors.size()]);
    }

    public static boolean initNativeLibs()
    {
        LIB_LOADED = _initNativeLibs();
        return LIB_LOADED;
    }

    private static boolean _initNativeLibs()
    {
        if(!IS_SANE)
        {
            // super unlikely that this happens
            GedevoUtil.msgbox("GedevoNative static init is insane. Move to a sane platform, then try again.");
            return false;
        }

        String[] errors;
        if((errors = loadLibrary("gedevonative")) != null)
        {
            StringBuilder errcat = new StringBuilder();
            for(String e : errors)
                errcat.append("  * ").append(e).append("\n");

            GedevoUtil.msgbox("Failed to load GEDEVO native dynamic library.\n"
                            + "Alignments will not be possible - only basic functionality will work.\n"
                            + "(That you see this message probably means that you have an unsupported operating system -\n"
                            + "Try getting the source code from https://bitbucket.org/maxmalek/cytogedevo and compile it yourself)\n"
                            + "The actual errors were:\n" + errcat
            );
            return false;
        }
        if(!GedevoNative.init())
        {
            GedevoUtil.msgbox("GedevoNative.init() failed. How is this even possible?!");
            return false;
        }

        if(!GedevoNative.initThreading())
        {
            GedevoUtil.msgbox("Failed to set up GEDEVO to use Java threads.\n"
                    + "This means that all functionality will work, but alignments will be slow.\n"
                    + "(That you see this message means that something has gone terribly wrong internally.)"
            );
        }

        return true;
    }

}
