package org.cytoscape.gedevo.integration;

import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.Unloadable;

public class GedevoServices
{
    private GedevoServices() {}
    public static NodeLinkerService linker; // need to have this accessible for sticky link-mode button

    public static void register(GedevoApp app)
    {
        linker = new NodeLinkerService(app);
        new PairLayoutRegistrar(app);

        new Unloadable()
        {
            @Override
            public void unload()
            {
                linker = null;
            }
        };
    }
}
