-- replaces first two columns with DIP-xxxx to their uniprot number, if possible.

dofile("util.lua")

local string = string
local tins = table.insert
local tconcat = table.concat

local function loadnodes(fn)
    local all = {}
    for line in io.lines(fn) do
        local a = line:explode("|")
        local uniID
        local otherIDs = {}
        local ins = 1
        for i = 1, #a do
            local s = a[i]
            if s:startsWith("uniprotkb:") then
                uniID = s:sub(11)
            else
                local aa = s:explode(":")
                tins(otherIDs, aa[2] or aa[1])
            end
        end
        if uniID then
            for i = 1, #otherIDs do
                all[otherIDs[i]] = uniID
                --print(otherIDs[i], "->", uniID)
            end
        --else
        --    print("Unbound:", line)
        end
    end
    return all
end

local nodes1, nodes2, infile, outfile = ...

local lookup1 = loadnodes(nodes1)
local lookup2 = ((nodes1 == nodes2) and lookup1) or loadnodes(nodes2)

local out = io.open(outfile, "w")
out:setvbuf("full", 8*1024*1024)

local c = 0
for line in io.lines(infile) do
    if line == "" or line:sub(1, 1) == "#" then
        out:write(line, "\n")
    else
        local a = line:splitws()
        local r = lookup1[a[1]]
        if r then
            a[1] = r
            c = c + 1
        end
        r = lookup2[a[2]]
        if r then
            a[2] = r
            c = c + 1
        end
        
        out:write(tconcat(a, "\t"), "\n")
    end
end
print("DIP IDs replaced: " .. c)

out:close()
