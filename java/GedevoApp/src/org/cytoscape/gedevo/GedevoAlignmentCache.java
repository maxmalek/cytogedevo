package org.cytoscape.gedevo;

import org.cytoscape.gedevo.task.AlignmentTaskData;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;

import java.util.*;

public class GedevoAlignmentCache extends Unloadable
{
    private static GedevoAlignmentCache singleton;
    private Map<CyNetwork, Entry> cache = new WeakHashMap<CyNetwork, Entry>();

    private static class Entry
    {
        public Entry(AlignmentTaskData data, String ident)
        {
            this.data = data;
            this.ident = ident;
        }
        public AlignmentTaskData data;
        public String ident;
    }

    public GedevoAlignmentCache()
    {
        singleton = this;
    }

    @Override
    public void unload()
    {
        cache = null;
        singleton = null;
    }

    public static void clear()
    {
        if(singleton != null && singleton.cache != null)
            singleton.cache.clear();
    }

    public static void cache(AlignmentTaskData data)
    {
        if(data.cynet == null)
            throw new IllegalArgumentException("AlignmentTaskData.cynet == null");
        singleton.cache.put(data.cynet, makeEntry(data));
    }

    public static void uncache(AlignmentTaskData data)
    {
        removeForNetwork(data.cynet);
    }

    public static AlignmentTaskData get(CyNetwork cynet)
    {
        Entry e = singleton.cache.get(cynet);
        if(e == null)
            return null;
        if(e.ident.equals(generateIdent(cynet)))
            return e.data;

        GedevoApp.appInstance.log("Network identifier changed, assuming network was modified, dropping cached data");
        removeForNetwork(cynet);
        return null;
    }

    public static void removeForNetwork(CyNetwork cynet)
    {
        singleton.cache.remove(cynet);
    }

    public static Entry makeEntry(AlignmentTaskData data)
    {
        String ident = generateIdent(data.cynet);
        return new Entry(data, ident);
    }

    private static final Comparator<CyNode> nodecmp = new Comparator<CyNode>()
    {
        @Override
        public final int compare(CyNode o1, CyNode o2)
        {
            return o1.getSUID().compareTo(o2.getSUID());
        }
    };

    private static final Comparator<CyEdge> edgecmp = new Comparator<CyEdge>()
    {
        @Override
        public final int compare(CyEdge o1, CyEdge o2)
        {
            final int result1 = o1.getSource().getSUID().compareTo(o2.getSource().getSUID());
            return result1 != 0 ? result1 : o1.getTarget().getSUID().compareTo(o2.getTarget().getSUID());
        }
    };

    public static String generateIdent(CyNetwork cynet)
    {
        StringBuilder sb = new StringBuilder(1024*8);
        List<CyNode> nodes = new ArrayList<CyNode>(GedevoFilters.getNonGroupedNodes(cynet, cynet.getNodeList()));
        List<CyEdge> edges = new ArrayList<CyEdge>(GedevoFilters.getNonMappingEdges(cynet));
        Collections.sort(nodes, nodecmp);
        Collections.sort(edges, edgecmp);

        for(CyNode n : nodes)
            sb.append(n.getSUID()).append(';');
        sb.append('|');

        for(CyEdge e : edges)
            sb.append(e.getSource().getSUID()).append(',').append(e.getTarget().getSUID()).append(';');

        return sb.toString();
    }

}
