#ifndef USERSETTINGS_H
#define USERSETTINGS_H

#include "common.h"
#include <vector>


// When changing this, also adjust the switch in World.cpp - World::GetAlgo() !
// Also make sure this mirrors the enum on the java side!!
enum AlgoID
{
	ALGO_EVO,
	//ALGO_BEES, // not ported

	ALGO_MAX // needs to be last in list
};

struct CustomMatrixData
{
	enum ValueRange
	{
		CLAMP = 0,
		RESCALE = 1,
		OVERRIDE = 2,

		INVALID_VALUE_RANGE = -1
	};

	enum ModelType
	{
		DISTANCE = 0,
		SIMILARITY = 1,

		INVALID_MODEL_TYPE = -1
	};

	inline bool isOverride() const
	{
		return valueRange == OVERRIDE;
	}
	inline bool isWeight() const
	{
		return valueRange == CLAMP || valueRange == RESCALE;
	}

	inline const char *getValueRangeDesc() const
	{
		switch(valueRange)
		{
			case CLAMP: return "clamp";
			case RESCALE: return "rescale";
			case OVERRIDE: return "override";

			case INVALID_VALUE_RANGE: ; // avoid warnings for missing case value
		}
		return "missing case value - hit programmer with blunt object";
	}

	inline const char *getModelTypeDesc() const
	{
		switch(modelType)
		{
			case DISTANCE: return "distance";
			case SIMILARITY: return "similarity";

			case INVALID_MODEL_TYPE: ; // avoid warnings for missing case value
		}
		return "missing case value - hit programmer with blunt object";
	}

	inline const char *getValue1Desc() const
	{
		switch(valueRange)
		{
			case CLAMP:
			case RESCALE: return "pair score contribution factor";
			case OVERRIDE: return "first threshold";
			case INVALID_VALUE_RANGE: ;
		}
		return "missing case value - hit programmer with blunt object";
	}

	inline const char *getValue2Desc() const
	{
		switch(valueRange)
		{
		case CLAMP:
		case RESCALE: return "final score contribution factor";
		case OVERRIDE: return "second threshold";
		case INVALID_VALUE_RANGE: ;
		}
		return "missing case value - hit programmer with blunt object";
	}

	std::string filename;
	ValueRange valueRange;
	ModelType modelType;
	score value1; //pairScoreWeight or threshold1
	score value2; // finalScoreWeight or theshold2
	int leftNetwork; // group ID of network of the left column

	bool operator==(const CustomMatrixData& o) const
	{
		return filename == o.filename
			&& valueRange == o.valueRange
			&& modelType == o.modelType
			&& value1 == o.value1
			&& value2 == o.value2
			&& leftNetwork == o.leftNetwork;
	}
};


// See cpp file for settings description and default values.
// When changing members, make sure to:
// 1) modify the ALLSETTINGS #define in UserSettings.cpp accordingly
// 2) update the Java class as necessary
// 3) add java bindings in javabind.cpp
class UserSettings
{
public:
	typedef void (*DumpFunc)(const std::string& var, const std::string& val, void *ud);
	UserSettings();
	void Dump(DumpFunc f, void *ud);
	bool Import(const std::string& var, const std::string& val);

	bool CheckAndApply();

	// -- Temporary/runtime-generated settings --
	// (Intentionally moved to start of object to make sure it's in the CPU cache - accessed very often)
	bool _pairCalcGED : 1;
	bool _finalCalcGED : 1;
	bool _finalCalcGraphlet : 1;
	bool _finalCalcPairsum : 1;
	score _pairScaler;
	score _finalScaler;

	// ----- common settings -----

	// for global score
	score weightGraphlets;
	score weightPairsum;
	score weightGED;

	// for pairwise score
	score pairWeightGraphlets;
	score pairWeightGED;

	score pairNullValue;

	float basicHealth;
	float maxHealthDrop;
	uint varGroup;
	uint forceUndirectedEdges;
	uint matchSameNames;

	score ged_eAdd;
	score ged_eRm;
	score ged_eSub;
	score ged_eFlip;
	score ged_eD2U;
	score ged_eU2D;
	score ged_nAdd;
	score ged_nRm;

	// for population
	uint maxAgents;

	// evo-specific settings
	uint  evo_greedyInitOnInit;
	score evo_greedyInitScoreLimit;
	score evo_greedyInitPerRound;


	// ProgramState settings
	uint abort_seconds;
	uint abort_iterations;
	uint abort_nochange;

	// Misc settings
	uint numThreads;
	uint keepWorkfiles;
	uint autosaveSecs;
	uint saveSeries;
	std::string saveResultsFileName;
	uint saveResultsAddTimeStamp;
	std::string logger_file;
	uint logger_iterations;
	uint trimNames; // trim and cut off | in node names
	uint ignoreSelfLoops;

	std::vector<CustomMatrixData> customMatrix;


	bool _mustCalcGraphlets : 1;

	// Set to true if checked and applied
	bool _good : 1;


	inline score getPairWeightSum() const
	{
		return _pairScaler; // see UserSettings.cpp
	}
	inline score getFinalWeightSum() const
	{
		return _finalScaler;
	}
};

#endif
