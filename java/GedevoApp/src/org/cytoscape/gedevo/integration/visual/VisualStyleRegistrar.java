package org.cytoscape.gedevo.integration.visual;

import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.Unloadable;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.view.vizmap.VisualStyle;

public class VisualStyleRegistrar extends Unloadable
{
    GedevoApp app;
    VisualMappingManager vmm;
    VisualStyler styler;

    /*ArrayList<VisualStyle> registeredStyles = new ArrayList<VisualStyle>();

    private void add(VisualStyle vs)
    {
        registeredStyles.add(vs);
        vmm.addVisualStyle(vs);
    }*/

    /*private class StyleAction extends ContextMenuAction
    {
        VisualStyler.What what;
        public StyleAction(VisualStyler.What what)
        {
            this.what = what;
        }
        @Override
        public void onClick(CyNetworkView cyview)
        {
            styler.apply(cyview, what, false);
        }
    }*/

    public VisualStyleRegistrar(GedevoApp a)
    {
        app = a;
        vmm = app.cyreg.getService(VisualMappingManager.class);
        styler = new VisualStyler(a, this);

        //app.addViewContextMenuEntry("GEDEVO visualize", "CCS", new StyleAction(VisualStyler.What.CCS));
        //app.addViewContextMenuEntry("GEDEVO visualize", "Agreement", new StyleAction(VisualStyler.What.AGREEMENT));
        //app.addViewContextMenuEntry("GEDEVO visualize", "Edits", new StyleAction(VisualStyler.What.EDITS));
    }

    public VisualStyler getStyler()
    {
        return styler;
    }

    boolean removeStyle(VisualStyle rm)
    {
        for(VisualStyle vs : vmm.getAllVisualStyles())
            if(vs == rm || vs.getTitle().equalsIgnoreCase(rm.getTitle()))
            {
                vmm.removeVisualStyle(rm);
                return true;
            }
        return false;
    }

    boolean removeStyle(String rm)
    {
        for(VisualStyle vs : vmm.getAllVisualStyles())
            if(vs.getTitle().equalsIgnoreCase(rm))
            {
                vmm.removeVisualStyle(vs);
                return true;
            }
        return false;
    }

    @Override
    public void unload()
    {
        /*for(VisualStyle vs : registeredStyles)
            vmm.removeVisualStyle(vs);

        registeredStyles = null;*/
        app = null;
        styler = null;
    }
}
