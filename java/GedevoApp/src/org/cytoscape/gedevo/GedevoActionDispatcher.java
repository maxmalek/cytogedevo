package org.cytoscape.gedevo;

import org.cytoscape.gedevo.task.*;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import java.io.File;

public class GedevoActionDispatcher extends Unloadable
{
    private GedevoApp app;

    public GedevoActionDispatcher(GedevoApp a)
    {
        app = a;
    }

    public void doMerge(GedevoFilters.Filter[] src, final File toImport)
    {
        if(src.length != 2 || GedevoAlignmentUtil.isGEDEVONetwork(src[0].getCyNetwork()))
            throw new IllegalStateException("Need two seperate, non-GEDEVO networks");

        UserSettings settings = new UserSettings();
        settings.openViewWhenDone = true;
        settings.performInitialLayout = false;
        settings.mergedNetworkName = app.mainPanel.getMergedNetworkNameText();

        final AlignmentTaskData data = new AlignmentTaskData(settings);

        TaskIterator it = new TaskIterator();
        it.append(new ConstructCombinedNetworkTask(data, src));
        it.append(new FinishAlignmentTask(data));

        if(toImport != null)
            it.append(new AbstractTask()
            {
                @Override
                public void run(TaskMonitor taskMonitor) throws Exception
                {
                    // Delay task ctor until data.cynet is set
                    insertTasksAfterCurrentTask(new ImportAlignmentFileTask(data.cynet, toImport));
                }
                @Override
                public void cancel() {}
            });

        //launchBackgroundTasks(false, getTaskName(src), it); // remove from task list when done
        app.cytaskmgr.execute(it);
    }


    public void doAlignment(UserSettings settings, GedevoFilters.Filter[] src)
    {
        AlignmentTaskData data = null;
        final boolean continueSingle = src.length == 1 && GedevoAlignmentUtil.isGEDEVONetwork(src[0].getCyNetwork());
        if(continueSingle)
        {
            data = GedevoAlignmentCache.get(src[0].getCyNetwork());
            if(data != null && data.cynet != src[0].getCyNetwork())
                data = null; // wat?!
        }
        if(data == null)
        {
            data = new AlignmentTaskData(settings);
            GedevoApp.appInstance.log("Created new data");
        }
        else
        {
            data.settings = settings;
            GedevoApp.appInstance.log("Re-using cached data");
        }

        if(continueSingle && data.cynet == null)
            data.cynet = src[0].getCyNetwork();

        TaskIterator it = new TaskIterator();

        // The actual initialization order is very strict and slightly complicated:
        // - If starting from *two* source networks, combine them to a single network
        // - Convert the combined network into two import-friendly custom graphs

        if(data.cynet == null && src.length >= 2)
            it.append(new ConstructCombinedNetworkTask(data, src));

        if(data.graphs == null)
        {
            it.append(new ExtractGraphsTask(data));
            data.instance = null;
        }

        // Check this while it's possibly null
        final boolean needinstance = data.instance == null;

        // Always do this (sets data.instance)
        it.append(new SetupNativeInstanceTask(data));

        if(needinstance)
            it.append(new ImportGraphsTask(data));

        ApplyMappingTaskFactory mf = null;
        if(continueSingle)
        {
            mf = new ApplyMappingTaskFactory(data);
            it.append(mf);
            it.append(mf.makePrematchTask());
        }

        it.append(new InitAlignmentTask(data, false));

        if(mf != null)
            it.append(mf.makeConstructAgentTask());

        it.append(new InitAlignmentTask(data, true));

        if(settings.keepNativeInstance)
            it.append(new CacheAlignmentDataTask(data));

        it.append(new PerformAlignmentTask(data));
        //it.append(new WrapEnumerateCCSTask(data)); // not necessary at this point
        it.append(new CalculateAgreementTask(data));
        it.append(new CalculateEditsTask(data));

        it.append(new FinishAlignmentTask(data));

        launchBackgroundTasks(false, getTaskName(src), it); // remove from task list when done
    }

    private String getTaskName(GedevoFilters.Filter[] src)
    {
        StringBuilder sb = new StringBuilder("[");
        for(int i = 0; i < src.length; ++i)
        {
            sb.append(src[i].getNetworkName());
            if (i + 1 != src.length)
                sb.append(" / ");
        }
        sb.append("]");
        return sb.toString();
    }

    protected void launchBackgroundTasks(boolean keep, String prefix, TaskIterator it)
    {
        app.taskmgr.execute(prefix, it, keep);
    }

    protected void launchForegroundTasks(AbstractTask... tasks)
    {
        app.cytaskmgr.execute(new TaskIterator(tasks));
    }

    @Override
    public void unload()
    {
        app = null;
    }
}
