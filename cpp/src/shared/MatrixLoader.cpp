#include "MatrixLoader.h"
#include "UserSettings.h"
#include <fstream>
#include <sstream>
#include "ProgressBar.h"
#include "ProteinHolder.h"
#include "Node.h"
#include <math.h>

static bool isIn01Range(Matrix<score>& m)
{
	for (uint y = 0; y < m.height(); ++y)
		for (uint x = 0; x < m.width(); ++x)
		{
			const score v = m(x, y);
			if (v < 0 || v > 1)
				return false;
		}
	return true;
}

bool MatrixLoader::fit(Matrix<score>& dst, const Matrix<score>& src, const CustomMatrixData& data)
{
	ASSERT(!data.isOverride()); // handled elsewhere

	dst.resize(src.width(), src.height());

	switch(data.valueRange)
	{
		case CustomMatrixData::CLAMP:
		{
			for(uint y = 0; y < src.height(); ++y)
				for(uint x = 0; x < src.width(); ++x)
					dst(x, y) = clamp<score>(src(x, y), 0, 1);
		}
		break;

		case CustomMatrixData::RESCALE:
		{
			score minval = src(0, 0);
			score maxval = minval;
			for(uint y = 0; y < src.height(); ++y)
				for(uint x = 0; x < src.width(); ++x)
				{
					const score val = src(x, y);
					minval = std::min(minval, val);
					maxval = std::max(maxval, val);
				}
			const score scaler = score(1) / (maxval == minval ? minval : (maxval - minval));
			for(uint y = 0; y < src.height(); ++y)
				for(uint x = 0; x < src.width(); ++x)
					dst(x, y) = (src(x, y) - minval) * scaler; // rescale into [0 .. 1]
		}
		break;

		default:
			ASSERT(false);
		break;
	}

	switch(data.modelType)
	{
		case CustomMatrixData::DISTANCE:
			break; // nothing to do

		case CustomMatrixData::SIMILARITY:
			for(uint y = 0; y < src.height(); ++y)
				for(uint x = 0; x < src.width(); ++x)
					dst(x, y) = score(1) - dst(x, y); // convert distance to similarity
		break;

		default:
			ASSERT(false);
	}

	ASSERT(isIn01Range(dst));

	return true;
}

template <typename T>
static size_t countMatches(ProteinHolder& ph, uint group, const T& names)
{
	size_t c = 0;
	for(size_t i = 0; i < names.size(); ++i)
		c += !!ph.GetByName(group, names[i], false);
	return c;
}

template <typename T>
static int autodetectLeftGroup(ProteinHolder& ph, const T& leftNames, const T& rightNames)
{
	size_t nonflip = countMatches(ph, 0, leftNames) + countMatches(ph, 1, rightNames);
	size_t flip = countMatches(ph, 1, leftNames) + countMatches(ph, 0, rightNames);
	return (nonflip || flip) ? (nonflip > flip ? 0 : 1) : -1;
}

bool MatrixLoader::loadFile(CustomMatrixData& data, Matrix<score>& dst, ProteinHolder &ph)
{
	int leftGroup = data.leftNetwork;
	const std::string& fn = data.filename;
	ASSERT(leftGroup <= 1);

	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("MatrixLoader: ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	size_t linenum = 0;
	std::vector<const char*> leftNames, rightNames;
	std::vector<score> values;

	// save a TON of memory by storing the actual strings just once, and refer by pointer to the actual elements
	// have to do this on my box due to lack of RAM to hold everything in mem at once... sigh
	std::set<std::string> strings;

	{
		const size_t mtxSize = ph.GetGroup(0).size() * ph.GetGroup(1).size();
		printf("MatrixLoader: Loading file [%s], with at most %u unique entries\n", fn.c_str(), (uint)mtxSize);

		// don't know the actual maximum, but let's assume the worst
		leftNames.reserve(mtxSize);
		rightNames.reserve(mtxSize);
		values.reserve(mtxSize);

		ProgressBar bar(mtxSize);

		std::string name1, name2, line, valuestr;
		const bool trim = !!ph._settings.trimNames;

		while (std::getline(f, line))
		{
			++linenum;
			chompNewline(line);
			if(line.empty())
				continue;
			if(line[0] == '#')
				continue;

			// Don't load in value directly as some library implementations (visual studio 2013, i'm frowning on you)
			// set the failbit when the number to be parsed is out of representable range,
			// which in turn causes the whole check to fail.
			std::stringstream ss(line);
			if(!(ss >> name1 >> name2 >> valuestr))
			{
				printf("MatrixLoader: ERROR: Bad file (truncated line %u)\n", (uint)linenum);
				return false;
			}

			if(trim)
			{
				fixProteinName(name1);
				fixProteinName(name2);
			}

			// not so efficient, but gets the job done without keeping tons of string copies
			std::set<std::string>::iterator it = strings.insert(name1).first;
			leftNames.push_back(it->c_str());

			it = strings.insert(name2).first;
			rightNames.push_back(it->c_str());

			// This is quite hairy because invalid input returns 0, but better than
			// failing prematurely in case it fails to acknowledge that 1e-51 as float is as good as 0.
			const score value = (score)strtod(valuestr.c_str(), NULL);
			values.push_back(value);

			bar.Step();
		}
	}

	if(leftGroup < 0)
	{
		printf("MatrixLoader: Autodetecting which column belongs to which network...\n");
		leftGroup = autodetectLeftGroup(ph, leftNames, rightNames);
		if(leftGroup < 0)
		{
			printf("MatrixLoader: ERROR: Failed to autodetect leftGroup\n");
			return false;
		}
		printf("Autodetected: Left =  %s,\n"
		       "              Right = %s\n",
		       ph.GetNetworkFileName(leftGroup), ph.GetNetworkFileName(!leftGroup));
		data.leftNetwork = leftGroup;
	}

	printf("MatrixLoader: Entries loaded, generating matrix...\n");

	const uint varGroup = ph._settings.varGroup;
	const uint refGroup = !varGroup;

	// make sure that var group is always left, and ends up being the x axis of the generated matrix
	if(varGroup != leftGroup)
		leftNames.swap(rightNames);

	const uint w = ph.GetGroup(varGroup).size();
	const uint h = ph.GetGroup(refGroup).size();
	dst.resize(w+1, h+1);
	dst.fill(1);

	uint fails = 0, good = 0;
	for(size_t i = 0; i < leftNames.size(); ++i)
	{
		const char *name1 = leftNames[i];
		const char *name2 = rightNames[i];

		Node *n1 = ph.GetByName(varGroup, name1, false);
		Node *n2 = ph.GetByName(refGroup, name2, false);

		if(n1 && n2)
		{
			++good;
			dst(n1->id, n2->id) = values[i];
		}
		else
			++fails;
	}

	// var group must be on x axis, all other code expects this
	ASSERT(dst.width() == ph.GetGroup(ph._settings.varGroup).size() + 1);

	printf("MatrixLoader::load: Done %s, %u entries read, %u failed. Coverage: %f %%\n", fn.c_str(), good, fails, 100.0f * (good / ((float)w*(float)h)));

	return !!good;
}
