#include "GedevoState.h"

#include "ProgramState.h"
#include "Threading.h"


ProgramInstance::ProgramInstance(bool useThreadPool)
: _algo(NULL)
, _thpool(NULL)
, _progstate(NULL)
, _agentMgr(NULL)
, _done(false)
{
}

ProgramInstance::~ProgramInstance()
{
	if(_algo)
		_algo->Unlink();

	delete _algo;
	delete _progstate;
	delete _agentMgr;
	delete _thpool;
}

void ProgramInstance::shutdown()
{
	if(_progstate)
		_progstate->Shutdown();
}

bool ProgramInstance::preInit(AlgoID algoid)
{
	ASSERT(!_agentMgr);
	ASSERT(!_thpool);
	ASSERT(!_progstate);

	// This would be a good place to unlink + delete the old _algo and make a new one if they were different types.
	// But since Evo is the only one that is currently in use, no need to care about this.
	ASSERT(!_algo);
	

	if(threading::WasInit() && settings.numThreads != 1) // 0 is auto, so make a pool in that case as well
		_thpool = new ThreadPool();

	_progstate = new ProgramState(settings, _thpool);

	_agentMgr = new AgentMgr(_progstate->dh);

	_algo = createAlgoByID(algoid);
	if(!_algo)
	{
		delete _thpool;
		delete _progstate;
		delete _agentMgr;
		return false;
	}

	_algo->Link(*_agentMgr);

	return true;
}

bool ProgramInstance::init1()
{
	return _progstate->Init1(*_algo);
}

bool ProgramInstance::init2()
{
	_done = false;
	return _progstate->Init2();
}


void ProgramInstance::update()
{
	_progstate->NextIteration();
	_algo->Update();

	_done = _progstate->IsDone() // termination criterion reached?
		|| _algo->getBestAgent()->getEdgeCorrectness() == 1 // perfect solution?
		|| _progstate->MustQuit(); // termination request by user
}

ProteinHolder& ProgramInstance::getProteinHolder()
{
	return _progstate->ph;
}

DataHolder& ProgramInstance::getDataHolder()
{
	return _progstate->dh;
}

void ProgramInstance::setQuit(bool quit /* = true */)
{
	_progstate->SetQuit(quit);
}


#include "Evolution.h"

AlgoBase *ProgramInstance::createAlgoByID(int id)
{
	ASSERT(_progstate);

	switch(id)
	{
		case ALGO_EVO:
			return new Evo(_progstate->dh, _thpool);

		default:
			;
	}
	return NULL;
}

