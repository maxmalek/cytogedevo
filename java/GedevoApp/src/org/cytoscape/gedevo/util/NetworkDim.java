package org.cytoscape.gedevo.util;

import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;

public class NetworkDim
{
    public NetworkDim(CyNetworkView view)
    {
        double minx = Double.POSITIVE_INFINITY;
        double maxx = Double.NEGATIVE_INFINITY;
        double miny = Double.POSITIVE_INFINITY;
        double maxy = Double.NEGATIVE_INFINITY;
        for (View<CyNode> nv : view.getNodeViews())
        {
            double x = nv.getVisualProperty(BasicVisualLexicon.NODE_X_LOCATION);
            double y = nv.getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION);
            minx = Math.min(minx, x);
            maxx = Math.max(maxx, x);
            miny = Math.min(miny, y);
            maxy = Math.max(maxy, y);
        }
        this.minx = minx;
        this.miny = miny;
        this.maxx = maxx;
        this.maxy = maxy;
    }
    public final double minx;
    public final double miny;
    public final double maxx;
    public final double maxy;
}
