package org.cytoscape.gedevo.task;

import org.cytoscape.work.TaskMonitor;


public class InitAlignmentTask extends AbstractAlignmentTask
{
    private final boolean second;

    public InitAlignmentTask(AlignmentTaskData common, boolean second)
    {
        super(common);
        this.second = second;
    }

    @Override
    public void run(TaskMonitor mon) throws Exception
    {
        if(!second)
        {
            mon.setTitle("Init phase 1 ...");
            if (!common.instance.init1())
                throw new RuntimeException("Init phase 1 failed");
        }
        else
        {
            mon.setStatusMessage("Init phase 2 ... (this may take a bit)");

            if (!common.instance.init2())
                throw new RuntimeException("Init phase 2 failed");
        }
    }

    @Override
    public void cancel()
    {
        super.cancel();

        // TODO: kill C++
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
