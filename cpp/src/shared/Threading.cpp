#include "common.h"
#include "Threading.h"
#include "MersenneTwister.h"

static threading::ThreadStartFunc s_thstart = NULL;
static threading::ThreadJoinFunc s_thjoin = NULL;
static threading::WaitableCreateFunc s_wcreate = NULL;
static threading::WaitableFunc s_wdelete = NULL;
static threading::WaitableFunc s_wlock = NULL;
static threading::WaitableFunc s_wunlock = NULL;
static threading::WaitableFunc s_wwait = NULL;
static threading::WaitableFunc s_wsignal = NULL;
static threading::WaitableFunc s_wbroadcast = NULL;
static threading::GetNumCPUsFunc s_getncpu = NULL;

void threading::Quit()
{
	s_thstart = NULL;
	s_thjoin = NULL;
	s_wcreate = NULL;
	s_wdelete = NULL;
	s_wlock = NULL;
	s_wunlock = NULL;
	s_wwait = NULL;
	s_wsignal = NULL;
	s_wbroadcast = NULL;
}

bool threading::Init(ThreadStartFunc thstart,
                      ThreadJoinFunc thjoin,
                      WaitableCreateFunc wcreate,
                      WaitableFunc wdelete,
                      WaitableFunc wlock,
                      WaitableFunc wunlock,
                      WaitableFunc wwait,
                      WaitableFunc wsignal,
                      WaitableFunc wbroadcast,
                      GetNumCPUsFunc ncpu)
{
	ASSERT(!WasInit());
	bool good = ((s_thstart = thstart))
		&& ((s_thjoin = thjoin))
		&& ((s_wcreate = wcreate))
		&& ((s_wdelete = wdelete))
		&& ((s_wlock = wlock))
		&& ((s_wunlock = wunlock))
		&& ((s_wwait = wwait))
		&& ((s_wsignal = wsignal))
		&& ((s_wbroadcast = wbroadcast))
		&& ((s_getncpu = ncpu));
	if(!good)
		Quit();
	return good;
}

bool threading::WasInit()
{
	return s_thstart && s_thjoin && s_wcreate && s_wdelete && s_wlock && s_wunlock && s_wwait && s_wsignal && s_wbroadcast && s_getncpu;
}

// --------- Waitable ----------

// It's okay if we *create* a Waitable when MT is not enabled,
// but using it is not.
Waitable::Waitable()
: _w(NULL)
{
	if(s_wcreate)
	{
		_w = s_wcreate();
		ASSERT(_w);
	}
}

Waitable::~Waitable()
{
	if(s_wdelete)
		s_wdelete(_w);
}

void Waitable::lock()
{
	ASSERT(s_wlock);
	s_wlock(_w);
}

void Waitable::unlock()
{
	ASSERT(s_wunlock);
	s_wunlock(_w);
}

void Waitable::wait()
{
	ASSERT(s_wwait);
	s_wwait(_w);
}

void Waitable::signal()
{
	ASSERT(s_wsignal);
	s_wsignal(_w);
}

void Waitable::broadcast()
{
	ASSERT(s_wbroadcast);
	s_wbroadcast(_w);
}


class WorkerThread
{
public:
	WorkerThread(unsigned int id, ThreadPool& pool, unsigned int randomSeed, WorkerThread *prevThread)
		: _rng(randomSeed)
		, _id(id)
		, _pool(pool)
		, _threadPtr(NULL)
		, _prevThread(prevThread)
	{
	}

	void *run()
	{
		JobEntry je;
		do
		{
			_pool._fetchJob(je);
			//printf("Worker thread #%u starting job %p param %p\n", _id, je.func, je.param);
			if(je.func)
				je.func(_rng, je.param, je.param2, je.param3);
			//printf("Worker thread #%u finished.\n", _id);
			_pool._onJobEnd();
		}
		while(je.func);
		//printf("Worker thread #%u exiting.\n", _id);
		return NULL;
	};

	void start()
	{
		ASSERT(!_threadPtr);

		char buf[32];
		sprintf(buf, "W#%u", _id);
		_threadPtr = s_thstart(&buf[0], _Launch, this);
		if(!_threadPtr)
		{
			printf("ThreadPool ERROR: Failed to start thread!\n");
			exit(1);
		}
	}

	void join()
	{
		if(_threadPtr)
			s_thjoin(_threadPtr);
		delete this;
	}

	static void _Launch(void *p)
	{
		WorkerThread *self = (WorkerThread*)p;
		if(self->_pool.onThreadStart)
			self->_pool.onThreadStart(self->_pool.threadCallbackUserParam);
		//printf("Worker thread #%u started.\n", self->_id);
		if(self->_prevThread)
			self->_prevThread->start();
		self->run();
		if(self->_pool.onThreadExit)
			self->_pool.onThreadExit(self->_pool.threadCallbackUserParam);
	}

protected:

	MTRand _rng;

	unsigned int _id;
	ThreadPool& _pool;

	void *_threadPtr;
	WorkerThread *_prevThread;
};

ThreadPool::ThreadPool()
: onThreadStart(NULL)
, onThreadExit(NULL)
, threadCallbackUserParam(NULL)
, _quit(false)
, _busyThreads(0)
{
	ASSERT(threading::WasInit());
}

void ThreadPool::start(size_t workers /* = 0 */)
{
	_quit = false;

	if(!workers)
	{
		workers = s_getncpu();
		printf("ThreadPool: Detected %u CPU cores\n", (unsigned int)workers);
	}
	printf("ThreadPool start: %u workers\n", (unsigned int)workers);
	_spawnWorkers(workers);
}

ThreadPool::~ThreadPool()
{
	quit(true);
}

void ThreadPool::_spawnWorkers(size_t count)
{
	ASSERT(count);

	MTRand seedGen; // auto-initializes by date and time

	WorkerThread *last = NULL;
	for(size_t i = 0; i < count; ++i)
	{
		WorkerThread *w =  new WorkerThread((unsigned int)i, *this, seedGen.randInt(), last);
		_th.push_back(w);
		last = w;
	}

	// Start all threads in chain.
	// This one will subsequently start the next one, etc.
	// This is to prevent valgrind/helgrind choking on thread creation.
	// Maybe SDL is doing something wrong, i.e. parallel thread creation messes up; not sure.
	last->start();
}


void ThreadPool::quit(bool wait /* = true */)
{
	if(_quit)
		return;

	_quit = true;

	JobEntry je;
	je.func = NULL;
	je.param = je.param2 = je.param3 = NULL;

	{
		MTGuard g(_lock);
		for(size_t i = 0; i < _th.size(); ++i)
			_jobs.push_back(je);
		_lock.broadcast();
	}

	if (wait)
	{
		for(size_t i = 0; i < _th.size(); ++i)
		{
			//printf("ThreadPool: Join thread #%u\n", (uint)i);
			_th[i]->join();
		}
	}
	_th.clear();
}

void ThreadPool::waitUntilEmpty()
{
	MTGuard g(_lock);
	_lock.broadcast();
	while(_busyThreads || _jobs.size())
		_lock.wait();
}

bool ThreadPool::addJob(threadFunc f, void *param, void *param2, void *param3)
{
	MTGuard g(_lock);
	bool ret = addJobUnsafe(f, param, param2, param3);
	_lock.broadcast();
	return ret;
}

bool ThreadPool::addJobUnsafe(threadFunc f, void *param, void *param2, void *param3)
{
	if(_quit || !f)
	{
		printf("ThreadPool: AddJob %p -- IGNORED because quitting\n", f);
		return false;
	}
	//printf("ThreadPool: AddJob %p\n", f);
	JobEntry je;
	je.func = f;
	je.param = param;
	je.param2 = param2;
	je.param3 = param3;
	_jobs.push_back(je);
	return true;
}


void ThreadPool::assertIdle()
{
	MTGuard g(_lock);
	ASSERT(!_jobs.size());
	ASSERT(!_busyThreads);
}

void ThreadPool::_onJobEnd()
{
	MTGuard g(_lock);
	--_busyThreads;
	_lock.broadcast();
}

void ThreadPool::_fetchJob(JobEntry& e)
{
	MTGuard g(_lock);
	while(_jobs.empty())
		_lock.wait();
	e = _jobs.front();
	_jobs.pop_front();
	++_busyThreads;
}

bool ThreadPool::isIdle()
{
	MTGuard g(_lock);
	return !(_jobs.size() || _busyThreads);
}

