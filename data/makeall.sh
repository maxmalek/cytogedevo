#!/bin/bash

. config.inc

## Generate source data

./1_getdip.sh $DIP_PPI $DIP_SEQ
./2_parsedip.sh $DIP_PPI

## Prepare & generate BLAST data
./3_parsefasta.sh $DIP_SEQ
./4_blastall.sh

# Make raw data usable by GEDEVO
./5_cleanup.sh

## If everything worked, you can get rid of these now.
#rm work/*.z_pairlist
#rm -rf raw

## Optional: Get + extract GO data, create data2 from data1
## data2 is for use with later GO analysis

./6_uniprot.sh
./7_godata.sh

