package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.gedevo.GedevoFilters;
import org.cytoscape.gedevo.simplenet.Graph;
import org.cytoscape.model.CyNode;
import org.cytoscape.work.TaskMonitor;

import java.util.HashSet;

public class ExtractGraphsTask extends AbstractAlignmentTask
{
    public ExtractGraphsTask(AlignmentTaskData common)
    {
        super(common);
    }

    @Override
    public void run(TaskMonitor taskMonitor) throws Exception
    {
        common.graphs = new Graph[2];
        common.graphs[0] = convertToGraph(0);
        common.graphs[1] = convertToGraph(1);
    }

    private Graph convertToGraph(int netID)
    {
        GedevoFilters.EdgeFilterChain edgefilter = new GedevoFilters.EdgeFilterChain();
        GedevoFilters.NodeFilterChain nodefilter = new GedevoFilters.NodeFilterChain();

        GedevoFilters.INodeFilter nodeValueFilter =
            new GedevoFilters.AllNodesWithValueFilter<Integer>(common.cynet, ColumnNames.SOURCE_NETWORK_ID, netID);
        GedevoFilters.INodeFilter nodeUngroupedFilter =
            new GedevoFilters.AllNonGroupNodesFilter(common.cynet);

        nodefilter.add(nodeUngroupedFilter);
        nodefilter.add(nodeValueFilter);

        edgefilter.add(new GedevoFilters.AllEdgesFullyInNodeSet(new HashSet<CyNode>(nodefilter.filterNodes(common.cynet.getNodeList()))));

        if(common.settings.ignoreSelfLoops)
            edgefilter.add(new GedevoFilters.IgnoreSelfLoopsEdgeFilter(common.cynet));

        return Graph.importNetworkFilter(new GedevoFilters.Filter(common.cynet, nodefilter, edgefilter));
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }
}
