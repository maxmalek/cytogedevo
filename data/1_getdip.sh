#!/bin/bash

DIP_PPI=$1
DIP_SEQ=$2

if [ x$DIP_PPI == x ] || [ x$DIP_SEQ == x ]; then
    echo "$0 <PPIversion> <SEQversion>"
    echo "Specify DIP versions to get, like so:"
    echo "$0 dip20150101 dip20131201"
    exit 1
fi

if [ ! -d raw ]; then
    mkdir raw
fi

(
    cd raw

    if [ ! -e $DIP_PPI.txt ]; then
        url="http://dip.mbi.ucla.edu/dip/file?ds=archives&fn=${DIP_PPI}&ff=txt&fc=gz"
        echo "Get PPI [$url]"
        wget -O $DIP_PPI.txt.gz "$url"
        gunzip $DIP_PPI.txt.gz
    fi

    if [ ! -e $DIP_SEQ.seq ]; then
        #url="http://dip.mbi.ucla.edu/dip/file?ds=sequence&fn=${DIP_SEQ}&ff=seq&fc=gz"
        #echo "Get URL [$url]"
        #wget -O $DIP_SEQ.seq.gz "$url"
        #gunzip $DIP_SEQ.seq.gz
        
        # uncompressed
        url="http://dip.mbi.ucla.edu/dip/file?ds=sequence&fn=${DIP_SEQ}.seq"
        echo "Get SEQ [$url]"
        wget -O $DIP_SEQ.seq "$url"
    fi
    
    
    if [ -e $DIP_PPI.txt ] && [ -e $DIP_SEQ.seq ]; then
        echo "DIP data retrieved!"
    else
        echo "Something failed!"
        exit 1
    fi
)

