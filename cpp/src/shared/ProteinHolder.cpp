#include "ProteinHolder.h"
#include <algorithm>
#include <fstream>
#include <sstream>
#include <cmath>
#include "tools.h"
#include "ProgressBar.h"
#include "UserSettings.h"
#include "Threading.h"
#include "orca.h"


ProteinHolder::ProteinHolder(const UserSettings& settings)
	: _settings(settings)
	, _thpool(NULL)
	, _lightPrepared(false)
	, _heavyPrepared(false)
	, _nodeIDsDistributed(false)
{
}

ProteinHolder::~ProteinHolder()
{
	for(size_t i = 0; i < 2; ++i)
		for(NodeArray::iterator it = _grpV[i].begin(); it != _grpV[i].end(); ++it)
			delete *it;
}

void ProteinHolder::SetNetworkFileName(uint group, const std::string& name)
{
	ASSERT(group <= 1);
	_networkFileNames[group] = name;
}

Node *ProteinHolder::CreateAndAdd(uint group, const std::string& name)
{
	ASSERT(!_lightPrepared);
	ASSERT(group <= 1);
	NodeMap& m = _grpN[group];
	NodeArray& a = _grpV[group];

	const size_t idx = a.size();
	Node *n = new Node(group, (uint)idx+1, name); // id is never 0
	a.push_back(n);
	m.insert(std::make_pair(name, n)); // does not insert if name already exists as key

	return n;
}

Node *ProteinHolder::GetByName(uint group, const std::string& name, bool create /* = false */)
{
	ASSERT(!_lightPrepared);
	ASSERT(group <= 1);
	NodeMap& m = _grpN[group];

	NodeMap::iterator it = m.find(name);
	if(it != m.end())
		return it->second;

	return create ? CreateAndAdd(group, name) : NULL;
}

void ProteinHolder::AddPair(Node *p1, Node *p2)
{
	ASSERT(!_lightPrepared);
	ASSERT(p1->group == p2->group);

	if(p1 == p2 &&_settings.ignoreSelfLoops)
		return;

	p1->nb.insert(p2);
	p2->nb.insert(p1);

	p1->outgoing.insert(p2);
	p2->incoming.insert(p1);

	if(_settings.forceUndirectedEdges)
	{
		p1->incoming.insert(p2);
		p2->outgoing.insert(p1);
	}

	if(p1 == p2)
		p1->hasSelfLoop = 1;
}

void ProteinHolder::AddPairByIdx(uint groupIdx, uint i, uint j)
{
	ASSERT(!_lightPrepared);
	AddPair(_grpV[groupIdx][i], _grpV[groupIdx][j]);
}

void ProteinHolder::_AddPairByName(uint groupIdx, const std::string& name1, const std::string& name2)
{
	ASSERT(!_lightPrepared);
	Node *p1 = GetByName(groupIdx, name1, true);
	Node *p2 = GetByName(groupIdx, name2, true);
	ASSERT(p1 && p2); // can't fail
	AddPair(p1, p2);
}

// Reads simple .sif files exported by cytoscape - for testing
bool ProteinHolder::ReadNetworkSIF(const std::string& fn, uint groupIdx)
{
	ASSERT(!_lightPrepared);
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadNetworkSIF ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	std::string name1, name2, ty;
	uint c = 0;
	const uint trim = _settings.trimNames;

	while(true)
	{
		f >> name1 >> ty >> name2;
		if(!f)
			break;

		if(trim)
		{
			fixProteinName(name1);
			fixProteinName(name2);
		}

		strToLower(ty);

		if(ty == "undirectededge" || ty == "undirected" || ty == "u")
		{
			undirected:
			_AddPairByName(groupIdx, name1, name2);
			_AddPairByName(groupIdx, name2, name1);
			++c;
		}
		else if(ty == "directededge" || ty == "directed" || ty == "d")
		{
			_AddPairByName(groupIdx, name1, name2);
			++c;
		}
		else
		{
			printf("ReadNetworkSIF: WARNING: Unknown interaction type '%s', assuming undirected!\n", ty.c_str());
			goto undirected;
		}
	}

	printf("ReadNetworkSIF: %s -> %u connections (group %u)\n", fn.c_str(), c, groupIdx);
	return true;
}

// Reads simple .edgelist files
bool ProteinHolder::ReadNetworkEDGELIST(const std::string& fn, uint groupIdx, bool hasHeader)
{
	ASSERT(!_lightPrepared);
	std::ifstream f(fn.c_str());
	if(!f)
	{
		printf("ReadNetworkEDGELIST ERROR: File not found: %s\n", fn.c_str());
		return false;
	}

	std::string name1, name2;
	uint c = 0;
	const uint trim = _settings.trimNames;

	if (hasHeader)
		std::getline(f, name1);

	while(true)
	{
		f >> name1 >> name2;
		if(!f)
			break;

		if(trim)
		{
			fixProteinName(name1);
			fixProteinName(name2);
		}

		_AddPairByName(groupIdx, name1, name2);
		++c;
	}

	printf("ReadNetworkEDGELIST: %s -> %u connections (group %u)\n", fn.c_str(), c, groupIdx);
	return true;
}


bool ProteinHolder::PrepareLight()
{
	_lightPrepared = false;

	uint fails = 0;

	printf("Group sizes: {name->Node} = [%u, %u]\n",  (uint)_grpV[0].size(), (uint)_grpV[1].size());

	if(!(_grpV[0].size() && _grpV[1].size()))
	{
		printf("ERROR: At least one network is empty. Did the input files load correctly?\n");
		return false;
	}

	_SetupNodeIDs();

	for(uint g = 0; g < 2; ++g)
		for(NodeArray::iterator it = _grpV[g].begin(); it != _grpV[g].end(); ++it)
		{
			Node *p = *it;
			if(!(p->id
				&& p->group <= 1
				&& p->group == g
			) ) {
				++fails;
				printf("Prepare: Something is wrong with %s\n", p->name().c_str());
			}
		}

	if(fails)
	{
		printf("Prepare: %u ERRORS!\n", fails);
		return false;
	}

	_lightPrepared = true;

	return true;
}

// only call this once, when initing the first time. The data stay constant afterwards
bool ProteinHolder::PrepareHeavy()
{
	ASSERT(_lightPrepared);
	ASSERT(_nodeIDsDistributed);

	if(!_heavyPrepared)
	{
		if(_thpool)
		{
			_thpool->assertIdle();
			_thpool->addJob(_th_DoPrepareStep, this, (void*)(intptr_t)PREP_DEGREE_LOGS);
			_thpool->addJob(_th_DoPrepareStep, this, (void*)(intptr_t)PREP_CONN_MTX_AND_GRAPHLETS, (void*)(intptr_t)0);
			_thpool->addJob(_th_DoPrepareStep, this, (void*)(intptr_t)PREP_CONN_MTX_AND_GRAPHLETS, (void*)(intptr_t)1);
			_thpool->waitUntilEmpty();
		}
		else
		{
			_DoPrepareStep(PREP_DEGREE_LOGS);
			_DoPrepareStep(PREP_CONN_MTX_AND_GRAPHLETS, 0);
			_DoPrepareStep(PREP_CONN_MTX_AND_GRAPHLETS, 1);
		}
		_heavyPrepared = true;
	}

	_lightPrepared = false;
	printf("Prepare: Everything good!\n");

	return true;
}

void ProteinHolder::_DoPrepareStep(PrepareStep prep, int param)
{
	switch(prep)
	{
		case PREP_CONN_MTX_AND_GRAPHLETS:
			_SetupConnectivityMatrix(param);
			if(_settings._mustCalcGraphlets)
				_PrecalcGraphlets(param);
			break;

		case PREP_DEGREE_LOGS:
			_PrecalcDegreeLogs();
			break;

		default:
			ASSERT(false);
	}
}

void ProteinHolder::_th_DoPrepareStep(MTRand& , void *vthis, void *pIntId, void *pParamP)
{
	ProteinHolder *this_ = (ProteinHolder*)vthis;
	this_->_DoPrepareStep((PrepareStep)(intptr_t)pIntId, (int)(intptr_t)pParamP);
}



void ProteinHolder::_SetupConnectivityMatrix(uint g)
{
	printf("Setting up connectivity matrix for group %u ...\n", g);
	NodeArray& M = _grpV[g];
	Matrix<uchar>& C = _conn[g];
	uint sz = M.size();

	// Because calculating a large matrix in debug mode takes ages...
	if(_ReadConnectivityMatrixBin(g))
		return;

	C.resize(sz + 1, sz + 1); // leave space for null row/col

	ProgressBar bar(sz);

	// Fill null entries (first row and column)
	for(uint i = 0; i < sz + 1; ++i)
	{
		C(0, i) = 0;
		C(i, 0) = 0;
	}

	for(NodeArray::iterator a = M.begin(); a != M.end(); ++a)
	{
		ASSERT((*a)->id);
		for(NodeArray::iterator b = M.begin(); b != M.end(); ++b)
			C((*a)->id, (*b)->id) = (*a)->hasEdgeTo(*b) ? 1 : 0;
		bar.Step();
	}

	_WriteConnectivityMatrixBin(g);
}

void ProteinHolder::_PrecalcGraphlets(uint g)
{
	printf("Precalculating graphlets for group %u ...\n", g);
	OrcaGraphletCounter::GraphletMatrix orbits;
	OrcaGraphletCounter::count5(_conn[g], orbits);
    printf("Done; calculating logs...\n");

	for (size_t i = 0; i < _grpV[g].size(); ++i)
	{
		Node *p = _grpV[g][i];
		NodePrototype *proto = p->proto;
		for(uint orbit = 0; orbit < GRAPHLET_SIGS; ++orbit)
		{
			const OrcaGraphletCounter::uint64 gcount = orbits(orbit, i);
			proto->graphlets[orbit] = (uint)gcount;
			proto->graphletLogs[orbit] = log(score(gcount + 1)); // see GraphletScore.cpp
		}
	}
}

void ProteinHolder::_PrecalcDegreeLogs()
{
	printf("Precalculating degree logs...\n");
	for(uint j = 0; j < 2; ++j)
	{
		for (uint i = 0; i < _grpV[j].size(); ++i)
		{
			Node *p = _grpV[j][i];
			NodePrototype *proto = p->proto;

			// See NodeDistance.cpp
			proto->degreeLogIn  = log(score(p->getDegreeIncoming() + 1));
			proto->degreeLogOut = log(score(p->getDegreeOutgoing() + 1));
		}
	}
}

void ProteinHolder::Cleanup()
{
	for(uint g = 0; g < 2; ++g)
	{
		_conn[g].forceDeallocate();
		//NodeArray().swap(_grp[g]);
		for(NodeArray::iterator it = _grpV[g].begin(); it != _grpV[g].end(); ++it)
			(*it)->dropProto();
	}

	std::vector<std::pair<uint, uint> >().swap(prematchIdx);
}

bool ProteinHolder::PrematchByIdx(uint a, uint b)
{
	ASSERT(!_lightPrepared);
	ASSERT(_grpV[0].size());
	ASSERT(_grpV[1].size());

	if((size_t)a >= _grpV[0].size())
		return false;
	if((size_t)b >= _grpV[1].size())
		return false;

	prematchIdx.push_back(std::make_pair(a, b));
	return true;
}

// Prematched nodes always go before non-prematched nodes,
// otherwise sort by name
static bool _sortPrematchLessThan(Node *a, Node *b)
{
	return a->prematched >= 0
		? (b->prematched < 0 ? true : a->prematched < b->prematched)
		: (b->prematched >= 0 ? false : a->name() < b->name());
}

void ProteinHolder::_SetupNodeIDs()
{
	ASSERT(!_lightPrepared);

	const uint vargroup = _settings.varGroup;
	const uint refgroup = !vargroup;

	NodeArray& AV = _grpV[vargroup];
	NodeArray& BV = _grpV[refgroup];

	// Clear old prematching state if there is any
	for(size_t i = 0; i < AV.size(); ++i)
		AV[i]->prematched = -1;
	for(size_t i = 0; i < BV.size(); ++i)
		BV[i]->prematched = -1;

	// Prematch by previously stored pairs.
	int prematchID = 0;
	for(size_t i = 0; i < prematchIdx.size(); ++i)
	{
		Node *a = GetByIdx(vargroup, prematchIdx[i].first); // first of pair is always var group, second always ref
		Node *b = GetByIdx(refgroup, prematchIdx[i].second);

		printf("Prematched nodes: [%s] [%s] by prematch list\n", a->name().c_str(), b->name().c_str());
		a->prematched = prematchID;
		b->prematched = prematchID;
		++prematchID;
	}

	if(_settings.matchSameNames)
	{
		uint matched = 0;
		for(size_t i = 0; i < AV.size(); ++i)
		{
			// Try to match a with b, but only if neither of them have been prematched before
			Node *a = AV[i];
			if(a && a->prematched == -1)
			{
				Node *b = GetByName(refgroup, AV[i]->name(), false);
				if(b && b->prematched == -1)
				{
					printf("Prematched nodes: [%s] [%s] by name\n", a->name().c_str(), b->name().c_str());
					a->prematched = prematchID;
					b->prematched = prematchID;
					++prematchID;
					++matched;
				}
			}
		}
		printf("Nodes matched by equal name: %u\n", matched);
	}

	std::sort(AV.begin(), AV.end(), _sortPrematchLessThan);
	std::sort(BV.begin(), BV.end(), _sortPrematchLessThan);

	printf("Total nodes prematched: %d\n", prematchID);

	// Node IDs must be calculated exactly once.
	// Because all the matrices generated later depend on the IDs set here,
	// changing them would require a global re-calculation.
	// Since adding and removing nodes or edges is not allowed for any active instance,
	// doing this would (and MUST) result in a new instance where everything is recalculated.
	if(!_nodeIDsDistributed)
	{
		// Assign IDs by position in vector. IMPORTANT: 0 is reserved for "invalid node"
		for(size_t i = 0; i < AV.size(); ++i)
			AV[i]->id = i + 1;
		for(size_t i = 0; i < BV.size(); ++i)
			BV[i]->id = i + 1;
		_nodeIDsDistributed = true;
	}
}
