package org.cytoscape.gedevo.integration.customlayoutmenu;

import org.cytoscape.gedevo.Unloadable;
import org.cytoscape.view.layout.CyLayoutAlgorithm;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Map;

// Service listener stub class -- service listeners can't be unloaded in the cytoscape model,
// so let's at least add rudimentary unloading capabilities and drop everything possible
// when this class is no longer needed. After this it's still there, but doesn't hold any
// references and isn't otherwise in the way.
// Note: This is registered too late, and can never get hold of any properties specified
//       at registration time, so just ignore those altogether.
public class LayoutListenerForwarder extends Unloadable
{
    private ArrayList<LayoutListener> listeners = new ArrayList<LayoutListener>();
    private HashSet<CyLayoutAlgorithm> layouts = new HashSet<CyLayoutAlgorithm>();


    @SuppressWarnings("unused") // accessed via reflection
    public synchronized void addLayout(CyLayoutAlgorithm layout, Map propsIgnored)
    {
        if(listeners == null)
            return;

        layouts.add(layout);
        for(LayoutListener ll : listeners)
            ll.addLayout(layout);
    }

    @SuppressWarnings("unused") // accessed via reflection
    public synchronized void removeLayout(CyLayoutAlgorithm layout, Map propsIgnored)
    {
        if(listeners == null)
            return;

        layouts.remove(layout);
        for(LayoutListener ll : listeners)
            ll.removeLayout(layout);
    }

    public void addLayoutListener(LayoutListener ll)
    {
        if(listeners == null)
            return;

        listeners.add(ll);
        // Make sure each newly registered listener gets to know the layouts registered so far
        for(CyLayoutAlgorithm algo : new ArrayList<CyLayoutAlgorithm>(layouts))
            ll.addLayout(algo);
    }

    @Override
    public void unload()
    {
        listeners = null;
        layouts = null;
    }
}
