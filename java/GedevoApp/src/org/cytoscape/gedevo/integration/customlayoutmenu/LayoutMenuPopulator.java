package org.cytoscape.gedevo.integration.customlayoutmenu;

// Obtained by decompiling the corresponding cytoscape internal class (was faster than looking for source)
// Plus some adjustments

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.model.CyColumn;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.util.swing.GravityTracker;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.TaskManager;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LayoutMenuPopulator implements MenuListener, LayoutListener
{
    private Set<CyLayoutAlgorithm> algorithmSet;
    private Map<CyLayoutAlgorithm, JMenuItem> menuMap;
    private CyApplicationManager appMgr;
    private TaskManager tm;
    private GravityTracker gravityTracker;
    private JMenu layoutMenu;

    public LayoutMenuPopulator(GravityTracker gtracker, CyApplicationManager appMgr, TaskManager tm)
    {
        this.algorithmSet = new HashSet<CyLayoutAlgorithm>();
        this.menuMap = new HashMap<CyLayoutAlgorithm, JMenuItem>();
        this.appMgr = appMgr;
        this.tm = tm;
        this.gravityTracker = gtracker;
        this.layoutMenu = ((JMenu)this.gravityTracker.getMenu());
        this.layoutMenu.addMenuListener(this);
    }

    @Override
    public void addLayout(CyLayoutAlgorithm layout)
    {
        this.algorithmSet.add(layout);
    }

    @Override
    public void removeLayout(CyLayoutAlgorithm layout)
    {
        this.algorithmSet.remove(layout);
        if (this.menuMap.containsKey(layout)) {
            this.layoutMenu.remove(this.menuMap.remove(layout));
        }
    }

    public void menuCanceled(MenuEvent e) {}

    public void menuDeselected(MenuEvent e) {}

    public void menuSelected(MenuEvent e)
    {
        CyNetwork network = this.appMgr.getCurrentNetwork();


        boolean someSelected = false;
        if (network != null) {
            someSelected = network.getDefaultNodeTable().countMatchingRows("selected", true) > 0;
        }
        boolean enableMenuItem = checkEnabled();
        for (CyLayoutAlgorithm layout : this.algorithmSet)
        {
            if (this.menuMap.containsKey(layout)) {
                this.layoutMenu.remove(this.menuMap.remove(layout));
            }
            boolean usesNodeAttrs = false;
            if (network != null) {
                usesNodeAttrs = hasValidAttributes(layout.getSupportedNodeAttributeTypes(), network.getDefaultNodeTable());
            }
            boolean usesEdgeAttrs = false;
            if (network != null) {
                usesEdgeAttrs = hasValidAttributes(layout.getSupportedEdgeAttributeTypes(), network.getDefaultEdgeTable());
            }
            boolean usesSelected = (layout.getSupportsSelectedOnly()) && (someSelected);
            final String lname = layout.toString();
            double gravity = calcGravity(lname); // returns human name
            GedevoApp.log("Layout [g=" + ((float)gravity) + "]: " + lname);
            if (usesNodeAttrs || usesEdgeAttrs || usesSelected)
            {
                JMenu newMenu = new DynamicLayoutMenu(layout, network, enableMenuItem, appMgr, tm, usesNodeAttrs, usesEdgeAttrs, usesSelected);

                this.menuMap.put(layout, newMenu);
                this.gravityTracker.addMenu(newMenu, gravity);
            }
            else
            {
                JMenuItem newMenu = new StaticLayoutMenu(layout, enableMenuItem, appMgr, this.tm);
                this.menuMap.put(layout, newMenu);
                this.gravityTracker.addMenuItem(newMenu, gravity);
            }
        }
    }

    private boolean hasValidAttributes(Set<Class<?>> typeSet, CyTable attributes)
    {
        for (CyColumn column : attributes.getColumns()) {
            if (typeSet.contains(column.getType())) {
                return true;
            }
        }
        return false;
    }

    private boolean checkEnabled()
    {
        CyNetwork network = this.appMgr.getCurrentNetwork();
        if (network == null) {
            return false;
        }
        CyNetworkView view = this.appMgr.getCurrentNetworkView();
        if (view == null) {
            return false;
        }
        return true;
    }

    // cheap way to sort by name alphabetically
    // double will lose precision eventually with long strings but better than not sorted at all
    private double calcGravity(String s)
    {
        final double div = (double)('z' - 'a');
        s = s.toLowerCase();
        double g = 0.0;
        double m = 1000.0;
        final int len = s.length();
        for(int i = 0; i < len; ++i)
        {
            double d = (s.charAt(i) - 'a') / div;
            d = Math.min(1.0, Math.max(d, 0.0));
            g += d * m;
            m *= 0.1;
        }
        return g;
    }
}
