#include "Logger.h"
#include "ProgramState.h"


Logger::Logger(const ProgramState& state)
: _file(NULL)
, _linesWritten(0)
, _every(0)
, state(state)
{
}

Logger::~Logger()
{
	close();
}

void Logger::close()
{
	if(_file)
	{
		fclose(_file);
		_file = NULL;
	}
}

bool Logger::open(const char *fn, const time_t *when, bool append /* = true */)
{
	_every = state.settings.logger_iterations;
	if(!_every)
		return false;
	if(!state.GetWorkObject())
		return false;
	_file = fopen(fn, append ? "a" : "w");
	if(_file)
	{
		setvbuf(_file, NULL, _IOFBF, 4 * 1024);
		// initial row
		fprintf(_file, "# Log opened: %s\n", generateTimestampStr(when).c_str());
		fprintf(_file, "# Iteration\tGEDScore\tEC\tPairSumScore\tRawGED\tRawGEDPartial\tTimeSecs\n");
		return true;
	}
	return false;
}

void Logger::log()
{
	if(!_file)
		return;

	const uint iters = state.GetIterationCount();
	if(iters > 200 && iters % _every)
		return;

	const Agent* a = state.GetWorkObject()->getBestAgent();

	a->getScore(); // Make sure all values are present

	//                         1   2   3   4   5   6   7
	int res = fprintf(_file, "%u\t%f\t%f\t%f\t%u\t%u\t%u\n",
		iters,                     // 1, u
		a->getGEDScore(),          // 2, f
		a->getEdgeCorrectness(),   // 3, f
		a->getPairSumScore(),      // 4, f
		a->getGEDRawFull(),        // 5, u
		a->getGEDRawPartial(),     // 6, u
		state.GetTotalRunningTime()// 7, u
	);

	int res2 = 0;

	++_linesWritten;

	if(_linesWritten > 20)
	{
		_linesWritten = 0;
		res2 = fflush(_file);
	}

	if(res < 0 || res2 < 0)
		printf("WARNING: Failed to write to log file\n");
}
