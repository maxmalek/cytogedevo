#ifndef PROTEIN_NODE_H
#define PROTEIN_NODE_H

#include "common.h"
#include "MurmurHash3.h"
#include "Node.h"

struct NodeMapping : public NodeAndIDArray
{
	NodeMapping()
	: _hashed(false)
	{
	}

	inline bool operator==(const NodeMapping& o) const
	{
		// accurate method, but slow. Approxmate hash comparison is sufficient,
		// and collisions are not problematic for where op== is used.
		//return nodeId == o.nodeId; 

		ASSERT(_hashed);
		ASSERT(o._hashed);
		return !memcmp(&_hash[0], &o._hash[0], sizeof(_hash));
	}

	// for sorting
	inline bool operator<(const NodeMapping& o) const
	{
		//return nodeId < o.nodeId; // accurate method

		ASSERT(_hashed);
		ASSERT(o._hashed);
		return memcmp(&_hash[0], &o._hash[0], sizeof(_hash)) < 0;
	}

	inline void doHash()
	{
		if(!_hashed)
		{
			MurmurHash3_x64_128(&nodeId[0], size() * sizeof(uint), 0, &_hash[0]);
			_hashed = true;
		}
	}

private:
	unsigned char _hash[16]; // 2x uint64
	bool _hashed;
};


#endif
