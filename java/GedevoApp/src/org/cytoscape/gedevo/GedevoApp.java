package org.cytoscape.gedevo;

import org.cytoscape.app.swing.AbstractCySwingApp;
import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.application.swing.AbstractCyAction;
import org.cytoscape.application.swing.CySwingApplication;
import org.cytoscape.event.CyEventHelper;
import org.cytoscape.gedevo.integration.ContextMenuInit;
import org.cytoscape.gedevo.integration.GedevoListeners;
import org.cytoscape.gedevo.integration.GedevoServices;
import org.cytoscape.gedevo.integration.SelectMenuIntegrator;
import org.cytoscape.gedevo.integration.visual.VisualStyleRegistrar;
import org.cytoscape.gedevo.integration.visual.VisualStyler;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.subnetwork.CyRootNetworkManager;
import org.cytoscape.service.util.CyServiceRegistrar;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.CyNetworkViewManager;
import org.cytoscape.view.vizmap.VisualMappingManager;
import org.cytoscape.work.SynchronousTaskManager;
import org.cytoscape.work.swing.DialogTaskManager;
import org.cytoscape.work.undo.UndoSupport;

import java.awt.event.ActionEvent;

public final class GedevoApp extends AbstractCySwingApp
{
    public static final boolean DEBUG = false;
    public static final String VERSION_TEXT = "CytoGEDEVO v1.0.4";

    public static GedevoApp appInstance;

    public CySwingAppAdapter cyadapter;
    public CySwingApplication cyapp;
    public CyNetworkViewManager cyviewmgr;
    public CyNetworkViewFactory cyviewfac;
    public CyApplicationManager cyappmgr;
    public CyNetworkFactory cynetfac;
    public CyServiceRegistrar cyreg;
    public CyEventHelper cyeventmgr;
    public CyLayoutAlgorithmManager cylayoutmgr;
    public CyNetworkManager cynetmgr;
    public DialogTaskManager cytaskmgr;
    public SynchronousTaskManager cysynctaskmgr;
    public VisualMappingManager vmm;
    public CyRootNetworkManager cyrootmgr;
    public UndoSupport undo;

    public GedevoActionDispatcher dispatch;
    public GedevoTaskManager taskmgr;
    public ContextMenu contextmenu;
    public GedevoListeners listeners;
    public VisualStyleRegistrar stylereg;
    public VisualStyler styler;

    private boolean shuttingDown = false;
    private boolean _hasNativeLibs = false;

    public GedevoMainPanel mainPanel;

    private AbstractCyAction installEverythingAction;


    public GedevoApp(CySwingAppAdapter adapter)
    {
        super(adapter);
        appInstance = this;
        cyadapter = adapter;

        cyapp = cyadapter.getCySwingApplication();
        cynetmgr = cyadapter.getCyNetworkManager();
        cyviewmgr = cyadapter.getCyNetworkViewManager();
        cyviewfac = cyadapter.getCyNetworkViewFactory();
        cynetfac = cyadapter.getCyNetworkFactory();
        cyappmgr = cyadapter.getCyApplicationManager();
        cyreg = cyadapter.getCyServiceRegistrar();
        cyeventmgr = cyadapter.getCyEventHelper();
        cytaskmgr = cyadapter.getDialogTaskManager();
        cylayoutmgr = cyadapter.getCyLayoutAlgorithmManager();
        cyrootmgr = cyadapter.getCyRootNetworkManager();
        vmm = cyadapter.getVisualMappingManager();
        undo = cyadapter.getUndoSupport();

        cysynctaskmgr = cyreg.getService(SynchronousTaskManager.class);

        installLoader();
    }

    public boolean hasNativeLibs() { return _hasNativeLibs; }

    private void installLoader()
    {
        installEverythingAction = new AbstractCyAction("Load CytoGEDEVO", cyappmgr, "", cyviewmgr)
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                initApp();
            }
        };
        installEverythingAction.setPreferredMenu("Apps");
        addMenuAction(installEverythingAction);
    }

    private void clearLoader()
    {
        if(cyapp != null && installEverythingAction != null)
        {
            cyapp.removeAction(installEverythingAction);
            installEverythingAction = null;
        }
    }

    // Also accessed by GedevoAppStub via reflection
    public void initApp()
    {
        log("Starting GEDEVO plugin...");

        if(!((_hasNativeLibs = GedevoNativeUtil.initNativeLibs())))
        {
            log("Failed to load native libs");
        }

        install();
        refresh();

        log("Ready.");

        clearLoader();
    }

    public static void log(String s)
    {
         if(DEBUG)
             System.out.println(s);
    }

    public void addMenuAction(AbstractCyAction m)
    {
        cyapp.addAction(m);
    }

    public void addViewContextMenuEntry(String submenu, String label, ContextMenuAction action)
    {
        contextmenu.addNetworkViewEntry(submenu, label, action);
    }

    private void install()
    {
        dispatch = new GedevoActionDispatcher(this);
        mainPanel = new GedevoMainPanel(this);
        taskmgr = new GedevoTaskManager(this);
        //sessmgr = new GedevoSessionHandler(this);
        contextmenu = new ContextMenu(this);
        listeners = new GedevoListeners(this);
        stylereg = new VisualStyleRegistrar(this);
        styler = stylereg.getStyler();

        new SelectMenuIntegrator(this);
        new GedevoAlignmentCache();

        ContextMenuInit.create(this);
        GedevoServices.register(this);

        addMenuAction(new SimpleMenuAction(this, "About CytoGEDEVO...", "Apps")
        {
            @Override
            public void actionPerformed(ActionEvent e)
            {
                new AboutBox();
            }
        });

        if(DEBUG)
            new DebugStuff(this);
    }

    @SuppressWarnings("unused")
    // Accessed by GedevoAppStub via reflection
    public void teardown()
    {
        shuttingDown = true;
        clearLoader();
        Unloadable.unloadAll();

        cyadapter = null;
        cyapp = null;

        cynetmgr = null;
        cyviewmgr = null;
        cyviewfac = null;
        cyappmgr = null;
        cyreg = null;
        cyeventmgr = null;
        cytaskmgr = null;
        cynetfac = null;
        cylayoutmgr = null;
        cyrootmgr = null;

        dispatch = null;
        mainPanel = null;
        //sessmgr = null;
        contextmenu = null;
        listeners = null;
        stylereg = null;
        styler = null;
        appInstance = null;

        if(hasNativeLibs())
            GedevoNative.shutdown();
    }

    public void refresh()
    {
        if(shuttingDown)
            return;

        mainPanel.refresh();
    }

    public void repaint()
    {
        if(shuttingDown)
            return;

        cyadapter.getCySwingApplication().getJFrame().repaint();
    }

    public boolean gatherSettings(UserSettings settings)
    {
        return mainPanel.gatherSettings(settings);
    }
}

