package org.cytoscape.gedevo.integration.ccs;

public enum EditType
{
    SUBSTITUTED,    // edges correspond exactly
    ADDED,          // "Does not exist in other network"
    DELETED,        // used to differentiate addition from deletion when coloring a combined network
    DIRECTION_CHANGED, // undirected became directed or vice versa
    FLIPPED,
}
