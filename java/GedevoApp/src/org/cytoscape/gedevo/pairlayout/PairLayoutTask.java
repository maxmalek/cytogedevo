package org.cytoscape.gedevo.pairlayout;

import org.cytoscape.event.CyEventHelper;
import org.cytoscape.gedevo.GedevoAlignmentUtil;
import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.GedevoSafeTask;
import org.cytoscape.gedevo.GedevoUtil;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkFactory;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.layout.LayoutEdit;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.CyNetworkViewFactory;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;
import org.cytoscape.work.undo.UndoSupport;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class PairLayoutTask extends GedevoSafeTask
{
    private CyNetwork cynet;
    private Object layoutContext;
    private String layoutField;
    private Set<CyNode> selectedNodes = new HashSet<CyNode>();
    private UndoSupport undo;
    private CyNetworkView cyview;

    CyNetwork resultNet;
    CyNetworkView resultView;
    Map<CyNode, CyNode[]> resultBackmap;
    CyLayoutAlgorithm layout;
    Map<CyNode, double[]> positions;

    public static boolean isReady(CyNetworkView cyview)
    {
        CyNetwork net = cyview.getModel();
        return GedevoAlignmentUtil.isGEDEVONetwork(net);
    }

    public PairLayoutTask(CyNetworkView cyview,
                          CyLayoutAlgorithm layout, Object layoutContext, Set<View<CyNode>> views, String layoutField, UndoSupport undo) // <-- these can be NULL
    {
        Set<CyNode> selected = null;
        if(views != null && views.size() > 0)
        {
            selected = new HashSet<CyNode>();
            for (View<CyNode> v : views)
                selected.add(v.getModel());
        }
        this.cyview = cyview;
        this.undo = undo;
        init(cyview.getModel(), layout, layoutContext, selected, layoutField);
    }

    private void init(CyNetwork cynet,
                      CyLayoutAlgorithm layout, Object layoutContext, Set<CyNode> allowed, String layoutField) // <-- these can be NULL
    {
        this.cynet = cynet;
        this.layout = layout;
        this.layoutContext = layoutContext;
        this.layoutField = layoutField;
        this.selectedNodes = allowed;

        // Only applicable to combined networks constructed from gedevo alignment result
        if(cyview != null && !isReady(cyview))
            throw new IllegalArgumentException("PairLayoutTask: Missing columns in node table! (Not a gedevo alignment result?)");
    }

    public CyNetwork getSourceNetwork()
    {
        return cynet;
    }

    public CyNetwork getResult()
    {
        return resultNet;
    }

    public CyNetworkView getResultView()
    {
        return resultView;
    }

    public Map<CyNode, double[]> getPositions()
    {
        return positions;
    }

    public Map<CyNode, CyNode[]> getMapToOriginalNodes()
    {
        return resultBackmap;
    }

    @Override
    public void runSafe(final TaskMonitor mon) throws Exception
    {
        if (undo != null && cyview != null)
        {
            GedevoApp.appInstance.log("PairLayoutTask: saving undo...");
            undo.postEdit(new LayoutEdit("Pair layout [" + (layout == null ? "no layout" : layout.toString()) + "]", cyview));
        }

        CyNetworkFactory nf = GedevoApp.appInstance.cynetfac;
        CyNetwork overlapNet = nf.createNetwork();

        Map<CyNode, CyNode[]> backmap = new HashMap<CyNode, CyNode[]>();
        OverlapNetworkBuilder.constructOverlapNetwork(cynet, overlapNet, backmap);

        resultNet = overlapNet;
        resultBackmap = backmap;

        CyEventHelper eventHelper = GedevoApp.appInstance.cyeventmgr;
        eventHelper.flushPayloadEvents();

        if(layout == null)
        {
            GedevoApp.appInstance.log("PairLayoutTask: exiting without layout");
            return;
        }

        CyNetworkViewFactory viewfac = GedevoApp.appInstance.cyreg.getService(CyNetworkViewFactory.class);
        CyNetworkView view = viewfac.createNetworkView(resultNet);
        eventHelper.flushPayloadEvents();

        Set<View<CyNode>> nviews = new HashSet<View<CyNode>>();
        Set<View<CyNode>> layoutViews = nviews;
        if(selectedNodes != null)
        {
            GedevoApp.appInstance.log("PairLayoutTask: Selected " + selectedNodes.size());
            for (CyNode n : overlapNet.getNodeList())
            {
                CyNode[] originalNodes = backmap.get(n);
                boolean add = false;
                for (CyNode original : originalNodes)
                    if (selectedNodes.contains(original))
                    {
                        add = true;
                        break;
                    }
                if (add)
                    nviews.add(view.getNodeView(n));
            }
        }
        else
        {
            for (CyNode n : overlapNet.getNodeList())
                nviews.add(view.getNodeView(n));
            layoutViews = CyLayoutAlgorithm.ALL_NODE_VIEWS;
        }

        eventHelper.flushPayloadEvents(); // make sure the views are created

        GedevoApp.appInstance.log("PairLayoutTask: " + nviews.size() + " views");

        // Set some initial positions based on source network views.
        // Some layout algorithms don't like all nodes being stuck to (0, 0).
        boolean allzero = true;
        if(cyview != null)
        {
            GedevoApp.appInstance.log("PairLayoutTask: Assign initial locations");
            for (View<CyNode> v : nviews)
            {
                CyNode[] back = backmap.get(v.getModel());
                if(back == null)
                    continue;
                View<CyNode> oldview = cyview.getNodeView(back[0]);
                double x = oldview.getVisualProperty(BasicVisualLexicon.NODE_X_LOCATION);
                double y = oldview.getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION);
                v.setVisualProperty(BasicVisualLexicon.NODE_X_LOCATION, x);
                v.setVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION, y);
                allzero = allzero && Math.abs(x) < 1.0 && Math.abs(y) < 1.0;
            }
        }

        if(allzero)
        {
            GedevoApp.appInstance.log("PairLayoutTask: No prev. view or all at (0, 0), using grid layout for initial positions");
            CyLayoutAlgorithm grid = GedevoApp.appInstance.cyreg.getService(CyLayoutAlgorithmManager.class).getLayout("grid");
            if(grid != null)
            {
                TaskIterator it = grid.createTaskIterator(view, grid.getDefaultLayoutContext(), layoutViews, null);
                while (it.hasNext())
                    it.next().run(mon);
            }
            else
            {
                GedevoApp.appInstance.log("PairLayoutTask: Grid layout not possible, good luck with this broken thing");
            }
        }

        // make sure the views have positions assigned (cytoscape, wtf is going on with you)
        view.updateView();
        eventHelper.flushPayloadEvents();
        view.fitContent();
        view.updateView();
        eventHelper.flushPayloadEvents();
        view.updateView();

        Object ctx = layoutContext == null ? layout.getDefaultLayoutContext() : layoutContext;
        final TaskIterator layoutTaskIt = layout.createTaskIterator(view, ctx, layoutViews, layoutField);

        // WARNING: The actual layout is prone to fail with an internal StackOverflowError.
        // The prefuse force directed layout (which is used as a default when doing the initial layout after the alignment)
        // uses a quadtree and recursive inserts to populate it. For some reason this does *NOT* fail even for much bigger networks
        // if the view is already there and the GUI TaskManager (the one with the progess bar) is used.
        // -- Which is the default if the layout algo is chosen from the menu.
        // I tried really hard to get this fixed, by using the grid layout beforehand, but to no avail.
        // I'll just leave this here in case someone stumbles upon this.
        // Note: The StackOverflowError is never visible, it's caught internally. This is why it's impossible to handle failure here.

        // HACK:
        // Use 32 MB stack to hopefully have enough stack space for big networks.
        // This does *NOT* fix the problem, and is not guaranteed to work at all, but it's worth a try.
        Thread th = new Thread(null, null, "PairLayoutTask", 32L * 1024L * 1024L)
        {
            @Override
            public void run()
            {
                try
                {
                    while (layoutTaskIt.hasNext())
                        layoutTaskIt.next().run(mon);
                }
                catch(Exception ex)
                {
                    GedevoUtil.msgboxAsync(GedevoUtil.getStackTrace(ex));
                }
            }
        };
        th.start();
        th.join();


        GedevoApp.appInstance.log("PairLayoutTask: layout performed");
        eventHelper.flushPayloadEvents(); // just make sure the positions snapped

        positions = new HashMap<CyNode, double[]>();
        for(View<CyNode> v : nviews)
        {
            double x = v.getVisualProperty(BasicVisualLexicon.NODE_X_LOCATION);
            double y = v.getVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION);
            positions.put(v.getModel(), new double[] {x, y});
        }

        resultView = view;
    }

}
