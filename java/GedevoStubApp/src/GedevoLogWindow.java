// This class is a BIG HACK to support getting SOME info about what's happening

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class GedevoLogWindow
{
    private JList list1;
    private JPanel panel1;
    private JScrollPane scrollpane;
    private DefaultListModel listmodel;

    private JFrame parentFrame;
    private JFrame win;

    public GedevoLogWindow(JFrame parent)
    {
        parentFrame = parent;
        createUIComponents();
    }

    private void createUIComponents()
    {
        listmodel = new DefaultListModel();
        list1.setModel(listmodel);
        list1.setLayoutOrientation(JList.VERTICAL);

        win = new JFrame("Log window");
        win.add(panel1, BorderLayout.CENTER);
        win.pack();

        // Somewhat hackish code to figure out where to place the debug window
        // Adapted from https://stackoverflow.com/questions/10123735/get-effective-screen-size-from-java
        GraphicsConfiguration gconf = parentFrame.getGraphicsConfiguration();
        Rectangle desktop = gconf.getBounds();
        desktop.x = desktop.width - 200;
        desktop.width = 200;
        Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(gconf);
        desktop.height -= (screenInsets.top + screenInsets.bottom);

        win.setBounds(desktop);
        win.addWindowListener(new WindowListener()
        {
            @Override
            public void windowClosed(WindowEvent e)
            {
                GedevoLogWindow.this.setVisible(false);
            }

            @Override public void windowOpened(WindowEvent e) {}
            @Override public void windowClosing(WindowEvent e) {}
            @Override public void windowIconified(WindowEvent e) {}
            @Override public void windowDeiconified(WindowEvent e) {}
            @Override public void windowActivated(WindowEvent e) {}
            @Override public void windowDeactivated(WindowEvent e) {}
        });
        setVisible(true);
    }

    public void setVisible(boolean b)
    {
        if(win != null)
            win.setVisible(b);
    }

    final Runnable autoscroll = new Runnable(){public void run(){
        scrollpane.getVerticalScrollBar().setValue(scrollpane.getVerticalScrollBar().getMaximum());
    }};

    public synchronized void log(final String s)
    {
        if(listmodel.size() > 1000)
            listmodel.remove(0);
        listmodel.addElement(s);

        SwingUtilities.invokeLater(autoscroll); // necessary, else it wont't scroll down completely
    }

    public void unload()
    {
        win.dispose();
        win = null;
    }
}
