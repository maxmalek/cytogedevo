#ifndef ORCALIB_GRAPHLETS_H
#define ORCALIB_GRAPHLETS_H

#include <algorithm>
#include "Matrix.h"

#ifndef _MSC_VER
# include <stdint.h>
#endif

namespace OrcaGraphletCounter
{
#if defined(_MSC_VER)
	typedef __int64 int64;
	typedef unsigned __int64 uint64;
#else
	typedef int64_t int64;
	typedef uint64_t uint64;
#endif

	typedef Matrix<uint64> GraphletMatrix;

	// Write into matrix, final size will be 73x(#nodes)
	void count5(const Matrix<unsigned char>& connMtx, GraphletMatrix& orbits);
}

#endif

