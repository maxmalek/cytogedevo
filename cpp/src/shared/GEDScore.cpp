#include "GEDScore.h"
#include "DataHolder.h"
#include "NodeMapping.h"


#define ML 1

// WARNING: The code in this file is a mess.
// Leaving the commented out stuff here because it may give a clue what's going on;
// as the code gets really nasty further down. But speed matters, right?

#ifdef INTERNAL_VERIFICATION
# ifdef _MSC_VER
#  pragma message("INTERNAL_VERIFICATION is on, calculating GED will be VERY slow!")
# else
#  warning "INTERNAL_VERIFICATION is on, calculating GED will be VERY slow!"
# endif
#endif

// for verification only
static void calcPairGED_ultraslow(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged)
{
	const uint n1 = my.nodeId[i];
	const Node *p1 = my.node[i];

	const uint n2 = ref.nodeId[i];
	const Node *p2 = ref.node[i];

	switch(!n1 + !n2)
	{
		// both pointers valid
		case 0:
		{
			const uint p1g = p1->group;
			const uint p2g = p2->group;
			const size_t sz = my.size();
			ASSERT(p1g != p2g);
			const uchar *row1 = data.GetConnectionsRow(p1g, n1);
			const uchar *col1 = data.GetConnectionsCol(p1g, n1);
			const uchar *row2 = data.GetConnectionsRow(p2g, n2);
			const uchar *col2 = data.GetConnectionsCol(p2g, n2);
			ASSERT(!row1[0]); // These are dummy entries and known to be 0.
			ASSERT(!row2[0]);
			ASSERT(!col1[0]);
			ASSERT(!col2[0]);
			(void)row1;
			(void)row2;
			(void)col1;
			(void)col2;

			if (p1->hasSelfLoop){
				if (p2->hasSelfLoop){
					ged[GEDI_SUBST_SELF_LOOP] +=ML; 
				}
				else{
					ged[GEDI_REMOVE_SELF_LOOP] +=ML;
				}
			} else if (p2->hasSelfLoop) {
				ged[GEDI_ADD_SELF_LOOP] +=ML;
			}


			for(size_t j = 0; j < sz; ++j)
			{
				const uint p1_to_my = my.node[j] && p1->hasEdgeTo(my.node[j]);
				const uint my_to_p1 = my.node[j] && my.node[j]->hasEdgeTo(p1);

				const uint p2_to_ref = ref.node[j] && p2->hasEdgeTo(ref.node[j]);
				const uint ref_to_p2 = ref.node[j] && ref.node[j]->hasEdgeTo(p2);

				// Be sure the data in the matrices are really what we expect.
				ASSERT(p1_to_my == col1[my.nodeId[j]]);
				ASSERT(my_to_p1 == row1[my.nodeId[j]]);
				ASSERT(p2_to_ref == col2[ref.nodeId[j]]);
				ASSERT(ref_to_p2 == row2[ref.nodeId[j]]);

				// for debugging
				uint eRemoved = 0, eAdded = 0, eFlipped = 0, eChangedDir = 0, eSubst = 0;

				//                      1               2                4                 8
				const uint mask = (p1_to_my) | (my_to_p1 << 1) | (p2_to_ref << 2) | (ref_to_p2 << 3);

				++ged[mask];

				switch(mask)
				{
					case 0: // no connection at all
						break;

					// directed edges (1) vs nothing -> edge removed
					case 1: // p1->my, nothing on the other side 
					case 2: // my->p1, nothing on the other side
						++eRemoved;
						break;

					// directed edges (2) vs nothing -> edge added
					case 4: // p2->ref, ...
					case 8: // ref->p2, ...
						++eAdded; // one directed edge removed
						break;

					// undirected edge (1) vs nothing -> edge removed
					case 3: // p1<->my, nothing on the other side
						++eRemoved;
						break;

					// undirected edge (2) vs nothing -> edge added
					case 12: // p2<->ref, nothing on the other side
						++eAdded;
						break;

					// same -> substituted
					case 5: // p1->my && p2->ref
					case 10:// my->p1 && ref->p2
					case 15:// my<->p1 && ref<->p2  (all bits set)
						++eSubst;
						break;

					// flipped direction
					case 6: // my->p1, p2->ref
					case 9: // p1->ref, ref->p2
						++eFlipped;
						break;

					// directed -> undirected or vice versa
					case 7: // p1<->my, p2->ref
					case 11:// p1<->my, ref->p2
					case 13:// p1->my, p2<->ref
					case 14:// my->p1, p2<->ref
						++eChangedDir;
						break;

					default:
						ASSERT(false);
						
				}

				bool dummy = false; // DEBUG: breakpoint
                (void)dummy;
			}
		}
		break;

		// one pointer valid, the other NULL
		case 1:
		{
			if(p1) // p2 is NULL -> removed node
			{
				++(ged[GEDI_REMOVE_NODE]);
				ged[GEDI_REMOVE_UNDIRECTED] += p1->nb.size(); // all edges gone // FIXME: not necessarily undirected
				ged[GEDI_REMOVE_SELF_LOOP] += ML*p1->hasSelfLoop;
			}
			else // p1 is NULL -> added node
			{
				++(ged[GEDI_ADD_NODE]);
				ged[GEDI_ADD_UNDIRECTED] += p2->nb.size(); // all edges gone // FIXME: not necessarily undirected
				ged[GEDI_ADD_SELF_LOOP] += ML*p2->hasSelfLoop;
			}
		}
		break;

		default: ; // both NULL, nothing to do
	}
}

// slower version but does not require any index
void calcPairGED(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& gedx)
{
	const uint n1 = my.nodeId[i];
	const Node *p1 = my.node[i];

	const uint n2 = ref.nodeId[i];
	const Node *p2 = ref.node[i];

	GEDInfo ged;

	switch(!n1 + !n2)
	{
		// both pointers valid
		case 0:
		{
			const uint p1g = p1->group;
			const uint p2g = p2->group;
			ASSERT(p1g != p2g);
			const uchar *row1 = data.GetConnectionsRow(p1g, n1);
			const uchar *col1 = data.GetConnectionsCol(p1g, n1);
			const uchar *row2 = data.GetConnectionsRow(p2g, n2);
			const uchar *col2 = data.GetConnectionsCol(p2g, n2);
			ASSERT(!row1[0]); // These are dummy entries and known to be 0.
			ASSERT(!row2[0]);
			ASSERT(!col1[0]);
			ASSERT(!col2[0]);

			// the max. reference node ID is the point where all the NULL entries start in the reference mapping.
			// And we want to run until exactly that point, for now.
			// j will always contain the ID that the corresponding reference node would have:
			// Remember, ref[0] has ID 1, and so on.
			const uint *myptr = &my.nodeId[0];
			const uint *refptr = &ref.nodeId[0];
			const uint *end = &my.nodeId[data.GetMaxReferenceNodeID()];

			for( ; myptr != end; ++myptr, ++refptr)
			{
				const uint myNodeId = *myptr;
				const uint refNodeId = *refptr;
				++ged[GED_MASK(col1[myNodeId], row1[myNodeId], col2[refNodeId], row2[refNodeId])];
			}

			end = &my.nodeId[my.size()-1] + 1; // one past the end

			// Keep chugging along...
			// Here, we know we are in the NULL area of ref[].
			// So we only have to look at the variable mapping, because we know it's mapped against NULL.
			for( ; myptr != end; ++myptr)
			{
				const uint nodeId = *myptr;
				++ged[GED_MASK(col1[nodeId], row1[nodeId], 0, 0)];
			}

			// Finally, self loops
			if (p1->hasSelfLoop){
				if (p2->hasSelfLoop){
					ged[GEDI_SUBST_SELF_LOOP] += ML;
				}
				else{
					ged[GEDI_REMOVE_SELF_LOOP] += ML;
				}
			} else if (p2->hasSelfLoop) {
				ged[GEDI_ADD_SELF_LOOP] += ML;
			}

		}
		break;

		// one pointer valid, the other NULL
		case 1:
		{
			if(p1) // p2 is NULL -> removed node
			{
				++(ged[GEDI_REMOVE_NODE]);
				ged[GEDI_REMOVE_UNDIRECTED] += p1->nb.size(); // all edges gone
				ged[GEDI_REMOVE_SELF_LOOP] += ML*p1->hasSelfLoop;
			}
			else // p1 is NULL -> added node
			{
				++(ged[GEDI_ADD_NODE]);
				ged[GEDI_ADD_UNDIRECTED] += p2->nb.size(); // all edges gone
				ged[GEDI_ADD_SELF_LOOP] += ML*p2->hasSelfLoop;
			}
		}
		break;

	default: ; // both NULL, nothing to do
	}

	VERIFY_BLOCK
	{
		GEDInfo ck;
		calcPairGED_ultraslow(data, my, ref, i, ck);
		ASSERT(ged == ck);
	}

	for(uint i = 0; i < GEDI_MAX; ++i)
		gedx[i] += ged[i];
}

inline void _fixDoubleCounts_subst(GEDInfo& ged)
{
	ASSERT( (ged[GEDI_SUBST_UNDIRECTED] & 1) == 0 );
	ASSERT( (ged[GEDI_SUBST_DIRECTED_1] & 1) == 0 );
	ASSERT( (ged[GEDI_SUBST_DIRECTED_2] & 1) == 0 );
	ASSERT( (ged[GEDI_FLIP_1] & 1) == 0 );
	ASSERT( (ged[GEDI_FLIP_2] & 1) == 0 );
	ASSERT( (ged[GEDI_UNDIRECTED_TO_DIRECTED_1] & 1) == 0 );
	ASSERT( (ged[GEDI_UNDIRECTED_TO_DIRECTED_2] & 1) == 0 );
	ASSERT( (ged[GEDI_DIRECTED_TO_UNDIRECTED_1] & 1) == 0 );
	ASSERT( (ged[GEDI_DIRECTED_TO_UNDIRECTED_2] & 1) == 0 );

	ged[GEDI_SUBST_UNDIRECTED] >>= 1;
	ged[GEDI_SUBST_DIRECTED_1] >>= 1;
	ged[GEDI_SUBST_DIRECTED_2] >>= 1;
	ged[GEDI_FLIP_1] >>= 1;
	ged[GEDI_FLIP_2] >>= 1;
	ged[GEDI_UNDIRECTED_TO_DIRECTED_1] >>= 1;
	ged[GEDI_UNDIRECTED_TO_DIRECTED_2] >>= 1;
	ged[GEDI_DIRECTED_TO_UNDIRECTED_1] >>= 1;
	ged[GEDI_DIRECTED_TO_UNDIRECTED_2] >>= 1;
}

static void calcPairGEDIndexed_directed(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& gedx, const uint *myindex, const uint *refindex)
{
	const uint n1 = my.nodeId[i];
	const Node *p1 = my.node[i];

	const uint n2 = ref.nodeId[i];
	const Node *p2 = ref.node[i];

	switch(!p1 + !p2)
	{
		// both pointers valid
		case 0:
		{
			const uint p1g = p1->group;
			const uint p2g = p2->group;
			ASSERT(p1g != p2g);
			ASSERT(p1->group != p2->group);
			const uchar *row1 = data.GetConnectionsRow(p1g, n1);
			const uchar *col1 = data.GetConnectionsCol(p1g, n1);
			const uchar *row2 = data.GetConnectionsRow(p2g, n2);
			const uchar *col2 = data.GetConnectionsCol(p2g, n2);
			ASSERT(!row1[0]); // These are dummy entries and known to be 0.
			ASSERT(!row2[0]);
			ASSERT(!col1[0]);
			ASSERT(!col2[0]);

			GEDInfo ged;
			const uint * const refId = &ref.nodeId[0];

			// p1 vs row2i
			// myindex[nodeId] == position in my[].
			// Then, we know the myindex at which the node is stored,
			// and can look up the node in ref[], at this myindex.
			if(p1->outgoingv.size())
			{
				const uint *v = &(p1->outgoingv.nodeId[0]);
				const uint *vend = v + p1->outgoingv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(col1[nodeId] == 1);
					const uint idx = refId[myindex[nodeId-1]];
					++ged[GED_MASK(1, row1[nodeId], col2[idx], row2[idx])];
				}
			}
			if(p1->incomingv.size())
			{
				const uint *v = &(p1->incomingv.nodeId[0]);
				const uint *vend = v + p1->incomingv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(row1[nodeId] == 1);
					const uint idx = refId[myindex[nodeId-1]];
					++ged[GED_MASK(col1[nodeId], 1, col2[idx], row2[idx])];
				}
			}
			if(p1->undirectedv.size())
			{
				const uint *v = &(p1->undirectedv.nodeId[0]);
				const uint *vend = v + p1->undirectedv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(col1[nodeId] == 1);
					ASSERT(row1[nodeId] == 1);
					const uint idx = refId[myindex[nodeId-1]];
					++ged[GED_MASK(1, 1, col2[idx], row2[idx])];
				}
			}

			// p2 vs row1i
			// -- don't need an myindex vector here.
			// we know that v->nodeId can't be 0, because that would mean
			// a NULL found it's way into the neighbors vector, which is impossible.
			// This figures out the myindex from the ref node ID (which is always it's ID - 1),
			// and looks at the node on that myindex in my[].

			const uint * const myId = &my.nodeId[0];

			if(p2->outgoingv.size())
			{
				const uint *v = &(p2->outgoingv.nodeId[0]);
				const uint *vend = v + p2->outgoingv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(col2[nodeId] == 1);
					const uint idx = myId[refindex[nodeId-1]];
					++ged[GED_MASK(col1[idx], row1[idx], 1, row2[nodeId])];
				}
			}
			if(p2->incomingv.size())
			{
				const uint *v = &(p2->incomingv.nodeId[0]);
				const uint *vend = v + p2->incomingv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(row2[nodeId] == 1);
					const uint idx = myId[refindex[nodeId-1]];
					++ged[GED_MASK(col1[idx], row1[idx], col2[nodeId], 1)];
				}
			}
			if(p2->undirectedv.size())
			{
				const uint *v = &(p2->undirectedv.nodeId[0]);
				const uint *vend = v + p2->undirectedv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(col2[nodeId] == 1);
					ASSERT(row2[nodeId] == 1);
					const uint idx = myId[refindex[nodeId-1]];
					++ged[GED_MASK(col1[idx], row1[idx], 1, 1)];
				}
			}

			// Finally, self loops
			if (p1->hasSelfLoop){
				if (p2->hasSelfLoop){
					ged[GEDI_SUBST_SELF_LOOP] += ML; 
				}
				else{
					ged[GEDI_REMOVE_SELF_LOOP] += ML;
				}
			} else if (p2->hasSelfLoop) {
				ged[GEDI_ADD_SELF_LOOP] += ML;
			}

			// Some edges were seen twice, fix this.
			_fixDoubleCounts_subst(ged);

			// --------

			VERIFY_BLOCK
			{
				GEDInfo ck;
				calcPairGED(data, my, ref, i, ck);
				ASSERT(ged == ck);
			}

			// Copy over values that were just calculated.
			// Accumulation needs to be done in an extra array because of
			// _fixDoubleCounts_subst(), see above.
			for(uint i = 0; i < GEDI_MAX; ++i)
				gedx[i] += ged[i];

		}
		break;

		// one pointer valid, the other NULL
		case 1:
		{
			if(p1) // p2 is NULL -> removed node
			{
				++(gedx[GEDI_REMOVE_NODE]);
				gedx[GEDI_REMOVE_UNDIRECTED] += p1->nb.size(); // all edges gone
				gedx[GEDI_REMOVE_SELF_LOOP] += ML*p1->hasSelfLoop;
			}
			else // p1 is NULL -> added node
			{
				++(gedx[GEDI_ADD_NODE]);
				gedx[GEDI_ADD_UNDIRECTED] += p2->nb.size(); // all edges gone
				gedx[GEDI_ADD_SELF_LOOP] += ML*p2->hasSelfLoop;
			}
		}
		break;

	default: ; // both NULL, nothing to do
	}
}

// slightly faster than function for directed graphs
static void calcPairGEDIndexed_undirected(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged, const uint *myindex, const uint *refindex)
{
	const Node *p1 = my.node[i];
	const Node *p2 = ref.node[i];

	switch(!p1 + !p2)
	{
		// both pointers valid
		case 0:
		{
			ASSERT(p1->group != p2->group);
			const uchar *row1 = data.GetConnectionsRow(p1->group, p1->id);
			ASSERT(!row1[0]); // These are dummy entries and known to be 0 (not connected).
			const uchar *row2 = data.GetConnectionsCol(p2->group, p2->id);
			ASSERT(!row2[0]); // These are dummy entries and known to be 0.

			// undirected, means: rowX[i] == colX[i]

			// p1 vs row2i
			// myindex[nodeId] == position in my[].
			// Then, we know the myindex at which the node is stored,
			// and can look up the node in ref[], at this myindex.
			//uint er = 0; // edges removed
			uint es = 0; // edges substituted
			
			
			
			ged[GEDI_REMOVE_SELF_LOOP]+= ML*(  p1->hasSelfLoop  && (!p2->hasSelfLoop));
			ged[GEDI_ADD_SELF_LOOP]   += ML*(!(p1->hasSelfLoop) &&   p2->hasSelfLoop );
			ged[GEDI_SUBST_SELF_LOOP] += ML*(  p1->hasSelfLoop  &&   p2->hasSelfLoop );

			if(uint p1sz = (uint)p1->allv.size())
			{
				const uint * const refId = &ref.nodeId[0];
				const uint *v = &(p1->allv.nodeId[0]);
				const uint * const vend = v + p1sz;
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(row1[nodeId] == 1); // Connected means this is 1. And it must be connected.
					//const uint idx = ref[myindex[v->nodeId-1]].nodeId;
					//const uint v = row2[idx];
					//es += v;
					//er += (v ^ 1);

					es += row2[refId[myindex[nodeId-1]]];
				}
				ged[GEDI_REMOVE_UNDIRECTED] += (p1sz - es);
				//ged[GEDI_REMOVE_UNDIRECTED] += er;
				ged[GEDI_SUBST_UNDIRECTED] += es;
			}

			// p2 vs row1i
			// -- don't need an myindex vector here.
			// we know that v->nodeId can't be 0, because that would mean
			// a NULL found it's way into the neighbors vector, which is impossible.
			// This figures out the myindex from the ref node ID (which is always it's ID - 1),
			// and looks at the node on that myindex in my[].
			uint ea = 0; // edges added
			
			if(p2->allv.size())
			{
				const uint * const myId = &my.nodeId[0];
				const uint *v = &(p2->allv.nodeId[0]);
				const uint * const vend = v + p2->allv.size();
				for( ; v != vend; ++v)
				{
					const uint nodeId = *v;
					ASSERT(row2[nodeId] == 1); // 1 == connected, which is known to be so.
					ea += 1 ^ row1[myId[refindex[nodeId-1]]];
				}
				ged[GEDI_ADD_UNDIRECTED] += ea;
			}

			// --------

			VERIFY_BLOCK
			{
				GEDInfo ck, ck2;
				ck[GEDI_ADD_UNDIRECTED] = ea;
				ck[GEDI_REMOVE_UNDIRECTED] = (p1->allv.size() - es);
				ck[GEDI_SUBST_UNDIRECTED] = es;
				ck[GEDI_REMOVE_SELF_LOOP]= ML*(  p1->hasSelfLoop  && (!p2->hasSelfLoop));
				ck[GEDI_ADD_SELF_LOOP]   = ML*(!(p1->hasSelfLoop) &&   p2->hasSelfLoop );
				ck[GEDI_SUBST_SELF_LOOP] = ML*(  p1->hasSelfLoop  &&   p2->hasSelfLoop );
				calcPairGEDIndexed_directed(data, my, ref, i, ck2, myindex, refindex); // does more internal verification
				ASSERT(ck == ck2);
			}

		}
		break;

		// one pointer valid, the other NULL
		case 1:
		{
			if(p1) // p2 is NULL -> removed node
			{
				++(ged[GEDI_REMOVE_NODE]);
				ged[GEDI_REMOVE_UNDIRECTED] += p1->nb.size(); // all edges gone
				ged[GEDI_REMOVE_SELF_LOOP] += ML*p1->hasSelfLoop;
			}
			else // p1 is NULL -> added node
			{
				++(ged[GEDI_ADD_NODE]);
				ged[GEDI_ADD_UNDIRECTED] += p2->nb.size(); // all edges gone
				ged[GEDI_ADD_SELF_LOOP] += ML*p2->hasSelfLoop;
			}
		}
		break;

		default: ; // both NULL, nothing to do
	}
}


void GEDPopulateIndex(const NodeMapping& my, uint *index)
{
	for(size_t i = 0; i < my.size(); ++i)
	{
		const uint id = my.nodeId[i];
		ASSERT(id < my.size()); // This check is very crude and only prevents against values that are really far off.
		if(id)
			index[id-1] = i;
	}
}

void calcWholeGED(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, GEDInfo& ged)
{
	size_t sz = my.size();
	for(size_t i = 0; i < sz; ++i)
		calcPairGED(data, my, ref, i, ged);
}


// very fast, but needs index
void calcPairGEDIndexed(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, uint i, GEDInfo& ged, const uint *index)
{
	if(data.HasDirectedEdges())
	{
		calcPairGEDIndexed_directed(data, my, ref, i, ged, index, data.GetRefMappingIndex());
	}
	else
	{
		calcPairGEDIndexed_undirected(data, my, ref, i, ged, index, data.GetRefMappingIndex());

		VERIFY_BLOCK
		{
			GEDInfo ck;
			calcPairGEDIndexed_directed(data, my, ref, i, ck, index, data.GetRefMappingIndex());
			ASSERT(ck == ged);
		}
	}
}

void calcWholeGEDIndexed(const DataHolder& data, const NodeMapping& my, const NodeMapping& ref, GEDInfo& ged, const uint *index)
{
	size_t sz = my.size();
	if(data.HasDirectedEdges())
	{
		for(size_t i = 0; i < sz; ++i)
			calcPairGEDIndexed_directed(data, my, ref, i, ged, index, data.GetRefMappingIndex());
	}
	else
	{
		for(size_t i = 0; i < sz; ++i)
			calcPairGEDIndexed_undirected(data, my, ref, i, ged, index, data.GetRefMappingIndex());

		VERIFY_BLOCK
		{
			GEDInfo ck;
			for(size_t i = 0; i < sz; ++i)
				calcPairGEDIndexed_directed(data, my, ref, i, ck, index, data.GetRefMappingIndex());
			ASSERT(ck == ged);
		}
	}

	VERIFY_BLOCK
	{
		// (will fail if passed GED isn't clear, but we can safely assume that)
		GEDInfo ck;
		calcWholeGED(data, my, ref, ck);
		ASSERT(ged == ck);
	}
}

#undef ML
