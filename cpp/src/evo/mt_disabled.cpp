#include <stdlib.h>

// external interface
bool InitMT(void*)
{
    puts("!! GEDEVO was compiled without threading support. Single-threaded mode is SLOW, be warned!");
    return false;
}

void QuitMT()
{
}
