package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.AlignmentInfo;
import org.cytoscape.gedevo.GedevoAlignmentCache;
import org.cytoscape.gedevo.GedevoNative;
import org.cytoscape.gedevo.UserSettings;
import org.cytoscape.gedevo.simplenet.Graph;
import org.cytoscape.model.CyNetwork;

// stores common values shared by all alignment tasks.
// one instance is shared between a consecuive chain of alignment tasks
public final class AlignmentTaskData
{
    public AlignmentTaskData(UserSettings settings)
    {
        this.settings = settings;
    }

    public UserSettings settings;
    public CyNetwork cynet; // combined network
    public Graph[] graphs; // simple graphs constructed from combined network (invalidated when cynet is changed)
    public GedevoNative.Instance instance; // native instance constructed from graphs, settings, and input data. Set to null when no longer valid
    public final AlignmentInfo info = new AlignmentInfo();

    public void networkChanged()
    {
        graphs = null;
        instance = null;
        GedevoAlignmentCache.removeForNetwork(cynet);
    }
}
