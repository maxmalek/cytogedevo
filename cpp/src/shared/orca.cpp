/* 
Orca (Orbit Counting Algorithm) - A combinatorial approach to graphlet counting
Copyright (C) 2013  Tomaz Hocevar <tomaz.hocevar@fri.uni-lj.si>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

/* Modified to serve as a library */


// Define this to use STL hashmaps intead of the quick-and-dirty custom implementation below.
//#define USE_STL_HASHMAP

#include "orca.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <iostream>
#include <vector>

#ifdef USE_STL_HASHMAP
// some attempts to not require C++11...
#  if __cplusplus >= 201103L
#    include <unordered_map>
#    define HASH_MAP std::unordered_map
#  elif _MSC_VER
#    include <unordered_map>
#    define HASH_MAP std::tr1::unordered_map
#  else // untested!
#    include <tr1/unordered_map>
#    define HASH_MAP std::tr1::unordered_map
#  endif
#else
// below follows a minimal hash map implementation that only provides the functionality needed by orca.
// NOT STL compatible! (But does also not use a linked list for buckets like the STL one, which is a sort of braindead thing to do)

// (http://graphics.stanford.edu/~seander/bithacks.html)
inline size_t nextPowerOf2(size_t v)
{
	v--;
	v |= v >> 1;
	v |= v >> 2;
	v |= v >> 4;
	v |= v >> 8;
	v |= v >> 16;
	v++;
	v += (v == 0);
	return v;
}

template<typename KEY> struct hash_cast
{
	inline size_t operator()(const KEY& t) const
	{
		return (size_t)t;
	}
	inline size_t operator()(const KEY *t) const
	{
		return (size_t)*t;
	}
};

template<typename KEY, typename T> struct equal
{
	inline bool operator()(const KEY& a, const KEY& b, size_t /*hash*/, const T& /*value*/) const
	{
		return a == b;
	}
};

template
<
typename KEY,
typename T,
typename HASH,
typename CMP,
typename BucketType
>
class _MicroHashMapBase
{
protected:

	// ---- Main class start ----

	_MicroHashMapBase(size_t buckets, int loadFactor, const CMP& c, const HASH& h)
		: v(nextPowerOf2(buckets)), _size(0), _loadFactor(loadFactor), hash(h), cmp(c)
	{}

	_MicroHashMapBase(const _MicroHashMapBase& o)
		: v(o.v), _size(o._size), _loadFactor(o._loadFactor), hash(o.hash), cmp(o.cmp)
	{}

public:

	_MicroHashMapBase& operator=(const _MicroHashMapBase& o);

	T find0(const KEY& k)
	{
		const size_t h = hash(k);
		const size_t i = h & (v.size()-1); // assume power of 2
		BucketType& b = v[i];
		for(typename BucketType::iterator it = b.begin(); it != b.end(); ++it)
			if(cmp(k, it->first, h, it->second))
				return it->second;

		return T(0); // orca special: 0 when not found
	}

	T& operator[] (const KEY& k)
	{
		const size_t h = hash(k);
		size_t i = h & (v.size()-1); // assume power of 2
		{
			BucketType& b = v[i];
			for(typename BucketType::iterator it = b.begin(); it != b.end(); ++it)
				if(cmp(k, it->first, h, it->second))
					return it->second;
		}
		++_size;
		if(_enlargeIfNecessary())
			i = h & (v.size()-1);
		v[i].push_back(std::make_pair(k, T()));
		return v[i].back().second;
	}

	inline size_t size() const
	{
		return _size;
	}

private:

	inline bool _enlargeIfNecessary()
	{
		if(_size > v.size() * _loadFactor)
		{
			_enlarge();
			return true;
		}
		return false;
	}

	void _enlarge()
	{
		const size_t oldsize = v.size();
		const size_t andval = (oldsize * 2) - 1;
		v.resize(oldsize * 2);
		BucketType cp;
		for(size_t i = 0; i < oldsize; ++i)
		{
			cp.clear();
			std::swap(cp, v[i]); // use efficient swap
			// v[i] is now empty
			// this way can possibly copy elements 2 times, but means less container copying and memory stress overall
			for(typename BucketType::iterator it = cp.begin(); it != cp.end(); ++it)
				v[hash(it->first) & andval].push_back(*it); // assume power of 2
		}
	}

	std::vector<BucketType> v;
	size_t _size;
	unsigned _loadFactor;

	HASH hash; // hash functor
	CMP cmp;   // compare functor
};

template
<
typename KEY,
typename T,
typename HASH = hash_cast<KEY>,
typename CMP = equal<KEY, T>,
typename BucketType = std::vector<std::pair<KEY,T> >
>
class MicroHashMap : public _MicroHashMapBase<KEY, T, HASH, CMP, BucketType>
{
public:
	MicroHashMap(size_t buckets = 16, int loadFactor = 5)
		: _MicroHashMapBase<KEY, T, HASH, CMP, BucketType>(buckets, loadFactor, CMP(), HASH())
	{
	}
};

#define HASH_MAP MicroHashMap
#endif // not USE_STL_HASHMAP


namespace OrcaGraphletCounter {

typedef std::pair<int,int> PII;

struct PAIR
{
	unsigned a, b;
	inline PAIR(unsigned a0, unsigned b0)
		: a(std::min(a0,b0))
		, b(std::max(a0,b0))
	{}
};
inline bool operator<(const PAIR &x, const PAIR &y)
{
	return x.a < y.a || (x.a == y.a && x.b < y.b);
}
inline bool operator==(const PAIR &x, const PAIR &y)
{
	return x.a == y.a && x.b == y.b;
}
struct hash_PAIR
{
	inline size_t operator()(const PAIR &x) const
	{
		return (x.a<<8) ^ (x.b<<0);
	}
};

struct TRIPLE
{
	unsigned a, b, c;
	inline TRIPLE(unsigned a0, unsigned b0, unsigned c0)
	{
		if (a0 > b0) std::swap(a0, b0);
		if (b0 > c0) std::swap(b0, c0);
		if (a0 > b0) std::swap(a0, b0);
		a = a0;
		b = b0;
		c = c0;
	}
};
inline bool operator<(const TRIPLE &x, const TRIPLE &y)
{
	return x.a < y.a || (x.a == y.a && (x.b < x.b || (x.b == y.b && x.c < y.c)));
}
inline bool operator==(const TRIPLE &x, const TRIPLE &y)
{
	return x.a==y.a && x.b==y.b && x.c==y.c;
}
struct hash_TRIPLE
{
	inline size_t operator()(const TRIPLE &x) const
	{
		return (x.a<<16) ^ (x.b<<8) ^ (x.c<<0);
	}
};


void count5(const Matrix<unsigned char>& connMtx, GraphletMatrix& orbit)
{
	// ------ INIT PART ---------

	FastMatrix<unsigned char> adjacent;
	adjacent.load(connMtx);
	const size_t nNodes = connMtx.height();

	// kill self-loops
	for(size_t i = 0; i < nNodes; ++i)
		adjacent(i, i) = 0;

	// transform possibly directed into definitely undirected adjacency matrix
	size_t nEdges = 0;
	for(size_t y = 0; y < nNodes; ++y)
	{
		unsigned char *row = adjacent.getRow(y);
		for(size_t x = y; x < nNodes; ++x)
		{
			unsigned char hasEdge = row[x] | adjacent(y, x); // one direction is enough to count as undirected
			adjacent(y, x) = row[x] = hasEdge; // make directed edges undirected
			nEdges += hasEdge;
		}
	}

	std::vector<unsigned> nodeDegrees(nNodes, 0);
	std::vector<PAIR> edges; // list of edges
	edges.reserve(nEdges);

	std::vector<std::vector<unsigned> > adj(nNodes); // adjX - adjacency list of node x
	std::vector<std::vector<PII> > inc(nNodes); // incX - incidence list of node x: (y, edge id)

	// create edge list; figure out node degrees; prealloc adjacency & incidence lists
	for(size_t y = 0; y < nNodes; ++y)
	{
		unsigned char *row = adjacent.getRow(y);
		size_t deg = 0;
		for(size_t x = 0; x < nNodes; ++x)
		{
			if(row[x] && x < y) // self loops do not exist, so < instead of <= is safe. Matrix is symmetric, too
				edges.push_back(PAIR(x, y));
			deg += row[x];
		}
		nodeDegrees[y] = deg;
		inc[y].resize(deg);
		adj[y].resize(deg);
	}

	// set up adjacency, incidence lists
	{
		std::vector<size_t> d(nNodes, 0);
		for (size_t i = 0; i < nEdges; ++i)
		{
			const size_t a = edges[i].a;
			const size_t b = edges[i].b;
			const size_t da = d[a]++;
			const size_t db = d[b]++;

			adj[a][da] = b;
			adj[b][db] = a;
			inc[a][da] = PII(b,i);
			inc[b][db] = PII(a,i);
		}
		for (size_t i = 0; i < nNodes; ++i)
		{
			std::sort(adj[i].begin(), adj[i].end());
			std::sort(inc[i].begin(), inc[i].end());
		}
	}

	// orbit(o, x) - how many times does node x participate in orbit o
	orbit.resize(73, nNodes);
	orbit.fill(0);

	// ------ BEGIN MAIN PART -----------------------------------


	HASH_MAP<PAIR, int, hash_PAIR> common2;
	HASH_MAP<TRIPLE, int, hash_TRIPLE> common3;

#ifdef USE_STL_HASHMAP
	HASH_MAP<PAIR, int, hash_PAIR>::iterator common2_it;
	HASH_MAP<TRIPLE, int, hash_TRIPLE>::iterator common3_it;
#  define common3_get(x) (((common3_it=common3.find(x))!=common3.end())?(common3_it->second):0)
#  define common2_get(x) (((common2_it=common2.find(x))!=common2.end())?(common2_it->second):0)
#else
#  define common3_get(x) (common3.find0(x))
#  define common2_get(x) (common2.find0(x))
#endif

	// precompute common nodes
	for (size_t x = 0; x < nNodes; ++x)
	{
		const unsigned deg = nodeDegrees[x];
		const std::vector<unsigned>& adjX = adj[x];

		for (unsigned n1 = 0 ; n1 < deg; ++n1)
		{
			const unsigned a = adjX[n1];
			for (unsigned n2 = n1+1; n2 < deg; ++n2)
			{
				const unsigned b = adjX[n2];
				const unsigned adjacentAB = adjacent(a,b);
				common2[PAIR(a,b)]++;
				for (unsigned n3 = n2+1; n3 < deg; ++n3)
				{
					const unsigned c = adjX[n3];
					const unsigned st = adjacentAB + adjacent(a,c) + adjacent(b,c);
					if (st < 2)
						continue;
					common3[TRIPLE(a,b,c)]++;
				}
			}
		}
	}

	// precompute triangles that span over edges
	std::vector<unsigned> tri(nEdges, 0);
	for (size_t i = 0; i < nEdges; i++)
	{
		const unsigned x = edges[i].a;
		const unsigned y = edges[i].b;
		const unsigned degX = nodeDegrees[x];
		const unsigned degY = nodeDegrees[y];
		unsigned xi = 0;
		unsigned yi = 0;
		while(xi < degX && yi < degY)
		{
			const unsigned adjxxi = adj[x][xi];
			const unsigned adjyyi = adj[y][yi];
			if (adjxxi == adjyyi)
			{
				++tri[i];
				++xi;
				++yi;
			}
			else if (adjxxi < adjyyi)
				++xi;
			else
				++yi;
		}
	}

	// Clean up memory no longer required at this point
	std::vector<PAIR>().swap(edges);

	// count full graphlets
	std::vector<uint64> C5(nNodes);
	{
		std::vector<unsigned> neigh(nNodes);
		std::vector<unsigned> neigh2(nNodes);

		for (unsigned x = 0; x < unsigned(nNodes); ++x)
		{
			for (unsigned nx = 0; nx < nodeDegrees[x]; ++nx)
			{
				const unsigned y = adj[x][nx];
				if (y >= x)
					break;
				unsigned nn = 0;
				for (unsigned ny = 0; ny < nodeDegrees[y]; ++ny)
				{
					const unsigned z = adj[y][ny];
					if (z >= y)
						break;
					if (adjacent(x, z))
						neigh[nn++] = z;
				}
				for (unsigned i = 0; i < nn; ++i)
				{
					const unsigned z = neigh[i];
					unsigned nn2 = 0;
					for (unsigned j = i+1; j < nn; ++j)
					{
						const unsigned zz = neigh[j];
						if (adjacent(z, zz))
							neigh2[nn2++] = zz;
					}
					for (unsigned i2 = 0; i2 < nn2; i2++)
					{
						const unsigned zz = neigh2[i2];
						for (unsigned j2 = i2+1; j2 < nn2; j2++)
						{
							const unsigned zzz = neigh2[j2];
							if (adjacent(zz,zzz))
							{
								++C5[x];
								++C5[y];
								++C5[z];
								++C5[zz];
								++C5[zzz];
							}
						}
					}
				}
			}
		}
	}

	std::vector<unsigned> common_x(nNodes, 0);
	std::vector<unsigned> common_x_list(nNodes, 0);
	std::vector<unsigned> common_a(nNodes, 0);
	std::vector<unsigned> common_a_list(nNodes, 0);
	unsigned ncx = 0;
	unsigned nca = 0;

	// set up a system of equations relating orbit counts
	for (size_t x = 0; x < nNodes; ++x)
	{
		uint64 * const orbitX = &orbit(0, x);
		const std::vector<unsigned>& adjX = adj[x];
		const std::vector<PII>& incX = inc[x];
		const unsigned degX = nodeDegrees[x];

		for (unsigned i = 0; i < ncx; ++i)
			common_x[common_x_list[i]] = 0;
		ncx = 0;

		// smaller graphlets
		orbitX[0] = degX;
		for (unsigned int nx1 = 0; nx1 < degX; ++nx1)
		{
			const unsigned a = adjX[nx1];
			for (unsigned nx2 = nx1+1; nx2 < degX; nx2++)
			{
				const unsigned b = adjX[nx2];
				if (adjacent(a, b))
					++orbitX[3];
				else
					++orbitX[2];
			}
			for (unsigned na = 0;na < nodeDegrees[a]; ++na)
			{
				const unsigned b = adj[a][na];
				if (b != x && !adjacent(x,b))
				{
					++orbitX[1];
					if (common_x[b] == 0)
						common_x_list[ncx++] = b;
					common_x[b]++;
				}
			}
		}

		uint64 f_71=0, f_70=0, f_67=0, f_66=0, f_58=0, f_57=0; // 14
		uint64 f_69=0, f_68=0, f_64=0, f_61=0, f_60=0, f_55=0, f_48=0, f_42=0, f_41=0; // 13
		uint64 f_65=0, f_63=0, f_59=0, f_54=0, f_47=0, f_46=0, f_40=0; // 12
		uint64 f_62=0, f_53=0, f_51=0, f_50=0, f_49=0, f_38=0, f_37=0, f_36=0; // 8
		uint64 f_44=0, f_33=0, f_30=0, f_26=0; // 11
		uint64 f_52=0, f_43=0, f_32=0, f_29=0, f_25=0; // 10
		uint64 f_56=0, f_45=0, f_39=0, f_31=0, f_28=0, f_24=0; // 9
		uint64 f_35=0, f_34=0, f_27=0, f_18=0, f_16=0, f_15=0; // 4
		uint64 f_17=0; // 5
		uint64 f_22=0, f_20=0, f_19=0; // 6
		uint64 f_23=0, f_21=0; // 7

		for (unsigned nx1=0;nx1<degX;nx1++)
		{
			const unsigned a=incX[nx1].first, xa=incX[nx1].second;

			for (unsigned i=0;i<nca;i++) common_a[common_a_list[i]]=0;
			nca=0;
			for (unsigned na=0;na<nodeDegrees[a];na++) {
				const unsigned b=adj[a][na];
				for (unsigned nb=0;nb<nodeDegrees[b];nb++) {
					const unsigned c=adj[b][nb];
					if (c==a || adjacent(a,c)) continue;
					if (common_a[c]==0) common_a_list[nca++]=c;
					common_a[c]++;
				}
			}

			// x = orbit-14 (tetrahedron)
			for (unsigned nx2=nx1+1;nx2<degX;nx2++) {
				const unsigned b=incX[nx2].first, xb=incX[nx2].second;
				if (!adjacent(a,b)) continue;
				for (unsigned nx3=nx2+1;nx3<degX;nx3++) {
					const unsigned c=incX[nx3].first, xc=incX[nx3].second;
					if (!adjacent(a,c) || !adjacent(b,c)) continue;
					orbitX[14]++;
					f_70 += common3_get(TRIPLE(a,b,c))-1;
					f_71 += (tri[xa]>2 && tri[xb]>2)?(common3_get(TRIPLE(x,a,b))-1):0;
					f_71 += (tri[xa]>2 && tri[xc]>2)?(common3_get(TRIPLE(x,a,c))-1):0;
					f_71 += (tri[xb]>2 && tri[xc]>2)?(common3_get(TRIPLE(x,b,c))-1):0;
					f_67 += tri[xa]-2+tri[xb]-2+tri[xc]-2;
					f_66 += common2_get(PAIR(a,b))-2;
					f_66 += common2_get(PAIR(a,c))-2;
					f_66 += common2_get(PAIR(b,c))-2;
					f_58 += degX-3;
					f_57 += nodeDegrees[a]-3+nodeDegrees[b]-3+nodeDegrees[c]-3;
				}
			}

			// x = orbit-13 (diamond)
			for (unsigned nx2=0;nx2<degX;nx2++) {
				const unsigned b=incX[nx2].first, xb=incX[nx2].second;
				if (!adjacent(a,b)) continue;
				for (unsigned nx3=nx2+1;nx3<degX;nx3++) {
					const unsigned c=incX[nx3].first, xc=incX[nx3].second;
					if (!adjacent(a,c) || adjacent(b,c)) continue;
					orbitX[13]++;
					f_69 += (tri[xb]>1 && tri[xc]>1)?(common3_get(TRIPLE(x,b,c))-1):0;
					f_68 += common3_get(TRIPLE(a,b,c))-1;
					f_64 += common2_get(PAIR(b,c))-2;
					f_61 += tri[xb]-1+tri[xc]-1;
					f_60 += common2_get(PAIR(a,b))-1;
					f_60 += common2_get(PAIR(a,c))-1;
					f_55 += tri[xa]-2;
					f_48 += nodeDegrees[b]-2+nodeDegrees[c]-2;
					f_42 += degX-3;
					f_41 += nodeDegrees[a]-3;
				}
			}

			// x = orbit-12 (diamond)
			for (unsigned nx2=nx1+1;nx2<degX;nx2++) {
				const unsigned b=incX[nx2].first;
				if (!adjacent(a,b)) continue;
				for (unsigned na=0;na<nodeDegrees[a];na++) {
					const unsigned c=inc[a][na].first, ac=inc[a][na].second;
					if (c==x || adjacent(x,c) || !adjacent(b,c)) continue;
					orbitX[12]++;
					f_65 += (tri[ac]>1)?common3_get(TRIPLE(a,b,c)):0;
					f_63 += common_x[c]-2;
					f_59 += tri[ac]-1+common2_get(PAIR(b,c))-1;
					f_54 += common2_get(PAIR(a,b))-2;
					f_47 += degX-2;
					f_46 += nodeDegrees[c]-2;
					f_40 += nodeDegrees[a]-3+nodeDegrees[b]-3;
				}
			}

			// x = orbit-8 (cycle)
			for (unsigned nx2=nx1+1;nx2<degX;nx2++) {
				const unsigned b=incX[nx2].first, xb=incX[nx2].second;
				if (adjacent(a,b)) continue;
				for (unsigned na=0;na<nodeDegrees[a];na++) {
					const unsigned c=inc[a][na].first, ac=inc[a][na].second;
					if (c==x || adjacent(x,c) || !adjacent(b,c)) continue;
					orbitX[8]++;
					f_62 += (tri[ac]>0)?common3_get(TRIPLE(a,b,c)):0;
					f_53 += tri[xa]+tri[xb];
					f_51 += tri[ac]+common2_get(PAIR(c,b));
					f_50 += common_x[c]-2;
					f_49 += common_a[b]-2;
					f_38 += degX-2;
					f_37 += nodeDegrees[a]-2+nodeDegrees[b]-2;
					f_36 += nodeDegrees[c]-2;
				}
			}

			// x = orbit-11 (paw)
			for (unsigned nx2=nx1+1;nx2<degX;nx2++) {
				const unsigned b=incX[nx2].first;
				if (!adjacent(a,b)) continue;
				for (unsigned nx3=0;nx3<degX;nx3++) {
					const unsigned c=incX[nx3].first, xc=incX[nx3].second;
					if (c==a || c==b || adjacent(a,c) || adjacent(b,c)) continue;
					orbitX[11]++;
					f_44 += tri[xc];
					f_33 += degX-3;
					f_30 += nodeDegrees[c]-1;
					f_26 += nodeDegrees[a]-2+nodeDegrees[b]-2;
				}
			}

			// x = orbit-10 (paw)
			for (unsigned nx2=0;nx2<degX;nx2++) {
				const unsigned b=incX[nx2].first;
				if (!adjacent(a,b)) continue;
				for (unsigned nb=0;nb<nodeDegrees[b];nb++) {
					const unsigned c=inc[b][nb].first, bc=inc[b][nb].second;
					if (c==x || c==a || adjacent(a,c) || adjacent(x,c)) continue;
					orbitX[10]++;
					f_52 += common_a[c]-1;
					f_43 += tri[bc];
					f_32 += nodeDegrees[b]-3;
					f_29 += nodeDegrees[c]-1;
					f_25 += nodeDegrees[a]-2;
				}
			}

			// x = orbit-9 (paw)
			for (unsigned na1=0;na1<nodeDegrees[a];na1++) {
				const unsigned b=inc[a][na1].first, ab=inc[a][na1].second;
				if (b==x || adjacent(x,b)) continue;
				for (unsigned na2=na1+1;na2<nodeDegrees[a];na2++) {
					const unsigned c=inc[a][na2].first, ac=inc[a][na2].second;
					if (c==x || !adjacent(b,c) || adjacent(x,c)) continue;
					orbitX[9]++;
					f_56 += (tri[ab]>1 && tri[ac]>1)?common3_get(TRIPLE(a,b,c)):0;
					f_45 += common2_get(PAIR(b,c))-1;
					f_39 += tri[ab]-1+tri[ac]-1;
					f_31 += nodeDegrees[a]-3;
					f_28 += degX-1;
					f_24 += nodeDegrees[b]-2+nodeDegrees[c]-2;
				}
			}

			// x = orbit-4 (path)
			for (unsigned na=0;na<nodeDegrees[a];na++) {
				const unsigned b=inc[a][na].first;
				if (b==x || adjacent(x,b)) continue;
				for (unsigned nb=0;nb<nodeDegrees[b];nb++) {
					const unsigned c=inc[b][nb].first, bc=inc[b][nb].second;
					if (c==a || adjacent(a,c) || adjacent(x,c)) continue;
					orbitX[4]++;
					f_35 += common_a[c]-1;
					f_34 += common_x[c];
					f_27 += tri[bc];
					f_18 += nodeDegrees[b]-2;
					f_16 += degX-1;
					f_15 += nodeDegrees[c]-1;
				}
			}

			// x = orbit-5 (path)
			for (unsigned nx2=0;nx2<degX;nx2++) {
				const unsigned b=incX[nx2].first;
				if (b==a || adjacent(a,b)) continue;
				for (unsigned nb=0;nb<nodeDegrees[b];nb++) {
					const unsigned c=inc[b][nb].first;
					if (c==x || adjacent(a,c) || adjacent(x,c)) continue;
					orbitX[5]++;
					f_17 += nodeDegrees[a]-1;
				}
			}

			// x = orbit-6 (claw)
			for (unsigned na1=0;na1<nodeDegrees[a];na1++) {
				const unsigned b=inc[a][na1].first;
				if (b==x || adjacent(x,b)) continue;
				for (unsigned na2=na1+1;na2<nodeDegrees[a];na2++) {
					const unsigned c=inc[a][na2].first;
					if (c==x || adjacent(x,c) || adjacent(b,c)) continue;
					orbitX[6]++;
					f_22 += nodeDegrees[a]-3;
					f_20 += degX-1;
					f_19 += nodeDegrees[b]-1+nodeDegrees[c]-1;
				}
			}

			// x = orbit-7 (claw)
			for (unsigned nx2=nx1+1;nx2<degX;nx2++) {
				const unsigned b=incX[nx2].first;
				if (adjacent(a,b)) continue;
				for (unsigned nx3=nx2+1;nx3<degX;nx3++) {
					const unsigned c=incX[nx3].first;
					if (adjacent(a,c) || adjacent(b,c)) continue;
					orbitX[7]++;
					f_23 += degX-3;
					f_21 += nodeDegrees[a]-1+nodeDegrees[b]-1+nodeDegrees[c]-1;
				}
			}
		}

		// solve equations
		orbitX[72] = C5[x];
		orbitX[71] = (f_71-12*orbitX[72])/2;
		orbitX[70] = (f_70-4*orbitX[72]);
		orbitX[69] = (f_69-2*orbitX[71])/4;
		orbitX[68] = (f_68-2*orbitX[71]);
		orbitX[67] = (f_67-12*orbitX[72]-4*orbitX[71]);
		orbitX[66] = (f_66-12*orbitX[72]-2*orbitX[71]-3*orbitX[70]);
		orbitX[65] = (f_65-3*orbitX[70])/2;
		orbitX[64] = (f_64-2*orbitX[71]-4*orbitX[69]-1*orbitX[68]);
		orbitX[63] = (f_63-3*orbitX[70]-2*orbitX[68]);
		orbitX[62] = (f_62-1*orbitX[68])/2;
		orbitX[61] = (f_61-4*orbitX[71]-8*orbitX[69]-2*orbitX[67])/2;
		orbitX[60] = (f_60-4*orbitX[71]-2*orbitX[68]-2*orbitX[67]);
		orbitX[59] = (f_59-6*orbitX[70]-2*orbitX[68]-4*orbitX[65]);
		orbitX[58] = (f_58-4*orbitX[72]-2*orbitX[71]-1*orbitX[67]);
		orbitX[57] = (f_57-12*orbitX[72]-4*orbitX[71]-3*orbitX[70]-1*orbitX[67]-2*orbitX[66]);
		orbitX[56] = (f_56-2*orbitX[65])/3;
		orbitX[55] = (f_55-2*orbitX[71]-2*orbitX[67])/3;
		orbitX[54] = (f_54-3*orbitX[70]-1*orbitX[66]-2*orbitX[65])/2;
		orbitX[53] = (f_53-2*orbitX[68]-2*orbitX[64]-2*orbitX[63]);
		orbitX[52] = (f_52-2*orbitX[66]-2*orbitX[64]-1*orbitX[59])/2;
		orbitX[51] = (f_51-2*orbitX[68]-2*orbitX[63]-4*orbitX[62]);
		orbitX[50] = (f_50-1*orbitX[68]-2*orbitX[63])/3;
		orbitX[49] = (f_49-1*orbitX[68]-1*orbitX[64]-2*orbitX[62])/2;
		orbitX[48] = (f_48-4*orbitX[71]-8*orbitX[69]-2*orbitX[68]-2*orbitX[67]-2*orbitX[64]-2*orbitX[61]-1*orbitX[60]);
		orbitX[47] = (f_47-3*orbitX[70]-2*orbitX[68]-1*orbitX[66]-1*orbitX[63]-1*orbitX[60]);
		orbitX[46] = (f_46-3*orbitX[70]-2*orbitX[68]-2*orbitX[65]-1*orbitX[63]-1*orbitX[59]);
		orbitX[45] = (f_45-2*orbitX[65]-2*orbitX[62]-3*orbitX[56]);
		orbitX[44] = (f_44-1*orbitX[67]-2*orbitX[61])/4;
		orbitX[43] = (f_43-2*orbitX[66]-1*orbitX[60]-1*orbitX[59])/2;
		orbitX[42] = (f_42-2*orbitX[71]-4*orbitX[69]-2*orbitX[67]-2*orbitX[61]-3*orbitX[55]);
		orbitX[41] = (f_41-2*orbitX[71]-1*orbitX[68]-2*orbitX[67]-1*orbitX[60]-3*orbitX[55]);
		orbitX[40] = (f_40-6*orbitX[70]-2*orbitX[68]-2*orbitX[66]-4*orbitX[65]-1*orbitX[60]-1*orbitX[59]-4*orbitX[54]);
		orbitX[39] = (f_39-4*orbitX[65]-1*orbitX[59]-6*orbitX[56])/2;
		orbitX[38] = (f_38-1*orbitX[68]-1*orbitX[64]-2*orbitX[63]-1*orbitX[53]-3*orbitX[50]);
		orbitX[37] = (f_37-2*orbitX[68]-2*orbitX[64]-2*orbitX[63]-4*orbitX[62]-1*orbitX[53]-1*orbitX[51]-4*orbitX[49]);
		orbitX[36] = (f_36-1*orbitX[68]-2*orbitX[63]-2*orbitX[62]-1*orbitX[51]-3*orbitX[50]);
		orbitX[35] = (f_35-1*orbitX[59]-2*orbitX[52]-2*orbitX[45])/2;
		orbitX[34] = (f_34-1*orbitX[59]-2*orbitX[52]-1*orbitX[51])/2;
		orbitX[33] = (f_33-1*orbitX[67]-2*orbitX[61]-3*orbitX[58]-4*orbitX[44]-2*orbitX[42])/2;
		orbitX[32] = (f_32-2*orbitX[66]-1*orbitX[60]-1*orbitX[59]-2*orbitX[57]-2*orbitX[43]-2*orbitX[41]-1*orbitX[40])/2;
		orbitX[31] = (f_31-2*orbitX[65]-1*orbitX[59]-3*orbitX[56]-1*orbitX[43]-2*orbitX[39]);
		orbitX[30] = (f_30-1*orbitX[67]-1*orbitX[63]-2*orbitX[61]-1*orbitX[53]-4*orbitX[44]);
		orbitX[29] = (f_29-2*orbitX[66]-2*orbitX[64]-1*orbitX[60]-1*orbitX[59]-1*orbitX[53]-2*orbitX[52]-2*orbitX[43]);
		orbitX[28] = (f_28-2*orbitX[65]-2*orbitX[62]-1*orbitX[59]-1*orbitX[51]-1*orbitX[43]);
		orbitX[27] = (f_27-1*orbitX[59]-1*orbitX[51]-2*orbitX[45])/2;
		orbitX[26] = (f_26-2*orbitX[67]-2*orbitX[63]-2*orbitX[61]-6*orbitX[58]-1*orbitX[53]-2*orbitX[47]-2*orbitX[42]);
		orbitX[25] = (f_25-2*orbitX[66]-2*orbitX[64]-1*orbitX[59]-2*orbitX[57]-2*orbitX[52]-1*orbitX[48]-1*orbitX[40])/2;
		orbitX[24] = (f_24-4*orbitX[65]-4*orbitX[62]-1*orbitX[59]-6*orbitX[56]-1*orbitX[51]-2*orbitX[45]-2*orbitX[39]);
		orbitX[23] = (f_23-1*orbitX[55]-1*orbitX[42]-2*orbitX[33])/4;
		orbitX[22] = (f_22-2*orbitX[54]-1*orbitX[40]-1*orbitX[39]-1*orbitX[32]-2*orbitX[31])/3;
		orbitX[21] = (f_21-3*orbitX[55]-3*orbitX[50]-2*orbitX[42]-2*orbitX[38]-2*orbitX[33]);
		orbitX[20] = (f_20-2*orbitX[54]-2*orbitX[49]-1*orbitX[40]-1*orbitX[37]-1*orbitX[32]);
		orbitX[19] = (f_19-4*orbitX[54]-4*orbitX[49]-1*orbitX[40]-2*orbitX[39]-1*orbitX[37]-2*orbitX[35]-2*orbitX[31]);
		orbitX[18] = (f_18-1*orbitX[59]-1*orbitX[51]-2*orbitX[46]-2*orbitX[45]-2*orbitX[36]-2*orbitX[27]-1*orbitX[24])/2;
		orbitX[17] = (f_17-1*orbitX[60]-1*orbitX[53]-1*orbitX[51]-1*orbitX[48]-1*orbitX[37]-2*orbitX[34]-2*orbitX[30])/2;
		orbitX[16] = (f_16-1*orbitX[59]-2*orbitX[52]-1*orbitX[51]-2*orbitX[46]-2*orbitX[36]-2*orbitX[34]-1*orbitX[29]);
		orbitX[15] = (f_15-1*orbitX[59]-2*orbitX[52]-1*orbitX[51]-2*orbitX[45]-2*orbitX[35]-2*orbitX[34]-2*orbitX[27]);
	}
}

} // end namespace OrcaGraphletCounter

