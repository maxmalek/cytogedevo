#ifndef ALGOBASE_H
#define ALGOBASE_H

#include "common.h"
#include "UserSettings.h"
#include "AgentMgr.h"
#include "Threading.h"


struct RandomFunctor
{
	RandomFunctor(MTRand& rng) : _rng(rng) {}
	inline ptrdiff_t operator() (ptrdiff_t x)
	{
		ASSERT(x);
		return _rng.randInt((uint)(x - 1));
	}

	MTRand& _rng;
};

#define MT_SHUFFLE(a, b, r) { RandomFunctor __rf(r); std::random_shuffle((a), (b), __rf); }


#define RUN_JOB_AUTOMT(job, ...) \
{ \
	if(_thpool) \
		_thpool->addJob(job, __VA_ARGS__); \
	else \
		job(*_rng, __VA_ARGS__); \
}

class AlgoBase
{
public:
	virtual ~AlgoBase();

	// Prepares work on the passed mgr
	virtual void Init() = 0;

	// Does one iteration step
	virtual void Update() = 0;

	bool IsEmpty() const { return GetAgents().empty(); }

	virtual void Clear();

	void Link(AgentMgr& mgr);

	// Stops work on the current mgr. Must be called before calling another Init()
	void Unlink();

	inline AgentMgr& GetAgentMgr() const { ASSERT(_mgr); return *_mgr; }
	inline const AgentArray& GetAgents() const { ASSERT(_mgr); return _mgr->agents; }

	//for isomorphic networks only
	inline const bool perfectMatch(){return ((_mgr->agents[0]->getEdgeCorrectness()) == 1.);}

	inline AlgoID getAlgoID() const { return _algoid; }

	virtual const Agent *getBestAgent() const { return GetAgents()[0]; }

	// To add agents from externally, they will be considered in next Update()
	virtual void enqueueAgent(Agent *a) = 0;

protected:

	virtual void _OnUnlink() = 0;

	// Precalculates all scrores; multithreads automatically
	void _CalcScores();

	// Pass a thread pool pointer for multithreaded mode, pass NULL for single-threaded fallback
	AlgoBase(AlgoID algoid, const DataHolder& dh, ThreadPool *thpool);

	AgentMgr *_mgr;

	// Exactly one of the next 2 pointers is set, but never both!
	ThreadPool * const _thpool; // Manages worker threads and the parallel job queue
	MTRand *_rng;

	const DataHolder& _dataH;

	const UserSettings& _settings;

	AlgoID _algoid;
};

#endif
