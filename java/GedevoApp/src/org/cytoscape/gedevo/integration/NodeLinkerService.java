package org.cytoscape.gedevo.integration;

import org.cytoscape.gedevo.*;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.model.events.*;
import org.cytoscape.task.NetworkViewTaskFactory;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import java.util.*;

import static org.cytoscape.work.ServiceProperties.*;

public class NodeLinkerService extends Unloadable
{
    private GedevoApp app;
    private SingleLinkFactory singleLinkF;
    private MultiLinkFactory multiLinkF;

    public NodeLinkerService(GedevoApp a)
    {
        app = a;

        singleLinkF = new SingleLinkFactory();

        {
            Properties p = new Properties();
            p.setProperty(PREFERRED_MENU, "Edit");
            p.setProperty(ENABLE_FOR, "selectedNodesAndEdges");
            p.setProperty(TITLE, "GEDEVO: Fixate selected mapping edges and selected nodes + partners");
            p.setProperty(MENU_GRAVITY, "13.37");
            p.setProperty(ACCELERATOR, "alt p"); // TODO: make configurable?
            p.setProperty(COMMAND, "gedevolinkselected");
            p.setProperty(COMMAND_NAMESPACE, "network");
            multiLinkF = new MultiLinkFactory();
            app.cyreg.registerService(multiLinkF, NetworkViewTaskFactory.class, p);
        }

        app.log("NodeLinkerService registered");
    }

    @Override
    public void unload()
    {
        singleLinkF.disableLinkOnSelect();
        //app.cyreg.unregisterAllServices(singleLinkF);
        app.cyreg.unregisterAllServices(multiLinkF);

        singleLinkF = null;
        multiLinkF = null;

        app = null;
    }

    public void enableSingleLinkOnSelect()
    {
        singleLinkF.enableLinkOnSelect();
    }
    public void disableSingleLinkOnSelect()
    {
        singleLinkF.disableLinkOnSelect();
    }
    public TaskIterator createMultiLinkTaskIterator(CyNetworkView view) { return multiLinkF.createTaskIterator(view); }


    private class SingleLinkFactory implements AboutToRemoveNodesListener, NetworkAboutToBeDestroyedListener, RowsSetListener
    {
        private Map<CyNetwork, CyNode> chosenNodeByNet = new WeakHashMap<CyNetwork, CyNode>();
        private boolean on = false;

        public SingleLinkFactory()
        {
        }

        public void enableLinkOnSelect()
        {
            if(on)
                return;
            app.log("Link on select ON");
            Properties p = new Properties();
            app.cyreg.registerService(this, AboutToRemoveNodesListener.class, p);
            app.cyreg.registerService(this, NetworkAboutToBeDestroyedListener.class, p);
            app.cyreg.registerService(this, RowsSetListener.class, p);
            chosenNodeByNet.clear();
            on = true;
        }

        public void disableLinkOnSelect()
        {
            if(!on)
                return;
            app.log("Link on select OFF");
            app.cyreg.unregisterAllServices(this);
            chosenNodeByNet.clear();
            on = false;
        }

        @Override
        public void handleEvent(AboutToRemoveNodesEvent ev)
        {
            chosenNodeByNet.values().removeAll(ev.getNodes());
        }

        @Override
        public void handleEvent(NetworkAboutToBeDestroyedEvent ev)
        {
            chosenNodeByNet.remove(ev.getNetwork());
        }

        @Override
        public void handleEvent(RowsSetEvent ev)
        {
            if(ev.containsColumn(CyNetwork.SELECTED))
            {
                final CyNetwork cynet = GedevoAlignmentUtil.getNetworkForTable(ev.getSource());
                if(cynet == null)
                    return;
                if(ev.getSource() != cynet.getDefaultNodeTable())
                    return;

                Collection<RowSetRecord> c = ev.getColumnRecords(CyNetwork.SELECTED);
                if(c == null || c.isEmpty() || c.size() > 2) // max. 1 selected + 1 deselected
                    return;
                CyNode chosen = chosenNodeByNet.get(cynet);
                if(chosen == null)
                {
                    for(RowSetRecord r : c)
                    {
                        if (Boolean.TRUE.equals(r.getValue()))
                        {
                            chosen = cynet.getNode(r.getRow().get(CyNetwork.SUID, Long.class));
                            app.log("Chosen Node: " + chosen); // TODO: some sort of visual indicator?
                            chosenNodeByNet.put(cynet, chosen);
                        }
                    }
                    return;
                }

                app.log("Link " + chosen + " ...");
                boolean linked = false;

                for(RowSetRecord r : c)
                {
                    if(Boolean.TRUE.equals(r.getValue()))
                    {
                        CyNode linkwith = GedevoAlignmentUtil.getNodeByUID(cynet, r.getRow().get(ColumnNames.NODE_UID, Long.class));
                        linked = null != GedevoAlignmentUtil.mapSingleNodes(cynet, chosen, linkwith, true);
                        chosenNodeByNet.remove(cynet);
                        if(linked)
                        {
                            app.log("... with " + linkwith);
                            app.cyeventmgr.flushPayloadEvents();
                            for (CyNetworkView v : app.cyviewmgr.getNetworkViews(cynet))
                                v.updateView();
                        }
                    }
                }

                if(!linked)
                {   // FIXME: make function, dedup code + from above
                    for (RowSetRecord r : c)
                    {
                        if (Boolean.TRUE.equals(r.getValue()))
                        {
                            chosen = cynet.getNode(r.getRow().get(CyNetwork.SUID, Long.class));
                            app.log("Chosen Node: " + chosen); // TODO: some sort of visual indicator?
                            chosenNodeByNet.put(cynet, chosen);
                        }
                        else
                        {
                            app.log("Deselected Node: " + chosen); // TODO: some sort of visual indicator?
                            chosenNodeByNet.remove(cynet);
                        }
                    }
                }

            }
        }

        /*@Override
        public boolean isReady(CyNetworkView cyview)
        {
            CyNetwork cynet = cyview.getModel();
            if(!GedevoAlignmentUtil.isGEDEVONetwork(cynet))
                return false;
            List<CyNode> selected = CyTableUtil.getNodesInState(cynet, CyNetwork.SELECTED, true);
            return selected != null && selected.size() == 1;
        }*/
    }


    private class MultiLinkFactory implements NetworkViewTaskFactory
    {
        @Override
        public TaskIterator createTaskIterator(final CyNetworkView cyview)
        {
            return new TaskIterator(new GedevoSafeTask()
            {
                @Override
                public void runSafe(TaskMonitor taskMonitor) throws Exception
                {
                    UIShortcuts.fixateSelectedEdgesAndNodes(cyview);
                }
            });
        }

        @Override
        public boolean isReady(CyNetworkView cyview)
        {
            if(cyview == null)
                return false;
            CyNetwork cynet = cyview.getModel();
            if(cynet == null || !GedevoAlignmentUtil.isGEDEVONetwork(cynet))
                return false;
            List<CyNode> selectedNodes = CyTableUtil.getNodesInState(cynet, CyNetwork.SELECTED, true);
            if(selectedNodes != null && !selectedNodes.isEmpty())
                return true;
            List<CyEdge> selectedEdges = CyTableUtil.getEdgesInState(cynet, CyNetwork.SELECTED, true);
            return selectedEdges != null && !selectedEdges.isEmpty();
        }
    }

}
