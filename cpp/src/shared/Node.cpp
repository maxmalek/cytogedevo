#include "Node.h"
#include "common.h"


Node::Node(uint group, uint id, const std::string& name)
: id(id)
, group(group)
, hasSelfLoop(0)
, prematched(-1)
, tag(-1)
, proto(new NodePrototype)
, _name(name)
{
}

Node::~Node()
{
	dropProto();
}

