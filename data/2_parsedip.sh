#!/bin/bash

. config.inc

FILE=raw/$1.txt

if [ ! -d work ]; then
    mkdir work
fi
 
COUNTER=0
for TAXON in ${SPECIESTAXONS[@]}; do
    SPECIES=${SPECIESNAMES[$COUNTER]}
    echo "Parsing $SPECIES"
    awk "/taxid:${TAXON}.*taxid:${TAXON}/{ print \$1\"\t\"\$2 | \"sort\" }" $FILE | sort -k1 -k2 > work/${SPECIES}.edgelist
    echo "Generating node list"
    awk '{print $1"\n"$2}' work/${SPECIES}.edgelist | sort -u  > work/${SPECIES}.nodelist
    let COUNTER++
done

