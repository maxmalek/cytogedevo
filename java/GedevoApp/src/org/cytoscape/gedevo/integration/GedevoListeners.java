package org.cytoscape.gedevo.integration;

import org.cytoscape.application.events.SetCurrentNetworkViewEvent;
import org.cytoscape.application.events.SetCurrentNetworkViewListener;
import org.cytoscape.gedevo.GedevoAlignmentCache;
import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.Unloadable;
import org.cytoscape.gedevo.integration.customlayoutmenu.LayoutListenerForwarder;
import org.cytoscape.model.events.*;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.layout.CyLayoutAlgorithmManager;
import org.cytoscape.view.model.events.NetworkViewAboutToBeDestroyedEvent;
import org.cytoscape.view.model.events.NetworkViewAboutToBeDestroyedListener;
import org.cytoscape.view.model.events.NetworkViewAddedEvent;
import org.cytoscape.view.model.events.NetworkViewAddedListener;

import java.util.Properties;

public class GedevoListeners extends Unloadable
{
    private GedevoApp app;
    private CreateListener cListener;
    private DestroyListener dListener;
    private DestroyedListener destroyedListener;
    private DestroyViewListener dViewListener;
    private CreateViewListener cViewListener;
    public LayoutListenerForwarder layoutListener;
    public SetViewListener setViewListener;

    public GedevoListeners(GedevoApp a)
    {
        app = a;
        Properties none = new Properties();

        cListener = new CreateListener();
        app.cyreg.registerService(cListener, NetworkAddedListener.class, none);

        dListener = new DestroyListener();
        app.cyreg.registerService(dListener, NetworkAboutToBeDestroyedListener.class, none);

        dViewListener = new DestroyViewListener();
        app.cyreg.registerService(dViewListener, NetworkViewAboutToBeDestroyedListener.class, none);

        cViewListener = new CreateViewListener();
        app.cyreg.registerService(cViewListener, NetworkViewAddedListener.class, none);

        destroyedListener = new DestroyedListener();
        app.cyreg.registerService(destroyedListener, NetworkDestroyedListener.class, none);

        setViewListener = new SetViewListener();
        app.cyreg.registerService(setViewListener, SetCurrentNetworkViewListener.class, none);

        // This one can not be truly unloaded, but it takes care of disabling itself as good as possible
        layoutListener = new LayoutListenerForwarder();
        app.cyreg.registerServiceListener(layoutListener, "addLayout", "removeLayout", CyLayoutAlgorithm.class);
        CyLayoutAlgorithmManager am = app.cyreg.getService(CyLayoutAlgorithmManager.class);
        for(CyLayoutAlgorithm algo : am.getAllLayouts())
            layoutListener.addLayout(algo, null); // 2nd param is ignored
    }

    @Override
    public void unload()
    {
        if(cListener != null)
            app.cyreg.unregisterAllServices(cListener);
        if(dListener != null)
            app.cyreg.unregisterAllServices(dListener);
        if(dViewListener != null)
            app.cyreg.unregisterAllServices(dViewListener);
        if(cViewListener != null)
            app.cyreg.unregisterAllServices(cViewListener);
        if(destroyedListener != null)
            app.cyreg.unregisterAllServices(destroyedListener);
        if(setViewListener != null)
            app.cyreg.unregisterAllServices(setViewListener);

        cListener = null;
        dListener = null;
        dViewListener = null;
        cViewListener = null;
        layoutListener = null;
        destroyedListener = null;
        setViewListener = null;

        app = null;
    }

    private class CreateListener implements NetworkAddedListener
    {
        @Override
        public void handleEvent(NetworkAddedEvent ev)
        {
            app.refresh();
        }
    }

    private class DestroyListener implements NetworkAboutToBeDestroyedListener
    {
        @Override
        public void handleEvent(NetworkAboutToBeDestroyedEvent ev)
        {
            GedevoAlignmentCache.removeForNetwork(ev.getNetwork());
            app.refresh();
        }
    }

    private class DestroyedListener implements NetworkDestroyedListener
    {
        @Override
        public void handleEvent(NetworkDestroyedEvent networkDestroyedEvent)
        {
            app.refresh();
        }
    }

    private class DestroyViewListener implements NetworkViewAboutToBeDestroyedListener
    {
        @Override
        public void handleEvent(NetworkViewAboutToBeDestroyedEvent ev)
        {
            app.refresh();
        }
    }

    private class CreateViewListener implements NetworkViewAddedListener
    {
        @Override
        public void handleEvent(NetworkViewAddedEvent ev)
        {
            app.refresh();
        }
    }

    private class SetViewListener implements SetCurrentNetworkViewListener
    {
        @Override
        public void handleEvent(SetCurrentNetworkViewEvent ev)
        {
            app.mainPanel.handleActiveViewChanged(ev.getNetworkView());
        }
    }
}
