#!/bin/bash

. config.inc

FASTAFILE=raw/$1.seq


for SPECIES in ${SPECIESNAMES[@]}; do
    echo "Extract FASTA for $SPECIES"
    fn=work/$SPECIES.fasta 
    [ -e $fn ] && rm $fn


    while read prot; do
        dprot=${prot%%|*}
        egrep -e "${dprot}$" -e "${dprot}[|]" -A 1 $FASTAFILE >> $fn
    done < work/$SPECIES.nodelist
done
