package org.cytoscape.gedevo.task;

import org.cytoscape.gedevo.*;
import org.cytoscape.gedevo.util.CyNodePair;
import org.cytoscape.model.*;
import org.cytoscape.work.TaskMonitor;

import javax.swing.*;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

public final class ExportAlignmentTask extends GedevoTask
{
    private File outfile;
    private CyNetwork cynet;
    private boolean rich;
    private boolean selectedOnly;
    private boolean hadSpaces;

    public ExportAlignmentTask(String filename, CyNetwork cynet, boolean rich, boolean selectedOnly)
    {
        this(new File(filename), cynet, rich, selectedOnly);
    }

    public ExportAlignmentTask(File outfile, CyNetwork cynet, boolean rich, boolean selectedOnly)
    {
        this.outfile = outfile;
        this.cynet = cynet;
        this.rich = rich;
        this.selectedOnly = selectedOnly;
    }

    public static ExportAlignmentTask askForSave(CyNetwork cynet, boolean rich, boolean selectedOnly)
    {
        File which = FileSelectorBox.ask(true);
        return which != null ? new ExportAlignmentTask(which, cynet, rich, selectedOnly) : null;
    }

    @Override
    public boolean canFinishNow()
    {
        return false;
    }

    @Override
    public void run(TaskMonitor mon) throws Exception
    {
        mon.setStatusMessage("Exporting alignment...");
        PrintStream ps = new PrintStream(new BufferedOutputStream(new FileOutputStream(outfile), 64*1024));
        if(rich)
            printRich(ps, mon);
        else
            printSimple(ps, mon);
        ps.close();

        if(hadSpaces)
        {
            final String msg = "WARNING: Some node names contain whitespace; the exported file will fail to import!";
            mon.showMessage(TaskMonitor.Level.WARN, msg);
            SwingUtilities.invokeLater(new Runnable()
            {
                @Override
                public void run()
                {
                    GedevoUtil.msgbox(msg);
                }
            });
        }
    }

    private List<CyNodePair> getNodePairs()
    {
        return selectedOnly
            ? GedevoAlignmentUtil.getSelectedAlignedNodePairs(cynet)
            : GedevoAlignmentUtil.getAlignedNodePairs(cynet, true);
    }

    private void printSimple(final PrintStream ps, TaskMonitor mon)
    {
        List<CyNodePair> pairs = getNodePairs();
        double step = 1.0 / pairs.size();
        double done = 0.0;
        for(CyNodePair p : pairs)
        {
            printNodePair(ps, p);
            ps.println();
            mon.setProgress(done += step);
        }
    }

    private void printRich(final PrintStream ps, TaskMonitor mon)
    {
        List<CyNodePair> pairs = getNodePairs();
        CyTable tab = cynet.getDefaultNodeTable();
        List<String> cols = ColumnNames.getScoreColumnNames(tab);

        ps.println("## === CytoGEDEVO alignment export ===");
        {
            CyTable nettab = cynet.getDefaultNetworkTable();
            CyRow row = nettab.getRow(cynet.getSUID());
            if(row != null)
            {
                String name = row.get(CyNetwork.NAME, String.class);
                ps.println("## Network name: " + name);
                List < String > sl = row.getList(ColumnNames.SOURCE_NETWORKS_LIST, String.class);
                if(sl != null)
                {
                    ps.println("## Source networks:");
                    for(String s : sl)
                        ps.println("##   " + s);
                }
            }
        }
        ps.println();
        ps.println();
        ps.print("P1\tP2");
        for(String colname : cols)
        {
            ps.print('\t');
            ps.print(ColumnNames.buildScoreNameFromColumnName(colname));
        }
        final boolean exportCCS = tab.getColumn(ColumnNames.CCS) != null && tab.getColumn(ColumnNames.CCS_SIZE) != null;
        if(exportCCS)
            ps.print("\tCCS\tCCS_size");
        ps.print("\tInfo");
        ps.println();
        ps.println();

        double step = 1.0 / pairs.size();
        double done = 0.0;
        for(CyNodePair p : pairs)
        {
            CyRow row = printNodePair(ps, p);

            for(String colname : cols)
            {
                ps.print('\t');
                double d = row.get(colname, Double.class);
                ps.print((float)d); // don't need that much precision, throw off some bits
            }

            if(exportCCS)
            {
                final int ccs = row.get(ColumnNames.CCS, Integer.class);
                ps.print('\t');
                ps.print(ccs);
                final int ccsSize = row.get(ColumnNames.CCS_SIZE, Integer.class);
                ps.print('\t');
                ps.print(ccsSize);
            }

            ps.print(p.fixed ? "\tprematched" : "\t-");

            ps.println();

            mon.setProgress(done += step);
        }
    }
    private String checkSpaces(String s)
    {
        if(!hadSpaces)
        {
            int sz = s.length();
            for (int i = 0; i < sz; i++)
                if (Character.isWhitespace(s.charAt(i)))
                {
                    hadSpaces = true;
                    break;
                }
        }
        return s;
    }

    private CyRow printNodePair(final PrintStream ps, CyNodePair p)
    {
        CyRow row = cynet.getRow(p.a != null ? p.a : p.b);
        if (p.a != null)
        {
            ps.print(checkSpaces(row.get(CyNetwork.NAME, String.class)));
            ps.print('\t');
            if (p.b != null)
                ps.print(checkSpaces(row.get(ColumnNames.NODE_PARTNER_NAME, String.class)));
            else
                ps.print('-');
        } else
        {
            ps.print("-\t");
            ps.print(checkSpaces(row.get(CyNetwork.NAME, String.class)));
        }
        return row;
    }
}
