package org.cytoscape.gedevo.pairlayout;

import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;

import java.util.Map;

public class SideBySide extends IPairLayout
{
    public static class Factory implements IPairLayout.Factory
    {
        @Override
        public IPairLayout create(PairLayoutTask pl)
        {
            return new SideBySide(pl);
        }

        @Override
        public String getName()
        {
            return "Side by Side";
        }
    }

    public static Factory getFactory()
    {
        return new Factory();
    }

    private PairLayoutTask pl;
    public SideBySide(PairLayoutTask pl)
    {
        this.pl = pl;
    }

    public void apply(CyNetworkView view)
    {
        if(view.getModel() != pl.getSourceNetwork())
            throw new IllegalArgumentException("passed view is unrelated to pair layout");

        Map<CyNode, double[]> positions = pl.getPositions();
        Map<CyNode, CyNode[]> backmap = pl.getMapToOriginalNodes();

        double minx = Double.POSITIVE_INFINITY;
        double miny = Double.POSITIVE_INFINITY;
        double maxx = Double.NEGATIVE_INFINITY;
        double maxy = Double.NEGATIVE_INFINITY;
        for(double[] p : positions.values())
        {
            minx = Math.min(minx, p[0]);
            miny = Math.min(miny, p[1]);
            maxx = Math.max(maxx, p[0]);
            maxy = Math.max(maxy, p[1]);
        }
        double dimx = maxx - minx;
        dimx *= 1.1; // extra margin
        double dimy = maxy - miny;

        int maxsrc = 0;
        CyNetwork net = view.getModel();
        for(View<CyNode> v : view.getNodeViews())
        {
            CyNode n = v.getModel();
            maxsrc = Math.max(maxsrc, net.getRow(n).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class, -1));
        }
        final int numsrc = maxsrc + 1;

        double[] offsx = new double[numsrc];
        //double[] offsy = new double[numsrc];

        for(int i = 0; i < numsrc; ++i)
        {
            offsx[i] = i * (dimx + 100); // slight offset improves seperating very small networks
            //offsy[i] = i * dimy;
        }

        for(CyNode virt : positions.keySet())
        {
            double[] pos = positions.get(virt);
            CyNode[] nodes = backmap.get(virt);
            if(nodes == null)
                continue;

            for (int i = 0; i < nodes.length; ++i)
            {
                CyNode n = nodes[i];
                int netID = net.getRow(n).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class);
                double x = pos[0] + offsx[netID];
                double y = pos[1];

                View<CyNode> v = view.getNodeView(nodes[i]);
                v.setVisualProperty(BasicVisualLexicon.NODE_X_LOCATION, x);
                v.setVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION, y);
            }
        }
    }
}
