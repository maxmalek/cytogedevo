package org.cytoscape.gedevo.task;

public abstract class AbstractAlignmentTask extends GedevoTask
{
    protected final AlignmentTaskData common;


    public AbstractAlignmentTask(AlignmentTaskData common)
    {
        this.common = common;
    }
}
