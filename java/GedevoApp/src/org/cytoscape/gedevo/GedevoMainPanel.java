package org.cytoscape.gedevo;

import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.gedevo.resources.Resources;
import org.cytoscape.gedevo.util.NetworkWrapper;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.vizmap.mappings.ContinuousMapping;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class GedevoMainPanel extends Unloadable implements CytoPanelComponent
{
    public GedevoApp app;
    private GedevoMainPanelComponent ui;

    public GedevoMainPanel(GedevoApp a)
    {
        app = a;
        ui = new GedevoMainPanelComponent(app);
        app.cyreg.registerService(this, CytoPanelComponent.class, new Properties());

    }

    void updateAvailNetworks(JComboBox box, List<NetworkWrapper> nets, boolean includeAlreadyAligned)
    {
        String oldsel = box.getSelectedItem() != null ? box.getSelectedItem().toString() : null;

        box.removeAllItems();
        for(NetworkWrapper n : nets)
        {
            if(!includeAlreadyAligned && GedevoAlignmentUtil.isGEDEVONetwork(n.cynet))
                continue;
            box.addItem(n);
        }
        if(nets.isEmpty())
            box.addItem("(Load some networks first...)");

        /*box.addItem("--------------");
        box.addItem(new GedevoUtil.NamedClosure("[Choose file...]")
        {
            @Override public Void run(Object... unused)
            {
                GedevoUtil.msgbox("NYI: choose file"); // NYI: <-
                return null;
            }
        });*/

        // Restore old selection
        if(oldsel != null)
        {
            int num = box.getItemCount();
            for (int i = 0; i < num; ++i)
            {
                Object itm = box.getItemAt(i);
                if (oldsel.equals(itm.toString()))
                {
                    box.setSelectedItem(itm);
                    break;
                }
            }
        }
    }

    public void refresh()
    {
        ArrayList<NetworkWrapper> sorted = new ArrayList<NetworkWrapper>();
        for(CyNetwork cynet : app.cynetmgr.getNetworkSet())
            sorted.add(new NetworkWrapper(cynet));

        Collections.sort(sorted);

        updateAvailNetworks(ui.comboBox1, sorted, true);
        updateAvailNetworks(ui.comboBox2, sorted, false);

        ui.refresh();
    }

    @Override
    public Component getComponent()
    {
        return ui.panel1;
    }

    @Override
    public CytoPanelName getCytoPanelName()
    {
        return CytoPanelName.WEST;
    }

    @Override
    public String getTitle()
    {
        return "Gedevo";
    }

    @Override
    public Icon getIcon()
    {
        return Resources.getTabIcon();
    }

    @Override
    public void unload()
    {
        app.cyreg.unregisterService(this, CytoPanelComponent.class);

        ui = null;
        app = null;
    }

    public boolean gatherSettings(UserSettings settings)
    {
        return ui.gatherSettings(settings);
    }

    public void addTaskBox(GedevoTaskBox tb)
    {
        ui.addTaskBox(tb);
    }

    public void setMappingPreview(ContinuousMapping<?, Paint> mapping, String desc)
    {
        ui.setMappingPreview(mapping, desc);
    }

    public void handleActiveViewChanged(CyNetworkView view)
    {
        ui.updateCommonTasksPanel(view);
    }


    public boolean isColorBlindSelected()
    {
        return ui.isColorBlindSelected();
    }

    public String getMergedNetworkNameText()
    {
        return ui.mergedNetworkNameText.getText();
    }
}
