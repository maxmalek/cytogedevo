package org.cytoscape.gedevo;

import org.cytoscape.work.TaskMonitor;

public class GedevoTaskMonitor implements TaskMonitor
{
    GedevoTaskBox taskbox;

    public GedevoTaskMonitor(GedevoTaskBox box)
    {
        taskbox = box;
    }

    @Override
    public void setTitle(String s)
    {
        taskbox.setTitle(s);
    }

    @Override
    public void setProgress(double v)
    {
        taskbox.setProgress(v);
    }

    @Override
    public void setStatusMessage(String s)
    {
        taskbox.setDetail(s);
    }

    @Override
    public void showMessage(Level level, String s)
    {
        switch(level)
        {
            case ERROR:
                taskbox.setError(s);
                break;
        }
    }
}
