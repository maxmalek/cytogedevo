#include <sstream>
#include <stdexcept>

#include "org_cytoscape_gedevo_GedevoNative.h"
#include "org_cytoscape_gedevo_GedevoNative_Instance.h"
#include "org_cytoscape_gedevo_GedevoNative_Network.h"

#include "mt_jni.h"

#include "Network.h"
#include "GedevoState.h"
#include "ProgramState.h"


// See the following URL for some of the exception magic used here
// https://stackoverflow.com/questions/4138168/what-happens-when-i-throw-a-c-exception-from-a-native-java-method

//This is how we represent a Java exception already in progress
struct ThrownJavaException : std::runtime_error
{
	ThrownJavaException() :std::runtime_error("") {}
	ThrownJavaException(const std::string& msg) : std::runtime_error(msg) {}
};

static void assert_no_exception(JNIEnv *env)
{
	if (env->ExceptionCheck()==JNI_FALSE) 
		throw ThrownJavaException("assert_no_exception");
}

static void assert_throw(const char *msg)
{
	throw ThrownJavaException(msg);
}

template<typename T> T handle_cast(jlong handle)
{
	T ptr = reinterpret_cast<T>(handle);
	ASSERT(ptr);
	return ptr;
}

//used to throw a new Java exception. use full paths like:
//"java/lang/NoSuchFieldException"
//"java/lang/NullPointerException"
//"java/security/InvalidParameterException"
struct NewJavaException : public ThrownJavaException
{
	NewJavaException(JNIEnv * env, const char* type="", const char* message="")
		:ThrownJavaException(type+std::string(" ")+message)
	{
		jclass newExcCls = env->FindClass(type);
		if (newExcCls != NULL)
			env->ThrowNew(newExcCls, message);
		//if it is null, a NoClassDefFoundError was already thrown
	}
};

static void swallow_cpp_exception_and_throw_java(JNIEnv * env)
{
	try
	{
		throw;
	}
	catch(const ThrownJavaException&)
	{
		//already reported to Java, ignore
	}
	catch(const std::bad_alloc& rhs)
	{
		//translate OOM C++ exception to a Java exception
		NewJavaException(env, "java/lang/OutOfMemoryError", rhs.what()); 
	}
	catch(const std::ios_base::failure& rhs)
	{
		//sample translation
		//translate IO C++ exception to a Java exception
		NewJavaException(env, "java/io/IOException", rhs.what()); 

		//TRANSLATE ANY OTHER C++ EXCEPTIONS TO JAVA EXCEPTIONS HERE
	}
	catch(const std::exception& e)
	{
		//translate unknown C++ exception to a Java exception
		NewJavaException(env, "java/lang/Exception", e.what());
	}
	catch(...)
	{
		//translate unknown C++ exception to a Java exception
		NewJavaException(env, "java/lang/Error", "Unknown exception type");
	}
}

#if !_MSC_VER || _HAS_EXCEPTIONS
#  define _J_TRY try {
#  define _J_CATCH(jj) } catch(...) { swallow_cpp_exception_and_throw_java(jj); }
#  define _J_CATCH_RETURN(jj, ret) } catch(...) { swallow_cpp_exception_and_throw_java(jj); return ret; }
#  define _J_SAFE(jj, code) do { _J_TRY code _J_CATCH(jj) } while(0)
#  define _J_SAFE_RETURN(jj, ret, code) do { _J_TRY code _J_CATCH_RETURN(jj, ret) } while(0)
#else
#  define _J_TRY
#  define _J_CATCH(jj)
#  define _J_CATCH_RETURN(jj, ret)
#  define _J_SAFE(jj, code) code
#  define _J_SAFE_RETURN(jj, ret, code) code
#endif

enum PrimitiveType
{
	J_INT,
	J_BYTE,
	J_SHORT,
	J_LONG,
	J_FLOAT,
	J_DOUBLE,
	J_CHAR,
	J_BOOL,
	J_STRING,
};
static const char *s_fieldNames[] = { "I", "B", "S", "J", "F", "D", "C", "Z", "Ljava/lang/String;" };

template <typename T> struct TypeMap {};

#define MAP_TYPE(cty, jprim, getter, setter) \
	template <> struct TypeMap<cty> \
	{ \
		enum { Type = jprim }; \
		static cty ValueOf(JNIEnv *jj, jobject obj, jfieldID fid) \
		{ \
			return (cty)jj->getter(obj, fid); \
		} \
		static jfieldID GetFieldID(JNIEnv *jj, jclass cls, const char *name) \
		{ \
			const jfieldID fid = jj->GetFieldID(cls, name, s_fieldNames[Type]); \
			ASSERT(fid); \
			return fid; \
		} \
		static void SetValue(const cty& val, JNIEnv *jj, jobject obj, jfieldID fid) \
		{ \
			jj->setter(obj, fid, val); \
		} \
	};

MAP_TYPE(int, J_INT, GetIntField, SetIntField);
MAP_TYPE(uint, J_INT, GetIntField, SetIntField);
//MAP_TYPE(unsigned char, J_BYTE, GetByteField, SetByteField);
//MAP_TYPE(short, J_SHORT, GetShortField, SetShortField);
MAP_TYPE(jlong, J_LONG, GetLongField, SetLongField);
MAP_TYPE(float, J_FLOAT, GetFloatField, SetFloatField);
MAP_TYPE(double, J_DOUBLE, GetDoubleField, SetDoubleField);
//MAP_TYPE(jchar, J_CHAR, GetCharField, SetCharField);
MAP_TYPE(bool, J_BOOL, GetBooleanField, SetBooleanField);
MAP_TYPE(jstring, J_STRING, GetObjectField, SetObjectField);

#undef MAP_TYPE

template <typename T> static T getField(JNIEnv *jj, jclass cls, jobject obj, const char *name)
{
	return TypeMap<T>::ValueOf(jj, obj, TypeMap<T>::GetFieldID(jj, cls, name));
}

template <typename T> static void autoGetField(T& dst, JNIEnv *jj, jclass cls, jobject obj, const char *name)
{
	dst = getField<T>(jj, cls, obj, name);
}

template <typename T> static void setField(const T& val, JNIEnv *jj, jclass cls, jobject obj, const char *name)
{
	TypeMap<T>::SetValue(val, jj, obj, TypeMap<T>::GetFieldID(jj, cls, name));
}


static SimpleNetwork *createFromArrays(SimpleNetwork::NodeList& nodes, SimpleNetwork::EdgeList& edges)
{
	SimpleNetwork *net = new SimpleNetwork(nodes, edges);
	if(!net->prepare())
	{
		ASSERT(false && "huh?");
		delete net;
		net = NULL;
	}
	return net;
}

static void jstringToStdString(JNIEnv *jj, jstring js, std::string& s)
{
	s.clear();
	if(const char *cstr =jj->GetStringUTFChars(js, NULL))
	{
		s = cstr;
		jj->ReleaseStringUTFChars(js, cstr);
	}
}

static jstring charptrToJstring(JNIEnv *jj, const char *s)
{
	return jj->NewStringUTF(s);
}

void javaRegisterThread(void *user)
{
	JavaVM *jvm = (JavaVM*)user;
	JNIEnv *dummy;
	jvm->AttachCurrentThread((void**)&dummy, NULL);
}

void javaUnregisterThread(void *user)
{
	JavaVM *jvm = (JavaVM*)user;
	jvm->DetachCurrentThread();
}



extern "C" {


JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_initN
	(JNIEnv *jj, jclass cls)
{
	return true;
}

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_initThreadingN
(JNIEnv *jj, jclass cls)
{
	jni_pass pass;
	pass.jj = jj;
	pass.cls = cls;
	return InitMT(&pass);
}

JNIEXPORT void JNICALL Java_org_cytoscape_gedevo_GedevoNative_shutdownN
	(JNIEnv *, jclass)
{
	QuitMT();
}

JNIEXPORT void JNICALL Java_org_cytoscape_gedevo_GedevoNative_callThreadLaunchFuncPtr
(JNIEnv *jj, jclass, jlong hf, jlong huser)
{
	void (*f)(void*) = handle_cast<void (*)(void*)>(hf);
	void *user = handle_cast<void*>(huser);

	f(user);
}



JNIEXPORT jlong JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Network_constructN
(JNIEnv *, jclass)
{
	return reinterpret_cast<intptr_t>(new SimpleNetwork);
}

JNIEXPORT void JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Network_destructN
(JNIEnv *, jclass, jlong handle)
{
	delete handle_cast<SimpleNetwork*>(handle);
}

JNIEXPORT jlong JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Network_createFromArraysN
(JNIEnv *jj, jclass, jobjectArray jnodes, jintArray ja, jintArray jb)
{
	size_t sz = jj->GetArrayLength(jnodes);
	SimpleNetwork::NodeList nodes(sz);
	for(size_t i = 0; i < sz; ++i)
	{
		jstring js = (jstring)jj->GetObjectArrayElement(jnodes, i);
		jstringToStdString(jj, js, nodes[i]);
	}

	sz = jj->GetArrayLength(ja);
	SimpleNetwork::EdgeList edges(sz);
	jint *ap = jj->GetIntArrayElements(ja, NULL);
	jint *bp = jj->GetIntArrayElements(jb, NULL);
	for(size_t i = 0; i < sz; ++i)
		edges[i] = std::make_pair(ap[i], bp[i]);
	jj->ReleaseIntArrayElements(ja, ap, JNI_ABORT);
	jj->ReleaseIntArrayElements(jb, bp, JNI_ABORT);

	ASSERT(!jj->ExceptionCheck());

	return reinterpret_cast<intptr_t>(createFromArrays(nodes, edges));
}


JNIEXPORT jlong JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_constructN
(JNIEnv *jj, jclass, jboolean useThreadPool)
{
	ProgramInstance *ins = new ProgramInstance(!!useThreadPool);
	
	// This is only necessary when threads are not known to the JVM, i.e. spawned by another library
	/*if(ThreadPool *pool = ins->getThreadPool())
	{
		pool->onThreadStart = javaRegisterThread;
		pool->onThreadExit = javaUnregisterThread;
		jj->GetJavaVM((JavaVM**)&pool->threadCallbackUserParam);
	}*/

	return reinterpret_cast<intptr_t>(ins);
}


JNIEXPORT void JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_destructN
(JNIEnv *, jclass, jlong handle)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	delete ins;
}

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_isDoneN
(JNIEnv *, jclass, jlong handle)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	return ins->isDone();
}

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_init1N
(JNIEnv *jj, jclass, jlong handle)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	_J_SAFE_RETURN(jj, false,
		return ins->init1();
	);
}

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_init2N
(JNIEnv *jj, jclass, jlong handle)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	_J_SAFE_RETURN(jj, false,
		return ins->init2();
	);
}

JNIEXPORT void JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_updateN
(JNIEnv *jj, jclass, jlong handle, jint iterations)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	//_J_SAFE(jj,
		for(jint i = 0; i < iterations && !ins->isDone(); ++i)
			ins->update();
	//);
}

JNIEXPORT void JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_shutdownN
(JNIEnv *jj, jclass, jlong handle)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	ins->shutdown();
}

#define ASSIGN(dst, name) autoGetField(dst, jj, cls, jsettings, #name)
#define A(name) ASSIGN(settings.name, name)
#define ATY(ty, name) do { ty __tmp; ASSIGN(__tmp, name); settings.name = __tmp; } while(0)
#define ASTR(name) do { jstring __js; ASSIGN(__js, name); jstringToStdString(jj, __js, settings.name); } while(0)
#define GGET(dst, cls_, obj, name, jty, ty) do { jty __tmp; autoGetField(__tmp, jj, cls_, obj, #name); dst = (ty)__tmp; } while(0)
#define GGETSTR(dst, cls_, obj, name) do { jstring __js; autoGetField(__js, jj, cls_, obj, #name); jstringToStdString(jj, __js, dst); } while(0)

static bool instance_applySettings(JNIEnv *jj, ProgramInstance *ins, jobject jsettings)
{
	UserSettings& settings = ins->settings;
	jclass cls = jj->GetObjectClass(jsettings);

	ATY(double, weightPairsum);
	ATY(double, weightGraphlets);
	ATY(double, weightGED);

	ATY(double, pairWeightGraphlets);
	ATY(double, pairWeightGED);

	A(basicHealth);
	A(maxHealthDrop);

	ATY(double, pairNullValue);

	ATY(bool, forceUndirectedEdges);
	ATY(bool, matchSameNames);
	ATY(bool, ignoreSelfLoops);

	A(maxAgents);

	A(abort_seconds);
	A(abort_iterations);
	A(abort_nochange);

	A(autosaveSecs);

	A(numThreads);
	ATY(bool, keepWorkfiles);
	ATY(bool, trimNames);
	ATY(bool, evo_greedyInitOnInit);

	ATY(double, ged_eAdd);
	ATY(double, ged_eRm);
	ATY(double, ged_eSub);
	ATY(double, ged_eFlip);
	ATY(double, ged_eD2U);
	ATY(double, ged_eU2D);
	ATY(double, ged_nAdd);
	ATY(double, ged_nRm);

	A(basicHealth);
	A(maxHealthDrop);

	ATY(bool, saveResultsAddTimeStamp);
	ASTR(saveResultsFileName);
	A(autosaveSecs);

	ASTR(logger_file);
	A(logger_iterations);
	

	// custom matrix stuff
	do
	{
		settings.customMatrix.clear();
	
		jclass jsettingscls = jj->GetObjectClass(jsettings);
		jfieldID fid_customMatrixData = jj->GetFieldID(jsettingscls, "customMatrixData", "[Lorg/cytoscape/gedevo/UserSettings$CustomMatrixData;");
		jclass jcmdataCls = jj->FindClass("org/cytoscape/gedevo/UserSettings$CustomMatrixData");

		ASSERT(jsettingscls && fid_customMatrixData && jcmdataCls);

		jobjectArray a_customMatrixData = (jobjectArray)jj->GetObjectField(jsettings, fid_customMatrixData);
		jsize sz = a_customMatrixData ? jj->GetArrayLength(a_customMatrixData) : 0;
		if(!sz)
			break;
		
		std::vector<CustomMatrixData>& m = settings.customMatrix;
		m.resize(sz); // all arrays must be same length

		for(jsize i = 0; i < sz; ++i)
		{
			CustomMatrixData& data = m[i];
			jobject jdata = jj->GetObjectArrayElement(a_customMatrixData, i);

			GGETSTR(data.filename, jcmdataCls, jdata, filename);

			#define B(name, jty, ty) GGET(data.name, jcmdataCls, jdata, name, jty, ty)
			B(value1, double, score);
			B(value2, double, score);
			B(leftNetwork, int, int);
			B(modelType, int, CustomMatrixData::ModelType);
			B(valueRange, int, CustomMatrixData::ValueRange);
			#undef B
		}
	}
	while(0);

	return settings.CheckAndApply();
}

#undef AGET
#undef ASSIGN
#undef A
#undef ATY

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_applySettingsN
(JNIEnv *jj, jclass, jlong handle, jobject jsettings)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	return instance_applySettings(jj, ins, jsettings);
}

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_preInitN
(JNIEnv *jj, jclass, jlong handle, jint algoid, jobject jsettings)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);

	if(!instance_applySettings(jj, ins, jsettings))
		return false;

	return ins->preInit((AlgoID)algoid);
}

static void generateJavaToInternalIndexVector(std::vector<size_t>& posLookup, const NodeArray& nodes)
{
	size_t maxjidx = 0;
	for(size_t i = 0; i < nodes.size(); ++i)
		maxjidx = std::max<size_t>(maxjidx, nodes[i]->tag);

	posLookup.resize(maxjidx+1, -1);
	for(size_t i = 0; i < nodes.size(); ++i)
		posLookup[nodes[i]->tag] = i;
}

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_applyPrematchN
(JNIEnv *jj, jclass, jlong inshandle, jintArray aa, jintArray ab)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(inshandle);
	jint *a = jj->GetIntArrayElements(aa, NULL);
	jint *b = jj->GetIntArrayElements(ab, NULL);
	size_t sz = jj->GetArrayLength(aa);
	ASSERT(sz == jj->GetArrayLength(ab));

	bool good = true;
	ProteinHolder& ph = ins->getProteinHolder();

	// where in {my|ref} is node with a given java index
	uint vargroup = ins->settings.varGroup;
	uint refgroup = !vargroup;
	std::vector<size_t> myPosLookup, refPosLookup;
	generateJavaToInternalIndexVector(myPosLookup, ph.GetGroup(vargroup));
	generateJavaToInternalIndexVector(refPosLookup, ph.GetGroup(refgroup));

	for(size_t i = 0; i < sz; ++i)
		good = ph.PrematchByIdx(myPosLookup[a[i]], refPosLookup[b[i]]) && good; // this assumes that the nodes passed

	// nothing changed here, so JNI_ABORT it is
	jj->ReleaseIntArrayElements(aa, a, JNI_ABORT);
	jj->ReleaseIntArrayElements(ab, b, JNI_ABORT);

	return good;
}

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_constructAgentN
(JNIEnv *jj, jclass, jlong inshandle, jintArray aa, jintArray ab)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(inshandle);
	jint *a = jj->GetIntArrayElements(aa, NULL);
	jint *b = jj->GetIntArrayElements(ab, NULL);
	size_t sz = jj->GetArrayLength(aa);
	ASSERT(sz == jj->GetArrayLength(ab));

	AgentMgr *amgr = ins->getAgentMgr();
	ProteinHolder& ph = ins->getProteinHolder();
	Agent *agent = amgr->newAgent();
	NodeMapping& my = agent->getMappingNonConst();
	const NodeMapping& ref = agent->getReferenceMapping();
	ASSERT(ref.size());
	ASSERT(my.size());
	ASSERT(sz <= ref.size());
	uint vargroup = ins->settings.varGroup;
	uint refgroup = !vargroup;
	my.clear();

	// where in {my|ref} is node with a given java index
	std::vector<size_t> myPosLookup, refPosLookup;
	generateJavaToInternalIndexVector(myPosLookup, ph.GetGroup(vargroup));
	generateJavaToInternalIndexVector(refPosLookup, ph.GetGroup(refgroup));

	// where in ref array is node #i?
	std::vector<uint> whereIsRefNode(ref.size(), -1); // -1 to catch errors
	GEDPopulateIndex(ref, &whereIsRefNode[0]);

	// Go over the java arrays and figure out how to fill `my` -- where to place nodes.
	// We want the nodes with the java indices a[i] and b[i] to correspond,
	// but `ref` is static and can not be changed.
	my.resize(ref.size());
	for(size_t i = 0; i < sz; ++i)
	{
		// Pairs passed from java are always valid (i.e. no pairs with a NULL on either side)
		Node *mynode = ph.GetByIdx(vargroup, myPosLookup[a[i]]);
		ASSERT(mynode);
		ASSERT(mynode->tag == a[i]);

		// just some extra checking
		Node *refnode = ph.GetByIdx(refgroup, refPosLookup[b[i]]);
		ASSERT(refnode);
		ASSERT(refnode->tag == b[i]);
		ASSERT(refnode->prematched == mynode->prematched);

		uint indexInRef = whereIsRefNode[refnode->id - 1];
		ASSERT(ref.node[indexInRef] == refnode);
		ASSERT(!my.node[indexInRef]);
		my.set(indexInRef, mynode);
	}

	agent->completePartialMapping();

	ins->getAlgo()->enqueueAgent(agent);

	// nothing changed here, so JNI_ABORT it is
	jj->ReleaseIntArrayElements(aa, a, JNI_ABORT);
	jj->ReleaseIntArrayElements(ab, b, JNI_ABORT);

	return true;
}

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_importNetworkN
(JNIEnv *jj, jclass, jlong inshandle, jlong nethandle, jint group, jstring jname)
{
	const uint groupIdx = (uint)group;
	ProgramInstance *ins = handle_cast<ProgramInstance*>(inshandle);
	SimpleNetwork *net = handle_cast<SimpleNetwork*>(nethandle);
	ProteinHolder& ph = ins->getProteinHolder();

	const SimpleNetwork::NodeList& nodes = net->getNodeList();
	const SimpleNetwork::EdgeList& edges = net->getEdgeList();

	int tag = 0;
	// First, make sure the nodes are created.
	// Nodes may be reordered internally, so use the tag to store the original array index used on the java side.
	for(SimpleNetwork::NodeList::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
	{
		Node *n = ph.CreateAndAdd(groupIdx, *it);
		n->tag = tag++;
	}

	// Create edges between nodes
	for(SimpleNetwork::EdgeList::const_iterator it = edges.begin(); it != edges.end(); ++it)
		ph.AddPairByIdx(groupIdx, it->first, it->second);

	std::string name;
	jstringToStdString(jj, jname, name);
	ph.SetNetworkFileName(group, name);

	return JNI_TRUE;
}

JNIEXPORT void JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_setQuitN
(JNIEnv *, jclass, jlong handle, jboolean quit)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	ASSERT(ins);
	ins->setQuit(!!quit);
}

#define SET(ty, name, src) setField<ty>(src, jj, infocls, info, #name)

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_writeOutInfoN
(JNIEnv *jj, jclass, jlong handle, jobject info, jlong idx)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	const AgentArray& aa = ins->getAgentMgr()->agents;
	if(idx < 0 || aa.size() < (size_t)idx)
		return JNI_FALSE;

	const Agent *a = aa[(unsigned)idx];
	const ProgramState *state = ins->getProgramState();

	jclass infocls = jj->GetObjectClass(info);

	SET(double, edgeCorrectness, a->getEdgeCorrectness());
	SET(jlong, unalignedEdges, a->getGEDRawFull());
	SET(jlong, partialAlignedEdges, a->getGEDRawPartial());
	SET(jlong, alignedEdges, a->getGEDRawSubstituted());

	SET(jlong, iterations, state->GetIterationCount());
	SET(int, lifeTime, a->getLife());
	SET(int, iterationsWithoutScoreChange, state->GetIterationsWithoutScoreChange());
	SET(int, runningTime, state->GetRunningTime());
	SET(int, numAgents, (int)aa.size());

	return JNI_TRUE;
}
#undef SET

JNIEXPORT jboolean JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_writeOutResultN
(JNIEnv *jj, jclass, jlong handle, jobject info, jlong idx)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	const AgentArray& aa = ins->getAgentMgr()->agents;
	if(idx < 0 || aa.size() < (size_t)idx)
		return JNI_FALSE;

	const Agent *a = aa[(unsigned)idx];
	const ProgramState *state = ins->getProgramState();

	const NodeMapping& my = a->getMapping();
	const NodeMapping& ref = a->getReferenceMapping();
	const UserSettings& settings = ins->settings;
	const DataHolder& dh = ins->getDataHolder();

	jclass infocls = jj->GetObjectClass(info);

	jsize numScores = 2; // pair score + GED score always exist

	numScores += !!settings._mustCalcGraphlets;
	numScores += dh.GetNumCustomMatrices();

	// just look at this #$&*$* boilerplate code...
	{
		// prepare nodes field...
		jclass arraycls = jj->FindClass("java/lang/Object");
		jobjectArray agroups = jj->NewObjectArray(2, arraycls, NULL);

		jintArray anodes1 = jj->NewIntArray(my.size());
		jintArray anodes2 = jj->NewIntArray(my.size());

		jj->SetObjectArrayElement(agroups, 0, anodes1);
		jj->SetObjectArrayElement(agroups, 1, anodes2);
		ASSERT(!jj->ExceptionCheck());

		jint *nodes1 = jj->GetIntArrayElements(anodes1, NULL);
		jint *nodes2 = jj->GetIntArrayElements(anodes2, NULL);
		ASSERT(!jj->ExceptionCheck());

		for(size_t i = 0; i < my.size(); ++i)
		{
			// During network import, the java array index was saved in the tag.
			nodes1[i] = my.node[i] ? my.node[i]->tag : -1;
			nodes2[i] = ref.node[i] ? ref.node[i]->tag : -1;
		}

		jj->ReleaseIntArrayElements(anodes1, nodes1, 0);
		jj->ReleaseIntArrayElements(anodes2, nodes2, 0);
		ASSERT(!jj->ExceptionCheck());

		// prepare scores field...
		jclass stringcls = jj->FindClass("java/lang/String");
		jobjectArray ascores = jj->NewObjectArray(numScores, arraycls, NULL); // array of arrays
		jobjectArray anames = jj->NewObjectArray(numScores, stringcls, NULL); // array of strings

		ASSERT(!jj->ExceptionCheck());

		// note that in java, the score names get "gedevo" prefixed, so it's "gedevoPairScore", and so on.
		// See GedevoAlignmentUtil.java, linkPartnerNodes(),  assignScoresToRow(), createScoreColumns().
		// ColumnNames.java stores

		int widx = 0;
		// pair score
		{
			jdoubleArray apairscores = jj->NewDoubleArray(my.size());
			jdouble *pairscores = jj->GetDoubleArrayElements(apairscores, NULL);
			for(size_t i = 0; i < my.size(); ++i)
				pairscores[i] = a->getPairScore(i);
			jj->ReleaseDoubleArrayElements(apairscores, pairscores, 0);
			jj->SetObjectArrayElement(ascores, widx, apairscores);
			jj->SetObjectArrayElement(anames, widx, charptrToJstring(jj, "PairScore"));
			++widx;
		}
		ASSERT(!jj->ExceptionCheck());

		// GED score
		{
			jdoubleArray agedscores = jj->NewDoubleArray(my.size());
			jdouble *gedscores = jj->GetDoubleArrayElements(agedscores, NULL);
			for(size_t i = 0; i < my.size(); ++i)
				gedscores[i] = a->getPairGEDScore(i);
			jj->ReleaseDoubleArrayElements(agedscores, gedscores, 0);
			jj->SetObjectArrayElement(ascores, widx, agedscores);
			jj->SetObjectArrayElement(anames, widx, charptrToJstring(jj, "GEDScore"));
			++widx;
		}
		ASSERT(!jj->ExceptionCheck());

		// if present: graphlet score
		if(settings._mustCalcGraphlets)
		{
			jdoubleArray agrscores = jj->NewDoubleArray(my.size());
			jdouble *grscores = jj->GetDoubleArrayElements(agrscores, NULL);
			for(size_t i = 0; i < my.size(); ++i)
				grscores[i] = dh.GetGraphletScore_unsafe(my.nodeId[i], ref.nodeId[i]);
			jj->ReleaseDoubleArrayElements(agrscores, grscores, 0);
			jj->SetObjectArrayElement(ascores, widx, agrscores);
			jj->SetObjectArrayElement(anames, widx, charptrToJstring(jj, "GrletSigScore"));
			++widx;
		}
		ASSERT(!jj->ExceptionCheck());

		for(size_t m = 0; m < settings.customMatrix.size(); ++m)
		{
			jdoubleArray as = jj->NewDoubleArray(my.size());
			jdouble *s = jj->GetDoubleArrayElements(as, NULL);
			for(size_t i = 0; i < my.size(); ++i)
				s[i] = dh.GetCustomScoreRaw(m, my.nodeId[i], ref.nodeId[i]);
			jj->ReleaseDoubleArrayElements(as, s, 0);
			jj->SetObjectArrayElement(ascores, widx, as);
			std::stringstream ss;
			ss << "CustomScore" << (m+1); // HMM: name?
			jj->SetObjectArrayElement(anames, widx, charptrToJstring(jj, ss.str().c_str()));
			++widx;
		}
		ASSERT(!jj->ExceptionCheck());

		jfieldID fid_nodes = jj->GetFieldID(infocls, "nodes", "[[I");
		jfieldID fid_scores = jj->GetFieldID(infocls, "scores", "[[D");
		jfieldID fid_names = jj->GetFieldID(infocls, "scoreNames", "[Ljava/lang/String;");
		ASSERT(fid_nodes && fid_scores && fid_names);

		jj->SetObjectField(info, fid_nodes, agroups);
		jj->SetObjectField(info, fid_scores, ascores);
		jj->SetObjectField(info, fid_names, anames);
	}

	ASSERT(!jj->ExceptionCheck());

	return JNI_TRUE;
}

JNIEXPORT jlong JNICALL Java_org_cytoscape_gedevo_GedevoNative_00024Instance_getNumAgentsN
(JNIEnv *, jclass, jlong handle)
{
	ProgramInstance *ins = handle_cast<ProgramInstance*>(handle);
	return (jlong)ins->getAgentMgr()->agents.size();
}


} // end extern "C"

