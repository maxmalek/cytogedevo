import org.cytoscape.application.swing.AbstractCyAction;


public abstract class GedevoStubMenuAction extends AbstractCyAction
{
    protected final GedevoStubApp app;

    public GedevoStubMenuAction(GedevoStubApp a, String label)
    {
        super(
                label,
                a.getCyAdapter().getCyApplicationManager(),
                "",
                a.getCyAdapter().getCyNetworkViewManager()
        );
        app = a;
        setPreferredMenu("Apps");
    }
}
