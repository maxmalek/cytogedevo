package org.cytoscape.gedevo.integration.customlayoutmenu;

import org.cytoscape.application.CyApplicationManager;
import org.cytoscape.model.*;
import org.cytoscape.view.layout.CyLayoutAlgorithm;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;
import org.cytoscape.work.TaskManager;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DynamicLayoutMenu extends JMenu implements MenuListener
{
    private final boolean usesNodeAttrs;
    private final boolean usesEdgeAttrs;
    private final boolean supportsSelectedOnly;
    private final CyNetwork network;
    private static final String UNWEIGHTED = "(none)";
    private final LayoutLauncher launch;

    public DynamicLayoutMenu(CyLayoutAlgorithm layout, CyNetwork network, boolean enabled, CyApplicationManager appMgr, TaskManager tm, boolean usesNodeAttrs, boolean usesEdgeAttrs, boolean supportsSelectedOnly)
    {
        super(layout.toString());
        this.launch = new LayoutLauncher(layout, appMgr, tm);
        this.network = network;
        this.supportsSelectedOnly = supportsSelectedOnly;
        this.usesNodeAttrs = usesNodeAttrs;
        this.usesEdgeAttrs = usesEdgeAttrs;

        addMenuListener(this);
        setEnabled(enabled);
    }

    public void menuCanceled(MenuEvent e) {}

    public void menuDeselected(MenuEvent e) {}

    public void menuSelected(MenuEvent e)
    {
        removeAll();
        if (supportsSelectedOnly)
            addSelectedOnlyMenus(this);
        else if (usesNodeAttrs)
            addNodeAttributeMenus(this, false);
        else if (usesEdgeAttrs)
            addEdgeAttributeMenus(this, false);
        else
            throw new RuntimeException("Layout algorithm expected valid table columns or selected");
    }

    private void addNodeAttributeMenus(JMenu parent, boolean selectedOnly)
    {
        CyTable nodeAttributes = network.getDefaultNodeTable();
        addAttributeMenus(parent, nodeAttributes, launch.layout.getSupportedNodeAttributeTypes(), selectedOnly);
    }

    private void addEdgeAttributeMenus(JMenu parent, boolean selectedOnly)
    {
        CyTable edgeAttributes = network.getDefaultEdgeTable();
        addAttributeMenus(parent, edgeAttributes, launch.layout.getSupportedEdgeAttributeTypes(), selectedOnly);
    }

    private void addAttributeMenus(JMenu parent, CyTable attributes, Set<Class<?>> typeSet, boolean selectedOnly)
    {
        parent.add(new LayoutAttributeMenuItem(UNWEIGHTED, selectedOnly));
        for (CyColumn column : attributes.getColumns())
            if (typeSet.contains(column.getType()))
                parent.add(new LayoutAttributeMenuItem(column.getName(), selectedOnly));
    }

    private void addSelectedOnlyMenus(JMenu parent)
    {
        JMenuItem allNodes;
        JMenuItem selNodes;
        if (usesNodeAttrs || usesEdgeAttrs)
        {
            allNodes = new JMenu("All Nodes");
            selNodes = new JMenu("Selected Nodes Only");
            if (usesNodeAttrs)
            {
                addNodeAttributeMenus((JMenu)allNodes, false);
                addNodeAttributeMenus((JMenu)selNodes, true);
            }
            else
            {
                addEdgeAttributeMenus((JMenu)allNodes, false);
                addEdgeAttributeMenus((JMenu)selNodes, true);
            }
        }
        else
        {
            allNodes = new LayoutAttributeMenuItem("All Nodes", false);
            selNodes = new LayoutAttributeMenuItem("Selected Nodes Only", true);
        }
        parent.add(allNodes);
        parent.add(selNodes);
    }

    protected class LayoutAttributeMenuItem extends JMenuItem implements ActionListener
    {
        boolean selectedOnly = false;

        public LayoutAttributeMenuItem(String label, boolean selectedOnly)
        {
            super(label);
            addActionListener(this);
            this.selectedOnly = selectedOnly;
        }

        public void actionPerformed(ActionEvent e)
        {
            List<CyNetworkView> views = launch.appMgr.getSelectedNetworkViews();
            for (CyNetworkView netView : views)
            {
                Set<View<CyNode>> nodeViews;
                if ((launch.layout.getSupportsSelectedOnly()) && (this.selectedOnly))
                    nodeViews = getSelectedNodeViews(netView);
                else
                    nodeViews = CyLayoutAlgorithm.ALL_NODE_VIEWS;

                String layoutAttribute = null;
                if ((launch.layout.getSupportedNodeAttributeTypes().size() > 0) || (launch.layout.getSupportedEdgeAttributeTypes().size() > 0))
                {
                    layoutAttribute = e.getActionCommand();
                    if (layoutAttribute.equals("(none)"))
                        layoutAttribute = null;
                }
                launch.doLayout(nodeViews, layoutAttribute);
            }
        }

        private Set<View<CyNode>> getSelectedNodeViews(CyNetworkView view)
        {
            List<CyNode> selectedNodes = CyTableUtil.getNodesInState(view.getModel(), CyNetwork.SELECTED, true);
            if (selectedNodes.isEmpty())
                return CyLayoutAlgorithm.ALL_NODE_VIEWS;

            Set<View<CyNode>> nodeViews = new HashSet<View<CyNode>>();
            for (CyNode n : selectedNodes)
            {
                View<CyNode> nodeView = view.getNodeView(n);
                if (nodeView.getVisualProperty(BasicVisualLexicon.NODE_VISIBLE))
                    nodeViews.add(nodeView);
            }
            return nodeViews;
        }
    }
}
