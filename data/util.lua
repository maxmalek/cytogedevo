-- included by other files


function string.explode(p, d, skip)
  local t, ll, l, nw
  
  ll = 0
  t = {}
  if(#p == 1) then return {p} end
    while true do
      l=string.find(p,d,ll,true) -- find the next d in the string
      if l~=nil then -- if "not not" found then..
        nw = string.sub(p,ll,l-1)
        if not skip or #nw > 0 then
            table.insert(t, nw) -- Save it in our array.
        end
        ll=l+1 -- save just after where we found it for searching next time.
      else
        nw = string.sub(p,ll)
        if not skip or #nw > 0 then
            table.insert(t, nw) -- Save what's left in our array.
        end
        break -- Break at end, as it should be, according to the lua manual.
      end
    end
  return t
end

local normws = { ["\t"] = " " }
function string.splitws(s)
    s = s:gsub(".", normws)
    return s:explode(" ", true)
end

function string.startsWith(String,Start)
   return String:sub(1,#Start)==Start
end

function string.endsWith(String,End)
   return End=='' or String:sub(-#End)==End
end

function table.clear(t)
    for k, _ in pairs(t) do
        t[k] = nil
    end
end

function table.count(t)
    local c = 0
    for _, _ in pairs(t) do
        c = c + 1
    end
    return c
end

-- ensure Lua 5.2+ compat
if not rawget(_G, "unpack") then
    rawset(_G, "unpack", table.unpack)
end


setmetatable(_G, { __index = function(g, k) error(k) end, __newindex = function(g, k) error(k) end })

assert(unpack)
