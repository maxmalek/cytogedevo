local strmatch = string.match
local tins = table.insert
local type = type
local pairs = pairs
local tostring = tostring
local strfmt = string.format

local isSafeTableKey

do
    local lua_reserved_keywords = {
        'and',    'break',  'do',
        'else',   'elseif', 'end',
        'false',  'for',    'function',
        'if',     'in',     'local',
        'nil',    'not',    'or',
        'repeat', 'return', 'then',
        'true',   'until',  'while',
    }

    local keywords = {}
    for _, w in pairs(lua_reserved_keywords) do
        keywords[w] = true
    end

    isSafeTableKey = function(s)
        return type(s) == "string" and not (keywords[s] or strmatch(s, "[^a-zA-Z]"))
    end
end

local dump_simple_i

local function dump_simple_table(x, out)
    out:write("{")
    for key, val in pairs(x) do
        if isSafeTableKey(key) then
            out:write(key, "=")
        else
            out:write("[")
            dump_simple_i(key, out)
            out:write("]=")
        end
        dump_simple_i(val, out)
        out:write(",")
    end
    return out:write("}")
end

local function dump_simple_string(x, out)
    out:write(strfmt("%q", x))
end

local function dump_simple_value(x, out)
    out:write(tostring(x))
end

local function dump_ignore()
end

local dumpfunc = {
    table = dump_simple_table,
    string = dump_simple_string,
    number = dump_simple_value,
    boolean = dump_simple_value,
    userdata = dump_ignore,
}
local function dump_error(x, out)
    error("serialize: Cannot dump type " .. type(x) .. " (\"" .. tostring(x) .. "\")")
end
setmetatable(dumpfunc, {
    __index = function(t, k)
        return dump_error
    end
})

dump_simple_i = function(x, out)
    return dumpfunc[type(x)](x, out)
end

local function serialize_save(out, x)
    dump_simple_i(x, out)
end

return serialize_save
