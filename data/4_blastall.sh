#!/bin/bash

. config.inc

if ! hascmd blastp; then
    echo "blastp not found!"
    exit 1
fi

# fast in-memory file system so that it's not slowed down by HDD (must end with /)
tmpd="/dev/shm/"
# fallback...
if [ ! -d $tmpd ]; then
    tmpd=$TEMP/
fi
if [ ! -d $tmpd ]; then
    mkdir tmp
    tmpd=tmp/
fi
echo "Using $tmpd as temp dir for sequences"

doblast(){
    # outdir, tempdir, taxon-ID1, taxon-ID2
    ./pairblast.sh work "$tmpd" "$1" "$2"
}

# If possible, this should be run in parallel or on different machines
i=0
len=${#BLASTNUMS1[@]}
while [ $i -lt $len ]; do
    a=${BLASTNUMS1[i]}
    b=${BLASTNUMS2[i]}
    doblast $a $b
    let i++
done
