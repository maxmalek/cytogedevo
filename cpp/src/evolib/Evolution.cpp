#include "Evolution.h"
#include "Agent.h"
#include <algorithm>
#include "DataHolder.h"
#include "ProgramState.h"


void Evo::_Init_Random_MT(MTRand& rng, void *a, void *evo, void*)
{
	((Evo*)evo)->_InitMappingRandom((Agent*)a, rng);
	((Agent*)a)->locked = true;
}

void Evo::_Init_Greedy_MT(MTRand& rng, void *a, void *evo, void*)
{
	((Evo*)evo)->_InitMappingGreedy((Agent*)a, rng);
	((Agent*)a)->locked = true;
}

void Evo::_Mutate_MT(MTRand& rng, void *a, void *evo, void*)
{
	((Evo*)evo)->_InitOffspringAgent((Agent*)a, rng);
}


Evo::Evo(const DataHolder& dh, ThreadPool *thpool)
: AlgoBase(ALGO_EVO, dh, thpool)
, _dropDupCounter(0)
{
}

Evo::~Evo()
{
	/*for(size_t i = 0; i < _nextGeneration.size(); ++i)
		delete _nextGeneration[i];*/

	ASSERT(_nextGeneration.empty());
}

void Evo::Clear()
{
	AlgoBase::Clear();

	for(size_t i = 0; i < _nextGeneration.size(); ++i)
		delete _nextGeneration[i];
	_nextGeneration.clear();
}

void Evo::Init()
{
	// if an agent was added from externally via enqueueAgent(), make sure they end up in the working population.
	// Possibly not fully/properly initialized agents must be fixed now.
	for(size_t i = 0; i < _nextGeneration.size(); ++i)
	{
		_nextGeneration[i]->makeDirty();
		ASSERT(_nextGeneration[i]->sanityCheck());
	}
	MergeOffspring();

	if(GetAgents().size() < _settings.maxAgents)
	{
		uint need = _settings.maxAgents - GetAgents().size();
		//printf("Creating initial population of %u\n", need);
		CreateInitialPopulation(need);
	}
	ASSERT(_nextGeneration.empty());
}

void Evo::Update()
{
	ASSERT(_mgr);

	Select();

	// Scores were calculated at this point, Agent::getScore() will simply return a cached value
	// when invoked by the comparator functor.

	// For performance reasons, don't do this everytime
	++_dropDupCounter;
	if(_dropDupCounter >= 10)
	{
		_dropDupCounter = 0;
		_mgr->SortForDuplicateRemoval();
		_mgr->DropDuplicates();
	}
	else
	{
		_mgr->Sort();
	}

	CreateOffspring(_settings.maxAgents);

	DealDamage();

	MergeOffspring();
}

void Evo::_OnUnlink()
{
	if(_mgr)
		MergeOffspring();
}

void Evo::CreateInitialPopulation(uint n)
{
	AgentArray& agents = _mgr->agents;
	agents.reserve(n);
	const uint greedy = _settings.evo_greedyInitOnInit;
	if(_thpool)
	{
		_thpool->assertIdle();
		_thpool->lock();
		while(n--)
		{
			Agent *a = _mgr->newAgent();
			agents.push_back(a);
			_thpool->addJobUnsafe( (greedy ? _Init_Greedy_MT : _Init_Random_MT) , a, this);
		}
		_thpool->unlock();
		_thpool->waitUntilEmpty();
	}
	else
	{
		while(n--)
		{
			Agent *a = _mgr->newAgent();
			agents.push_back(a);
			if(greedy)
				_Init_Greedy_MT(*_rng, a, this);
			else
				_Init_Random_MT(*_rng, a, this);
		}
	}
}

// TODO: make this formula based on user parameters.
// Also tweak it. Right now it's totally linear - would another distribution be better?
float Evo::CalcDamageForPos(uint pos)
{
	AgentArray& agents = _mgr->agents;

	ASSERT(pos < agents.size());
	if(agents.size() < 2)
		return 0;

	// Ratio: 0 = best, 1 = worst
	float ratio = float(pos) / (agents.size()-1);
	
	if(ratio <= 0.1f)
		return 0; // never damage upper ranks

	// Deal increasing damage to lower ranks
	return (ratio - 0.1f) * 0.9f * _settings.maxHealthDrop;
}

void Evo::Select()
{
	// Offload score calculation to worker threads and wait until they are all finished.
	_CalcScores();

	AgentArray& agents = _mgr->agents;
	for(size_t i = 0; i < agents.size(); ++i)
	{
		ASSERT(agents[i]->sanityCheck());
		agents[i]->incrLife();
	}

}

void Evo::DealDamage()
{
	// Decrease health of bad agents, if they die off, remove them from the vector.
	AgentArray& agents = _mgr->agents;
	size_t sz = agents.size();
	size_t alive = sz;
	for(size_t i = 0; i < sz; ++i)
	{
		Agent *a = agents[i];
		a->decrHealth(CalcDamageForPos(i));
		if(!a->isAlive())
		{
			_mgr->deleteAgent(agents[i]);
			agents[i] = NULL;
			--alive;
		}
	}

	_mgr->Compact();

	ASSERT(agents.size() == alive);
}

void Evo::enqueueAgent(Agent *a)
{
	_nextGeneration.push_back(a);
}

void Evo::CreateOffspring(uint n)
{
	_mgr->AssertCompacted();
	_nextGeneration.reserve(_nextGeneration.size() + n);

	if(_thpool)
	{
		_thpool->assertIdle();
		_thpool->lock();
	}

	// First create from old generation, then add next generation.
	// It is important that agents does not get modified during all operations here.
	uint greedyCount = uint(n * _settings.evo_greedyInitPerRound);
	uint i;
	// Greedy is intentionally pushed to the queue first, for efficiency.
	// It's a lengthy operation so in case a thread gets free
	// and no more greedy inits are in the queue, it can already continue with
	// other jobs before the other threads finish their greedy init.
	for(i = 0; i < greedyCount; ++i)
	{
		Agent *a = _mgr->newAgent();
		_nextGeneration.push_back(a);
		RUN_JOB_AUTOMT(_Init_Greedy_MT, a, this);
	}
	for( ; i < n; ++i)
	{
		Agent *a = _mgr->newAgent();
		_nextGeneration.push_back(a);
		RUN_JOB_AUTOMT(_Mutate_MT, a, this);
	}
	if(_thpool)
	{
		_thpool->unlock();
		_thpool->waitUntilEmpty();
	}

#ifdef _DEBUG
	//printf("Evo: created %u offspring\n", n);
#endif
}

void Evo::MergeOffspring()
{
	ASSERT(_mgr);
	AgentArray& agents = _mgr->agents;
	size_t ng = _nextGeneration.size();
	agents.reserve(agents.size() + ng);
	for(size_t i = 0; i < ng; ++i)
	{
		_nextGeneration[i]->locked = true;
		agents.push_back(_nextGeneration[i]);
	}
	_nextGeneration.clear();
}


// called by worker threads
void Evo::_InitOffspringAgent(Agent *n, MTRand& rng)
{
	ASSERT(n);
	ASSERT(!n->locked);

begin:

	uint r = rng.randInt(127);

	// Low chance to fully randomize an Agent.
	// This is not useful anymore after a few rounds, but it can provide some additional randomness
	if(r < 10)
	{
		_InitMappingRandom(n, rng);
	}
	// Some chance to mutate one existing agent to hopefully create a better one.
	else if(r < 30)
	{
		const Agent *a = _mgr->GetRandomAgent(rng);
		n->copyFrom(*a);
		_DoPMXMutation(n, rng, 1.2f, 128); // TODO: make adjustable, or change over time
	}
	else if(r < 50)
	{
		const Agent *a = _mgr->GetRandomAgent(rng, 15);
		n->copyFrom(*a);
		_DoPMXMutation(n, rng, 1.6f, 0); // TODO: make adjustable, or change over time
	}
	// Some chance for crossover between 2 parents
	else if(r < 70)
	{
		const Agent *a = _mgr->GetRandomAgent(rng);
		const Agent *b = _mgr->GetRandomAgent(rng);
		if(a == b)
		{
			n->copyFrom(*a);
			_DoPMXMutation(n, rng, 0.33f);
		}
		else
			_DoPMXCrossover2(n, rng, a, b);
	}
	// Crossover between 2, but one of them is a good one
	else if(r < 90)
	{
 		const Agent *a = _mgr->GetRandomAgent(rng, 10);
		const Agent *b = _mgr->GetRandomAgent(rng);
		if(a == b)
		{
			n->copyFrom(*a);
			_DoPMXMutation(n, rng, 0.33f);
		}
		else
			_DoPMXCrossover2(n, rng, a, b);
	}
	// Directed mutation of one of the better half
	else if(r < 110)
	{
		const Agent *a = _mgr->GetRandomAgent(rng, _mgr->agents.size() / 2);
		n->copyFrom(*a);
		if(!_DoDirectedMutation(n, rng, a->getMapping().size() /*/ 2*/, 20)) // TODO: make configurable
			goto begin;
	}
	// Multi-parent crossover
	else
	{
		const uint pc =  3 + rng.randInt(5);
		const Agent *parentArr[16];
		for(uint i = 1; i < pc; ++i)
			parentArr[i] = _mgr->GetRandomAgent(rng);
		// make sure one of the better ones is always a parent
		// TODO: make configurable
		parentArr[0] = _mgr->GetRandomAgent(rng, 15);
		_DoOctopusCrossover(n, rng, parentArr, pc);
	}

	n->makeDirty();

	n->locked = true;
	ASSERT(n->sanityCheck());
}


void Evo::_InitMappingRandom(Agent *a, MTRand& rng)
{
	const size_t prematched = _dataH.getFirstNonPrematchedIndex();
	// Prevent underflow + crash when a network is mapped against itself.
	if(prematched == a->getMapping().size())
		return;

	const size_t sz = a->getMapping().size() - prematched - 1; // -1 because rng is in [0...n], *not* [0...n)

	// this is essentially what std::random_shuffle does.
	for(size_t i = prematched; i < sz; ++i)
		a->swap(i, rng.randInt(sz) + prematched);
}

// single agent PMX mutation - modifies inline, used on clone only
void Evo::_DoPMXMutation(Agent *a, MTRand& rng, score avgMult /* = 1 */, uint forcedDivider /* = 128 */)
{
	const uint sz = (uint)a->getMapping().size();
	ASSERT(sz); // help static code analysis

	score avg = a->getPairSumScore();
	avg *= avgMult;

	// Determine a counter that inverts the normal action.
	// I.e. if a pair is bad, keep it anyways,
	// and if a pair is good, break it.
	// This can lower the score, but increases mutation rate and helps getting out
	// of local minima, if they ever happen.
	// (Doing it this way because constantly polling the RNG eats way more CPU,
	//  and precisely guessing the chance isn't necessary here)
	// TODO: make this configurable and/or derive from a parameter??
	uint doNotInvertCounter = forcedDivider ? rng.randInt(sz / forcedDivider) : -1;
	uint invertedTimes = 0; // for debugging

	uint *swappable = (uint*)alloca(sz * sizeof(uint));
	ASSERT(swappable); // help static code analysis
	uint *swappable_wpos = swappable;
	for(uint i = _dataH.getFirstNonPrematchedIndex(); i < sz; ++i)
	{
		// smaller is better
		if(a->getPairScore(i) > avg)
		{
			// score sucks -> found a pair to swap... reserve low chance that this pair will not be swapped
			if(doNotInvertCounter)
				*swappable_wpos++ = i;
		}
		else if(forcedDivider)
		{
			if(!doNotInvertCounter)
			{
				// low chance to swap although score is good
				*swappable_wpos++ = i;
			}

			if(doNotInvertCounter)
				--doNotInvertCounter;
			else
			{
				doNotInvertCounter = rng.randInt(sz / forcedDivider);
				++invertedTimes;
			}
		}
	}

	// swappable does now contain indices in the mapping which have a bad score,
	// and can be swapped around without causing too much harm to the score.
	// Initially, it is sorted.
	if(swappable != swappable_wpos)
	{
		const size_t swapsize = swappable_wpos - swappable;
/*#ifdef _DEBUG
		printf("PMX: swap %u / %u  (%u times forced, forceDiv = %u, avgMult = %.1f)\n",
			(uint)swapsize, sz, invertedTimes, forcedDivider, avgMult);
#endif*/

		// Make sure the indices are randomly distributed
		MT_SHUFFLE(swappable, swappable_wpos, rng);
		

		for(size_t i = 0; i < swapsize - 1; i += 2)
		{
			a->swap(swappable[i], swappable[i+1]);
		}
	}
}

template <typename T> static size_t _count_nulls(T a, size_t s)
{
	size_t ret = 0;
	for(size_t i = 0; i < s; ++i)
		if(!a[i])
			++ret;
	return ret;
}

template <typename T> static size_t _count_0xff(T a, size_t s)
{
	size_t ret = 0;
	for(size_t i = 0; i < s; ++i)
		if(a[i] == 0xff)
			++ret;
	return ret;
}

// Temporary struct to make multi-parent crossover easier
struct _Dat
{
	_Dat() {}

	_Dat(const Agent *who, uint p, uchar idx)
		: _score(const_cast<Agent*>(who)->getPairScore(p)) // HACK: ouch, ffffugly
		, pos(p)				// -- but we can assume that the score was already calculated elsewhere.
		, originIdx(idx)
	{
	}
		
	score _score;
	uint pos;
	uchar originIdx;
	inline bool operator< (const _Dat& o) const { return _score < o._score; } // for sorting; lower is better
};

void Evo::_DoPMXCrossover2(Agent *out, MTRand& rng, const Agent *a, const Agent *b)
{
	const Agent *parentArr[] = { a, b }; // passed into crossover Agent ctor later
	_DoOctopusCrossover(out, rng, parentArr, 2);
}

// Doing a crossover, taking the best scores
void Evo::_DoOctopusCrossover(Agent *out, MTRand& rng, const Agent * const *parentArr, uint parentCount)
{
	ASSERT(parentCount);
	uint sz = parentArr[0]->getMapping().size();
	uint usednode_size = _dataH.GetMaxVariableNodeID();
	uint nullsInMapping = sz - usednode_size;
	ASSERT(nullsInMapping == _dataH.GetMaxReferenceNodeID());
	uint prematched = _dataH.getFirstNonPrematchedIndex();

	_Dat * const dv = new _Dat[usednode_size * parentCount];
	_Dat *dvptr = dv;

	for(uint pid = 0; pid < parentCount; ++pid)
	{
		const Agent *parent = parentArr[pid];
		ASSERT(parent);
		const NodeMapping& m = parent->getMapping();
		for(uint i = prematched; i < sz; ++i)
		{
			if(m.node[i])
				*(dvptr++) = _Dat(parentArr[pid], i, pid);
		}
	}

	size_t dvsize = dvptr - dv;

	ASSERT(dvsize == (usednode_size - prematched) * parentCount);

	if(!dvsize)
		return;

	_Dat * const dvend = dvptr;
	std::sort(dv, dvptr);
	dvptr = dv;

	// shuffle perfect scores (= from begin to first non-0 score entry), for a better distribution
	for( ; dvptr != dvend; ++dvptr)
		if(dvptr->_score > 0)
		{
			++dvptr; // point one past the last 0-score entry; as shuffling affects [first, last)
			break;
		}
	MT_SHUFFLE(dv, dvptr, rng);


	// The vector does now contain the best scores along with the respective indexes.
	// Create mask for proper agent construction from multiple parents.
	uchar *mask = (uchar*)alloca(sz); // contains ID of which parent to take the pair at each index
	// The first part of the mask doesn't matter where it comes from, because it's the same
	// for all mappings if prematched pairs exist
	memset(mask,              0x00, prematched);
	// Use 0xff to mark as not yet determined
	memset(mask + prematched, 0xff, sz - prematched);


	uchar *usednode = (uchar*)alloca(usednode_size); // at index i: 0 - node #i still unused, 1: node used.
	memset(usednode, 0, usednode_size); // Initially, no used node

	// mark those that are prematched as used
	// Can use any parents' mapping because all other parents' mappings will be the same in [0, prematched)
	for(uint i = 0; i < prematched; ++i)
	{
		const uint nodeId = parentArr[0]->getMapping().nodeId[i];
		if(nodeId) // will be 0 if prematched against NULL
			usednode[nodeId - 1] = 1;
	}

	uint missing = sz - prematched;
	uint i = 0;
	//uint taken = 0; // debug
	//uint given = prematched; // debug
	do
	{
		const _Dat& d = dv[i++];
		if(mask[d.pos] == 0xff) // no value yet?
		{
			uint nodeId = parentArr[d.originIdx]->getMapping().nodeId[d.pos]; // FFFFUUUUU
			ASSERT(nodeId);
			if(!usednode[nodeId - 1])
			{
				//printf("[%u]: mask[%u] = %u  (score: %f)\n", i-1, d.pos, d.originIdx, d.score);
				mask[d.pos] = d.originIdx;
				--missing;
				usednode[nodeId - 1] = 1;
				//++given;
			}
			else
			{
				//printf("[%u] Node %u already taken\n", i, nodeId);
				//++taken;
			}
		}
	}
	while(missing && i < dvsize);

	ASSERT(missing >= nullsInMapping);

	out->octopusMerge(parentArr, mask, sz);

	size_t nodesMissing = _count_nulls(usednode, usednode_size);
	size_t nullsMissing = missing - nodesMissing;
	ASSERT(nullsMissing == nullsInMapping);
	(void)nullsMissing;
	(void)nullsInMapping;

	std::vector<const Node*> leftover;
	if(nodesMissing)
	{
		leftover.reserve(nodesMissing);

		// Determine which valid nodes were not selected in the prev. steps.
		for(i = 0; i < usednode_size; ++i)
		{
			if(!usednode[i])
				leftover.push_back(_dataH.GetNodeByID(i + 1));
		}

		ASSERT(leftover.size() == nodesMissing);
	}

	// Pad with NULLs. Now this vector has exactly as many entries as are missing
	// to form a complete mapping.
	leftover.resize(missing, (const Node*)NULL);

	// If there are nodes not yet distributed, shuffle them and distribute randomly.
	// It is not possible that the vector is empty at this point.
	// If no nodes were added, it's all NULLs and shuffling is useless.
	if(nodesMissing)
	{
		MT_SHUFFLE(leftover.begin(), leftover.end(), rng);
	}

	// Assume there is the same amount of unused entries in mask
	// as nodes that have yet to be distributed. Everything else will fail.

	// Distribute missed nodes and NULLs to complete the mapping.
	uint rpos = 0;
	for(i = 0; i < sz; ++i) // size of alloca()'d block (size of mapping)
	{
		if(mask[i] == 0xff)
		{
			ASSERT(rpos < leftover.size());
			out->_setMappingPos(i, leftover[rpos++]);
		}
	}

	ASSERT(rpos == leftover.size()); // otherwise there would be nodes not distributed

	// Now, the mapping contains one node exactly once - None missing, none doubled;
	// and a bunch of NULLs thrown in between.
	ASSERT(out->sanityCheck());

	delete [] dv;
}

bool Evo::_DoDirectedMutation(Agent *out, MTRand& rng, uint limit, uint npairs)
{
	ASSERT(limit); // help static code analysis
	const uint sz = (uint)out->getMapping().size();
	ASSERT(sz); // help static code analysis
	if(limit > sz)
		limit = sz;
	if(npairs > limit)
		npairs = limit;
	bool changed = false;
	

	if(limit == sz)
	{
		const uint prematchIdx = _dataH.getFirstNonPrematchedIndex();
		const uint usepairs = std::min(sz - 1 - prematchIdx, npairs);
		ASSERT(usepairs); // help static code analysis

		// If we're considering all pairs, there is no need to calculate the score for all of them.
		// Just select some pairs, calc the score, and that's it.
		uint * const tmp = (uint*)alloca(usepairs * sizeof(uint));
		ASSERT(tmp); // help static code analysis
		uint * const end = tmp + usepairs; // one past end
		uint *ptr = tmp;

		do
		{
			// Append random numbers until range is full
			do 
			{
				*ptr++ = rng.randInt(usepairs) + prematchIdx;
			}
			while (ptr < end);
			std::sort(tmp, end);
			ptr = std::unique(tmp, end); // requires sorted
			// ptr == end if no duplicates were removed
		}
		while(ptr < end);
		MT_SHUFFLE(tmp, end, rng);
		// There is some chance that swapping some pairs will
		// cause a different pair score to change (due to neighborhood modification),
		// which *might* cause verification to fail -- Didn't test.
		// This isn't a problem here but just saying in case someone stumbles upon this.
		for(uint i = 0; i < usepairs; ++i)
		{
			PairInfo pi(out->getPairScore(tmp[i]), tmp[i]);
			if(_DirectImprovePair(out, pi))
				changed = true;
		}
		
	}
	else
	{
		PairInfo * const worstPairs = (PairInfo*)alloca(limit * sizeof(PairInfo));
		out->getWorstPairs(worstPairs, limit, &Agent::getPairScore);

		// Not perfectly safe, i.e. may swap a pair although it has fixed it earlier.
		// Not a problem though because the chance for this to happen isn't so big, and even if,
		// it resembles natural mutation. Sometimes shit just happens.
		MT_SHUFFLE(worstPairs, worstPairs + limit, rng);
		for(uint i = 0; i < npairs; ++i)
			if(_DirectImprovePair(out, worstPairs[i]))
				changed = true;
	}

	
	return changed;
}

bool Evo::_DirectImprovePair(Agent *out, const PairInfo& pair)
{
	const uint sz = (uint)out->getMapping().size();
	const score worstScore = pair.scor;
	uint worstIndex = pair.index;
	if(worstIndex >= sz) // Entry was left out? (It's unsigned(-1) then)
		return false;

	uint foundIndex = uint(-1); // invalid
	// best scores found so far, inited with a reasonable value
	//score foundScorew = worstScore;
	//score foundScorei = worstScore;
	score foundScoreSum = worstScore * 2;
	//score oldScorei = -1;

	// TODO: another possibility here:
	// swap greedily until a better pair is found, then use that, and continue with the worse score of both.
	// abort if nothing better can be found or after a certain nuber of swaps.
	for(uint i = _dataH.getFirstNonPrematchedIndex(); i < sz; ++i)
	{
		//score oldpsi = out->getPairScore(i); // score before swap
		out->swapTemp(i, worstIndex);
		// new scores
		score psw = out->getPairScore(worstIndex);
		score psi = out->getPairScore(i);
		/*if(psw < foundScorew // make sure we take the best score found so far...
			&& psi <= oldpsi) // ... but only if the other pair doesn't get worse
			*/
		if(psw + psi < foundScoreSum)
		{
			//foundScorew = psw;
			//foundScorei = psi; // for stats
			foundScoreSum = psw + psi;
			foundIndex = i;
			//oldScorei = oldpsi; // for stats
		}
		// revert the change, for now
		out->swapTemp(i, worstIndex);
	}

	if(foundIndex < sz) // found a good pair to swap?
	{
		out->swap(worstIndex, foundIndex);
		//#ifdef _DEBUG
		//printf("Directed[%u]: Improved (%f, %f) -> (%f, %f)\n", w, worstScore, oldScorei, foundScorew, foundScorei);
		//#endif
		return true;
	}
	return false;
}

void Evo::_InitMappingGreedy(Agent *a, MTRand& rng)
{
	if(!_dataH.HasCombinedPairScore())
	{
		_InitMappingRandom(a, rng); // can't do heuristics without any pair score
		return;
	}

	const NodeMapping& orig = _dataH.GetDefaultMapping();
	const NodeMapping& ref = _dataH.GetRefMapping();

	NodeMapping& my = a->getMappingNonConst();
	const size_t sz = my.size();
	const size_t prematched = _dataH.getFirstNonPrematchedIndex();
	const size_t nonNull = _dataH.GetMaxVariableNodeID() - prematched;
	const score limit
		= _settings.evo_greedyInitScoreLimit < 0
		? 1
		: _settings.evo_greedyInitScoreLimit;
	uint backInsertPos = sz; // one past the end
	size_t i;

	ASSERT(nonNull); // help static code analysis
	uint *indexOrder = (uint*)alloca(nonNull * sizeof(uint)); // indices of valid nodes in orig
	ASSERT(indexOrder); // help static code analysis
	for(i = 0; i < nonNull; ++i)
		if(orig.node[i + prematched])
			indexOrder[i] = i + prematched; // prematched nodes are never taken into account here.
	MT_SHUFFLE(&indexOrder[0], &indexOrder[nonNull], rng);

	// Overwrite all entries with empty (except prematched at the beginning)
	memset(&my.nodeId[prematched], 0, (sz - prematched) * sizeof(uint));
	memset(&my.node[prematched],   0, (sz - prematched) * sizeof(const Node*));
	
	for(i = 0; i < nonNull; ++i)
	{
		const uint idx = indexOrder[i]; // for all valid nodes in my, in random order, ...
		register score best = limit;
		uint bestj = uint(-1);

		// Check all possible pairs except those already prematched
		for(size_t j = prematched; j < sz; ++j)
		{
			const uint myid = orig.nodeId[j];
			const uint refid = ref.nodeId[j];

			if(!refid || myid) // If ref node is NULL or a node was already assigned here, skip
				continue;

			register const score sc = _dataH.GetCombinedPairScore_unsafe(myid, refid);

			if(sc < best)
			{
				best = sc;
				bestj = j;

				// perfect match, done here
				if(sc == 0)
					break;
			}
		}

		// May happen if all matches were considered too bad
		// -- if so, just insert this node at the back,
		// where it maps to NULL and isn't in the way.
		bool backInserted = false;
		if(bestj == uint(-1))
		{
			// Keep going back until we hit the next free entry
			// (backInsertPos is past the end or already used, so we need to find a free spot first)
			do
				--backInsertPos;
			while(ref.nodeId[backInsertPos]);

			bestj = backInsertPos;
			backInserted = true;
		}

		// Must be free
		ASSERT(!my.node[bestj]);

		// put node in "my" mapping at the position
		// that was determined to be the best
		my.node[bestj] = orig.node[idx];
		my.nodeId[bestj] = orig.nodeId[idx];
	}

	a->makeDirty();
	a->setHealth(_settings.basicHealth);

	//a->sanityCheck();
}

