dofile("util.lua")

local pairs = pairs
local string = string
local table = table
local tins = table.insert
local tconcat = table.concat


local args = {...}
local gofile = table.remove(args, 1)
local outfile = table.remove(args, 1)
assert(#args >= 1, "shrinkgos.lua <gofile> <outfile> [nodelist files...]")


local function loadnodes(fn, all)
    all = all or {}
    for line in io.lines(fn) do
        local a = line:explode("|")
        local uniID
        local IDs = {}
        local ins = 1
        for i = 1, #a do
            local aa = a[i]:explode(":")
            all[aa[2] or aa[1]] = true
        end
    end
    return all
end

local allnodes = {}
for _, fn in ipairs(args) do
    print("Loading node list: " .. fn)
    loadnodes(fn, allnodes)
end

local out = io.open(outfile, "w")

local readaccs = true
local skipgos = false
local nUsed = 0
local nUnused = 0
local nDropped = 0
for line in io.lines(gofile) do
    if readaccs then
        readaccs = false
        skipgos = true
        local t = {}
        for _, acc in pairs(line:explode("\t", true)) do
            if allnodes[acc] then
                skipgos = false
                tins(t, acc)
                nUsed = nUsed + 1
            else
                nUnused = nUnused + 1
            end
        end
        if not skipgos then
            out:write(tconcat(t, "\t"), "\n")
        else
            nDropped = nDropped + 1
        end
    else
        readaccs = true
        if not skipgos then
            out:write(line, "\n")
        end
        skipgos = false
    end
end

print("Used: " .. nUsed)
print("Unused: " .. nUnused)
print("Dropped: " .. nDropped)
