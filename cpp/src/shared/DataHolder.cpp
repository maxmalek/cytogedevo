#include "common.h"
#include <algorithm>
#include <fstream>
#include <sstream>
#include "DataHolder.h"
#include "ProteinHolder.h"
#include "GEDScore.h"
#include "GraphletScore.h"
#include "ProgressBar.h"
#include "tools.h"
#include "GEDScore.h"
#include "UserSettings.h"
#include "Threading.h"
#include "MatrixLoader.h"


DataHolder::DataHolder(UserSettings& settings)
: _ph(NULL), _settings(settings), _thpool(NULL), _wasInit(false)
{
}

DataHolder::~DataHolder()
{
}


bool DataHolder::PrecalcLight()
{
	if(!_settings._good)
	{
		printf("DataHolder: ERROR: UserSetting::CheckAndApply() not called!");
		return false;
	}

	ASSERT(_ph);
	ASSERT(_settings.varGroup == 0 || _settings.varGroup == 1);

	_wasInit = false;

	_CalcMisc();
	_GenerateDefaultMappings();
	_FillGEDScoreArray();

	printf("DataHolder: Max variable  node ID: %u\n", GetMaxVariableNodeID());
	printf("DataHolder: Max reference node ID: %u\n", GetMaxReferenceNodeID());

	printf("DataHolder: Max variable  node degree: %u\n", GetMaxVariableNodeDegree());
	printf("DataHolder: Max reference node degree: %u\n", GetMaxReferenceNodeDegree());

	printf("DataHolder: variable edge amount: %u\n", GetVariableEdgeAmount());
	printf("DataHolder: reference edge amount: %u\n", GetReferenceEdgeAmount());

	return true;
}

bool DataHolder::PrecalcHeavy()
{
	if(!_LoadCustomMatrices()) // does nothing if no extra matrices specified
		return false;

	_TransferConnectionMatrix(0);
	_TransferConnectionMatrix(1);

	_FillNeighborVector(0);
	_FillNeighborVector(1);
	
	_SetupGraphletMatrix();

	// Cleanup after the to-be-dropped matrix data were used.
	// Some of the above functions require node protos to be present, so this can't be done earlier.
	_ph->Cleanup();

	// Must be after BLAST and graphlet data are available
	_SetupCustomOverrideMatrices();
	_CalcCombinedPairScores();
	_CalcCombinedFinalScores();

	// The rest
	_CalcMisc2();

	_wasInit = true;
	_currentSettings = _settings;
	printf("DataHolder: Done!\n");
	return true;
}

void DataHolder::_SetupGraphletMatrix()
{
	if(!_settings._mustCalcGraphlets)
	{
		_lookupGraphletScore.forceDeallocate();
		return;
	}

	if(!_lookupGraphletScore.empty())
		return;

	// Graphlet sigs matrix dumpfile checksum/filename generation
	std::string grletFile;
	{
		uint adler = adler32(0, NULL, 0);
		{
			const NodeArray& gx = _ph->GetGroup(_settings.varGroup);
			for(size_t i = 0; i < gx.size(); ++i)
				adler = adler32(adler, gx[i]->name().c_str(), gx[i]->name().length() + 1); // include '\0'
		}
		{
			const NodeArray& gy = _ph->GetGroup(!_settings.varGroup);
			for(size_t i = 0; i < gy.size(); ++i)
				adler = adler32(adler, gy[i]->name().c_str(), gy[i]->name().length() + 1); // include '\0'
		}
		char buf[128];
		sprintf(&buf[0], "grsigs-%u.bin", adler);
		grletFile = &buf[0];
	}

	if(_settings._mustCalcGraphlets)
	{
		printf("DataHolder: Setting up graphlet score lookup matrix (file: %s)...\n", grletFile.c_str());
		if(!_lookupGraphletScore.readFromFile(grletFile.c_str()))
		{
			_PrecalcGraphletScores(_lookupGraphletScore);
			if(_settings.keepWorkfiles && _lookupGraphletScore.writeToFile(grletFile.c_str()))
				printf("DataHolder: Dumped graphlet score lookup matrix to file %s\n", grletFile.c_str());
		}
	}
}

/*template <typename DST, typename SRC> static void _transferMatrix_MT(MTRand&, void *p1, void *p2, void*)
{
	DST& to = *((*DST)p1);
	SRC& from = *((*SRC)p2);
	to.load(from);
}*/

void DataHolder::_TransferConnectionMatrix(uint group)
{
	if(!_lookupConnRow[group].empty())
		return;

	// Symmetric, no x/y swapping required
	printf("DataHolder: Transferring connection lookup matrix, group %u ...\n", group);
	FastMatrix<uchar>& cr = _lookupConnRow[group];
	FastMatrixColumnWise<uchar>& cc = _lookupConnCol[group];
	cr.load(_ph->GetConnMatrix(group));
	cc.load(_ph->GetConnMatrix(group));
}

struct _ParamCarrier
{
	_ParamCarrier(uint size, const std::vector<Node*>& gxv_, const std::vector<Node*>& gyv_, Matrix<score>& m_)
		: bar(size), gxv(gxv_), gyv(gyv_), m(m_) {}
	ProgressBar bar;
	Waitable lock;
	const std::vector<Node*>& gxv;
	const std::vector<Node*>& gyv;
	Matrix<score>& m;
};

static void _CalcGrletMatrixRow(MTRand& /*unused*/, void *p1, void *p2, void *)
{
	uint iy = (uint)(intptr_t(p1));
	_ParamCarrier& prm = *((_ParamCarrier*)p2);
	Matrix<score>& m = prm.m;

	size_t xsize = prm.gxv.size();
	const Node *py = prm.gyv[iy];
	const uint y = py->id;

	for(size_t ix = 0; ix < xsize; ++ix)
	{
		const Node *px = prm.gxv[ix];
		const uint x = px->id;
		m(x, y) = calcGraphletDistance(px, py);
	}

	{
		MTGuard(prm.lock);
		prm.bar.Step();
	}
}

void DataHolder::_PrecalcGraphletScores(Matrix<score> &m)
{
	ASSERT(_settings._mustCalcGraphlets);

	// Symmetric, no x/y swapping required (but doing it anyway, to be absolutely safe)
	const NodeArray& gxv = _ph->GetGroup(_settings.varGroup);
	const NodeArray& gyv = _ph->GetGroup(!_settings.varGroup);

	// No UserSettings multipliers in use, can return after putting pairNullValue into the the first row/col if it's already populated
	if(_InitScoreMatrix(m, gxv, gyv, false))
		return;

	printf("DataHolder: Precalculating graphlet scores...\n");
	_ParamCarrier prm(gyv.size(), gxv, gyv, m);

	if(_thpool)
	{
		_thpool->lock();
		for(size_t iy = 0; iy < gyv.size(); ++iy)
			_thpool->addJobUnsafe(_CalcGrletMatrixRow, (void*)(intptr_t(iy)), &prm); // HACK: abuse void* as int
		_thpool->unlock();
		_thpool->waitUntilEmpty();
	}
	else
	{
		MTRand *rngDummy = NULL; // dummy -- not used in the function below -> dereferenced, but not accessed -> no problem
		for(size_t iy = 0; iy < gyv.size(); ++iy)
			_CalcGrletMatrixRow(*rngDummy, (void*)(intptr_t(iy)), &prm, NULL);
	}
}

// The neighbor vector is used in GED calculation, because the neighbor set is too slow
// when accessed linearly.
void DataHolder::_FillNeighborVector(uint group)
{
	printf("DataHolder: Setting up node neighbors vector, group %u ...\n", group);
	const NodeArray& g = _ph->GetGroup(group);
	for(NodeArray::const_iterator it = g.begin(); it != g.end(); ++it)
	{
		Node *node = *it;
		node->allv.clear();
		node->allv.reserve(node->nb.size());

		node->outgoingv.clear();
		node->outgoingv.reserve(node->outgoing.size());

		node->incomingv.clear();
		node->incomingv.reserve(node->incoming.size());

		node->undirectedv.clear();
		node->undirectedv.reserve(node->undirectedv.size());

		for(NodeSet::iterator jt = node->nb.begin(); jt != node->nb.end(); ++jt)
		{
			node->allv.push_back(*jt);
			if(node->hasEdgeTo(*jt) && node->hasEdgeFrom(*jt))
				node->undirectedv.push_back(*jt);
		}

		// Make sure that no undirected node ends up in incomingv/outgoingv
		for(NodeSet::iterator jt = node->outgoing.begin(); jt != node->outgoing.end(); ++jt)
			if(!node->hasEdgeFrom(*jt))
				node->outgoingv.push_back(*jt);

		for(NodeSet::iterator jt = node->incoming.begin(); jt != node->incoming.end(); ++jt)
			if(!node->hasEdgeTo(*jt))
				node->incomingv.push_back(*jt);

		ASSERT(node->allv.size() == node->outgoingv.size() + node->incomingv.size() + node->undirectedv.size());
	}
}

void DataHolder::_GenerateDefaultMappings()
{
	printf("DataHolder: Generating default mappings...\n");

	uint refGroup = !_settings.varGroup;
	printf("DataHolder: Var group = %u, ref group = %u\n", _settings.varGroup, refGroup);

	_refMapping.clear();
	_defaultMapping.clear();

	const NodeArray &ref = _ph->GetGroup(refGroup);
	_refMapping.clear();
	ASSERT(ref.size() == GetMaxReferenceNodeID());
	for(NodeArray::const_iterator it = ref.begin(); it != ref.end(); ++it)
	{
		ASSERT((*it)->group == refGroup);
		_refMapping.push_back(*it);
		//ASSERT((*it)->id == _refMapping.size()); // This property is important: Node ID - 1 == array position.
	}

	const NodeArray &nodes = _ph->GetGroup(_settings.varGroup);
	ASSERT(nodes.size() == GetMaxVariableNodeID());
	for(NodeArray::const_iterator it = nodes.begin(); it != nodes.end(); ++it)
	{
		ASSERT((*it)->group == _settings.varGroup);
		_defaultMapping.push_back(*it);
		//ASSERT((*it)->id == _defaultMapping.size()); // This property is important: Node ID - 1 == array position.
	}

	ASSERT(_refMapping.size() == GetMaxReferenceNodeID());
	ASSERT(_defaultMapping.size() == GetMaxVariableNodeID());

	size_t finalsize  = _refMapping.size() + _defaultMapping.size();
	_refMapping.resize(finalsize);
	_defaultMapping.resize(finalsize);

	_refIndex.resize(GetMaxReferenceNodeID());
	GEDPopulateIndex(_refMapping, &_refIndex[0]);

	_defaultIndex.resize(GetMaxVariableNodeID());
	GEDPopulateIndex(_defaultMapping, &_defaultIndex[0]);
}

void DataHolder::_CalcMisc()
{
	const NodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const NodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis

	_nodeMaxID[0] = 0;
	_nodeMaxID[1] = 0;
	_nodeMaxDegree[0] = 0;
	_nodeMaxDegree[1] = 0;
	_totalEdgeAmount[0] = 0;
	_totalEdgeAmount[1] = 0;
	_firstNonPrematchedIndex = 0;

	bool undirected = true;
	bool prematched = true;

	// -- Reference group
	for(size_t i = 0; i < gyv.size(); ++i)
	{
		const Node *py = gyv[i];
		const uint grp = !_settings.varGroup;
		const uint edges = (uint)py->nb.size() + py->hasSelfLoop;
		const uint degree = (uint)py->nb.size() + 2 * py->hasSelfLoop; // degree is defined with self loops counted twice

		_nodeMaxID[grp] = std::max(_nodeMaxID[grp], py->id);
		_nodeMaxDegree[grp] = std::max(_nodeMaxDegree[grp], degree);

		_totalEdgeAmount[grp] += edges;

		if(undirected)
		{
			// edges from py -> jn ...
			for(NodeSet::const_iterator jt = py->outgoing.begin(); jt != py->outgoing.end(); ++jt)
			{
				const Node *jn = *jt;
				// ... must also have an edge from jn -> py, otherwise there are also directed edges.
				ASSERT(jn->hasEdgeFrom(py));
				undirected = undirected && jn->hasEdgeTo(py);
			}
		}
	}

	// -- Variable group
	for(size_t i = 0; i < gxv.size(); ++i)
	{
		const Node *px = gxv[i];
		const uint grp = _settings.varGroup;
		const uint edges = (uint)px->nb.size() + px->hasSelfLoop;
		const uint degree = (uint)px->nb.size() + 2 * px->hasSelfLoop; // degree is defined with self loops counted twice

		_nodeMaxID[grp] = std::max(_nodeMaxID[grp], px->id);
		_nodeMaxDegree[grp] = std::max(_nodeMaxDegree[grp], degree);

		_totalEdgeAmount[grp] += edges;

		if(undirected)
		{
			// edges from px -> jn ...
			for(NodeSet::const_iterator jt = px->outgoing.begin(); jt != px->outgoing.end(); ++jt)
			{
				const Node *jn = *jt;
				// ... must also have an edge from jn -> px, otherwise there are also directed edges.
				ASSERT(jn->hasEdgeFrom(px));
				undirected = undirected && jn->hasEdgeTo(px);
			}
		}

		// prematched nodes?
		if(prematched && px->prematched < 0)
		{
			prematched = false;
			_firstNonPrematchedIndex = i;
			printf("DataHolder: First not prematched index: %u\n", _firstNonPrematchedIndex);
		}
	}

	// Huh? Happens if a network is aligned against itself and prematching is turned on,
	// i.e. ALL nodes can be pre-matched
	if(prematched)
		_firstNonPrematchedIndex = std::min(gxv.size(), gyv.size());


	// Make sure both are called.
	bool prematchOk = _CheckPrematch(0);
	prematchOk = _CheckPrematch(1) && prematchOk;

	if(!prematchOk)
	{
		printf("DataHolder: WARNING: Node prematching is messed up, disabling that altogether.\n");
		_firstNonPrematchedIndex = 0;
		for(size_t i = 0; i < gxv.size(); ++i)
			gxv[i]->prematched = -1;
		for(size_t i = 0; i < gyv.size(); ++i)
			gyv[i]->prematched = -1;
	}

	if(undirected)
	{
		printf("DataHolder: Graphs have only undirected edges, can use faster GED calculation.\n");
		_hasDirectedEdges = false;
	}
	else
	{
		printf("DataHolder: Graphs have directed edges, using slower GED calculation.\n");
		_hasDirectedEdges = true;
	}

	_nodeMaxDegreeTotal = std::max(_nodeMaxDegree[0], _nodeMaxDegree[1]);

	ASSERT(_nodeMaxID[0]);
	ASSERT(_nodeMaxID[1]);

	// ensure even number (must be; each edge is seen twice)
	ASSERT((_totalEdgeAmount[0] & 1) == 0);
	ASSERT((_totalEdgeAmount[1] & 1) == 0);
	_totalEdgeAmount[0] /= 2;
	_totalEdgeAmount[1] /= 2;

	_worstGEDRaw = _totalEdgeAmount[0] + _totalEdgeAmount[1];

	printf("DataHolder: Total edge count: %u\n", _worstGEDRaw);
}

void DataHolder::_CalcMisc2()
{
	const NodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const NodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis

	score highestNode = -1, highestEdge = -1;
	for(uint i = GEDI_MIN_EDGE; i < GEDI_MAX_EDGE; ++i)
		if(_gedScores[i] > highestEdge)
			highestEdge = _gedScores[i];
	for(uint i = GEDI_MIN_NODE; i < GEDI_MAX_NODE; ++i)
		if(_gedScores[i] > highestNode)
			highestNode = _gedScores[i];

	_worstGEDScore = (_worstGEDRaw * highestEdge) + ((gxv.size() + gyv.size()) * highestNode);

	printf("DataHolder: Worst GED score: %f\n", _worstGEDScore);
}

bool DataHolder::_CheckPrematch(uint g)
{
	bool good = true;
	const NodeArray& gv = _ph->GetGroup(g);
	const NodeArray& go = _ph->GetGroup(!g);

	for(size_t i = 0; i < gv.size(); ++i)
	{
		Node *p = gv[i];

		if(i >= _firstNonPrematchedIndex && p->prematched >= 0)
		{
			// We may end up here if resuming a state where prematching was turned on later.
			// That way, nodes that would be prematched
			printf("DataHolder: WARNING: Prematched node id out-of-order, ignoring: %s in network %u\n",
				p->name().c_str(), g+1);
			p->prematched = -1;
		}

		if(i < go.size())
		{
			Node *o = go[i];
			if(p->prematched != o->prematched)
			{
				printf("DataHolder: WARNING: Different prematch state at mapping index %u\n", (uint)i);
				p->prematched = -1;
				o->prematched = -1;
				good = false;
			}
		}
	}
	return good;
}

void DataHolder::_CalcCombinedPairScores()
{
	const bool useGraphlet = _settings.pairWeightGraphlets > 0;

	bool hasCustomPairContrib = false;
	for(size_t i = 0; i < _settings.customMatrix.size(); ++i)
		if(_settings.customMatrix[i].isWeight() && _settings.customMatrix[i].value1) // value1 == pair score
		{
			hasCustomPairContrib = true;
			break;
		}

	if(!useGraphlet && !hasCustomPairContrib)
	{
		_lookupCombinedPairScore.clear();
		printf("DataHolder: combined pair score not required\n");
		return;
	}

	// must recalculate if any of the multipliers involved in score calculation change.
	// if nothing changes, recalc can be skipped.
	if(!_lookupCombinedPairScore.empty()
	   && _settings.pairNullValue == _currentSettings.pairNullValue
	   && _settings.pairWeightGraphlets == _currentSettings.pairWeightGraphlets
	   && _settings.customMatrix == _currentSettings.customMatrix)
	{
		printf("DataHolder: combined pair score is cached and valid, no need to recalc\n");
		return;
	}

	printf("DataHolder: Precalculating combined pair score matrix...\n");

	const NodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const NodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis

	uint xmax = gxv.size() + 1;
	uint ymax = gyv.size() + 1;

	_lookupCombinedPairScore.resize(xmax, ymax);
	_lookupCombinedPairScore.fill(0);

	if(useGraphlet)
	{
		const score pw = _settings.pairWeightGraphlets;
		for(uint y = 0; y < ymax; ++y)
			for(uint x = 0; x < xmax; ++x)
				_lookupCombinedPairScore(x, y) += (_lookupGraphletScore(x, y) * pw);
	}

	{
		Matrix<score> tm;
		for(size_t i = 0; i < _settings.customMatrix.size(); ++i)
			if(_settings.customMatrix[i].isWeight())
			{
				// Keep the original matrix data intact; they are needed for the final results later.
				MatrixLoader::fit(tm, _customMatricesRaw[i], _settings.customMatrix[i]);
				const score pw = _settings.customMatrix[i].value1;
				for(uint y = 0; y < ymax; ++y)
					for(uint x = 0; x < xmax; ++x)
						_lookupCombinedPairScore(x, y) += tm(x, y) * pw;
			}
	}

	// We can't pre-scale the score into [0..1] here, this is done in Agent::getPairScore().
	// Means the values in _lookupCombinedPairScore are likely to be > 1 until scaled later.
}

void DataHolder::_CalcCombinedFinalScores()
{
	const bool useGraphlet = _settings._finalCalcGraphlet;

	bool hasCustomFinalContrib = false;
	for(size_t i = 0; i < _settings.customMatrix.size(); ++i)
		if(_settings.customMatrix[i].isWeight() && _settings.customMatrix[i].value2) // value2 == final score
		{
			hasCustomFinalContrib = true;
			break;
		}

	if(!useGraphlet && !hasCustomFinalContrib)
	{
		_lookupCombinedFinalScore.clear();
		printf("DataHolder: combined final score not required\n");
		return;
	}

	// must recalculate if any of the multipliers involved in score calculation change.
	// if nothing changes, recalc can be skipped.
	if(!_lookupCombinedFinalScore.empty()
		&& _settings.pairNullValue == _currentSettings.pairNullValue
		&& _settings.weightGraphlets == _currentSettings.weightGraphlets
		&& _settings.customMatrix == _currentSettings.customMatrix)
	{
		printf("DataHolder: combined final score is cached and valid, no need to recalc\n");
		return;
	}

	printf("DataHolder: Precalculating combined final score matrix...\n");

	const NodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const NodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis

	uint xmax = gxv.size() + 1;
	uint ymax = gyv.size() + 1;

	_lookupCombinedFinalScore.resize(xmax, ymax);
	_lookupCombinedFinalScore.fill(0);

	if(useGraphlet)
	{
		const score fw = _settings.weightGraphlets;
		for(uint y = 0; y < ymax; ++y)
			for(uint x = 0; x < xmax; ++x)
				_lookupCombinedFinalScore(x, y) += (_lookupGraphletScore(x, y) * fw);
	}

	{
		Matrix<score> tm;
		for(size_t i = 0; i < _settings.customMatrix.size(); ++i)
			if(_settings.customMatrix[i].isWeight())
			{
				// Keep the original matrix data intact; they are needed for the final results later.
				MatrixLoader::fit(tm, _customMatricesRaw[i], _settings.customMatrix[i]);
				const score fw = _settings.customMatrix[i].value2;
				for(uint y = 0; y < ymax; ++y)
					for(uint x = 0; x < xmax; ++x)
						_lookupCombinedFinalScore(x, y) += tm(x, y) * fw;
			}
	}

	// We can't pre-scale the score into [0..1] here, this is done in Agent::_CalcFinalScore().
	// Means the values in _lookupCombinedFinalScore are likely to be > 1 until scaled later.
}

void DataHolder::_FillGEDScoreArray()
{
	_gedScores[GEDI_NONE] = 0; // <- The value at this index is never read.

	_gedScores[GEDI_ADD_UNDIRECTED] =
	_gedScores[GEDI_ADD_DIRECTED_1] =
	_gedScores[GEDI_ADD_DIRECTED_2] = 
	_gedScores[GEDI_ADD_SELF_LOOP]  = _settings.ged_eAdd;

	_gedScores[GEDI_REMOVE_UNDIRECTED] =
	_gedScores[GEDI_REMOVE_DIRECTED_1] =
	_gedScores[GEDI_REMOVE_DIRECTED_2] = 
	_gedScores[GEDI_REMOVE_SELF_LOOP]  =_settings.ged_eRm;

	_gedScores[GEDI_SUBST_UNDIRECTED] =
	_gedScores[GEDI_SUBST_DIRECTED_1] =
	_gedScores[GEDI_SUBST_DIRECTED_2] = 
	_gedScores[GEDI_SUBST_SELF_LOOP]  = _settings.ged_eSub;

	_gedScores[GEDI_FLIP_1] =
	_gedScores[GEDI_FLIP_2] = _settings.ged_eFlip;

	_gedScores[GEDI_DIRECTED_TO_UNDIRECTED_1] =
	_gedScores[GEDI_DIRECTED_TO_UNDIRECTED_2] = _settings.ged_eD2U;

	_gedScores[GEDI_UNDIRECTED_TO_DIRECTED_1] =
	_gedScores[GEDI_UNDIRECTED_TO_DIRECTED_2] = _settings.ged_eU2D;

	_gedScores[GEDI_ADD_NODE] = _settings.ged_nAdd;

	_gedScores[GEDI_REMOVE_NODE] = _settings.ged_nRm;
}

// just loads the raw files, no processing done at this time
bool DataHolder::_LoadCustomMatrices()
{
	_customMatricesRaw.resize(_settings.customMatrix.size());

	for(size_t i = 0; i < _customMatricesRaw.size(); ++i)
	{
		CustomMatrixData &data = _settings.customMatrix[i];
		if(!MatrixLoader::loadFile(data, _customMatricesRaw[i], *_ph))
		{
			_customMatricesRaw[i].clear();
			return false;
		}
	}
	return true;
}

void DataHolder::_SetupCustomOverrideMatrices()
{
	bool hasOverride = false;
	for(size_t i = 0; i < _settings.customMatrix.size(); ++i)
		if(_settings.customMatrix[i].isOverride())
		{
			hasOverride = true;
			break;
		}

	if(!hasOverride)
	{
		_customOverrideMtx.forceDeallocate();
		printf("DataHolder: No override data present\n");
		return;
	}

	const NodeArray& gxv = _ph->GetGroup(_settings.varGroup); // variable group on X axis
	const NodeArray& gyv = _ph->GetGroup(!_settings.varGroup); // reference group on Y axis
	uint xmax = gxv.size() + 1;
	uint ymax = gyv.size() + 1;

	_customOverrideMtx.resize(xmax, ymax);
	_customOverrideMtx.fill(std::make_pair(score(-1), score(-1)));

	for(size_t i = 0; i < _settings.customMatrix.size(); ++i)
		if(_settings.customMatrix[i].isOverride())
		{
			const Matrix<score>& m = _customMatricesRaw[i];
			const score thmin = _settings.customMatrix[i].value1; // threshold min
			const score thmax = _settings.customMatrix[i].value2; // threshold max
			ASSERT(thmin >= 0 && thmin <= 1);
			ASSERT(thmax >= 0 && thmax <= 1);
			ASSERT(thmin <= thmax);

			// Keep the borders default initialized (= invalid)

			switch(_settings.customMatrix[i].modelType)
			{
				case CustomMatrixData::DISTANCE:
					for(uint y = 1; y < ymax; ++y)
						for(uint x = 1; x < xmax; ++x)
						{
							const score s = clamp<score>(m(x, y), 0, 1);
							std::pair<score, score>& p = _customOverrideMtx(x, y);
							if(s <= thmin && (s < p.first || p.first < 0))
								p.first = s;
							if(s <= thmax && (s < p.second || p.second < 0))
								p.second = s;
						}
				break;

				case CustomMatrixData::SIMILARITY:
					for(uint y = 1; y < ymax; ++y)
						for(uint x = 1; x < xmax; ++x)
						{
							const score s = clamp<score>(m(x, y), 0, 1);
							const score d = 1 - s;
							std::pair<score, score>& p = _customOverrideMtx(x, y);
							if(s >= thmin && (d < p.first || p.first < 0))
								p.first = d;
							if(s >= thmax && (d < p.second || p.first < 0))
								p.second = d;
						}
				break;

				default:
					ASSERT(false);
		}

	// Fix those values that were not set - it is assumed later that all values are > 0; and if <= 1, the value was below a certain threshold.
	for(size_t i = 0; i < _settings.customMatrix.size(); ++i)
		if(_settings.customMatrix[i].isOverride())
			for(uint y = 0; y < ymax; ++y)
				for(uint x = 0; x < xmax; ++x)
				{
					std::pair<score, score>& p = _customOverrideMtx(x, y);
					if(p.first < 0)
						p.first = score(FLT_MAX);
					if(p.second < 0)
						p.second = score(FLT_MAX);
				}
		}
}