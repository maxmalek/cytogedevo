package org.cytoscape.gedevo.pairlayout;

import org.cytoscape.gedevo.ColumnNames;
import org.cytoscape.gedevo.GedevoApp;
import org.cytoscape.gedevo.integration.visual.VisualStyler;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.view.model.CyNetworkView;
import org.cytoscape.view.model.View;
import org.cytoscape.view.presentation.property.BasicVisualLexicon;

import java.util.Map;

public class Overlapped extends IPairLayout
{
    public static class Factory implements IPairLayout.Factory
    {
        @Override
        public IPairLayout create(PairLayoutTask pl)
        {
            return new Overlapped(pl);
        }

        @Override
        public String getName()
        {
            return "Overlapped";
        }
    }

    public static Factory getFactory()
    {
        return new Factory();
    }


    private PairLayoutTask pl;
    public Overlapped(PairLayoutTask pl)
    {
        this.pl = pl;
    }

    public void apply(CyNetworkView view)
    {
        if(view.getModel() != pl.getSourceNetwork())
            throw new IllegalArgumentException("passed view is unrelated to pair layout");

        Map<CyNode, double[]> positions = pl.getPositions();
        Map<CyNode, CyNode[]> backmap = pl.getMapToOriginalNodes();

        int maxsrc = 0;
        CyNetwork net = view.getModel();
        for(View<CyNode> v : view.getNodeViews())
        {
            CyNode n = v.getModel();
            maxsrc = Math.max(maxsrc, net.getRow(n).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class));
        }
        final int numsrc = maxsrc + 1;

        double[] offsx = new double[numsrc];
        double[] offsy = new double[numsrc];

        double size = view.getVisualProperty(BasicVisualLexicon.NODE_SIZE);

        for(int i = 0; i < numsrc; ++i)
        {
            offsx[i] = i * (size / 6); // HACK: what
            offsy[i] = i * (size / 6);
        }

        for(CyNode virt : positions.keySet())
        {
            double[] pos = positions.get(virt);
            CyNode[] nodes = backmap.get(virt);

            for (int i = 0; i < nodes.length; ++i)
            {
                CyNode n = nodes[i];
                int netID = net.getRow(n).get(ColumnNames.SOURCE_NETWORK_ID, Integer.class);
                double x = pos[0] + offsx[netID];
                double y = pos[1] + offsy[netID];

                View<CyNode> v = view.getNodeView(nodes[i]);
                v.setVisualProperty(BasicVisualLexicon.NODE_X_LOCATION, x);
                v.setVisualProperty(BasicVisualLexicon.NODE_Y_LOCATION, y);
            }
        }


        GedevoApp.appInstance.styler.apply(view, VisualStyler.What.TRANSPARENT_OVERLAP, false);
    }
}
