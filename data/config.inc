#!/bin/bash
#-----------------------------

# DIP interaction data version (automatically downloaded)
DIP_PPI=dip20150101

# DIP sequence data version (automatically downloaded)
DIP_SEQ=dip20150101

# Networks of interest
SPECIESTAXONS=("4932"        "83333"  "6239"      "7227"           "9606")
SPECIESNAMES=("cerevisiae"   "coli"   "elegans"   "melanogaster"   "sapiens")

# Network pairs to generate all-vs-all BLAST E-value matrices for.
# Numbers are indices in taxons array. One pair is column-wise!
# cerevisiae (0) vs sapiens (4)
# melanogaster (3) vs sapiens (4)
BLASTNUMS1=(0 3)
BLASTNUMS2=(4 4)

#------------------------------

hascmd() {
    type "$1" >/dev/null 2>&1
    return $?
}

needlua() {
    if [ "$LUAEXE" == "" ]; then
        LUAEXE=$(for x in luajit lua lua51 lua52 lua53; do
            if hascmd $x; then
                echo $x
                return
            fi
        done)
        
        if [ "$LUAEXE" == "" ]; then
            echo "Lua not found! (Will accept any of luajit, lua, lua51, lua52, lua53)"
            exit 1
        fi
    fi
}
