#ifndef MATRIXLOADER_H
#define MATRIXLOADER_H

#include "common.h"
#include "Matrix.h"

class ProteinHolder;
struct CustomMatrixData;

namespace MatrixLoader
{
	bool loadFile(CustomMatrixData& data, Matrix<score>& dst, ProteinHolder &ph);
	bool fit(Matrix<score>& dst, const Matrix<score>& src, const CustomMatrixData& data);
}

#endif
