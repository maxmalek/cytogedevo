cmake_minimum_required(VERSION 2.6)

project(gedevo)

message(STATUS "CMake detected system: ${CMAKE_SYSTEM}")
message(STATUS "CMake detected compiler: ${CMAKE_CXX_COMPILER_ID}")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/")


OPTION(BUILD_JNI "Build JNI bindings?" FALSE)
OPTION(BUILD_STANDALONE "Build standalone commandline program?" TRUE)
option(USE_DOUBLE "Use doubles instead of floats? (Higher precision, but uses more memory and will possibly run slower)" FALSE)

add_subdirectory(src)

