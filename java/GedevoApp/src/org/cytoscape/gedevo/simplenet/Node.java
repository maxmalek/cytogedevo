package org.cytoscape.gedevo.simplenet;

import org.cytoscape.model.CyNode;

import java.util.HashSet;

public final class Node
{
    public final int id;
    public HashSet<Node> incoming = new HashSet<Node>();
    public HashSet<Node> outgoing = new HashSet<Node>();
    public HashSet<Node> neighbors = new HashSet<Node>(); // any direction
    public String name;
    public CyNode cynode;

    public Node(int id)
    {
        this(id, "");
    }

    public Node(int id, String n)
    {
        this.id = id;
        this.name = n;
    }
}

