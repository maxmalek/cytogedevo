package org.cytoscape.gedevo;

import org.cytoscape.gedevo.task.GedevoTask;
import org.cytoscape.work.Task;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.TaskMonitor;

import java.util.ArrayList;
import java.util.List;

public class GedevoTaskManager extends Unloadable
{
    private GedevoApp app;


    public GedevoTaskManager(GedevoApp a)
    {
        app = a;
    }

    @Override
    public void unload()
    {
        app = null;
    }

    private void runJob(Runnable r)
    {
        Thread th = new Thread(r);
        th.start();
    }

    private static class MultiTaskRunner extends GedevoTask implements IUnloadable
    {
        Task currentTask; // possibly a GedevoTask
        List<Runnable> cleanupFuncs;
        TaskIterator iter;
        GedevoTaskBox box;

        public MultiTaskRunner(TaskIterator it)
        {
            unloadLater(this);
            iter = it;
        }

        @Override
        public void run(TaskMonitor mon)
        {
            try
            {
                runTasks(mon);
            }
            catch(Exception ex)
            {
                mon.showMessage(TaskMonitor.Level.ERROR, ex.toString());
                GedevoUtil.msgboxAsync(GedevoUtil.getStackTrace(ex));
            }
            unloadNow(this);

            iter = null;
            currentTask = null;

            if(cleanupFuncs != null)
            {
                for (Runnable r : cleanupFuncs)
                    r.run();
                cleanupFuncs = null;
            }
        }

        public void addCleanupFunc(Runnable r)
        {
            if(cleanupFuncs == null)
                cleanupFuncs = new ArrayList<Runnable>();
            cleanupFuncs.add(r);
        }

        private void runTasks(TaskMonitor mon) throws Exception
        {
            while(true)
            {
                synchronized(this)
                {
                    if(iter == null || !iter.hasNext())
                        return;
                    currentTask = iter.next();
                    if(box != null)
                        box.setCanFastFinish(canFinishNow());
                    if(mustFinish)
                        finishNow();
                }
                currentTask.run(mon); // this is probably lengthy; don't keep any lock held during call
            }
        }

        @Override
        public synchronized void cancel()
        {
            iter = null;
            cleanupFuncs = null;
            unloadNow(this);
            if(currentTask != null)
            {
                currentTask.cancel();
                currentTask = null;
            }
        }

        @Override
        public void unload()
        {
            cancel();
        }

        @Override
        public synchronized void finishNow()
        {
            super.finishNow();

            if(currentTask instanceof GedevoTask)
                ((GedevoTask)currentTask).finishNow();
        }

        @Override
        public synchronized boolean canFinishNow()
        {
            return currentTask instanceof GedevoTask && ((GedevoTask)currentTask).canFinishNow();
        }
    }

    public void execute(String prefix, final TaskIterator it, final boolean keep)
    {
        final MultiTaskRunner mt = new MultiTaskRunner(it);
        final GedevoTaskBox box = new GedevoTaskBox(prefix, mt);
        mt.box = box;
        app.mainPanel.addTaskBox(box);
        final TaskMonitor mon = box.getTaskMonitor();

        mt.addCleanupFunc(new Runnable()
        {
            @Override
            public void run()
            {
                if(keep)
                    box.setFinished();
                else if(!box.hasError())
                    box.removeSelf();
            }
        });

        runJob(new Runnable()
        {
            @Override
            public void run()
            {
                mt.run(mon);
            }
        });
    }

}
