#!/bin/bash

. config.inc
needlua

if [ ! -d raw ]; then
    mkdir raw
fi
if [ ! -d work ]; then
    mkdir work
fi

(
    cd raw
    
    if [ ! -e uniprot_sprot.dat ]; then
        #url="ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz"
        # switzerland mirror is faster in europe
        url="ftp://ftp.expasy.org/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.dat.gz"
        echo "Get UNIPROT [$url]"
        wget "$url"
        gunzip uniprot_sprot.dat.gz
        
        if [ -e uniprot_sprot.dat ]; then
            echo "Swissprot data retrieved"
        else
            echo "FAILED to get Swissprot data"
            exit 1
        fi
    fi
)

echo "Extracting GO terms from swissprot"

# extract GO terms from raw file
# (This file can already be used to count GO terms in later steps, but is a bit large)
$LUAEXE parseuniprot.lua raw/uniprot_sprot.dat work/uniprot_sprot.gos


